from sage.all import PolynomialRing, Hom


def reindex_poly(p, varname='x'):
    R = p.parent()
    S = PolynomialRing(R.base(), varname, R.ngens() + 1)
    H = Hom(R, S)
    Phi = H(S.gens()[1:])
    return Phi(p)
