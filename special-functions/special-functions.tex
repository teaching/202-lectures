\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning, shapes.geometric}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}




\title{Special Kinds of Functions}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Special Properties of Functions}
\subsection{Injectivity}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A function $f:X\to Y$ is \emph{injective} if $f(a)=f(b)$ implies $a=b$.
  \end{definition}

  \pause
  \begin{block}{Note}
    Injective functions are also called \emph{one-to-one}.
  \end{block}

  \pause
  \begin{block}{Intuition}
    Injective functions are the functions whose outputs correspond to unique
    inputs.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $f:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,above right=.25cm and 4cm of a1, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        \node[mydot,below=of b4,label={right:$\varepsilon$}] (b5) {};%

        \path[very thick, ->] (a1) edge (b5);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b1);%
        \path[very thick, ->] (a4) edge (b5);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] (b3) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause
    This function is \emph{not injective} because $f(-3)=f(22)$, but
    $-3\neq22$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $g:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,above right=.25cm and 4cm of a1, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        \node[mydot,below=of b4,label={right:$\varepsilon$}] (b5) {};%

        \path[very thick, ->] (a1) edge (b4);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b1);%
        \path[very thick, ->] (a4) edge (b5);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] (b3) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause This function is injective because each output corresponds to a
    unique input.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the function $f:\mathbb{R}\to \mathbb{R}$ defined by $f(x)=x^2$
    is one-to-one.
  \end{example}
  \pause
  \begin{block}{Solution}
    This function is \emph{not one-to-one} because $f(-1)=f(1)$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the function $h:\mathbb{R}\to \mathbb{R}$ defined by $h(t)=e^t$
    is one-to-one.
  \end{example}
  \pause
  \begin{block}{Solution}
    Suppose that $h(a)=h(b)$. \pause%
    This means that $e^a=e^b$. \pause%
    Taking natural logarithms gives $\ln(e^a)=\ln(e^b)$, \pause%
    so $a=b$. \pause%
    Hence $h$ is one-to-one.
  \end{block}
\end{frame}


\subsection{Surjectivity}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A function $f:X\to Y$ is \emph{surjective} if $\Image(f)=Y$.
  \end{definition}

  \pause
  \begin{block}{Note}
    Surjective functions are also called \emph{onto}.
  \end{block}

  \pause
  \begin{block}{Intuition}
    Surjective functions are the functions that ``hit'' everything in their
    target.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $f:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,above right=.25cm and 4cm of a1, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        \node[mydot,below=of b4,label={right:$\varepsilon$}] (b5) {};%

        \path[very thick, ->] (a1) edge (b5);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b1);%
        \path[very thick, ->] (a4) edge (b5);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] (b3) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause%
    This function is \emph{not surjective} since
    $\Image(f)=\Set{\alpha,\beta,\varepsilon}\neq Y$. \pause%
    Note that $f$ is not injective.
  \end{example}
\end{frame}





\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $g:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,above right=.25cm and 4cm of a2, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        % \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        % \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        \node[mydot,below=of b2,label={right:$\varepsilon$}] (b3) {};%

        \path[very thick, ->] (a1) edge (b3);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b3);%
        \path[very thick, ->] (a4) edge (b1);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] (b2) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause%
    This function is surjective since
    $\Image(g)=\Set{\alpha,\beta,\varepsilon}=Y$. \pause%
    Note that $g$ is not injective.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if $f:\mathbb{R}\to \mathbb{R}$ defined by $f(x)=e^x$ is onto.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that $f(x)=0$ is unsolvable. Thus $f$ is \emph{not onto.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if $h:(0,\infty)\to \mathbb{R}$ defined by $h(t)=\ln(t)$ is onto.
  \end{example}
  \pause
  \begin{block}{Solution}
    Consider an arbitrary element of the target $b\in \mathbb{R}$. \pause%
    Then 
    \[
    h(e^{b})=\pause\ln(e^{b})=\pause b
    \]\pause%
    This shows that $h$ is onto.
  \end{block}
\end{frame}


\subsection{Bijectivity}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A function $f:X\to Y$ is \emph{bijective} if $f$ is both injective and
    surjective.
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $g:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,above right=.25cm and 4cm of a2, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        % \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        % \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        \node[mydot,below=of b2,label={right:$\varepsilon$}] (b3) {};%

        \path[very thick, ->] (a1) edge (b3);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b3);%
        \path[very thick, ->] (a4) edge (b1);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] (b2) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause%
    This function is surjective but not injective. \pause%
    Thus $g$ is \emph{not bijective}.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $g:X\to Y$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[mydot,label={left:$-3$}] (a1) {}; %
        \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
        \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
        \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

        \node[mydot,right=4cm of a1, label={right:$\alpha$}] (b1) {};%
        \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
        \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
        \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
        % \node[mydot,below=of b2,label={right:$\varepsilon$}] (b3) {};%

        \path[very thick, ->] (a1) edge (b3);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b4);%
        \path[very thick, ->] (a4) edge (b1);%

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
        \draw[very thick] ($ (b2)!.5!(b3) $) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
      \end{scope}
    \end{tikzpicture}
    \]
    \pause%
    This function is both one-to-one and onto. \pause%
    Thus $g$ is bijective.
  \end{example}
\end{frame}


\section{Linear Transformations}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{linear transformation} is a function $T:\mathbb{R}^{n}\to
    \mathbb{R}^{m}$ that
    \pause
    \begin{description}
    \item[respects addition] $T(\vv{v}+\vv{w})=T(\vv{v})+T(\vv{w})$\pause
    \item[respects scalar multiplication] $T(c\cdot\vv{v})=c\cdot T(\vv{v})$
    \end{description}
  \end{definition}

  \pause
  \begin{block}{Idea}
    Linear transformations are exactly those functions that respect linear
    combinations
    \[
    T(c_1\cdot\vv*{v}{1}+\dotsb+c_k\cdot\vv*{v}{k})
    =\pause c_1\cdot T(\vv*{v}{1})+\dotsb+c_k\cdot T(\vv*{v}{k})
    \]
  \end{block}
\end{frame}


\subsection{Examples}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the function $T:\mathbb{R}^{3}\to \mathbb{R}$ defined by
    \[
    T(x_1,x_2,x_3)=2\,x_1-3\,x_2+x_3
    \]
    is a linear transformation.
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{To check that $T$ respects addition, note that}
    \begin{align*}
      \onslide<3->{T(\vv{v}+\vv{w})                             }
      \onslide<3->{&= T(v_1+w_1, v_2+w_2, v_3+w_3)            \\ }
      \onslide<4->{&= 2\,(v_1+w_1)-3\,(v_2+w_2)+(v_3+w_3)     \\ }
      \onslide<5->{&= (2\,v_1-3\,v_2+v_3)+(2\,w_1-3\,w_2+w_3) \\ }
      \onslide<6->{&= T(\vv{v})+T(\vv{w})                       }
    \end{align*}
    \onslide<7->{To check that $T$ respects scalar multiplication, note that}
    \begin{align*}
      \onslide<8->{T(c\cdot\vv{v})                                   }
      \onslide<8->{&= T(c\cdot v_1, c\cdot v_2, c\cdot v_3)        \\ }
      \onslide<9->{&= 2\,(c\cdot v_1)-3\,(c\cdot v_2)+(c\cdot v_3) \\ }
      \onslide<10->{&= c\cdot(2\,v_1-3\,v_2+v_3)                    \\ }
      \onslide<11->{&= c\cdot T(\vv{v})                               }
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the function $f:\mathbb{R}\to \mathbb{R}$ defined by
    $f(x)=m\cdot x+b$ is a linear transformation.
  \end{example}
  \pause
  \begin{block}{Solution}
    To check if $f$ respects addition, note that
    \pause
    \[
    \begin{array}{rclcl}
      f(x+y)    &=\pause& m\cdot(x+y)+b             &=\pause& m\cdot x+m\cdot y+b \\ \pause
      f(x)+f(y) &=\pause& (m\cdot x+b)+(m\cdot y+b) &=\pause& m\cdot x+m\cdot y+2\,b
    \end{array}
    \]
    \pause
    To check if $f$ respects scalar multiplication, note that
    \pause
    \[
    \begin{array}{rclcl}
      f(c\cdot x) &=\pause& m\cdot(c\cdot x)+b &=\pause& m\cdot(c\cdot x)+b\\ \pause
      c\cdot f(x) &=\pause& c\cdot(m\cdot x+b) &=\pause& m\cdot(c\cdot x)+c\cdot b
    \end{array}
    \]
    \pause
    The function $f$ is a linear transformation if and only if $b=\pause0$.
  \end{block}
\end{frame}

\subsection{Matrices and Linear Transformations}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Let $A$ be a $m\times n$ matrix. Then the formula $f(\vv{x})=A\vv{x}$
    defines a function $f:\pause\mathbb{R}^{n}\to \pause\mathbb{R}^{m}$.
  \end{block}

  \pause
  \begin{block}{Notation}
    For a $m\times n$ matrix $A$, we use the notation $T_A$ to denote the
    function $T_A:\mathbb{R}^{n}\to \mathbb{R}^{m}$ defined by
    $T_A(\vv{x})=A\vv{x}$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $m\times n$ matrix. Then the function $T_A$ is a linear
    transformation.
  \end{theorem}
  \pause
  \begin{proof}
    To check that $T_A$ respects addition, note that
    \pause
    \[
    T_A(\vv{v}+\vv{w})
    =\pause A(\vv{v}+\vv{w})
    =\pause A\vv{v}+A\vv{w}
    =\pause T_A(\vv{v})+T_A(\vv{w})
    \]
    \pause
    To check that $T_A$ respects scalar multiplication, note that
    \[
    T_A(c\cdot\vv{v})
    =\pause A(c\cdot\vv{v})
    =\pause c\cdot A\vv{v}
    =\pause c\cdot T_A(\vv{v})
    \]
    \pause
    Hence $T_A$ is a linear transformation.\alt<+->{\qedhere}{\phantom\qedhere}
  \end{proof}
\end{frame}


\begin{sagesilent}
  import lineartrans
  A = matrix([(1, -14, -5, -1), (0, -1, 2, -3), (1, -4, 3, 1)])
  T = lineartrans.linear_trans(A)
\end{sagesilent}
\newcommand{\mycoords}{
  \begin{bmatrix}
    x_1\\ x_2\\ x_3\\ x_4
  \end{bmatrix}
}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine if the function $T:\mathbb{R}^{4}\to \mathbb{R}^{3}$ defined by
    \[
    T(x_1, x_2, x_3, x_4)=\sage{T}
    \]
    is a linear transformation.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that $T=T_A$ where\pause%
    \[
    A=\sage{A}
    \]
    \pause%
    Thus $T$ is a linear transformation.
  \end{block}
\end{frame}


\newcommand{\myTA}{
  \begin{bmatrix}
    T(\vv*{e}{1}) & T(\vv*{e}{2}) & \dotsb & T(\vv*{e}{n})
  \end{bmatrix}}
\newcommand{\myx}{
  \begin{bmatrix}
    x_1\\ x_2\\ \vdots\\ x_n
  \end{bmatrix}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 13.2 in Simon \& Blume]
    Every linear transformation $T:\mathbb{R}^{n}\to \mathbb{R}^{m}$ is of the
    form $T=T_A$ for some $m\times n$ matrix $A$.
  \end{theorem}
  \onslide<2->{
    \begin{proof}
      Note that every $\vv{x}=\langle x_1,x_2,\dotsc,x_n\rangle$ is
      of the form
      \[
      \vv{x}
      =x_1\cdot\vv*{e}{1}+x_2\cdot\vv*{e}{2}+\dotsb+x_n\cdot\vv*{e}{n}
      \]}%
    \onslide<3->{Since $T$ is linear, it follows that}%
    \begin{align*}
      \onslide<4->{T(\vv{x})}
      \onslide<4->{&= T(x_1\cdot\vv*{e}{1}+x_2\cdot\vv*{e}{2}+\dotsb+x_n\cdot\vv*{e}{n})          \\}
      \onslide<5->{&= x_1\cdot T(\vv*{e}{1})+x_2\cdot T(\vv*{e}{2})+\dotsb+x_n\cdot T(\vv*{e}{n}) \\}
      \onslide<6->{&= \myTA\myx}
    \end{align*}%
    \onslide<7->{This shows that $T=T_A$ where $A=\myTA$.}%
    \alt<7->{\qedhere}{\phantom\qedhere}
  \end{proof}
\end{frame}
\endgroup


\newcommand{\mytextA}{
  \begin{array}{c}
    \textnormal{linear transformations}\\
    \mathbb{R}^{n}\to \mathbb{R}^{m}
  \end{array}}
\newcommand{\mytextB}{
  \begin{array}{cc}
    m\times n\textnormal{ matrices}
  \end{array}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    There is a correspondence
    \[
    \Set*{\mytextA}
    \longleftrightarrow
    \Set*{\mytextB}
    \]
    \pause
    Every linear transformation $T:\mathbb{R}^{n}\to \mathbb{R}^{m}$ is of the
    form $T=T_A$ where
    \[
    A=\myTA
    \]
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    This correspondence allows us to translate abstract statements about linear
    transformations into numerical statements about matrices
    \[
    \begin{array}{rcl}
      \Set*{\mytextA}       & \longleftrightarrow & \Set*{\mytextB} \\
      \onslide<2->{\Image(T)            } & \onslide<3->{=                   & \Col(A)                 } \\
      \onslide<4->{"T\text{ injective}" } & \onslide<5->{\longleftrightarrow & "\rank(A)=\#\mycolumns" } \\
      \onslide<6->{"T\text{ surjective}"} & \onslide<7->{\longleftrightarrow & "\rank(A)=\#\myrows"    } \\
      \onslide<9->{\ker(T)                & =}                               & \onslide<8->{\Null(A)   }
    \end{array}
    \]
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(5, 15, 3), (1, -40, 8)])
  T = lineartrans.linear_trans(A)
  e1, e2, e3 = [matrix.column(col) for col in identity_matrix(3).columns()]
\end{sagesilent}
\renewcommand{\myTA}{
  \begin{bmatrix}
    T(\vv*{e}{1}) & T(\vv*{e}{2}) & T(\vv*{e}{3})
  \end{bmatrix}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the linear transformation $T:\mathbb{R}^{3}\to \mathbb{R}^{2}$
    defined by
    \[
    T(x_1, x_2, x_3)
    =\sage{T}
    \]\pause%
    The matrix associated to $T$ is the $2\times 3$ matrix $A$ defined by
    \[
    A =\pause \myTA = \pause\sage{A}
    \]\pause%
    Note that $\rref(A)=\sage{A.rref()}$. \pause%
    Thus $T$ is surjecctive but not injective.
  \end{example}
\end{frame}


\section{Quadratic Forms}
\subsection{Definition}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{quadratic form} on $\mathbb{R}^{n}$ is a function
    $Q:\mathbb{R}^{n}\to \mathbb{R}$ of the form
    \[
    Q(x_1,x_2,\dotsc,x_n)=\sum_{\substack{i,j=1\\ i\geq j}}^nq_{ij}x_ix_j
    \]
    where $q_{ij}\in\mathbb{R}$.
  \end{definition}
\end{frame}


\subsection{Examples}
\begin{sagesilent}
  set_random_seed(304824424)
  import quadform
  Q1, p1 = quadform.randform(QQ, 2)
  Q2, p2 = quadform.randform(ZZ, 3)
  Q3, p3 = quadform.randform(ZZ, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Quadratic Form on $\mathbb{R}^{2}$]
    $Q(x_1,x_2)=\sage{p1}$
  \end{example}

  \pause
  \begin{example}[Quadratic Form on $\mathbb{R}^{3}$]
    $Q(x_1,x_2,x_3)=\sage{p2}$
  \end{example}

  \pause
  \begin{example}[Quadratic Form on $\mathbb{R}^{4}$]
    {\scriptsize$Q(x_1,x_2,x_3,x_4)=\sage{p3}$}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The function $Q(x_1,x_2)=x_1^2-3\,x_1x_2+1$ is \emph{not} a quadratic
    form. Quadratic forms must be a sum of terms of the form $q_{ij}x_ix_j$.
  \end{example}
\end{frame}


\subsection{Quadratic Forms and Symmetric Matrices}


\newcommand{\mytextX}{
  \begin{array}{c}
    \textnormal{quadratic forms}\\
    \mathbb{R}^{n}\to \mathbb{R}
  \end{array}}
\newcommand{\mytextY}{
  \begin{array}{cc}
    n\times n\textnormal{ symmetric matrices}
  \end{array}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A function $Q:\mathbb{R}^{n}\to \mathbb{R}$ is a quadratic form on
    $\mathbb{R}^{n}$ if and only if there is a symmetric matrix $A$ satisfying
    $Q(\vv{x})=\vv{x}^\intercal A\vv{x}$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    This theorem establishes a correspondence
    \[
    \begin{array}[rcl]{rcl}
      \Set*{\mytextX} & \longleftrightarrow & \Set*{\mytextY}
    \end{array}
    \]\pause%
    Given a quadratic form
    $\displaystyle Q(x_1,\dotsc,x_n)=\sum_{\substack{i,j=1\\ i\geq j}}^n
    q_{ij}x_ix_j$ we define $A$ by
    \[
    a_{ij}
    =
    \begin{cases}
      q_{ij} & i=j \\
      \nicefrac{q_{ij}}{2} & i\neq j
    \end{cases}
    \]
  \end{block}
\end{frame}



\begin{sagesilent}
  set_random_seed(4520842)
  var('x1 x2 x3 x4')
  v2 = matrix.column([x1, x2])
  v3 = matrix.column([x1, x2, x3])
  A2 = random_matrix(ZZ, 2)
  Q2 = A2.T*A2
  p2 = expand((v2.T*Q2*v2)[0, 0])
  A3 = random_matrix(ZZ, 3)
  Q3 = A3.T*A3
  p3 = expand((v3.T*Q3*v3)[0, 0])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The quadratic form $Q(x_1,x_2)=\sage{p2}$ is of the form \pause%
    \[
    Q(x_1,x_2)
    =\sage{v2.transpose()}\sage{Q2}\sage{v2}
    \]
  \end{example}

  \pause
  \begin{example}
    The quadratic form
    \[
    Q(x_1,x_2,x_3)=\sage{p3}
    \]
    is of the form \pause%
    \[
    Q(x_1,x_2,x_3)
    =\sage{v3.transpose()}\sage{Q3}\sage{v3}
    \]
  \end{example}
\end{frame}



\section{Compositions of Functions}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider a diagram of functions
    \[
    \begin{tikzcd}
      X\arrow{r}{f} \pgfmatrixnextcell Y\arrow{r}{g} \pgfmatrixnextcell Z
    \end{tikzcd}
    \]\pause%
    The \emph{composition} $g\circ f$ is the function $g\circ f:X\to Z$ defined
    by the formula $(g\circ f)(x)=g(f(x))$.
  \end{definition}

  \pause
  \begin{block}{Note}
    The composition $g(f(x))$ is only defined if
    \[
    \Target(f)=\Domain(g)
    \]
  \end{block}
\end{frame}


\subsection{Examples}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the functions $f:X\to Y$ and $g:Y\to Z$.
    \[
    \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
      >=latex, shorten >= 3pt, shorten <= 3pt, ]

      \begin{scope}[node distance=1/4]
        \node[inner sep=2pt] (a1) {$-3$}; %
        \node[inner sep=2pt, below=of a1] (a2) {$2$}; %
        \node[inner sep=2pt, below=of a2] (a3) {$17$}; %
        \node[inner sep=2pt, below=of a3] (a4) {$22$}; %

        \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$};%

        \node[inner sep=2pt, above right=.25cm and 3cm of a1] (b1) {$\alpha$};%
        \node[inner sep=2pt, below=of b1] (b2) {$\beta$};%
        \node[inner sep=2pt, below=of b2] (b3) {$\gamma$};%
        \node[inner sep=2pt, below=of b3] (b4) {$\delta$};%
        \node[inner sep=2pt, below=of b4] (b5) {$\varepsilon$};%

        \path[very thick, ->] (a1) edge (b5);%
        \path[very thick, ->] (a2) edge (b2);%
        \path[very thick, ->] (a3) edge (b1);%
        \path[very thick, ->] (a4) edge (b5);%

        \draw[very thick] (b3) ellipse (1.3cm and 1.6cm) node [above=1.6cm] {$Y$};%

        \onslide<2->{
        \node[inner sep=2pt,right=3cm of b2] (c1) {$\pi$};%
        \node[inner sep=2pt,below=of c1] (c2) {$\tau$};%
        \node[inner sep=2pt,below=of c2] (c3) {$\Omega$};%

        \draw[very thick] (c2) ellipse (1cm and 1.4cm) node [above=1.4cm] {$Z$};%

        \path[very thick, ->] (b1) edge (c3);%
        \path[very thick, ->] (b2) edge (c1);%
        \path[very thick, ->] (b3) edge (c2);%
        \path[very thick, ->] (b4) edge (c1);%
        \path[very thick, ->] (b5) edge (c3);}%

      \end{scope}
    \end{tikzpicture}
    \]
    \onslide<3->{The composition $g\circ f$ is the function $X\to Z$ defined by}
    \begin{align*}
      \onslide<4->{g(f(-3)) &=} \onslide<5->{g(\varepsilon)} & \onslide<7->{g(f(2)) &=} \onslide<8->{g(\beta)} & \onslide<10->{g(f(17)) &=} \onslide<11->{g(\alpha)} & \onslide<13->{g(f(22)) &=} \onslide<14->{g(\varepsilon)} \\
      &\onslide<6->{= \Omega}          &                      &\onslide<9->{= \pi}       &                        &\onslide<12->{= \Omega}    &                                 &\onslide<15->{= \Omega}
    \end{align*}
    \onslide<16->{Note that $\Image(g\circ f)=}\onslide<17>{\Set{\Omega,\pi}$.}
  \end{example}

\end{frame}
\endgroup


\newcommand{\mydiag}{    
  \begin{tikzcd}[column sep = small]
      \mathbb{R}^{2}\arrow{r}{f}
      \pgfmatrixnextcell \mathbb{R}^{3}\arrow{r}{g}
      \pgfmatrixnextcell \mathbb{R}
    \end{tikzcd}}
\newcommand{\myf}{
  \begin{bmatrix}
    x^2+y \\
    \sin(2\,x)\\
    y^3
  \end{bmatrix}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the diagram of functions $\mydiag$ defined by 
    \begin{align*}
      \onslide<2->{f(x, y) &= \myf} & \onslide<3->{g(x, y, z) &= 3\,x-\cos(yz)}
    \end{align*}
    \onslide<4->{The composition $g(f(x, y))$ is defined by}
    \begin{align*}
      \onslide<5->{g(f(x,y)) 
        &=} \onslide<6->{g\left(x^2+y, \sin(2\,x), y^3\right) \\
        &=} \onslide<7->{3\,(x^2+y)-\cos(\sin(2\,x)y^3)}
    \end{align*}
  \end{example}
\end{frame}


\end{document}
