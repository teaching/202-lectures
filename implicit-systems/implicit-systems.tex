\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Systems of Implicit Functions}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Motivation}
\subsection{Set-Up}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider a system of equations
  \begin{equation}
    \label{eq:mysys}
    \begin{array}{rcl}
      \onslide<1->{F_1(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_1} \\
      \onslide<2->{F_2(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_2} \\
      \onslide<3->{& \vdots &} \\
      \onslide<4->{F_k(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_k}
    \end{array}\tag{$\ast$}
  \end{equation}
  \onslide<5->{Suppose that $P=(\vv{x}^\ast, \vv{y}^\ast)$ solves
  \eqref{eq:mysys}.}

  \begin{block}{\onslide<6->{Question}}
    \onslide<7->{Can $\Set{y_1,\dotsc,y_n}$ be viewed as endogenous variables near the point
    $P$? }%
    \onslide<8->{That is, is each $y_i$ of the form $y_i=y_i(x_1,\dotsc,x_m)$?}
  \end{block}
\end{frame}


\subsection{A Linear Example}

\begin{sagesilent}
  A = matrix([(1, 4, -3, -7, -35, -4), (3, 12, -14, -15, -107, -22), (0, 0, -1, 1, -1, -2)])
  b = A.columns()[-1]
  A = A.matrix_from_columns(range(A.ncols()-1))
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\newcommand{\mySystem}[1]{#1
\begin{array}{rcrcrcrcrcr}
  x &+&  4\,y &-&  3\,z &-&  7\,r &-&  35\,s &=&  -4 \\
  3\,x &+& 12\,y &-& 14\,z &-& 15\,r &-& 107\,s &=& -22 \\
  & &       & &    -z &+&     r &-&      s &=&  -2
\end{array}}
\newcommand{\myOriginal}[1]{#1
\rref\left[\begin{array}{rrrrr|r}
    1\tikzmark{x1} & -3\tikzmark{z1} & -7\tikzmark{r1} & 4\tikzmark{y1} & -35\tikzmark{s1} & -4 \\
    3 & -14 & -15 & 12 & -107 & -22 \\
    0 & -1 & 1 & 0 & -1 & -2
  \end{array}\right]=
\left[\begin{array}{rrrrr|r}
    1\tikzmark{x2} & 0\tikzmark{z2} & 0\tikzmark{r2} & 4\tikzmark{y2} & -2\tikzmark{s2} & 2 \\
    0 & 1 & 0 & 0 & 4 & 2 \\
    0 & 0 & 1 & 0 & 3 & 0
  \end{array}\right]
\begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
  \node at (x1.north west) {$x$};
  \node at (z1.north west) {$z$};
  \node at (r1.north west) {$r$};
  \node at (y1.north west) {$y$};
  \node at (s1.north west) {$s$};

  \node at (x2.north west) {$x$};
  \node at (z2.north west) {$z$};
  \node at (r2.north west) {$r$};
  \node at (y2.north west) {$y$};
  \node at (s2.north west) {$s$};
\end{tikzpicture}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(x, y, z, r, s)=(2, 0, 2, 0, 0)$ satisfies the system
    \[
    \mySystem{\scriptsize}
    \]
    \onslide<2->{Suppose that $y$ is increased from $y=0$ to $y=1$ and that $s$
    is decreased from $s=0$ to $s=-2$. }%
    \onslide<3->{Approximate the corresponding values of $x$, $z$, and $r$.}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{Note that
    \[
    \myOriginal{\scriptsize}
    \]}%
    \onslide<5->{It follows that}
    \begin{align*}
      \onslide<6->{x(y, s)  &=} \onslide<7->{2-4\,y+2\,s} & \onslide<12->{x(1, -2) &=} \onslide<13->{-6} \\
      \onslide<8->{z(y, s)  &=} \onslide<9->{2-4\,s     } & \onslide<14->{z(1, -2) &=} \onslide<15->{10} \\
      \onslide<10->{r(y, s) &=} \onslide<11->{-3\,s     } & \onslide<16->{r(1, -2) &=} \onslide<17->{ 6} \\
    \end{align*}
  \end{block}

\end{frame}
\endgroup


\subsection{A Non-Linear Example}

\begin{sagesilent}
  var('x y z')
  G = x**2+y**2-z*exp(z**2-4)
  x0, y0, z0 = -2, 3, 2
  P = (x0, y0, z0)
  c = G(x=x0, y=y0, z=z0)
  x1, y1 = x0-1/2, y0+1/6
  Gx = G.diff(x)
  Gy = G.diff(y)
  Gz = G.diff(z).canonicalize_radical()
  GxEval = Gx(x=x0, y=y0, z=z0)
  GyEval = Gy(x=x0, y=y0, z=z0)
  GzEval = Gz(x=x0, y=y0, z=z0)
  Dz = -1/GzEval*matrix([GxEval, GyEval])
  DeltaXY = matrix.column([x1-x0, y1-y0])
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $\sage{P}$ satisfies
    \[
    \sage{G}=\sage{c}
    \]
    \onslide<2->{Suppose that $x$ is decreased from $x=\sage{x0}$ to
    $x=\sage{x1}$ and that $y$ is increased from $y=\sage{y0}$ to
    $y=\sage{y1}$. }%
    \onslide<3->{Approximate the corresponding change in $z$.}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{We are interested in the level set $G=\sage{c}$ where $G(x, y,
    z)=\sage{G}$. }%
    \onslide<5->{The partials of $G$ are}
    \begin{align*}
      \onslide<6->{G_x          &=} \onslide<7->{\sage{Gx}}       & \onslide<8->{G_y          &=} \onslide<9->{\sage{Gy}}      & \onslide<10->{G_z         &=} \onslide<11->{\sage{Gz}} \\
      \onslide<12->{G_x\sage{P} &=} \onslide<13->{\sage{GxEval}}  & \onslide<14->{G_y\sage{P} &=} \onslide<15->{\sage{GyEval}} & \onslide<16->{G_z\sage{P} &=} \onslide<17->{\sage{GzEval}} \\
    \end{align*}%
    \onslide<18->{The implicit function theorem provides the approximation}
    \[
    \onslide<19->{z\left(\sage{x1}, \sage{y1}\right)
    \approx}
    \onslide<20->{L\left(\sage{x1}, \sage{y1}\right)
    =} \onslide<21->{\sage{z0}+\sage{Dz}\sage{DeltaXY}
    =} \onslide<22->{\sage{z0+(Dz*DeltaXY)[0,0]}}
    \]
  \end{block}
\end{frame}
\endgroup


\subsection{A New Example}

\begin{sagesilent}
  var('w x y z')
  g1 = w**2+x**2+y**2+z**2
  g2 = 2*x+3*x*y*z+2*w*z
  w0, x0, y0, z0 = 2, -1, 3, 4
  w1 = w0+2
  x1 = x0-3
  P = (w0, x0, y0, z0)
  g1Eval = g1(w=w0, x=x0, y=y0, z=z0)
  g2Eval = g2(w=w0, x=x0, y=y0, z=z0)
  G = matrix.column([g1, g2])
  c = matrix.column([g1Eval, g2Eval])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(w, x, y, z)=\sage{P}$ satisfies the system
    \[
    \begin{array}{rcr}
      \sage{g1} &=& \sage{g1Eval} \\
      \sage{g2} &=& \sage{g2Eval} \\
    \end{array}
    \]
    \onslide<2->{Suppose that $w$ is increased from $w=\sage{w0}$ to
    $w=\sage{w1}$ and that $x$ is decreased from $x=\sage{x0}$ to
    $x=\sage{x1}$. }%
    \onslide<3->{Approximate the corresponding values of $y$ and $z$}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{This system is of the form $G=\vv{c}$ where}
    \begin{align*}
      \onslide<5->{G(w, x, y, z) &=} \onslide<6->{\sage{G}} & \onslide<7->{\vv{c} &=} \onslide<8->{\sage{c}}
    \end{align*}
    \onslide<9->{Here, $G:\mathbb{R}^{4}\to \mathbb{R}^{2}$. }%
    \onslide<10->{We need a new implicit function theorem!}
  \end{block}
\end{frame}


\section{The General Implicit Function Theorem}
\subsection{Statement}


\newcommand{\myJacobian}{
\left[
  \begin{array}{cccccc}
    \pderiv{F_1}{x_1}                  & \dotsb & \pderiv{F_1}{x_m}                  & \pderiv{F_1}{y_1}                  & \dotsb & \pderiv{F_1}{y_n} \\
    \vdots                             & \ddots & \vdots                             & \vdots                             & \ddots & \vdots            \\
    \tikzmark{lower1}\pderiv{F_k}{x_1} & \dotsb & \pderiv{F_k}{x_m}\tikzmark{lower2} & \tikzmark{lower3}\pderiv{F_k}{y_1} & \dotsb & \pderiv{F_k}{y_n}\tikzmark{lower4}
  \end{array}
\right]
  % code from https://tex.stackexchange.com/questions/102460/underbraces-in-matrix-divided-in-blocks
\begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
  \draw[decorate, thick] (lower2.south) -- (lower1.south)
  node [midway,below=5pt] {$D_{\vv{x}}F$};%
  \draw[decorate, thick] (lower4.south) -- (lower3.south)
  node [midway,below=5pt] {$D_{\vv{y}}F$};%
\end{tikzpicture}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Our system of equations
  \begin{equation}
    \label{eq:mysys}
    \begin{array}{rcl}
      F_1(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_1 \\
      F_2(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_2 \\
      & \vdots & \\
      F_k(x_1,\dotsc,x_m,y_1,\dotsc,y_n) &=& c_k
    \end{array}\tag{$\ast$}
  \end{equation}
  is of the form $F=\vv{c}$ where $F:\mathbb{R}^{m+n}\to \mathbb{R}^{k}$. %
  \onslide<2->{Note that
  \[
  DF
  =
  \myJacobian
  \]}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 15.7 in Simon \& Blume]
    Consider a system
    \[
    F(x_1,\dotsc,x_m,y_1,\dotsc,y_n)=\vv{c}
    \]\pause
    Then $\vv{y}=\vv{y}(x_1,\dotsc,x_m)$, provided that $D_{\vv{y}}F$ is
    invertible.  \pause Moreover, we can solve for the Jacobian derivative
    $D\vv{y}$ with
    \[
    (D_{\vv{y}}F)\cdot(D\vv{y})=-D_{\vv{x}}F
    \]
  \end{theorem}

  \pause
  \begin{block}{Note}
    The variables $\Set{y_1,\dotsc,y_n}$ are \emph{endogenous variables} \pause
    and the variables $\Set{x_1,\dotsc,x_m}$ are the \emph{corresponding
    exogenous variables}.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The equation $(D_{\vv{y}}F)\cdot(D\vv{y})=-D_{\vv{x}}F$ can be thought of as
    \[
    (D_{\Set{\textnormal{endo}}}F)
    \cdot
    \frac{\partial(\textnormal{endo})}{\partial(\textnormal{exo})}
    =-D_{\Set{\textnormal{exo}}}F
    \]
  \end{block}

  \pause
  \begin{block}{Note}
    When $D_{\Set{\textnormal{endo}}}F$ is invertible, we have
    \[
    \frac{\partial(\textnormal{endo})}{\partial(\textnormal{exo})}
    =(D_{\Set{\textnormal{endo}}}F)^{-1}\cdot(-D_{\Set{\textnormal{exo}}}F)
    \]
  \end{block}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  g1Eval = g1(w=w0, x=x0, y=y0, z=z0)
  g2Eval = g2(w=w0, x=x0, y=y0, z=z0)
  G = matrix.column([g1, g2])
  c = matrix.column([g1Eval, g2Eval])
  DG = jacobian(G, (w, x, y, z))
  DGEval = DG(w=w0, x=x0, y=y0, z=z0)
  DxG = jacobian(G, (w, x))
  DxGEval = DxG(w=w0, x=x0, y=y0, z=z0)
  DyG = jacobian(G, (y, z))
  DyGEval = DyG(w=w0, x=x0, y=y0, z=z0)
  var('w x y z')
  g1 = w**2+x**2+y**2+z**2
  g2 = 2*x+3*x*y*z+2*w*z
  w0, x0, y0, z0 = 2, -1, 3, 4
  w1 = w0+2
  x1 = x0-3
  P = (w0, x0, y0, z0)
\end{sagesilent}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(w, x, y, z)=\sage{P}$ satisfies the system
    \[
    \begin{array}{rcr}
      \sage{g1} &=& \sage{g1Eval} \\
      \sage{g2} &=& \sage{g2Eval} \\
    \end{array}
    \]
    \onslide<2->{Suppose that $w$ is increased from $w=\sage{w0}$ to
    $w=\sage{w1}$ and that $x$ is decreased from $x=\sage{x0}$ to
    $x=\sage{x1}$. }  %
    \onslide<3->{Approximate the corresponding values of $y$ and $z$.}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{This system is of the form $F(w,x,y,z)=\vv{c}$ where}
    \begin{align*}
      \onslide<5->{F(w,x,y,z) &=} \onslide<6->{\sage{G}} &
      \onslide<7->{\vv{c}     &=} \onslide<8->{\sage{c}}
    \end{align*}
    \onslide<9->{We wish to determine if $\Set{y,z}$ is endogenous. }%
    \onslide<10->{Note that}
    \begin{align*}
      \onslide<11->{D_{\Set{y,z}}F  &=} \onslide<12->{\sage{DyG}} & 
      \onslide<13->{D_{\Set{w, x}}F &=} \onslide<14->{\sage{DxG}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\newcommand{\myExDY}{
\begin{bmatrix}
  \displaystyle\pderiv{y}{w} & \displaystyle\pderiv{y}{x} \\ \\
  \displaystyle\pderiv{z}{w} & \displaystyle\pderiv{z}{x}
\end{bmatrix}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    At the point $(w, x, y, z)=\sage{P}$, we have
    \begin{align*}
      \onslide<2->{D_{\Set{y, z}}F &=} \onslide<3->{\sage{DyGEval}} & 
      \onslide<4->{D_{\Set{w, x}}F &=} \onslide<5->{\sage{DxGEval}}
    \end{align*}
    \onslide<6->{Note that $\det D_{\Set{y,
    z}}=}\onslide<7->{\sage{DyGEval.det()}\neq 0$. }%
    \onslide<8->{The implicit function theorem gives}
    \[
    \onslide<9->{\frac{\partial(y, z)}{\partial(w, x)}=}
    \onslide<10->{(D_{\Set{y, z}}F)^{-1}\cdot(-D_{\Set{w,x}}F)}
    \]
    \onslide<11->{This equation takes the form}
    \[
    \onslide<12->{\myExDY=}
    \onslide<13->{{\sage{DyGEval}}^{-1}\sage{-DxGEval}=}
    \onslide<14->{\frac{1}{\sage{DyGEval.det()}}\sage{DyGEval.adjoint() * (-DxGEval)}}
    \]
  \end{block}
\end{frame}
\endgroup



\newcommand{\myLocLinEx}{
\begin{bmatrix}
  y(4, -4)\\ z(4, -4)
\end{bmatrix}}
\newcommand{\myDelta}{
\begin{bmatrix}
  4-2\\ -4+1
\end{bmatrix}}
\begin{sagesilent}
  vvY0 = matrix.column([y0, z0])
  DyEval = DyGEval.inverse() * -DxGEval
  Delta = matrix.column([4-2, -4+1])
  myAdjoint = DyGEval.adjoint() * (-DxGEval)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    When $w$ is increased from $w=\sage{w0}$ to $w=\sage{w1}$ and $x$ is
    decreased from $x=\sage{x0}$ to $x=\sage{x1}$, our approximation is
    \begin{align*}
      \onslide<2->{\myLocLinEx
      &\approx} \onslide<3->{L(2+2, -1-2) \\
      &=} \onslide<4->{\sage{vvY0}+\frac{1}{\sage{DyGEval.det()}}\sage{myAdjoint}\sage{Delta} \\
      &=} \onslide<5->{\sage{vvY0+ (1/DyGEval.det())*myAdjoint * Delta}}
    \end{align*}
  \end{block}
\end{frame}



\begin{sagesilent}
  var('p q r s')
  F1 = p*q-r
  F2 = q-r**3-3*s
  F3 = r**3+s**3-2*s*r
  F = matrix.column([F1, F2, F3])
  p0, q0, r0, s0 = 1/4, 4, 1, 1
  P = (p0, q0, r0, s0)
  F10 = F1(p=p0, q=q0, r=r0, s=s0)
  F20 = F2(p=p0, q=q0, r=r0, s=s0)
  F30 = F3(p=p0, q=q0, r=r0, s=s0)
  DpqrF = jacobian(F, (p, q, r))
  DpqrF0 = DpqrF(p=p0, q=q0, r=r0, s=s0)
  DsF = jacobian(F, s)
  DsF0 = DsF(p=p0, q=q0, r=r0, s=s0)
\end{sagesilent}
\newcommand{\myP}{(\nicefrac{1}{4}, 4, 1, 1)}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(p, q, r, s)=\myP$ satisfies the equations
    \begin{align*}
      \sage{F1} &= \sage{F10} &
      \sage{F2} &= \sage{F20} &
      \sage{F3} &= \sage{F30}
    \end{align*}
    \onslide<2->{Show that $p$, $q$, and $r$ can be viewed as functions of $s$
    near this point and compute $\pderiv{p}{s}$, $\pderiv{q}{s}$, and
    $\pderiv{r}{s}$.}
  \end{example}

  \begin{block}{\onslide<3->{Solution}}
    \onslide<4->{This system is of the form $F=\vv{O}$ where}
    \[
    \onslide<4->{F=}\onslide<5->{\sage{F}}
    \]
    \onslide<6->{We wish to determine if $\Set{p, q, r}$ is endogenous. }%
    \onslide<7->{Note that}
    \begin{align*}
      \onslide<8->{D_{\Set{p, q, r}}F &=} \onslide<9->{\sage{DpqrF}} &
      \onslide<10->{D_{\Set{s}}F &=} \onslide<11->{\sage{DsF}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\newcommand{\mySys}{
\begin{bmatrix}
  \nicefrac{\partial p}{\partial s} \\
  \nicefrac{\partial q}{\partial s} \\
  \nicefrac{\partial r}{\partial s}
\end{bmatrix}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    At the point $(p, q, r, s)=\myP$, we have
    \begin{align*}
      \onslide<2->{D_{\Set{p, q, r}}F &=} \onslide<3->{\sage{DpqrF0}} &
      \onslide<4->{D_{\Set{s}}F     &=} \onslide<5->{\sage{DsF0}}
    \end{align*}
    \onslide<6->{Note that $\det D_{\Set{p, q, r}}F=}\onslide<7->{\sage{DpqrF0.det()}\neq
    0$. }%
    \onslide<8->{The implicit function theorem gives}
    \[
    \onslide<9->{(D_{\Set{p,q,r}}F)\cdot\frac{\partial(p,q,r)}{\partial (s)}=-D_{\Set{s}}F}
    \]
    \onslide<10->{This equation takes the form}
    \[
    \onslide<11->{\sage{DpqrF0}\mySys=\sage{-DsF0}}
    \]
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  a1, a2, a3 = DpqrF0.columns()
  b, = (-DsF0).columns()
  A1 = matrix.column([b, a2, a3])
  A2 = matrix.column([a1, b, a3])
  A3 = matrix.column([a1, a2, b])
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    We may solve this system using Cramer's rule\pause
    \[
    \begin{array}{*5{>{\displaystyle}c}p{5cm}}
      \pderiv{p}{s} &=& \frac{\det\sage{A1}}{4} &=& \sage{A1.det() / DpqrF0.det()} \\ \\ \pause
      \pderiv{q}{s} &=& \frac{\det\sage{A2}}{4} &=& \sage{A2.det() / DpqrF0.det()} \\ \\ \pause
      \pderiv{r}{s} &=& \frac{\det\sage{A3}}{4} &=& \sage{A3.det() / DpqrF0.det()} 
    \end{array}
    \]
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  var('x1 x2 y1 y2 y3')
  F1 = x1**2+2*x2**2-3*y1**2+4*y1*y2-y2**2+y3**3
  F2 = x1+3*x2-4*x1*x2+4*y1**2-2*y2**2+y3**2
  F3 = x1**3-x2**3+4*y1**2+2*x1*y2-3*y3**2
  F = matrix.column([F1, F2, F3])
  DxF = jacobian(F, (x1, x2))
  DyF = jacobian(F, (y1, y2, y3))
  x10, x20, y10, y20, y30 = 0, 1, 0, -1, 1
  P = (x10, x20, y10, y20, y30)
  DxF0 = DxF(x1=x10, x2=x20, y1=y10, y2=y20, y3=y30)
  DyF0 = DyF(x1=x10, x2=x20, y1=y10, y2=y20, y3=y30)
  F10 = F1(x1=x10, x2=x20, y1=y10, y2=y20, y3=y30)
  F20 = F2(x1=x10, x2=x20, y1=y10, y2=y20, y3=y30)
  F30 = F3(x1=x10, x2=x20, y1=y10, y2=y20, y3=y30)
  c = matrix.column([F10, F20, F30])
\end{sagesilent}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(x_1, x_2, y_1, y_2, y_3)=\sage{P}$ satisfies the equations
    \[
    \begin{array}{rcr}
      \sage{F1} &=& \sage{F10} \\
      \sage{F2} &=& \sage{F20} \\
      \sage{F3} &=& \sage{F30} 
    \end{array}
    \]
    \onslide<2->{Show that $\Set{y_1, y_2, y_3}$ is endogenous at this point and
    compute the partials $\nicefrac{\partial y_i}{\partial x_j}$.}
  \end{example}

  \begin{block}{\onslide<3->{Solution}}
    \onslide<3->{This system is of the form $F=\vv{c}$ where}
    \begin{align*}
      \onslide<4->{F &= \sage{F} & \vv{c} &= \sage{c}}
    \end{align*}
    \onslide<5->{Note that}
    \begin{align*}
      \onslide<6->{D_{\Set{y_1, y_2, y_3}} &= \mysage{\tiny}{DyF}} &
      \onslide<7->{D_{\Set{x_1, x_2}}     &= \mysage{\tiny}{DxF}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\newcommand{\myPartials}{
\begin{bmatrix}
  \pderiv{y_1}{x_1} & \pderiv{y_1}{x_2} \\ \\
  \pderiv{y_2}{x_1} & \pderiv{y_2}{x_2} \\ \\
  \pderiv{y_3}{x_1} & \pderiv{y_3}{x_2} 
\end{bmatrix}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    At the point $(x_1, x_2, y_1, y_2, y_3)=\sage{P}$, we have
    \begin{align*}
      \onslide<2->{D_{\Set{y_1, y_2, y_3}}F &= \sage{DyF0}} &
      \onslide<3->{D_{\Set{x_1, x_2}}F     &= \sage{DxF0}}
    \end{align*}
    \onslide<4->{Note that $\det D_{\Set{y_1,y_2,y_3}}F=\sage{DyF0.det()}\neq
    0$. }%
    \onslide<5->{The implicit function theorem gives}
    \[
    \onslide<6->{(D_{\Set{y_1,y_2,y_3}}F)\cdot\frac{\partial(y_1,y_2,y_3)}{\partial(x_1,x_2)}
    =-D_{\Set{x_1,x_2}}F}
    \]
    \onslide<7->{This equation takes the form}
    \[
    \onslide<8->{\sage{DyF0}\myPartials=\sage{-DxF0}}
    \]
  \end{block}
\end{frame}
\endgroup


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    To solve for the partials $\nicefrac{\partial y_i}{\partial x_j}$, we may
    invert $D_{\Set{y_1,y_2,y_3}}F$. \onslide<2->{This gives}
    \begin{align*}
      \onslide<3->{\myPartials
      &=} \onslide<4->{(D_{\Set{y_1,y_2,y_3}}F)^{-1}\cdot (-D_{\Set{x_1, x_2}}F) \\
      &=} \onslide<5->{{\sage{DyF0}}^{-1}\sage{-DxF0} \\
      &=} \onslide<6->{\frac{1}{\sage{DyF0.det()}}\sage{DyF0.adjoint()}\sage{-DxF0} \\
      &=} \onslide<7->{\frac{1}{\sage{DyF0.det()}}\sage{DyF0.adjoint() * (-DxF0)}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup



\begin{sagesilent}
  var('x y a b c')
  F1 = x**2*c +a*x*y+y**2*b-b**2
  F2 = x**2*b+y**2-a**2+3*c
  F = matrix.column([F1, F2])
  x0, y0, a0, b0, c0 = 0, 1, 2, 1, 1
  P = (x0, y0, a0, b0, c0)
  F10 = F1(x=x0, y=y0, a=a0, b=b0, c=c0)
  F20 = F2(x=x0, y=y0, a=a0, b=b0, c=c0)
  F0 = F(x=x0, y=y0, a=a0, b=b0, c=c0)
  a1, b1, c1 = a0+1/10, b0-1/5, c0+2/5
  Delta = matrix.column([a1, b1, c1])
  x0y0 = matrix.column([x0, y0])
  DxyF = jacobian(F, (x, y))
  DabcF = jacobian(F, (a, b, c))
  DxyF0 = DxyF(x=x0, y=y0, a=a0, b=b0, c=c0)
  DabcF0 = DabcF(x=x0, y=y0, a=a0, b=b0, c=c0)
  J = DxyF0.inverse() * (-DabcF0)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The point $(x, y, a, b, c)=\sage{P}$ satisfies the equations
    \begin{align*}
      \sage{F1} &= \sage{F10} \\
      \sage{F2} &= \sage{F20}
    \end{align*}
    \onslide<2->{Suppose that $a$ is increased by $\nicefrac{1}{10}$, $b$ is
      decreased by $\nicefrac{1}{5}$, and $c$ is increased by
      $\nicefrac{2}{5}$. }%
    \onslide<3->{Approximate the corresponding values of $x$ and $y$.}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<5->{The system is of the form $F=\vv{O}$ where}
    \[
    \onslide<6->{F=\sage{F}}
    \]
    \onslide<7->{We wish to determine if $\Set{x,y}$ is endogenous. }%
    \onslide<8->{Note that}
    \begin{align*}
      \onslide<9->{D_{\Set{x, y}}F &=} \onslide<10->{\sage{DxyF}} &
      \onslide<11->{D_{\Set{a, b, c}}F &=} \onslide<12->{\sage{DabcF}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\newcommand{\myJacobi}[2]{\frac{\partial(#1)}{\partial(#2)}}
\newcommand{\myLast}{
  \left[
  \begin{array}{*3{>{\displaystyle}c}p{5cm}}
    \pderiv{x}{a} & \pderiv{x}{b} & \pderiv{x}{c} \\ \\
    \pderiv{y}{a} & \pderiv{y}{b} & \pderiv{y}{c} 
  \end{array}\right]}
\begingroup
\small
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    At the point $(x, y, z, b, c)=\sage{P}$, we have
    \begin{align*}
      \onslide<2->{D_{\Set{x, y}}F &=} \onslide<3->{\sage{DxyF0}} &
      \onslide<4->{D_{\Set{a, b, c}}F &=} \onslide<5->{\sage{DabcF0}}
    \end{align*}
    \onslide<6->{The implicit function theorem gives}
    \[
    \onslide<6->{(D_{\Set{x, y}}F)\cdot\myJacobi{x, y}{a, b, c}
    =-D_{\Set{a, b, c}}F}
    \]
    \onslide<7->{This equation takes the form}
    \begin{align*}
      \onslide<8->{\sage{DxyF0}\myLast=\sage{-DabcF0}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    Inverting $D_{\Set{x, y}}F$ gives
    \begin{align*}
      \onslide<2->{\myLast 
        &=} \onslide<3->{{\sage{DxyF0}}^{-1}\sage{-DabcF0} \\
        &=} \onslide<4->{\sage{DxyF0.inverse()}\sage{-DabcF0} \\
        &=} \onslide<5->{\sage{J}}
    \end{align*}

  \end{block}
\end{frame}



\newcommand{\myLastApprox}{
  \left[
    \begin{array}{c}
      x(2+\nicefrac{1}{10}, 1-\nicefrac{1}{5}, 1+\nicefrac{2}{5})\\ 
      y(2+\nicefrac{1}{10}, 1-\nicefrac{1}{5}, 1+\nicefrac{2}{5})
    \end{array}
  \right]}
\newcommand{\myLastDelta}{
  \left[
    \begin{array}{r}
      \nicefrac{1}{10} \\
      -\nicefrac{1}{5} \\
      \nicefrac{2}{5}
    \end{array}
  \right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    This gives the approximation
    \begin{align*}
      \onslide<2->{\myLastApprox
        &\approx} \onslide<3->{\sage{x0y0}+\sage{J}\myLastDelta \\
        &=} \onslide<4->{\sage{x0y0+J*Delta}}
    \end{align*}
  \end{block}
\end{frame}


\end{document}
