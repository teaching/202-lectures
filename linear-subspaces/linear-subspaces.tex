\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}




\title{Linear Subspaces of $\mathbb{R}^n$}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{Definitions}
\subsection{Subcollections of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    If $V$is a subcollection of $\mathbb{R}^n$, then we write
    $V\subset\mathbb{R}^n$ or $V\subseteq\mathbb{R}^n$.
  \end{definition}


  \pause
  \begin{example}
    Let $V=\Set*{\langle1,0\rangle, \langle3, -34\rangle,
      \langle22,-22\rangle}$. Then $V\subset\mathbb{R}^2$.
  \end{example}

  \pause
  \begin{example}
    Let $V$ be the subcollection of $\mathbb{R}^3$ consisting of all vectors
    $\vv{v}=\langle v_1,v_2,v_3\rangle$ where $v_3=11$. Then
    $V\subset\mathbb{R}^3$. \pause %
    Note that $\langle2,-22,11\rangle\in V$ but $\langle-2,1,0\rangle\notin V$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The notation $V=\Set*{\vv{v}\in\mathbb{R}^n\given P}$ means ``$V$ consists
    of all $\vv{v}\in\mathbb{R}^n$ that satisfy the property $P$. This notation
    is called \emph{set-builder notation}.
  \end{definition}

  \pause

  \begin{block}{Idea}
    Set-builder notation is an efficient way to define subcollections of
    $\mathbb{R}^n$.
  \end{block}

  \pause

  \begin{example}
    Let $V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2\geq 0}$. Then
    $\langle 1, 3\rangle\in V$ but $\langle1,-3\rangle\notin V$.
  \end{example}

  \pause

  \begin{example}
    Let $S=\Set*{\vv{v}\in\mathbb{R}^4\given\norm{\vv{v}}=1}$. Then $\langle1,
    0, 0, 0\rangle\in S$ but $\langle1, 1, 1, 1\rangle\notin S$.
  \end{example}
\end{frame}


\subsection{Linear Subspaces of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A subcollection $V$ of $\mathbb{R}^n$ is a \emph{linear subspace of
      $\mathbb{R}^n$} if 
    \begin{enumerate}
    \item<2-> $\vv{O}\in V$
    \item<3-> $\vv{v},\vv{w}\in V$ implies $\vv{v}+\vv{w}\in V$
    \item<4-> $c\in\mathbb{R}$ and $\vv{v}\in V$ implies $c\cdot\vv{v}\in V$
    \end{enumerate}
  \end{definition}

  \begin{block}{\onslide<5->{Idea}}
    \onslide<5->{
      Linear subspaces of $\mathbb{R}^n$ are exactly the subcollections of
      $\mathbb{R}^n$ that are ``closed'' under linear combinations.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2=1}$. Is $V$ a
    linear subspace of $\mathbb{R}^2$?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{We can check each axiom of linear subspaces individually.}
    \begin{enumerate}
    \item<3-> Note that $\vv{O}\notin V$ since $\vv{O}=\langle0,0\rangle$ but $0\neq
      1$. Thus the first axiom of linear subspaces fails.
    \item<4-> Note that $\langle0,1\rangle\in V$ and $\langle1,1\rangle\in V$ but
      \[
      \langle0,1\rangle+\langle1,1\rangle\notin V
      \]
      Thus the second axiom of linear subspaces fails.
    \item<5-> Note that $\langle0,1\rangle\in V$ but
      \[
      2\cdot\langle0,1\rangle=\langle0,2\rangle\notin V
      \]
      Thus the third axiom of linear subspaces fails.
    \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $V=\Set*{\vv{v}\in\mathbb{R}^3\given \norm{\vv{v}}=1}$. Is $V$ a linear
    subspace of $\mathbb{R}^3$?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{We can check each axiom individually.}
    \begin{enumerate}
    \item<3-> Note that $\vv{O}\notin V$ since $\norm{\vv{O}}=0\neq 1$. Thus the
      first axiom of linear subspaces fails.
    \item<4-> Note that $\langle1, 0, 0\rangle\in V$ and
      $\langle0,1,0\rangle\in V$ but
      \[
      \langle1,0,0\rangle+\langle0,1,0\rangle=\langle1,1,0\rangle\notin V
      \]
      Thus the second axiom of linear subspaces fails.
    \item<5-> Note that $\langle1,0,0\rangle\in V$ but
      \[      
      -10\cdot\langle1,0,0\rangle=\langle-10,0,0\rangle\notin V
      \]
      Thus the third axiom of linear subspaces fails.
    \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $V=\Set*{\langle x,y\rangle\in\mathbb{R}^2\given x\geq0\textnormal{ and
      }y\geq0}$. Is $V$ a linear subspace of $\mathbb{R}^2$?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{We can check the axioms individually.}
    \begin{enumerate}
    \item<3-> Note that $\vv{O}\in V$ since $\vv{O}=\langle0,0\rangle$. The first
      axiom passes.
    \item<4-> Suppose that $\langle v_1,v_2\rangle\in V$ and $\langle
      w_1,w_2\rangle\in V$. This means that $v_1,v_2,w_1,w_2\geq 0$. Then
      \[
      \langle v_1,v_2\rangle+\langle w_1,w_2\rangle=\langle v_1+w_1,v_2+w_2\rangle\in V
      \]
      since $v_1+w_1\geq 0$ and $v_2+w_2\geq 0$. The second axiom passes.
    \item<5-> Note that $\langle1,0\rangle\in V$ but
      $-22\cdot\langle1,0\rangle=\langle-22,0\rangle\notin V$. The third axiom
      fails.
    \end{enumerate}
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $V=\Set*{\vv{v}\in\mathbb{R}^2\given\vv{v}=\langle x+y, y\rangle}$. Is
    $V$ a linear subspace of $\mathbb{R}^2$?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{We can check the axioms individually.}
    \begin{enumerate}
    \item<3-> Note that $\vv{O}=\langle0,0\rangle=\langle0+0,0\rangle\in V$. The
      first axiom passes.
    \item<4-> Suppose that $\langle x+y, y\rangle\in V$ and $\langle a+b,b\rangle\in
      V$. Then 
      \[
      \left\langle x+y,y\rangle+\langle a+b,b\rangle
        =\langle (x+a)+(y+b), y+b\right\rangle\in V
      \]
      The first axiom passes.
    \item<5-> Suppose that $\langle x+y, y\rangle\in V$. Then 
      \[
      c\cdot\langle x+y, y\rangle
      =\langle cx+cy, cy\rangle
      \in V
      \]
      The third axiom passes.
    \end{enumerate}
  \end{block}
\end{frame}


\section{Important Linear Subspaces}
\subsection{Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $m\times n$ matrix and let 
    $
    V=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    $.
    Then $V$ is a linear subspace of $\mathbb{R}^n$.
  \end{theorem}
  \onslide<2->{
    \begin{proof}
      We can check the axioms individually.}
    \begin{enumerate}
    \item<3-> Note that $\vv{O}\in V$ since $A\vv{O}=\vv{O}$.
    \item<4-> Suppose that $\vv{x}\in V$ and that $\vv{y}\in V$. Then
      \[
      A(\vv{x}+\vv{y})
      = A\vv{x}+A\vv{y}
      = \vv{O}+\vv{O}
      = \vv{O}
      \]
      Thus $\vv{x}+\vv{y}\in V$.
    \item<5-> Suppose that $\vv{x}\in V$. Then $A(c\cdot\vv{x})=c\cdot
      A\vv{x}=c\cdot\vv{O}=\vv{O}$. Hence $c\cdot\vv{x}\in V$. 
    \end{enumerate}
    \onslide<6->{All axioms pass!}%
    \alt<6>{\qedhere}{\phantom\qedhere}
  \end{proof}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The linear subspace
    \[
    \Null(A)=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    \]
    of $\mathbb{R}^n$ is called the \emph{null space of $A$}. 
  \end{definition}

\end{frame}


\begin{sagesilent}
  A = matrix([(1, 7, 13, 0), (-1, 4, 9, 0), (0, 5, 10, 0)])
  u = vector([1, -2, 1, 2])
  v = vector([-49, -6, -25, -1])
  w = vector([22, -11, 0, 1])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\sage{A}$. Which of the vectors
    \begin{align*}
      \vv{u} &= \sage{u} & \vv{v} &= \sage{v} & \vv{w} &= \sage{w}
    \end{align*}
    is in $\Null(A)$?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \begin{align*}
      A\vv{u} &= \sage{matrix.column(A*u)} & 
      A\vv{v} &= \sage{matrix.column(A*v)} &
      A\vv{w} &= \sage{matrix.column(A*w)}
    \end{align*}
    \pause
    Thus $\vv{u}\in\Null(A)$ while $\vv{v}\notin\Null(A)$ and
    $\vv{w}\notin\Null(A)$.
  \end{block}
\end{frame}



\begin{sagesilent}
  A = matrix(2, 3, [-3,-4,3,0,6,-1])
  var('x y z')
  vvx = matrix.column([x, y, z])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the subcollection of $\mathbb{R}^3$ given by
    \[
    V=
    \Set*{\langle x,y,z\rangle\in\mathbb{R}^3\given
      \begin{array}{rcrcrcr}
        -3\,x & - & 4\,y & + & 3\,z & = & 0 \\
        &   & 6\,y & - &    z & = & 0
      \end{array}}
    \]
    Is $V$ a linear subspace of $\mathbb{R}^3$?
  \end{example}

  \pause

  \begin{block}{Solution}
    Note that the equations defining $V$ are exactly the equations given by
    $A\vv{x}=\vv{O}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{vvx}
    \end{align*}
    \pause
    Thus $V=\Null(A)$, so $V$ is a linear subspace of $\mathbb{R}^3$.
  \end{block}
\end{frame}


\subsection{Span}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ be a collection of
    vectors in $\mathbb{R}^n$. We define
    $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ as the collection of
    all linear combinations of $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$.
  \end{definition}

  \pause

  \begin{block}{Note}
    Simon and Blume use the notation
    $\mathcal{L}[\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}]$ instead of
    $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$.
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Elements of $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ are exactly
    the vectors $\vv{b}$ of the form
    \[
    \vv{b}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}
    \]
    for some scalars $c_1,c_2,\dotsc,c_k$.
  \end{block}

  \pause

  \begin{block}{Note}
    Every vector $\vv{b}$ of the form
    \[
    \vv{b}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}
    \]
    is a vector in $\mathbb{R}^n$. Hence
    $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ is a subcollection of
    $\mathbb{R}^n$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(2, 12, -14, -14), (-5, -19, 24, 24), (-1, -8, 9, 9)])
  b = vector([3,10,-1])
  M = A.augment(b, subdivide=True)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{matrix.column(b)}}$ in
    ${\footnotesize\Span\Set*{\sage{v1},\sage{v2},\sage{v3},\sage{v4}}}$?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
    \rref\sage{M}=\sage{M.rref()}
    \]
    \pause
    This means that the equation
    \[
    c_1\cdot\vv*{v}{1}
    +c_2\cdot\vv*{v}{2}
    +c_3\cdot\vv*{v}{3}
    +c_4\cdot\vv*{v}{4}
    =\vv{b}
    \]
    is unsolvable. \pause Thus
    $\vv{b}\notin\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(16, -64, 32), (5, -19, 10), (5, -20, 10)])
  b = matrix.column([-160, -48, -50])
  M = A.augment(b, subdivide=True)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{b}}$ in $\Span{\footnotesize\Set*{\sage{v1},
        \sage{v2}, \sage{v3}}}$?
  \end{example}
  
  \pause

  \begin{block}{Solution}
    Note that
    \[
    \rref\sage{M}=\sage{M.rref()}
    \]
    \pause
    This means that the equation
    \[
    c_1\cdot\vv*{v}{1}
    +c_2\cdot\vv*{v}{2}
    +c_3\cdot\vv*{v}{3}
    =\vv{b}
    \]
    has infinitely many solutions. \pause Thus
    $\vv{b}\in\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$.
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myAcols}{
    \begin{bmatrix}
      \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}
    \end{bmatrix}}
  \newcommand{\myAcolsb}{
    \begin{bmatrix}
      \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k} & \mid & \vv{b}
    \end{bmatrix}}
  \begin{theorem}
    A vector $\vv{b}$ is in $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$
    if and only if the system $\myAcolsb$ is consistent.
  \end{theorem}

  \pause

  {\footnotesize
    \begin{corollary}
      $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}=\mathbb{R}^n$ if and
      only if $\rank\myAcols=\#\myrows$
    \end{corollary}}
\end{frame}



\begin{sagesilent}
  A = matrix([(1, 3, -1, 1), (2, 6, 3, -3), (0, 0, -1, 1)])
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
  L = matrix([(3/5, 1/5, 0), (-2/5, 1/5, 0), (-2/5, 1/5, 1)])
  var('b1 b2 b3')
  b = matrix.column([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors
    \begin{align*}
      \vv*{v}{1} &= {\tiny\sage{v1}} &
      \vv*{v}{2} &= {\tiny\sage{v2}} &
      \vv*{v}{3} &= {\tiny\sage{v3}} &
      \vv*{v}{4} &= {\tiny\sage{v4}} 
    \end{align*}
    Describe all $\vv{b}={\tiny\sage{b}}$ in
    $\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$.
  \end{example}

  \pause

  \begin{block}{Solution}
    Row-reducing gives
    {\tiny
      \[
      \sage{M}\rightsquigarrow\sage{L*M}
      \]}\pause%
    The elements of
    $\Span{\scriptsize\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}}$ are
    the vectors parallel to the plane $-\frac{2}{5}\,x+\frac{1}{5}\,y+z=0$. \pause Note
    that
    $\mathbb{R}^3\neq\Span{\scriptsize\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}}$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(1,2),(1,1)])
  v1, v2 = [matrix.column(col) for col in A.columns()]
  L = A.inverse()
  b = matrix.column([b1, b2])
  M = A.change_ring(SR).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Does ${\scriptsize\Set*{\sage{v1},\sage{v2}}}$ span $\mathbb{R}^2$?
  \end{example}

  \pause

  \begin{block}{Solution}
    Since $\rref{\scriptsize\sage{A}}={\scriptsize\sage{A.rref()}}$,
    $\Span{\scriptsize\Set*{\sage{v1},\sage{v2}}}=\mathbb{R}^2$. \pause In fact,
    the row-reduction
    \[
    \sage{M}\rightsquigarrow\sage{L*M}
    \]
    \pause
    shows that every $\vv{b}=\langle b_1,b_2\rangle$ satisfies
    \[
    c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}=\vv{b}
    \]
    \pause
    where $c_1=\pause-b_1+2\,b_2$ and $c_2=\pause b_1-b_2$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ be a collection of
    vectors in $\mathbb{R}^n$. Then
    $V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ is a linear subspace
    of $\mathbb{R}^n$.
  \end{theorem}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv*{v}{1}=\langle1, 0\rangle$ and $\vv*{v}{2}=\langle1,1\rangle$. \pause The
    elements $\vv{b}$ of $\Span\Set{\vv*{v}{1},\vv*{v}{2}}$ are the elements of
    the form\pause
    \begin{align*}
      \vv{b}
      &= c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2} \\
      &= c_1\cdot\langle1,0\rangle+c_2\cdot\langle1,1\rangle \\
      &= \langle c_1+c_2,c_2\rangle
    \end{align*}\pause
    This shows that 
    \[
    \Span\Set{\vv*{v}{1},\vv*{v}{2}}
    = \Set{\vv{b}\in\mathbb{R}^2\given\vv{b}=\langle x+y, y\rangle}
    \]\pause
    In particular, this shows that
    \[
    V=\Set{\vv{b}\in\mathbb{R}^2\given\vv{b}=\langle x+y, y\rangle}
    \]
    is a linear subspace of $\mathbb{R}^2$.
  \end{example}
\end{frame}


\subsection{Column Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{column space} of $A$ is defined
    as 
    \[
    \Col(A)=\Span\Set*{\textnormal{columns of }A}
    \]
    Note that $\Col(A)$ is a linear subspace of $\mathbb{R}^m$.
  \end{definition}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 5)
  a1, a2, a3, a4, a5 = [matrix.column(col) for col in A.columns()]
  B = random_matrix(ZZ, 5, 3)
  b1, b2, b3 = [matrix.column(col) for col in B.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A={\tiny\sage{A}}$. \pause Then
    \[
    \Col(A)=\Span{\tiny\Set*{\sage{a1},\sage{a2},\sage{a3},\sage{a4},\sage{a5}}}
    \]
  \end{example}

  \pause

  \begin{example}
    Let $B={\tiny\sage{B}}$. \pause Then
    \[
    \Col(B)=\Span{\tiny\Set*{\sage{b1}, \sage{b2}, \sage{b3}}}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix([(1, 0, 5), (-3, 1, -11), (-5, -1, -29), (1, -3, -7)])
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
  b = matrix.column((1, -1, -6, -4))
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A={\tiny\sage{A}}$ and let $\vv{b}={\tiny\sage{b}}$. Is $\vv{b}$ in
    $\Col(A)$?
  \end{example}

  \pause

  \begin{block}{Solution}
    Note that
    \[
    \rref{\tiny\sage{M}}={\tiny\sage{M.rref()}}
    \]\pause
    Thus the equation
    \[
    c_1\cdot\vv*{v}{1}
    +c_2\cdot\vv*{v}{2}
    +c_3\cdot\vv*{v}{3}
    =\vv{b}
    \] 
    has no solution. \pause Hence $\vv{b}\notin\Col(A)$.
  \end{block}
\end{frame}


\end{document}
