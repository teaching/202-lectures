\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}


\title{The Theory of Endogenous and Exogenous Variables}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-2}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={3-}]
  % \end{columns}
\end{frame}


\section{Case Study on Dependency}
\subsection{A $4\times 5$ Example}

% A = matrix([(1, -4, -15, -15, -8, -8), (-4, 17, 63, 63, 34, 34), (3, -12, -45, -45, -23, -25), (-3, 13, 48, 48, 26, 26)])

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the system
    {\footnotesize
      \[
      \begin{array}{rcrcrcrcrcrcr}
        x_1 &-&  4\,x_2 &-& 15\,x_3 &-& 15\,x_4 &-&  8\,x_5 &=& -8 \\
        -4\,x_1 &+& 17\,x_2 &+& 63\,x_3 &+& 63\,x_4 &+& 34\,x_5 &=& 34 \\
        3\,x_1 &-& 12\,x_2 &-& 45\,x_3 &-& 45\,x_4 &-& 23\,x_5 &=& -25 \\
        -3\,x_1 &+& 13\,x_2 &+& 48\,x_3 &+& 48\,x_4 &+& 26\,x_5 &=& 26
      \end{array}
      \]}%
    \onslide<2->{What is the rank? }%
    \onslide<3->{Which variables are dependent? }%
    \onslide<4->{Which variables are free? }
    \onslide<5->{How do we describe the solutions?}
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution}
    The reduced row-echelon form of the system is
    \[
    \rref{\tiny
      \left[\begin{array}{rrrrr|r}
          1 \tikzmark{o1}&  -4 \tikzmark{o2}&  -15 \tikzmark{o3}&  -15 \tikzmark{o4}&  -8 \tikzmark{o5}&  -8 \\
          -4 & 17 & 63 & 63 & 34 & 34 \\
          3 & -12 & -45 & -45 & -23 & -25 \\
          -3 & 13 & 48 & 48 & 26 & 26
        \end{array}\right]}
    =
    \left[\begin{array}{rrrrr|r}
        1\tikzmark{x1} & 0\tikzmark{x2} & -3\tikzmark{x3} & -3\tikzmark{x4} & 0\tikzmark{x5} & 0 \\
        0 & 1 & 3 & 3 & 0 & 4 \\
        0 & 0 & 0 & 0 & 1 & -1 \\
        0 & 0 & 0 & 0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {{\tiny$x_1$}};
      \node at (o2.north west) {{\tiny$x_2$}};
      \node at (o3.north west) {{\tiny$x_3$}};
      \node at (o4.north west) {{\tiny$x_4$}};
      \node at (o5.north west) {{\tiny$x_5$}};

      \node at (x1.north west) {$x_1$};
      \node at (x2.north west) {$x_2$};
      \node at (x3.north west) {$x_3$};
      \node at (x4.north west) {$x_4$};
      \node at (x5.north west) {$x_5$};
    \end{tikzpicture}
    \]%
    \onslide<2->{The rank of the system is $r=3$. }%
    \onslide<3->{The dependent variables are $\Set{x_1,x_2,x_5}$ }%
    \onslide<4->{and the free variables are $\Set{x_3,x_4}$. }%
    \onslide<5->{The solutions to the system are described by}
    \[
    \onslide<6->{
    \left[\begin{array}{c}
        x_1\\ x_2\\ x_3\\ x_4\\ x_5
      \end{array}\right]
    =}%
  \onslide<7->{
    \left[\begin{array}{c}
        3\,x_3+3\,x_4\\
        4-3\,x_3-3\,x_4\\
        x_3\\
        x_4\\
        -1
      \end{array}\right]
    =}%
  \onslide<8->{
    \left[\begin{array}{r}
        0\\ 4\\ 0\\ 0\\ -1
      \end{array}\right]
    +}%
  \onslide<9->{
    x_3\left[\begin{array}{r}
        3\\-3\\1\\0\\0
      \end{array}\right]
    +}%
  \onslide<10->{
    x_4\left[\begin{array}{r}
        3\\-3\\0\\1\\0
      \end{array}\right]}
    \]
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    In this system, is it possible to view $\Set{x_2,x_4,x_5}$ as the dependent
    variables and $\Set{x_1,x_3}$ as the free variables?
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer}
    Rearrange the columns and row reduce
    \[
    \rref {\scriptsize \left[\begin{array}{rrrrr|r}
          -4 \tikzmark{o2}& -15\tikzmark{o4} & -8\tikzmark{o5} & 1\tikzmark{o1} & -15\tikzmark{o3} & -8 \\
          17 & 63 & 34 & -4 & 63 & 34 \\
          -12 & -45 & -23 & 3 & -45 & -25 \\
          13 & 48 & 26 & -3 & 48 & 26
        \end{array}\right]}%
    = \left[\begin{array}{rrrrr|r}
        1\tikzmark{x2} & 0\tikzmark{x4} & 0\tikzmark{x5} & 1\tikzmark{x1} & 0\tikzmark{x3} & 4 \\
        0 & 1 & 0 & -\nicefrac{1}{3} & 1 & 0 \\
        0 & 0 & 1 & 0 & 0 & -1 \\
        0 & 0 & 0 & 0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {{\scriptsize$x_1$}};% 
      \node at (o2.north west) {{\scriptsize$x_2$}};% 
      \node at (o3.north west) {{\scriptsize$x_3$}};% 
      \node at (o4.north west) {{\scriptsize$x_4$}};% 
      \node at (o5.north west) {{\scriptsize$x_5$}};%

      \node at (x1.north west) {$x_1$};% 
      \node at (x2.north west) {$x_2$};% 
      \node at (x3.north west) {$x_3$};% 
      \node at (x4.north west) {$x_4$};% 
      \node at (x5.north west) {$x_5$};%
    \end{tikzpicture}
    \]
    \onslide<2->{Now, the dependent variables are $\Set{x_2,x_4,x_5}$ }%
    \onslide<3->{and the free variables are $\Set{x_1,x_3}$. }%
    \onslide<4->{The solutions are given by}
    \[
    \onslide<5->{
    \left[\begin{array}{c}
        x_1\\ x_2\\ x_3\\ x_4\\ x_5
      \end{array}\right]
    =}%
  \onslide<6->{
    \left[\begin{array}{c}
        x_1\\
        4-x_1\\
        x_3 \\
        (\nicefrac{1}{3})\,x_1-x_3 \\
        -1
      \end{array}\right]
    =}%
  \onslide<7->{
    \left[\begin{array}{r}
        0\\ 4\\ 0\\ 0\\ -1
      \end{array}\right]
    +}%
  \onslide<8->{
    x_1\left[\begin{array}{r}
        1\\-1\\0\\\nicefrac{1}{3}\\0
      \end{array}\right]
    +}%
    \onslide<9->{
    x_3\left[\begin{array}{r}
        0\\0\\1\\-1\\0
      \end{array}\right]}%
    \]
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The ordering of the variable columns in a system influences whether or not a
    given collection of variables is dependent.
  \end{block}
  
\end{frame}


\section{Endogenous and Exogenous Variables}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Setup}
    Suppose the system
    \begin{equation}\label{eq:setup}
      \begin{array}{ccccccccc}
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}\tag{$\ast$}%
    \end{equation}%
    is consistent with rank $r$. 
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{endogenous collection} of \eqref{eq:setup} is a collection of $r$
    of the variables $x_i$ such that the submatrix formed by the columns
    corresponding to these variables has rank $r$.

    \pause%

    In this situation, the other variables form an \emph{exogenous collection}.
  \end{definition}
\end{frame}


\subsection{A $4\times 6$ Example}

% A = matrix([[1,0,0,-1,-1,2],[0,1,0,-2,0,0],[0,0,1,1,-1,2],[0,0,0,0,0,0]])
% b = vector([1,-3,-2,0])
% M = A.augment(b, subdivide=True)
% x1, x2, x3, x4, x5, x6, b = M.columns()

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the system
    \[
    \left[\begin{array}{rrrrrr|r}
        1\tikzmark{x1} & 0\tikzmark{x2} & 0\tikzmark{x3} & -1\tikzmark{x4} & -1\tikzmark{x5} & 2\tikzmark{x6} & 1 \\
        0 & 1 & 0 & -2 & 0 & 0 & -3 \\
        0 & 0 & 1 & 1 & -1 & 2 & -2 \\
        0 & 0 & 0 & 0 & 0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north west) {$x_1$};%
      \node at (x2.north west) {$x_2$};%
      \node at (x3.north west) {$x_3$};%
      \node at (x4.north west) {$x_4$};%
      \node at (x5.north west) {$x_5$};%
      \node at (x6.north west) {$x_6$};%
    \end{tikzpicture}
    \]%
    \pause
    This system is consistent with rank $r=3$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    Is $\Set{x_1,x_2,x_3}$ endogenous?
  \end{block}
  \pause
  \begin{block}{Answer}
    Note that
    \[
    \rref
    \left[\begin{array}{rrr}
        1\tikzmark{o1} & 0\tikzmark{o2} & 0\tikzmark{o3} \\
        0 & 1 & 0 \\
        0 & 0 & 1 \\
        0 & 0 & 0
      \end{array}\right]
    =
    \left[\begin{array}{rrr}
        1\tikzmark{x1} & 0\tikzmark{x2} & 0\tikzmark{x3} \\
        0 & 1 & 0 \\
        0 & 0 & 1 \\
        0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x_1$};%
      \node at (o2.north west) {$x_2$};%
      \node at (o3.north west) {$x_3$};%

      \node at (x1.north west) {$x_1$};%
      \node at (x2.north west) {$x_2$};%
      \node at (x3.north west) {$x_3$};%
    \end{tikzpicture}
    \]%
    \pause% 
    Since $\textnormal{rank}=3=r$, the collection $\Set{x_1,x_2,x_3}$ is
    endogenous. 
    \pause%
    In this case the corresponding exogenous collection is
    $\Set{x_4,x_5,x_6}$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 2}
    Is $\Set{x_2,x_5,x_6}$ endogenous?
  \end{block}
  \pause%
  \begin{block}{Answer}
    Note that
    \[
    \rref
    \left[\begin{array}{rrr}
        0\tikzmark{o2} & -1\tikzmark{o5} & 2\tikzmark{o6} \\
        1 & 0 & 0 \\
        0 & -1 & 2 \\
        0 & 0 & 0
      \end{array}\right]
    =
    \left[\begin{array}{rrr}
        1\tikzmark{x2} & 0\tikzmark{x5} & 0\tikzmark{x6} \\
        0 & 1 & -2 \\
        0 & 0 & 0 \\
        0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o2.north west) {$x_2$};%
      \node at (o5.north west) {$x_5$};%
      \node at (o6.north west) {$x_6$};%

      \node at (x2.north west) {$x_2$};%
      \node at (x5.north west) {$x_5$};%
      \node at (x6.north west) {$x_6$};%
    \end{tikzpicture}
    \]
    \pause%
    Since $\textnormal{rank}=2<3=r$, the collection $\Set{x_2,x_5,x_6}$ is
    \emph{not endogenous}.  
    \pause%
    Consequently, the collection $\Set{x_1,x_3,x_4}$ is \emph{not exogenous}.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 3}
    Is $\Set{x_1,x_4,x_6}$ endogenous?
  \end{block}
  \pause
  \begin{block}{Answer}
    Note that
    \[
    \rref
    \left[\begin{array}{rrr}
        1\tikzmark{o1} & -1\tikzmark{o4} & 2\tikzmark{o6} \\
        0 & -2 & 0 \\
        0 & 1 & 2 \\
        0 & 0 & 0
      \end{array}\right]
    =
    \left[\begin{array}{rrr}
        1\tikzmark{x1} & 0\tikzmark{x4} & 0\tikzmark{x6} \\
        0 & 1 & 0 \\
        0 & 0 & 1 \\
        0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x_1$};%
      \node at (o4.north west) {$x_4$};%
      \node at (o6.north west) {$x_6$};%

      \node at (x1.north west) {$x_1$};%
      \node at (x4.north west) {$x_4$};%
      \node at (x6.north west) {$x_6$};%
    \end{tikzpicture}
    \]
    \pause
    Since $\textnormal{rank}=3=r$, the collection $\Set{x_1,x_4,x_6}$ is
    endogenous. 
    \pause
    Consequently, the collection $\Set{x_2,x_3,x_5}$ is exogenous.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 4}
    Is $\Set{x_1,x_2}$ endogenous?
  \end{block}
  \pause
  \begin{block}{Answer}
    No. An endogenous collection must consist of exactly $r=3\neq 2$
    variables. Consequently, $\Set{x_3,x_4,x_5,x_6}$ is not exogenous.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 5}
    Is $\Set{x_1,x_2,x_3,x_4}$ endogenous?
  \end{block}
  \pause
  \begin{block}{Answer}
    No. An endogenous collection must consist of exactly $r=3\neq 4$
    variables. Consequently, $\Set{x_5,x_6}$ is not exogenous.
  \end{block}
\end{frame}


\subsection{The Linear Implicit Function Theorem}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why do we care about endogenous and exogenous variables?
  \end{block}

  \pause

  \begin{theorem}[Linear Implicit Function Theorem]
    A collection of variables can be viewed as dependent variables if and only
    if the collection is endogenous. 
  \end{theorem}

  \pause

  \begin{block}{Warning}
    Simon and Blume require that
    \[
    \textnormal{rank}=\#\textnormal{equations}
    \]
    but this is unnecessary!
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Endogenous variables can always be written in terms of their corresponding
    exogenous variables.
    \begin{enumerate}
    \item<2-> replace the system with its reduced row-echelon form
    \item<3-> reorder the columns of the system so that the endogenous variables are
      the first $r$ columns
    \item<4-> row-reduce to solve for the endogenous variables in terms of the
      corresponding exogenous variables
    \end{enumerate}
  \end{block}
\end{frame}


\section{Examples}
\subsection{A $2\times 5$ Example}

% A = matrix([(-3, -12, -13, -26, -7, 23), (-2, -8, -9, -18, -5, 16)])

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    One computes
    \[
    \rref
    \left[\begin{array}{rrrrr|r}
        -3\tikzmark{o1} & -12\tikzmark{o2} & -13\tikzmark{o3} & -26\tikzmark{o4} & -7\tikzmark{o5} & 23 \\
        -2 & -8 & -9 & -18 & -5 & 16
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$v$};%
      \node at (o2.north west) {$w$};%
      \node at (o3.north west) {$x$};%
      \node at (o4.north west) {$y$};%
      \node at (o5.north west) {$z$};%
    \end{tikzpicture}
    =
    \left[\begin{array}{rrrrr|r}
        1\tikzmark{x1} & 4\tikzmark{x2} & 0\tikzmark{x3} & 0\tikzmark{x4} & -2\tikzmark{x5} & 1 \\
        0 & 0 & 1 & 2 & 1 & -2
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north west) {$v$};%
      \node at (x2.north west) {$w$};%
      \node at (x3.north west) {$x$};%
      \node at (x4.north west) {$y$};%
      \node at (x5.north west) {$z$};%
    \end{tikzpicture}
    \]
    \pause
    The system is consistent and the rank is $r=2$.
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Is $\Set{x, z}$ endogenous? If so, write $\Set{x, z}$ in terms of their
    corresponding exogenous variables.
  \end{block}
  \pause
  \begin{block}{Answer}
    Note that 
    \[
    \rref
    \left[\begin{array}{rr}
        0\tikzmark{o1} & -2\tikzmark{o2} \\
        1 & 1
      \end{array}\right]
    =
    \left[\begin{array}{rr}
        1\tikzmark{x1} & 0\tikzmark{x2} \\
        0 & 1
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x$};
      \node at (o2.north west) {$z$};

      \node at (x1.north west) {$x$};
      \node at (x2.north west) {$z$};
    \end{tikzpicture}
    \]
    \pause
    Since $\textnormal{rank}=2=r$, $\Set{x, z}$ is endogenous. 
    \pause
    The corresponding exogenous variables are $\Set{v, w, y}$.
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer (continued)}
    To solve for $\Set{x, z}$ in terms of $\Set{v, w, y}$, note that
    \[
    \rref\left[\begin{array}{rrrrr|r}
        0\tikzmark{o1} & -2\tikzmark{o2} & 1\tikzmark{o3} & 4\tikzmark{o4} & 0\tikzmark{o5} & 1 \\
        1 & 1 & 0 & 0 & 2 & -2
      \end{array}\right]
    =
    \left[\begin{array}{rrrrr|r}
        1\tikzmark{x1} & 0\tikzmark{x2} & \nicefrac{1}{2}\tikzmark{x3} & 2\tikzmark{x4} & 2\tikzmark{x5} & -\nicefrac{3}{2} \\
        0 & 1 & -\nicefrac{1}{2} & -2 & 0 & -\nicefrac{1}{2}
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x$};
      \node at (o2.north west) {$z$};
      \node at (o3.north west) {$v$};
      \node at (o4.north west) {$w$};
      \node at (o5.north west) {$y$};

      \node at (x1.north west) {$x$};
      \node at (x2.north west) {$z$};
      \node at (x3.north west) {$v$};
      \node at (x4.north west) {$w$};
      \node at (x5.north west) {$y$};
    \end{tikzpicture}
    \]
    \pause
    The solutions to the system are given by
\[
\left[\begin{array}{c}
    v\\ w\\ x\\ y\\ z
  \end{array}\right]
=\pause
\left[\begin{array}{c}
    v\\ w\\ -\nicefrac{3}{2}-(\nicefrac{1}{2})\,v-2\,w-2\,y \\ y\\ -\nicefrac{1}{2}+(\nicefrac{1}{2})\,v+2\,w
  \end{array}\right]
\]
  \end{block}
\end{frame}

\subsection{A $4\times 6$ Example}

% A = matrix([(9, 25, -57, -59, -9, -116, 143), (-4, -11, 25, 26, 4, 51, -63), (-2, -10, 26, 23, 2, 50, -57), (0, 3, -9, -1, 0, -5, 0)])

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The reduced row-echelon form of 
    \[
    \left[\begin{array}{rrrrrr|r}
        9\tikzmark{o1} & 25\tikzmark{o2} & -57\tikzmark{o3} & -59\tikzmark{o4} & -9\tikzmark{o5} & -116\tikzmark{o6} & 143 \\
        -4 & -11 & 25 & 26 & 4 & 51 & -63 \\
        -2 & -10 & 26 & 23 & 2 & 50 & -57 \\
        0 & 3 & -9 & -1 & 0 & -5 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x_1$};
      \node at (o2.north west) {$x_2$};
      \node at (o3.north west) {$x_3$};
      \node at (o4.north west) {$x_4$};
      \node at (o5.north west) {$x_5$};
      \node at (o6.north west) {$x_6$};
    \end{tikzpicture}
    \]
    is
    \[
    \left[\begin{array}{rrrrrr|r}
        1\tikzmark{x1} & 0\tikzmark{x2} & 2\tikzmark{x3} & 0\tikzmark{x4} & -1\tikzmark{x5} & 3\tikzmark{x6} & -1 \\
        0 & 1 & -3 & 0 & 0 & -1 & -1 \\
        0 & 0 & 0 & 1 & 0 & 2 & -3 \\
        0 & 0 & 0 & 0 & 0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north west) {$x_1$};
      \node at (x2.north west) {$x_2$};
      \node at (x3.north west) {$x_3$};
      \node at (x4.north west) {$x_4$};
      \node at (x5.north west) {$x_5$};
      \node at (x6.north west) {$x_6$};
    \end{tikzpicture}
    \]
    \pause
    The system is consistent with rank $r=3$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    Is $\Set{x_1,x_2,x_3}$ endogenous? If so, write $\Set{x_1,x_2,x_3}$ in terms
    of their corresponding exogenous variables.
  \end{block}
  \pause
  \begin{block}{Answer}
    Note that
    \[
    \rref\left[\begin{array}{rrr}
        1\tikzmark{o1} & 0\tikzmark{o2} & 2\tikzmark{o3} \\
        0 & 1 & -3 \\
        0 & 0 & 0 \\
        0 & 0 & 0
      \end{array}\right]
    =\left[\begin{array}{rrr}
        1\tikzmark{x1} & 0\tikzmark{x2} & 2\tikzmark{x3} \\
        0 & 1 & -3 \\
        0 & 0 & 0 \\
        0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {$x_1$};
      \node at (o2.north west) {$x_2$};
      \node at (o3.north west) {$x_3$};

      \node at (x1.north west) {$x_1$};
      \node at (x2.north west) {$x_2$};
      \node at (x3.north west) {$x_3$};
    \end{tikzpicture}
    \]
    \pause
    Since $\textnormal{rank}=2<3=r$, $\Set{x_1,x_2,x_3}$ is not endogenous.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 2}
    Is $\Set{x_3,x_5,x_6}$ endogenous? If so, write $\Set{x_3,x_5,x_6}$ in terms
    of their corresponding exogenous variables.
  \end{block}
  \pause
  \begin{block}{Answer}
    Note that
    \[
    \rref\left[\begin{array}{rrr}
        2\tikzmark{o3} & -1\tikzmark{o5} & 3\tikzmark{o6} \\
        -3 & 0 & -1 \\
        0 & 0 & 2 \\
        0 & 0 & 0
      \end{array}\right]
    =
    \left[\begin{array}{rrr}
        1\tikzmark{x3} & 0\tikzmark{x5} & 0\tikzmark{x6} \\
        0 & 1 & 0 \\
        0 & 0 & 1 \\
        0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o3.north west) {$x_3$};
      \node at (o5.north west) {$x_5$};
      \node at (o6.north west) {$x_6$};

      \node at (x3.north west) {$x_3$};
      \node at (x5.north west) {$x_5$};
      \node at (x6.north west) {$x_6$};
    \end{tikzpicture}
    \]
    \pause
    Since $\textnormal{rank}=3=r$, $\Set{x_3,x_5,x_6}$ is endogenous. 
    \pause
    The corresponding exogenous variables are $\Set{x_1,x_2,x_4}$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer (continued)}
    To solve for $\Set{x_3,x_5,x_6}$ in terms of $\Set{x_1,x_2,x_4}$, consider
    \[
    {\scriptsize
      \rref
      \left[\begin{array}{rrrrrr|r}
          2\tikzmark{o3} & -1\tikzmark{o5} & 3\tikzmark{o6} & 1\tikzmark{o1} & 0\tikzmark{o2} & 0\tikzmark{o4} & -1 \\
          -3 & 0 & -1 & 0 & 1 & 0 & -1 \\
          0 & 0 & 2 & 0 & 0 & 1 & -3 \\
          0 & 0 & 0 & 0 & 0 & 0 & 0
        \end{array}\right]
      =
      \left[\begin{array}{rrrrrr|r}
          1\tikzmark{x3} & 0\tikzmark{x5} & 0\tikzmark{x6} & 0\tikzmark{x1} & -\nicefrac{1}{3}\tikzmark{x2} & -\nicefrac{1}{6}\tikzmark{x4} & \nicefrac{5}{6} \\
          0 & 1 & 0 & -1 & -\nicefrac{2}{3} & \nicefrac{7}{6} & -\nicefrac{11}{6} \\
          0 & 0 & 1 & 0 & 0 & \nicefrac{1}{2} & -\nicefrac{3}{2} \\
          0 & 0 & 0 & 0 & 0 & 0 & 0
        \end{array}\right]}
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (o1.north west) {{\scriptsize$x_1$}};
      \node at (o2.north west) {{\scriptsize$x_2$}};
      \node at (o3.north west) {{\scriptsize$x_3$}};
      \node at (o4.north west) {{\scriptsize$x_4$}};
      \node at (o5.north west) {{\scriptsize$x_5$}};
      \node at (o6.north west) {{\scriptsize$x_6$}};

      \node at (x1.north west) {{\scriptsize$x_1$}};
      \node at (x2.north west) {{\scriptsize$x_2$}};
      \node at (x3.north west) {{\scriptsize$x_3$}};
      \node at (x4.north west) {{\scriptsize$x_4$}};
      \node at (x5.north west) {{\scriptsize$x_5$}};
      \node at (x6.north west) {{\scriptsize$x_6$}};
    \end{tikzpicture}
    \]
    \pause
    The solutions to the system can be described by
    \[
    \left[
      \begin{array}{c}
        x_1\\ x_2\\ x_3\\ x_4\\ x_5\\ x_6
      \end{array}\right]
    =\pause
    \left[
      \begin{array}{c}
        x_1\\ x_2\\ \nicefrac{5}{6}+(\nicefrac{1}{3})\,x_2+(\nicefrac{1}{6})\,x_4\\ x_4\\ -\nicefrac{11}{6}+x_1+(\nicefrac{2}{3})\,x_2-(\nicefrac{7}{6})\,x_4\\ -\nicefrac{3}{2}-(\nicefrac{1}{2})\,x_4
      \end{array}\right]
    \]
  \end{block}
\end{frame}


\end{document}
