from sage.all import random_matrix, ZZ


def rrefrand(nrows, ncols, pivotcols, output=None):
    r = len(pivotcols)
    A = random_matrix(ZZ, nrows, ncols, algorithm='echelonizable', rank=r)
    while A.pivots() != tuple(pivotcols):
            A = random_matrix(
                ZZ, nrows, ncols, algorithm='echelonizable', rank=r)
    outdic = {'rref': A.rref(), 'original': A}
    if output is None:
        output = 'rref'
    return outdic[output]
