\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}




\title{Systems of Linear Equations and Gau{\ss}-Jordan Elimination}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-2}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={3-}]
  % \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={3-}]  
\end{frame}


\section{Motivation and Goals}
\subsection{Geometric Motivation}

\begin{sagesilent}
  n1 = vector([-34, 10, 2])
  n2 = vector([3, -19, -17])
  n3 = vector([10, 2, -9])
  P1 = (-1, 0, 1)
  P2 = (-10, 1, -2)
  P3 = (-4, -2, -1)
  vP1 = vector(P1)
  vP2 = vector(P2)
  vP3 = vector(P3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Let $\mathcal{P}_1, \mathcal{P}_2, \mathcal{P}_3$ be the planes with
    point-normal forms described by
    \begin{align*}
      \vv*{n}{1} &= \sage{n1} & \vv*{n}{2} &= \sage{n2} & \vv*{n}{3} &= \sage{n3} \\
      P_1 &= \sage{P1} & P_2 &= \sage{P2} & P_2 &= \sage{P2}
    \end{align*}
    What is the intersetion $\mathcal{P}_1\cap\mathcal{P}_2\cap\mathcal{P}_3$?
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer}
    To describe $\mathcal{P}_1\cap\mathcal{P}_2\cap\mathcal{P}_3$, we must
    simultaneously solve
    \begin{align*}
      \vv*{n}{1}\cdot\vv{P_1X} &= 0 &
      \vv*{n}{2}\cdot\vv{P_2X} &= 0 &
      \vv*{n}{3}\cdot\vv{P_3X} &= 0
    \end{align*}%
    \pause%
    In coordinates, this gives the system of equations
    \[
    \begin{array}{rcrcrcr}
      -34\,x & + & 10\,y & + &  2\,z & = & \sage{n1.dot_product(vP1)} \\
      3\,x & - & 19\,y & - & 17\,z & = & \sage{n2.dot_product(vP2)} \\
      10\,x & + &  2\,y & - &  9\,z & = & \sage{n3.dot_product(vP3)}
    \end{array}
    \]%
    \pause%
    Solving this system is\textellipsis\pause annoying!
  \end{block}
\end{frame}


\subsection{Algebraic Motivation}

\begin{sagesilent}
  v1 = vector([1, 2, -1, 3])
  v2 = vector([-1, -6, 3, 19])
  v3 = vector([-3, 15, 2, 0])
  b = vector([-4, 2, -3, -1])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Let
    $\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}\in\mathbb{R}^4$
    be given by
    \begin{align*}
      \vv*{v}{1} &= \sage{matrix.column(v1)} &
      \vv*{v}{2} &= \sage{matrix.column(v2)} &
      \vv*{v}{3} &= \sage{matrix.column(v3)}
    \end{align*}%
    Is $\vv{b}=\sage{matrix.column(b)}$ a linear combination of
    $\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}$?
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer}
    We wish to determine if the equation
    \[
    c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+c_3\cdot\vv*{v}{3}=\vv{b}
    \]
    is solvable. \pause%
    In coordinates, this gives the system of equations
    \[
    \begin{array}{rcrcrcr}
      c_1    &-&     c_2 &-&  3\,c_3 &=& -4   \\
      2\,c_1 &-&  6\,c_2 &+& 15\,c_3 &=& 2  \\
      -c_1   &+&  3\,c_2 &+&  2\,c_3 &=& -3   \\
      3\,c_1 &+& 19\,c_2 & &         &=& -1
    \end{array}
    \]%
    \pause%
    Solving this system is\textellipsis\pause annoying!
  \end{block}
\end{frame}



\subsection{Goal}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Set-Up}
    Consider the system of $m$ equations with $n$ variables.
    \begin{equation}\label{eq:goal}
      % \setlength\belowdisplayskip{0pt}
      \begin{array}{ccccccccc}\pause
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\ \pause
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\ \pause
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\ \pause
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}\tag{$\ast$}
    \end{equation}
    \pause
    A \emph{solution} to \eqref{eq:goal} is a point $X=(x_1,x_2,\dotsc,x_n)$ in
    $\mathbb{R}^n$ satisfying each equation in \eqref{eq:goal}.
  \end{block}
  \begin{columns}[onlytextwidth, T]
    \column{.5\textwidth}
    \pause
    \begin{block}{Question 1}
      Does a solution to \eqref{eq:goal} exist?
    \end{block}
    \pause
    \begin{block}{Question 2}
      How \emph{many} solutions exist?
    \end{block}
    \column{.5\textwidth}
    \pause
    \begin{block}{Question 3}
      Can we describe \emph{all} solutions?
    \end{block}
  \end{columns}

\end{frame}



\subsection{Examples}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The system of two equations and three variables
    \[
    \begin{array}{rcrcrcr}
      2\,x & + & y & + &    z & = 2 \\
         x &   &   & - & 2\,z & = 3
    \end{array}
    \]
    is solved by $X=(5, -4, 1)$ and by $X=(1, 2, -1)$. \pause The system has
    \emph{more than one solution}.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The system of three equations and two variables
    \begin{alignat}{6}
      x &\ & + &\ & 5\,y &\ &=& &\  13 \label{eq:noa}\\
      -2\,x &\ & - &\ & 9\,y &\ &=& &\ -24 \label{eq:nob}\\
      -x &\ & - &\ & 8\,y &\ &=& &\   7 \label{eq:noc}
    \end{alignat}
    has \emph{no solutions} because
    \[
    7\cdot\eqref{eq:noa}+3\cdot\eqref{eq:nob}+\eqref{eq:noc}
    =
    \begin{cases}
      \hspace{\fill} \onslide<2->{0} & \textnormal{LHS} \\
      \onslide<3->{26} & \textnormal{RHS}
    \end{cases}
    \]
  \end{example}
\end{frame}


\subsection{Substitution is a Bad Idea}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \setcounter{equation}{0}
  \begin{example}
    Find all solutions to the system
    \begin{alignat}{6}
      x &\ & + &\ & 2\,y &\ &=& &\  3 \label{eq:suba}\\
      3\,x &\ & + &\ & 4\,y &\ &=& &\  0 \label{eq:subb}
    \end{alignat}
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Bad Idea}
    Start deriving new equations. \pause Equation \eqref{eq:subb} is
    \begin{equation}
      \label{eq:subc}
      y=-\frac{3}{4}\,x
    \end{equation}\pause
    Plug \eqref{eq:subc} into \eqref{eq:suba} to obtain
    \begin{equation}
      \label{eq:subd}
      x+2\left(-\frac{3}{4}\,x\right)=3\implies x=-6
    \end{equation}\pause
    Plug \eqref{eq:subd} into \eqref{eq:subc} to obtain
    \begin{equation}
      \label{eq:sube}
      y=\left(-\frac{3}{4}\right)(-6)=\frac{9}{2}
    \end{equation}\pause
    Equations \eqref{eq:subd} and \eqref{eq:sube} then imply that
    $X=(-6,\nicefrac{9}{2})$ solves the system.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why is this a bad idea?
  \end{block}\pause
  \begin{block}{Answer 1}
    Equations get messy in high dimensions.
  \end{block}\pause
  \begin{block}{Answer 2}
    The procedure is not algorithmic.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Two systems are \emph{equivalent} if they have the same solutions.
  \end{definition}\pause
  \begin{block}{Good Idea}
    Replace ``hard'' systems with equivalent ``easy'' systems.
  \end{block}
\end{frame}


\section{Augmented Matrices and Reduced Row-Echelon Form}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider the system of equations
    \begin{equation}
      \begin{array}{ccccccccc}
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}\tag{$\ast$}\label{eq:augdef}
    \end{equation}
    \pause
    The \emph{augmented matrix} of \eqref{eq:augdef} is
    \[
    \left[
      \begin{array}{cccc|c}
        a_{11} & a_{12}  & \dotsb & a_{1n}                  & b_{1} \\ %&\tikzmark{upper1} a_{} & a_{} & a_{}\tikzmark{upper2} \\
        a_{21} & a_{22}  & \dotsb & a_{2n}                  & b_{2} \\ %& a_{}                  & a_{} & a_{}                  \\
        \vdots & \vdots & \ddots & \vdots                 & \vdots \\
        \tikzmark{lower1}a_{m1} & a_{m2}  & \dotsb & a_{mn}\tikzmark{lower2} & b_{m}\tikzmark{bcol}    %& a_{}                  & a_{} & a_{}                  \\
      \end{array}
    \right]
    \]
    % code from https://tex.stackexchange.com/questions/102460/underbraces-in-matrix-divided-in-blocks
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \pause
      \draw[decorate, thick] (lower2.south) -- (lower1.south)
      node [midway,below=5pt] {{\tiny``coefficient matrix''}};%
      \pause
      \node at (bcol.south) [below=-4pt] {{\footnotesize$\underset{\textnormal{``augmented column''}}{\uparrow}$}};
    \end{tikzpicture}
  \end{definition}
\end{frame}


\begin{sagesilent}
  A = matrix([[3,4,-8],[0,1,10],[9,7,-2]])
  b = vector([0,6,-5])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    There is a natural correspondence between systems of linear equations and
    augmented matrices.
    \begin{align*}
      \begin{array}{rcrcrcr}
        3\,x & + & 4\,y & - & 8\,z  & = & 0 \\
        &   &    y & + & 10\,z & = & 6 \\
        9\,x & + & 7\,y & - & 2\,z  & = & -5
      \end{array} && \longleftrightarrow &&
      \sage{M}
    \end{align*}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix is in \emph{reduced row-echelon form} (rref) if
    \begin{enumerate}
    \item<2-> any zero-rows occur at the bottom
    \item<3-> the first nonzero entry of a nonzero row is $1$ (called a
      \emph{pivot})
    \item<4-> every pivot occurs to the right of any previous pivots
    \item<5-> all nonpivot entries in a column containing a pivot are zero
    \end{enumerate}
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \matrix [
      inner sep=0pt, %<- set inner sep for all nodes 
      column sep=.33333em,
      nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
      matrix of math nodes,
      left delimiter=\lbrack,
      right delimiter=\rbrack      
      ] (m) {
        1 \pgfmatrixnextcell 2 \pgfmatrixnextcell 0 \pgfmatrixnextcell -5 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell -1 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 3 \pgfmatrixnextcell 0 \pgfmatrixnextcell -1 \pgfmatrixnextcell 1 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell -1 \pgfmatrixnextcell -2 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
      };% 
      \onslide<4->{\boxpivot{1-1}}
      \onslide<6->{\boxpivot{2-3}}
      \onslide<8->{\boxpivot{3-5}}
    \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Yes. }% 
    \onslide<3->{The pivots are in positions $(1,1)$, }%
    \onslide<5->{$(2,3)$, }%
    \onslide<7->{and $(3, 5)$.}
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \matrix [
      inner sep=0pt, %<- set inner sep for all nodes 
      column sep=.33333em,
      nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
      matrix of math nodes,
      left delimiter=\lbrack,
      right delimiter=\rbrack      
      ] (m) {
        1 \pgfmatrixnextcell 2 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 3 \\
        0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 3 \pgfmatrixnextcell 1 \pgfmatrixnextcell 1 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 2 \\
      };% 
      \onslide<4->{\boxpivot{1-1}}
      \onslide<6->{\boxpivot{2-2}}
      \onslide<8->{\boxpivot{3-4}}
      \onslide<10->{\draw[ultra thick, color=red] (m-1-2.north west) -- (m-3-2.south west) -- (m-3-2.south east) -- (m-1-2.north east) -- cycle;}
      \onslide<11->{\draw[ultra thick, color=red] (m-1-4.north west) -- (m-3-4.south west) -- (m-3-4.south east) -- (m-1-4.north east) -- cycle;}
    \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{No. }%
    \onslide<3->{There are pivots in positions $(1, 1)$, }%
    \onslide<5->{$(2, 2)$, }%
    \onslide<7->{and $(3, 4)$. }%
    \onslide<9->{However, the pivots in columns two and four are not the only
      nonzero entries in these columns.}%
  \end{block}
\end{frame}




\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \matrix [
      inner sep=0pt, %<- set inner sep for all nodes 
      column sep=.33333em,
      nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
      matrix of math nodes,
      left delimiter=\lbrack,
      right delimiter=\rbrack      
      ] (m) {
        0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell -2 \\
        1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 1 \pgfmatrixnextcell  1 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell  0 \\
      };% 
      \onslide<4->{\boxpivot{1-2}}
      \onslide<6->{\boxpivot{2-1}}
      \onslide<8->{\draw[ultra thick, color=red] 
        (m-2-1.north west) 
        -- (m-1-2.north west) 
        -- (m-1-2.north east) 
        -- (m-1-2.south east) 
        -- (m-2-1.south east)
        -- (m-2-1.south west)
        -- cycle;}
    \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{No. }%
    \onslide<3->{There are pivots in positions $(1, 2)$ }%
    \onslide<5->{and $(2, 1)$. }%
    \onslide<7->{However, the pivot in column one occurs to the left of the
      pivot in column two.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{example}
    Is the matrix
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \matrix [
      inner sep=0pt, %<- set inner sep for all nodes 
      column sep=.33333em,
      nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
      matrix of math nodes,
      left delimiter=\lbrack,
      right delimiter=\rbrack      
      ] (m) {
        0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 6 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell -4 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 2 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \\
        0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
      };% 
      \onslide<4->{\boxpivot{1-2}}
      \onslide<6->{\boxpivot{2-4}}
      \onslide<8->{\boxpivot{3-5}}
    \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Yes. }%
    \onslide<3->{The pivots are in positions $(1,2)$, }%
    \onslide<5->{$(2,4)$, }%
    \onslide<7->{and $(3,5)$.}
  \end{block}
\end{frame}


\subsection{Consistency and Variable Dependency}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A rref system is
    \begin{description}\pause
    \item[consistent] if the augmented column contains no pivot\pause
    \item[inconsistent] if the augmented column contains a pivot
    \end{description}
  \end{definition}

  \pause
  \begin{definition}
    A column in a consistent rref matrix corresponds to a
    \begin{description}\pause
    \item[free variable] if the column contains no pivot\pause
    \item[dependent variable] if the column contains a pivot
    \end{description}
  \end{definition}

\end{frame}


\subsection{Solving rref Systems}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Solving consistent rref systems is \emph{easy}: write the dependent
    variables in terms of the free variables.
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
    \left[\begin{array}{rrrr|r}
        \tikzmark{x1}1 & \tikzmark{x2}0 & \tikzmark{x3}0 & \tikzmark{x4}0 & 4 \\
        0 & 0 & 1 & 2 & -1 \\
        0 & 0 & 0 & 0 & 0
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north east) {$x_1$};
      \node at (x2.north east) {$x_2$};
      \node at (x3.north east) {$x_3$};
      \node at (x4.north east) {$x_4$};
    \end{tikzpicture}
    \]
    is consistent.%
    \onslide<2->{The free variables are }%
    \onslide<3->{$\Set{x_2,x_4}$ }%
    \onslide<4->{and the dependent variables are }%
    \onslide<5->{$\Set{x_1,x_3}$. }%
    \onslide<6->{The solutions are}
    \[
    \onslide<7->{\left[\begin{array}{c} x_1\\ x_2\\ x_3\\ x_4\end{array}\right]=}%
    \onslide<8->{\left[}\begin{array}{c} \onslide<9->{4}\\ \onslide<10->{x_2}\\ \onslide<11->{-1-2\,x_4}\\ \onslide<12->{x_4}\end{array}\onslide<8->{\right]}%
    \onslide<13->{= \left[}\begin{array}{r} \onslide<14->{4\\ 0\\ -1\\ 0}\end{array}\onslide<13->{\right]}
    \onslide<13->{+x_2\left[}\begin{array}{r} \onslide<15->{0\\ 1\\ 0\\ 0}\end{array}\onslide<13->{\right]}
    \onslide<13->{+x_4\left[}\begin{array}{r} \onslide<16->{0\\0\\-2\\1}\end{array}\onslide<13->{\right]}
    \]%
    \onslide<17->{Note that there are \emph{infinitely many solutions}.}%
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
    \left[\begin{array}{rrr|r}
        \tikzmark{x1}1 & \tikzmark{x2}0 & \tikzmark{x3}0 & 3 \\
        0 & 1 & 0 & -4 \\
        0 & 0 & 1 &  8
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north east) {$x_1$};%
      \node at (x2.north east) {$x_2$};%
      \node at (x3.north east) {$x_3$};%
    \end{tikzpicture}
    \]
    is consistent with no free variables. \pause The only solution is
    \[
    \left[\begin{array}{c} x_1\\ x_2\\ x_3\end{array}\right]
    = \left[\begin{array}{r} 3\\ -4\\ 8\end{array}\right]
    \]
    \pause
    Note that there is \emph{exactly one solution}.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
    \left[\begin{array}{rrrr|r}
        \tikzmark{x1}0 & \tikzmark{x2}1 & \tikzmark{x3}0 & \tikzmark{x4}0 & 0 \\
        0 & 0 & 0 & 1 & 0 \\
        0 & 0 & 0 & 0 & 1
      \end{array}\right]
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \node at (x1.north east) {$x_1$};%
      \node at (x2.north east) {$x_2$};%
      \node at (x3.north east) {$x_3$};%
      \node at (x4.north east) {$x_4$};%
    \end{tikzpicture}
    \]
    is inconsistent. \pause The last equation is $0=1$, which is impossible! \pause This
    system has \emph{no solutions}.
  \end{example}
\end{frame}


\subsection{The Number of Solutions to a rref System}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A consistent rref system has
    \begin{description}\pause
    \item[\emph{a unique solution}] if it has no free variables \pause
    \item[\emph{infinitely many solutions}] if it has at least one free variable
    \end{description}\pause
    An inconsistent rref system has no solutions.
  \end{theorem}
\end{frame}


\section{Elementary Row Operations}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    There are three types of \emph{elementary row operations}:
    \begin{description}\pause
    \item[row switching $R_i\leftrightarrow R_j$] a row can be switched with
      another row \pause
    \item[row multiplication $c\cdot R_i\to R_i$] a row can be multiplied by a
      nonzero scalar \pause
    \item[row addition $R_i+c\cdot R_j\to R_i$] a row can be replaced by the sum
      of that row and a scalar multiple of another row
    \end{description}
  \end{definition}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 4)
  b = vector([1,-1,2,3])
  M = A.augment(b, subdivide=True)
  N = copy(M)
  M.swap_rows(1,3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row switching]
    {\footnotesize
      \[
      \sage{N}\xrightarrow{R_2\leftrightarrow R_4}
      \pause
      \sage{M}
      \]}
  \end{example}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 4, 4)
  b = vector([1,0,1,3])
  M = A.augment(b, subdivide=True)
  N = copy(M)
  M.rescale_row(2,3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row multiplication]
    {\footnotesize
      \[
      \sage{N}\xrightarrow{3\cdot R_3\to R_3}
      \pause
      \sage{M}
      \]}
  \end{example}
\end{frame}



\begin{sagesilent}
  A = random_matrix(ZZ, 6, 4)
  b = vector([1,0,1,3,-4,-11])
  M = A.augment(b, subdivide=True)
  N = copy(M)
  M.add_multiple_of_row(4,1,-3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row addition]
    {\footnotesize
      \[
      \sage{N}\xrightarrow{R_5-3\cdot R_2\to R_5}
      \pause
      \sage{M}
      \]}
  \end{example}
\end{frame}


\subsection{Why We Care}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Performing an elementary row operation on an augmented matrix yields an
    equivalent system.
  \end{theorem}

  \pause
  \begin{block}{Idea}
    To solve a given system, use elementary row operations to obtain a rref
    system.
  \end{block}

  \pause
  \begin{theorem}[Gau\ss-Jordan Elimination]
    Every augmented matrix is equivalent to a unique rref augmented
    matrix. Furthermore, its rref may be obtained via elementary row operations.
  \end{theorem}
\end{frame}


\section{Gau\ss-Jordan Elimination}
\subsection{Examples}


\begin{sagesilent}
  import gaussjordan
  A = matrix([[1,2],[3,4]])
  b = vector([3,0])
  M = A.augment(b, subdivide=True)
  t = gaussjordan.reduction_info(M)
  E1, E2, E3 = t.elems
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find all solutions to the system
    \[
    \begin{array}{rcrcr}
      x & + & 2\,y & = & 3 \\
      3\,x & + & 4\,y & = & 0 
    \end{array}
    \]
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution}
    Row reduce
    \begin{align*}
      \onslide<2->{\sage{M}}
      &\onslide<3->{\xrightarrow{R_{2} - 3\cdot R_{1}\to R_{2}}} \onslide<4->{\sage{E1*M}} \\ 
      &\onslide<5->{\xrightarrow{-\frac{1}{2}\cdot R_{2}\to R_{2}}}\onslide<6->{\sage{E2*E1*M}} \\ 
      &\onslide<7->{\xrightarrow{R_{1} - 2\cdot R_{2}\to R_{1}}} \onslide<8->{\sage{E3*E2*E1*M}}
    \end{align*}%
    \onslide<9->{This shows that
      \[
      \rref\sage{M}=\sage{M.rref()}
      \]}%
    \onslide<10->{The rref of the system is consistent and has no free
      variables. }%
    \onslide<11->{Hence the unique solution is $X=(-6,\nicefrac{9}{2})$.}
  \end{block}
\end{frame}



\begin{sagesilent}
  A = matrix([[0,4,1],[2,6,-2],[4,8,-5]])
  b = vector([2,3,4])
  M = A.augment(b, subdivide=True)
  t = gaussjordan.reduction_info(M)
  E1, E2, E3, E4, E5, E6 = t.elems
  M1 = E1 * M
  M2 = E2 * M1
  M3 = E3 * M2
  M4 = E4 * M3
  M5 = E5 * M4
  M6 = E6 * M5
\end{sagesilent}

% 1    R_{1}\leftrightarrow R_{2} 
% 2    \frac{1}{2}\cdot R_{1}\to R_{1} 
% 3    R_{3} - 4\cdot R_{1}\to R_{3} 
% 4    \frac{1}{4}\cdot R_{2}\to R_{2} 
% 5    R_{1} - 3\cdot R_{2}\to R_{1} 
% 6    R_{3} + 4\cdot R_{2}\to R_{3}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find all solutions to
    \[
    \begin{array}{rcrcrcr}
      &   & 4\,x_2 & + & x_3    & = & 2 \\
      2\,x_1 & + & 6\,x_2 & - & 2\,x_3 & = & 3 \\
      4\,x_1 & + & 8\,x_2 & - & 5\,x_3 & = & 4
    \end{array}
    \]
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution}
    Row reduce
    \begin{align*}
      \onslide<2->{\sage{M}}
      &\onslide<3->{\xrightarrow{R_{1}\leftrightarrow R_{2}}}      \onslide<4->{\sage{M1}} \\
      &\onslide<5->{\xrightarrow{\frac{1}{2}\cdot R_{1}\to R_{1}}} \onslide<6->{\sage{M2}} \\
      &\onslide<7->{\xrightarrow{R_{3} - 4\cdot R_{1}\to R_{3}}}   \onslide<8->{\sage{M3}} 
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    \begin{align*}
      \onslide<2->{\dotsb\to\sage{M3}}
      &\onslide<3->{\xrightarrow{\frac{1}{4}\cdot R_{2}\to R_{2}}} \onslide<4->{\sage{M4}} \\
      &\onslide<5->{\xrightarrow{R_{1} - 3\cdot R_{2}\to R_{1}}}   \onslide<6->{\sage{M5}} \\
      &\onslide<7->{\xrightarrow{R_{3} + 4\cdot R_{2}\to R_{3}}}   \onslide<8->{\sage{M6}}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    This shows that 
    \[
    \rref\sage{M}=\sage{M.rref()}
    \]%
    \onslide<2->{The rref is consistent with free variable }%
    \onslide<3->{$\Set{x_3}$ }%
    \onslide<4->{and dependent variables }%
    \onslide<5->{$\Set{x_1,x_2}$. }%
    \onslide<6->{The system has infinitely many solutions given by}
    \[
    \onslide<7->{\left[\begin{array}{c} x_1\\ x_2\\ x_3\end{array}\right]=}
    \onslide<8->{\left[}\begin{array}{c} \onslide<9->{\frac{7}{4}\,x_3}\\ \onslide<10->{\frac{1}{2}-\frac{1}{4}\,x_3}\\ \onslide<11->{x_3}\end{array}\onslide<8->{\right]}
    \onslide<12->{= \left[}\begin{array}{r} \onslide<13->{0\\ \nicefrac{1}{2}\\0}\end{array}\onslide<12->{\right]}
    \onslide<12->{+x_3\left[}\begin{array}{r} \onslide<14->{\nicefrac{7}{4}\\-\nicefrac{1}{4}\\1}\end{array}\onslide<12->{\right]}
    \]
  \end{block}
\end{frame}


\section{Homogeneous Systems}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{definition}
    The system
    \[
    \begin{array}{ccccccccc}
      a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
      a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
      \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
      a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
    \end{array}
    \]
    is \emph{homogeneous} if $b_1=b_2=\dotsb=b_m=0$. 
  \end{definition}
  \pause
  \begin{block}{Note}
    Homogenous systems are always consistent, since $X=(0,0,\dotsc,0)$ is a
    solution. \pause The solution $X=(0,0,\dotsc,0)$ is called the \emph{trivial
      solution}. \pause All other solutions are called \emph{nontrivial solutions}.
  \end{block}
  
\end{frame}


\subsection{The Number of Solutions to a Homogeneous System}

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 5)
  b = zero_vector(4)
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{theorem}
    A homogeneous system of $m$ equations and $n$ variables has infinitely many
    solutions if $m<n$.
  \end{theorem}
  \pause
  \begin{example}
    The system 
    \[
    \sage{M}
    \]
    is homogeneous with $m=4$ equations and $n=5$ variables. \pause Since $4<5$, the
    system has infinitely many solutions.
  \end{example}
\end{frame}


\section{Rank and Nullity}
\subsection{Rank}

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=2)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{rank} of a consistent system is the number of pivots in its
    rref. Equivalently, the rank is the number of dependent variables.
  \end{definition}

  \pause
  \begin{example}
    One computes 
    \[
    \rref\sage{M}=\sage{M.rref()}
    \]
    \pause
    Thus the rank of the system is \pause two.
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every system satisfies 
    \begin{align*}
      \textnormal{rank}\leq\#\textnormal{variables} 
      &&
      \text{and}
      &&
      \textnormal{rank}\leq\#\textnormal{equations}
    \end{align*}
  \end{theorem}
  \pause
  \begin{theorem}
    Every consistent system has
    \begin{description}\pause
    \item[\emph{a unique solution}] if
      $\textnormal{rank}=\#\textnormal{variables}$ \pause
    \item[\emph{infinitely many solutions}] if
      $\textnormal{rank}<\#\textnormal{variables}$
    \end{description}
  \end{theorem}
\end{frame}


\subsection{Nullity}

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{nullity} of a consistent system is the number of nonpivot columns
    of in the coefficient matrix of its rref. Equivalently, the nullity is the
    number of free variables.
  \end{definition}

  \pause
  \begin{example}
    One computes 
    \[
    \rref\sage{M}=\sage{M.rref()}
    \]\pause
    Thus the nullity of the system is \pause one.
  \end{example}
\end{frame}



\begin{sagesilent}
  A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=2)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A consistent system has
    \begin{description}\pause
    \item[\emph{a unique solution}] if $\text{nullity}=0$ \pause
    \item[\emph{infinitely many solutions}] if $\text{nullity}>0$
    \end{description}
  \end{theorem}

  \pause
  \begin{example}
    One computes
    {\footnotesize
      \[
      \rref\sage{M}=\sage{M.rref()}
      \]}%
    \pause
    Here, we have $\text{rank}=\pause2$ \pause and $\text{nullity}=\pause3>0$. \pause Thus the system has
    infinitely many solutions.
  \end{example}
\end{frame}


\subsection{The Rank-Nullity Theorem}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Rank-Nullity Theorem]
    Every consistent system satisfies
    \[
    \textnormal{rank}+\textnormal{nullity}=\#\textnormal{variables}
    \]
  \end{theorem}

  \pause
  \begin{block}{Summary}
    Every consistent system has
    \begin{description}\pause
    \item[a unique solution] if $\text{rank}=\#\text{variables}$, or
      $\text{nullity}=0$ \pause
    \item[infinitely many solutions] if $\text{rank}<\#\text{variables}$, or
      $\text{nullity}>0$
    \end{description}
  \end{block}
\end{frame}


\subsection{Example}

\begin{sagesilent}
  import gaussjordan
  A = matrix([[2,0,1,-1],[0,1,2,1],[2,-1,-1,-2]])
  var('b1 b2 b3')
  b = vector([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
  t = gaussjordan.reduction_info(A)
  E1, E2, E3 = t.elems
  M1 = E1 * M
  M2 = E2 * M1
  M3 = E3 * M2
\end{sagesilent}
% 1    \frac{1}{2}\cdot R_{1}\to R_{1}
% 2    R_{3} - 2\cdot R_{1}\to R_{3}
% 3    R_{3} + R_{2}\to R_{3}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{example}
    How many solutions will a system with coefficient matrix
    \[
    A = \sage{A}
    \]
    have?
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{block}{Solution}
    Row reduce
    {\footnotesize
      \begin{align*}
        \onslide<2->{\sage{M}}
        &\onslide<3->{\xrightarrow{\frac{1}{2}\cdot R_{1}\to R_{1}} } \onslide<4->{\sage{M1}} \\
        &\onslide<5->{\xrightarrow{R_{3} - 2\cdot R_{1}\to R_{3}}   } \onslide<6->{ \sage{M2}} \\
        &\onslide<7->{\xrightarrow{R_{3} + R_{2}\to R_{3}}          } \onslide<8->{ \sage{M3}} 
      \end{align*}}%
    \onslide<9->{This system is consistent if and only if $-b_1+b_2+b_3=0$. }%
    \onslide<10->{Suppose $-b_1+b_2+b_3=0$. }%
    \onslide<11->{Then the system is consistent with $\text{nullity}=2>0$ and thus has infinitely many solutions.}%
  \end{block}
\end{frame}



\end{document}
