\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning, shapes.geometric}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}




\title{Functions Between Euclidean Spaces}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Definitions and Terminology}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $X$ and $Y$ be collections. \pause%
    A \emph{function $f$ from $X$ to $Y$} is a rule that associates to every
    element $x\in X$ and element $f(x)\in Y$.
  \end{definition}

  \pause
  \begin{block}{Note}
    If $f$ is a function from $X$ to $Y$, then we write $f:X\to Y$. \pause%
    \begin{description}
    \item[$X$] is the \emph{domain} of $f$ \pause%
    \item[$Y$] is the \emph{target} or \emph{codomain} of $f$
    \end{description}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The following function assigns every shape in $X$ to its color in $Y$.
    \[
    \input{colors.tex}
    \]
  \end{example}
\end{frame}


\subsection{Examples}
\newcommand{\myvvout}{
  \begin{bmatrix}
    pq+r\\ p-s\\ e^{ps^2}
  \end{bmatrix}
}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The formula $f(x)=2\,x+3$ defines a function $f:\pause \mathbb{R}\to \pause\mathbb{R}$.
  \end{example}

  \pause
  \begin{example}
    The formula
    \[
    g(x,y,z)=3\,x^3-\sin(xy)+e^{xz}
    \]
    defines a function $g:\pause\mathbb{R}^3\to \pause\mathbb{R}$.
  \end{example}

  \pause
  \begin{example}
    The formula
    \[
    h(p,q,r,s)=\myvvout
    \]
    defines a function $h:\pause\mathbb{R}^{4}\to \pause\mathbb{R}^{3}$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The parameterization of a line $\mathcal{L}$ in $\mathbb{R}^n$ is given by
    \[
    \vv{L}(t)=\vv{P}+t\cdot\vv{v}
    \]
    where $P$ is a point on $\mathcal{L}$ and $\vv{v}$ is a vector parallel to
    $\mathcal{L}$. \pause%
    This defines a function $\vv{L}:\pause\mathbb{R}\to \pause\mathbb{R}^{n}$.
  \end{example}

  \pause
  \begin{example}
    The parameterization of a plane $\mathcal{P}$ in $\mathbb{R}^n$ is given by
    \[
    \vv{L}(s, t)=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}
    \]
    where $P$ is a point on $\mathcal{P}$ and $\vv{v}$ and $\vv{w}$ are vectors
    parallel to $\mathcal{P}$. \pause%
    This defines a function $\vv{L}:\pause\mathbb{R}^2\to\pause\mathbb{R}^{n}$.
  \end{example}
\end{frame}

\subsection{The Cobb-Douglas Function}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The \emph{Cobb-Douglas} production function is
    \[
    Q(L, K)=4\,K^{\nicefrac{3}{4}}L^{\nicefrac{1}{4}}
    \]
    \onslide<2->{This function was proposed by Charles Cobb and Paul Douglas in
      1927 to explain the relationship between the variables}%
    \begin{align*}
      \onslide<3->{Q &= \text{total production (real value of all goods produced in a year)}  }  \\
      \onslide<4->{K &= \text{capital input (the real value of all machinery, equipment, etc)}}  \\
      \onslide<5->{L &= \text{labor input (the total number of person-hours worked in a year)}} 
    \end{align*}
    \onslide<6->{Note that the Cobb-Douglas formula defines production $Q$ as a
      function $Q:\mathbb{R}^{2}\to \mathbb{R}$.}
  \end{example}
\end{frame}


\subsection{Component Functions}

\newcommand{\mycomponents}{
  \begin{bmatrix}
    f_1(x_1,x_2,\dotsc,x_m) \\
    f_2(x_1,x_2,\dotsc,x_m) \\
    \vdots \\
    f_n(x_1,x_2,\dotsc,x_m)
  \end{bmatrix}
}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Important Observation}
    Every function $f:\mathbb{R}^{m}\to \mathbb{R}^{n}$ is of the form \pause%
    \[
    f(x_1,x_2,\dotsc,x_m)
    =\mycomponents
    \]
    \pause%
    where each $f_k$ is a function $f_k:\mathbb{R}^{m}\to \mathbb{R}$. \pause%
    The functions $f_1,f_2,\dotsc,f_n$ are the \emph{component functions} of
    $f$.
  \end{block}

  \pause
  \begin{block}{Idea}
    We can simplify the problem of studying a ``complicated''
    $f:\mathbb{R}^{m}\to \mathbb{R}^{n}$ by studying its component functions
    $f_1,f_2,\dotsc,f_n:\mathbb{R}^{m}\to \mathbb{R}$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The formula
    \[
    h(p,q,r,s)=\myvvout
    \]
    defines a function $h:\pause \mathbb{R}^{4}\to \pause\mathbb{R}^{3}$.
    \pause%
    The component functions of $h$ are \pause
    \begin{align*}
      h_1(p,q,r,s) &= pq+r \\
      h_2(p,q,r,s) &= p-s \\
      h_3(p,q,r,s) &= e^{ps^2}
    \end{align*}
    \pause%
    Note that $h_1,h_2,h_3:\pause\mathbb{R}^{4}\to \pause\mathbb{R}$.
  \end{example}
\end{frame}


\subsection{Images}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{image} of a function $f:X\to Y$ is the subcollection of $Y$
    defined by $\Image(f)=\Set{f(x)\given x\in X}$.
  \end{definition}

  \pause
  \begin{block}{Note}
    The image of a function $f$ is often called the \emph{range} of $f$,
    $\Range(f)=\Image(f)$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the following function $f:X\to Y$.
    \begin{columns}[onlytextwidth]
      \column{.75\textwidth}
      \[
      \begin{tikzpicture}[ mydot/.style={ circle, fill, inner sep=2pt },
        >=latex, shorten >= 3pt, shorten <= 3pt, ]

        \begin{scope}[node distance=1/4]
          \node[mydot,label={left:$-3$}] (a1) {}; %
          \node[mydot,below=of a1,label={left:$2$}] (a2) {}; %
          \node[mydot,below=of a2,label={left:$17$}] (a3) {}; %
          \node[mydot,below=of a3,label={left:$22$}] (a4) {}; %

          \node[mydot,above right=.25cm and 4cm of a1, label={right:$\alpha$}] (b1) {};%
          \node[mydot,below=of b1,label={right:$\beta$}] (b2) {};%
          \node[mydot,below=of b2,label={right:$\gamma$}] (b3) {};%
          \node[mydot,below=of b3,label={right:$\delta$}] (b4) {};%
          \node[mydot,below=of b4,label={right:$\varepsilon$}] (b5) {};%

          \onslide<2->{\path[very thick, ->] (a1) edge (b5);}%
          \onslide<4->{\path[very thick, ->] (a2) edge (b2);}%
          \onslide<6->{\path[very thick, ->] (a3) edge (b1);}%
          \onslide<8->{\path[very thick, ->] (a4) edge (b5);}%

          \draw[very thick] ($ (a2)!.5!(a3) $) ellipse (1.25cm and 1.4cm) node [above=1.4cm] {$X$}; %
          \draw[very thick] (b3) ellipse (1.3cm and 1.4cm) node [above=1.4cm] {$Y$};%
        \end{scope}
      \end{tikzpicture}
      \]
      \column{.25\textwidth}
      \begin{align*}
        \onslide<3->{f(-3) &= \varepsilon \\ }
        \onslide<5->{f(2) &= \beta        \\ }
        \onslide<7->{f(17) &= \alpha      \\ }
        \onslide<9->{f(22) &= \varepsilon    }
      \end{align*}
    \end{columns}
    \begin{description}
    \item[\onslide<10->{domain of $f$}] \onslide<10->{is }$\onslide<10->{X=}\onslide<11->{\Set{-3,2,17,22}$}
    \item[\onslide<12->{target of $f$}] \onslide<12->{is }$\onslide<12->{Y=}\onslide<13->{\Set{\alpha,\beta,\gamma,\delta,\varepsilon}}$
    \item[\onslide<14->{image of $f$}] \onslide<14->{is }$\onslide<14->{\Image(f)=}\onslide<15->{\Set{\alpha,\beta,\varepsilon}}$
    \end{description}
  \end{example}
\end{frame}


\section{Geometric Representations of Functions}
\subsection{Graphs}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{graph} of $f:\mathbb{R}^{m}\to \mathbb{R}^{n}$ is the collection
    \[
    \Graph(f)=\Set{(\vv{x},f(\vv{x}))\in \mathbb{R}^{m+n}\given \vv{x}\in \mathbb{R}^{m}}
    \]
  \end{definition}

  \pause
  \begin{block}{Note}
    $\Graph(f)\subset \mathbb{R}^{m+n}$
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f:\mathbb{R}\to \mathbb{R}$ be defined by $f(x)=x^2$. %
    \onslide<2->{The graph of $f$ is}
    \begin{align*}
      \onslide<3->{\Graph(f)}
      &\onslide<3->{= \Set{(x, f(x))\in \mathbb{R}^{2}\given x\in \mathbb{R}} \\}
      &\onslide<4->{= \Set{(x, x^2)\in \mathbb{R}^{2}\given x\in\mathbb{R}}}
    \end{align*}
    \onslide<5->{This graph is represented geometrically by
    \[
    \begin{tikzpicture}[scale=.33]
      \draw[ultra thick, <->] (-4,0) -- (4,0);
      \draw[ultra thick, <->] (0,-2) -- (0,10);

      \draw[very thick, domain=-3:3, smooth, variable=\x, blue, <->] plot ({\x},{\x*\x});

      \coordinate (P) at (2, 4);
      \node at (P) {\textbullet};
      \node[right] at (P) {$(a, a^2)$};
    \end{tikzpicture}
    \]}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $g:\mathbb{R}^{2}\to \mathbb{R}$ be defined by $g(x, y)=x^2+y^2$. %
    \onslide<2->{The graph of $g$ is}
    \begin{align*}
      \onslide<3->{\Graph(g)}
      &\onslide<3->{= \Set{(x, y, g(x, y))\in \mathbb{R}^{3}\given (x, y)\in \mathbb{R}^{2}} \\}
      &\onslide<4->{= \Set{(x, y, x^2+y^2)\in \mathbb{R}^{3}\given (x, y)\in \mathbb{R}^{2}}}
    \end{align*}
    \onslide<5->{This graph is represented geometrically by}
    \[
    \tdplotsetmaincoords{70}{135}
    \begin{tikzpicture}[tdplot_main_coords, scale=1, line cap=round, line join=round]
      \onslide<6->{
      \draw[thick,<->] (-2,0,0) -- (2,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};}

      \coordinate (O) at (0, 0, 0);
      \coordinate (P1) at (1, -1, 2);
      \coordinate (P2) at (-1, 1, 2);

      \coordinate (Q) at (0, 0, 1.5);

      \onslide<6->{
      \draw[thick,->] (O) -- (0,0,-1/2);
      \draw[thick,->] (Q) -- (0, 0, 3) node[anchor=south]{$z$};
      \draw[thick, dashed] (O) -- (Q);

      \draw[very thick] (O) parabola (P1);
      \draw[very thick] (O) parabola (P2);

      \draw[very thick, domain=0:2*pi, smooth, variable=\x]
      plot ({cos(\x r)*sqrt(2)},{sin(\x r)*sqrt(2)},{2});}

    \onslide<7->{
      \coordinate (P) at (1/2, 1, 5/4);
      \node at (P) {\textbullet};

      \draw[very thick, dotted]
      (1/2, 0, 0) -- (1/2, 1, 0) -- (0, 1, 0);
      \draw[very thick, dotted]
      (1/2, 1, 0) -- (P);}
      % \node[above] at (P) {{\tiny$(a, b, a^2+b^2)$}};
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\subsection{Level Sets}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. The \emph{level set} of $f$
    corresponding to $c\in \mathbb{R}$ is the collection
    \[
    L_c(f)=\Set*{\vv{x}\in \mathbb{R}^{n}\given f(\vv{x})=c}
    \]
    \pause
    Note that $L_c(f)\subset\Domain(f)$.
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The level sets of $f(x,y)=x^2+y^2$.
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \draw[ultra thick, <->] (-4, 0) -- (4, 0);
      \draw[ultra thick, <->] (0, -3) -- (0, 3);

      \onslide<1>{\node at (4, -5/2) {$c<0$};}
      \onslide<1>{\node at (4, 3) {$L_c(f)=\varnothing$};}

      \onslide<2>{\node at (4, -5/2) {$c=0$};}
      \onslide<2>{\node at (4, 3) {$x^2+y^2=0$};}
      \onslide<2>{\filldraw[blue] (0, 0) circle (3pt);}

      \pgfmathsetmacro{\r}{sqrt(1/2)}
      \onslide<3>{\node at (4, -5/2) {$c=\nicefrac{1}{2}$};}
      \onslide<3>{\node at (4, 3) {$x^2+y^2=\nicefrac{1}{2}$};}
      \onslide<3>{\draw[very thick, blue] (0, 0) circle (\r);}

      \onslide<4>{\node at (4, -5/2) {$c=1$};}
      \onslide<4>{\node at (4, 3) {$x^2+y^2=1$};}
      \onslide<4>{\draw[very thick, blue] (0, 0) circle (1);}

      \pgfmathsetmacro{\r}{sqrt(3/2)}
      \onslide<5>{\node at (4, -5/2) {$c=\nicefrac{3}{2}$};}
      \onslide<5>{\node at (4, 3) {$x^2+y^2=\nicefrac{3}{2}$};}
      \onslide<5>{\draw[very thick, blue] (0, 0) circle (\r);}

      \pgfmathsetmacro{\r}{sqrt(2)}
      \onslide<6>{\node at (4, -5/2) {$c=2$};}
      \onslide<6>{\node at (4, 3) {$x^2+y^2=2$};}
      \onslide<6>{\draw[very thick, blue] (0, 0) circle (\r);}

      \pgfmathsetmacro{\r}{sqrt(5/2)}
      \onslide<7>{\node at (4, -5/2) {$c=\nicefrac{5}{2}$};}
      \onslide<7>{\node at (4, 3) {$x^2+y^2=\nicefrac{5}{2}$};}
      \onslide<7>{\draw[very thick, blue] (0, 0) circle (\r);}

      \pgfmathsetmacro{\r}{sqrt(3)}
      \onslide<8>{\node at (4, -5/2) {$c=3$};}
      \onslide<8>{\node at (4, 3) {$x^2+y^2=3$};}
      \onslide<8>{\draw[very thick, blue] (0, 0) circle (\r);}

      \pgfmathsetmacro{\r}{sqrt(7/2)}
      \onslide<9>{\node at (4, -5/2) {$c=\nicefrac{7}{2}$};}
      \onslide<9>{\node at (4, 3) {$x^2+y^2=\nicefrac{7}{2}$};}
      \onslide<9>{\draw[very thick, blue] (0, 0) circle (\r);}

      \pgfmathsetmacro{\r}{sqrt(4)}
      \onslide<10>{\node at (4, -5/2) {$c=4$};}
      \onslide<10>{\node at (4, 3) {$x^2+y^2=4$};}
      \onslide<10>{\draw[very thick, blue] (0, 0) circle (\r);}
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    The level sets $L_c(f)$ are the ``cross sections'' of the graph of $f$.
    \[
    \tdplotsetmaincoords{70}{135}
    \begin{tikzpicture}[tdplot_main_coords, scale=3/2, line cap=round, line join=round]
      \draw[thick,<->] (-2,0,0) -- (2,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};

      \coordinate (O) at (0, 0, 0);
      \coordinate (P1) at (1, -1, 2);
      \coordinate (P2) at (-1, 1, 2);

      \coordinate (Q) at (0, 0, 1.5);

      \draw[thick,->] (O) -- (0,0,-1/2);
      \draw[thick,->] (Q) -- (0, 0, 3) node[anchor=south]{$z$};
      \draw[thick, dashed] (O) -- (Q);

      \draw[very thick] (O) parabola (P1);
      \draw[very thick] (O) parabola (P2);

      \draw[very thick, domain=0:2*pi, smooth, variable=\x]
      plot ({cos(\x r)*sqrt(2)},{sin(\x r)*sqrt(2)},{2});


      \pause
      \filldraw[blue] (O) circle (2pt);

      \pause
      \pgfmathsetmacro{\r}{sqrt(1/2)}
      \draw[very thick, blue, domain=0:2*pi, smooth, variable=\x]
      plot ({cos(\x r)*\r},{sin(\x r)*\r},{1/2});

      \pause
      \pgfmathsetmacro{\r}{sqrt(1)}
      \draw[very thick, blue, domain=0:2*pi, smooth, variable=\x]
      plot ({cos(\x r)*\r},{sin(\x r)*\r},{1});

      \pause
      \pgfmathsetmacro{\r}{sqrt(3/2)}
      \draw[very thick, blue, domain=0:2*pi, smooth, variable=\x]
      plot ({cos(\x r)*\r},{sin(\x r)*\r},{3/2});
    \end{tikzpicture}
    \]
  \end{block}
\end{frame}


\subsection{Images}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f:\mathbb{R}\to \mathbb{R}^{2}$ be the function $f(t)=\langle \cos(t),
    \sin(t)\rangle$. %
    \onslide<2->{The image of $f$ is}
    \begin{align*}
      \onslide<2->{\Image(f)}
      &\onslide<2->{= \Set{f(t)\in \mathbb{R}^{2}\given t\in \mathbb{R}} \\}
      &\onslide<3->{= \Set{\langle \cos(t), \sin(t)\rangle\given t\in \mathbb{R}}}
    \end{align*}
    \onslide<4->{This image is represented geometrically by
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1.25]
      \draw[ultra thick, <->] (-3/2, 0) -- (3/2, 0);
      \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);

      \draw[very thick, blue] (0, 0) circle (1);
    \end{tikzpicture}
    \]}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $h:\mathbb{R}\to \mathbb{R}^{3}$ be the function $h(t)=\langle\cos(t),
    \sin(t), t\rangle$. %
    \onslide<2->{The image of $h$ is}
    \begin{align*}
      \onslide<2->{\Image(h)}
      &\onslide<2->{= \Set{h(t)\in \mathbb{R}^{3}\given t\in \mathbb{R}} \\}
      &\onslide<3->{= \Set{\langle \cos(t), \sin(t), t\rangle\in \mathbb{R}^3\given t\in \mathbb{R}}}
    \end{align*}
    \onslide<4->{This image is represented geometrically by
    \[
    \tdplotsetmaincoords{70}{135}
    \begin{tikzpicture}[tdplot_main_coords, scale=1, line cap=round, line join=round]
      \draw[thick,<->] (-2,0,0) -- (2,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[thick,<->] (0,0,-1) -- (0,0,2) node[anchor=south]{$z$};

      \draw[very thick, domain=0:2*pi, samples=500, variable=\t, blue]
      plot ({cos(3*\t r)}, {sin(3*\t r)},{\t/4});
    \end{tikzpicture}
    \]}
  \end{example}
\end{frame}



\end{document}
