\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}


\title{The Multivariable Chain Rule}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{Motivation}
\subsection{The Single-Variable Chain Rule}

\Style{DDisplayFunc=outset,DShorten=true}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    What is $\D{e^{x^2}}{x}$?
  \end{example}\pause%
  \begin{block}{Solution}
    Let $u=x^2$. \pause%
    Then the chain rule implies that
    \[
    \frac{d}{dx}e^{x^2}
    =\pause \frac{d}{dx}e^u
    =\pause \frac{d(e^u)}{du}\cdot\frac{du}{dx}
    =\pause (e^u)\cdot(2\,x)
    \]\pause%
    In particular, when $x=-3$ we have $\dfrac{d}{dx}e^{x^2}=e^{9}\cdot(-6)$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    View $e^{x^2}$ as a composite of functions
    \[
    \begin{tikzcd}[row sep=.01cm]
      \mathbb{R}\arrow{r}{u}\pgfmatrixnextcell
      \mathbb{R}\arrow{r}{f}\pgfmatrixnextcell
      \mathbb{R}
    \end{tikzcd}
    \]
    where $u(x)=x^2$ and $f(u)=e^u$. \pause%
    The chain rule allows us to solve the ``hard'' problem of computing
    $\D{e^{x^2}}{x}$ by expressing $\D{e^{x^2}}{x}$ as the product of the
    ``easy'' derivatives
    \begin{align*}
      f^\prime(u) &= e^u & \text{and} && u^\prime(x) &= 2\,x
    \end{align*}
  \end{block}
\end{frame}

\subsection{Cobb-Douglas with Time and Interest}

\begin{sagesilent}
  var('r t K L')
  g1 = 10 * t ** 2 / r
  g2 = 6 * t ** 2 + 250 * r
  g1Eval = g1(t=10, r=1/10)
  g2Eval = g2(t=10, r=1/10)
  g = matrix.column([g1, g2])
  Dg = jacobian(g, (t, r))
  DgEval = Dg(t=10, r=1/10)
  Q = 4 * K ** (3 / 4) * L ** (1 / 4)
  QEx = Q(K=g1, L=g2)
  DQ = jacobian(Q, (K, L))
  DQEval = DQ(K=g1Eval, L=g2Eval)
\end{sagesilent}
\Style{DDisplayFunc=inset,DShorten=true}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Recall the Cobb-Douglas production function
    $Q=4\,K^{\nicefrac{3}{4}}L^{\nicefrac{1}{4}}$. %
    \onslide<2->{Suppose that capital $K$ and labor $L$ depend on time $t$ and
      the interest rate $r$ via the expressions
    \begin{align*}
      K &= \sage{g1} & L &= \sage{g2}
    \end{align*}}
  \onslide<3->{What is the rate of change of $Q$ with respect to $t$ when $t=10$ and
    $r=\nicefrac{1}{10}$?}
  \end{example}
  \begin{block}{\onslide<4->{Bad Idea}}
    \onslide<4->{Substitute the expressions for $K$ and $L$ into the expression
      for $Q$}
    \begin{align*}
      \onslide<5->{Q &= \sage{QEx}} & \onslide<6->{\pderiv{Q}{t} &= \sage{QEx.diff(t)}}
    \end{align*}
    \onslide<7->{This gives $\displaystyle\pderiv{Q}{t}(t=10,
    r=\nicefrac{1}{10})=\sage{QEx.diff(t)(t=10, r=1/10).canonicalize_radical()}$.}
  \end{block}
\end{frame}
\endgroup


\Style{DDisplayFunc=inset,DShorten=true}
\newcommand{\mycobbg}{
  \left[\begin{array}{r}
      K(t, r) \\ L(t, r)
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Let $g:\mathbb{R}^{2}\to \mathbb{R}^{2}$ be the function whose components
    are $K$ and $L$. 
    \begin{align*}
      \onslide<2->{
      \begin{tikzcd}[column sep=small]
        \mathbb{R}^{2}\arrow{r}{g} \pgfmatrixnextcell \mathbb{R}^{2}\arrow{r}{Q}
        \pgfmatrixnextcell \mathbb{R}
      \end{tikzcd}} &&
    \onslide<3->{g(t, r) &= \mycobbg} &
    \onslide<4->{Q(K, L) &= 4\,K^{\nicefrac{3}{4}}L^{\nicefrac{1}{4}}}
    \end{align*}
    \onslide<5->{The partial derivatives of $Q$ with respect to $t$ and $r$ are}
    \begin{align*}
      \onslide<6->{\pderiv{Q}{t} &= \frac{\partial}{\partial t} Q(g(t, r))} &
      \onslide<7->{\pderiv{Q}{r} &= \frac{\partial}{\partial r} Q(g(t, r))}
    \end{align*}
    \onslide<8->{We need a multivariable chain rule!}
  \end{block}
\end{frame}


\section{The Multivariable Chain Rule}
\subsection{Statement}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Multivariable Chain Rule]
    Consider a diagram of functions
    \[
    \begin{tikzcd}
      \mathbb{R}^{k}\arrow{r}{g} \pgfmatrixnextcell
      \mathbb{R}^{m}\arrow{r}{f} \pgfmatrixnextcell
      \mathbb{R}^{n}
    \end{tikzcd}
    \]\pause%
    Then $D(f\circ g)=Df\cdot Dg$.
  \end{theorem}
\end{frame}



\newcommand{\myfgdiag}{
  \begin{tikzcd}
    \mathbb{R}^{k}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{m}\arrow{r}{f} \pgfmatrixnextcell
    \mathbb{R}^{n}
  \end{tikzcd}}
\newcommand{\myfcomps}{
  \left[\begin{array}{c}
      f_1(s_1,\dotsc,s_m) \\
      f_2(s_1,\dotsc,s_m) \\
      \vdots \\
      f_n(s_1,\dotsc,s_m)
    \end{array}\right]}
\newcommand{\mygcomps}{
  \left[\begin{array}{c}
      g_1(t_1,\dotsc,t_k) \\
      g_2(t_1,\dotsc,t_k) \\
      \vdots              \\
      g_m(t_1,\dotsc,t_k)
    \end{array}\right]}
\newcommand{\myDfgcomps}{
  \left[\begin{array}{cccc}
      \pderiv{f_1}{t_1} & \pderiv{f_1}{t_2} & \dotsb & \pderiv{f_1}{t_k} \\ \\
      \pderiv{f_2}{t_1} & \pderiv{f_2}{t_2} & \dotsb & \pderiv{f_2}{t_k} \\ \\
      \vdots            & \vdots            & \ddots & \vdots            \\ \\
      \pderiv{f_n}{t_1} & \pderiv{f_n}{t_2} & \dotsb & \pderiv{f_n}{t_k}
    \end{array}\right]}
\newcommand{\myDfcomps}{
  \left[\begin{array}{cccc}
      \pderiv{f_1}{s_1} & \pderiv{f_1}{s_2} & \dotsb & \pderiv{f_1}{s_m} \\ \\
      \pderiv{f_2}{s_1} & \pderiv{f_2}{s_2} & \dotsb & \pderiv{f_2}{s_m} \\ \\
      \vdots            & \vdots            & \ddots & \vdots            \\ \\
      \pderiv{f_n}{s_1} & \pderiv{f_n}{s_2} & \dotsb & \pderiv{f_n}{s_m}
    \end{array}\right]}
\newcommand{\myDgcomps}{
  \left[\begin{array}{cccc}
      \pderiv{g_1}{t_1} & \pderiv{g_1}{t_2} & \dotsb & \pderiv{g_1}{t_k} \\ \\
      \pderiv{g_2}{t_1} & \pderiv{g_2}{t_2} & \dotsb & \pderiv{g_2}{t_k} \\ \\
      \vdots            & \vdots            & \ddots & \vdots            \\ \\
      \pderiv{g_m}{t_1} & \pderiv{g_m}{t_2} & \dotsb & \pderiv{g_m}{t_k}
    \end{array}\right]}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Coordinate Interpretation}
    Given a diagram $\myfgdiag$ we have
    \begin{align*}
      \onslide<2->{f(s_1,\dotsc,s_m) &=} \onslide<3->{\myfcomps} &
      \onslide<4->{g(t_1,\dotsc,t_k) &=} \onslide<5->{\mygcomps}
    \end{align*}
    \onslide<6->{The multivariable chain rule $D(f\circ g)=Df\cdot Dg$ gives the
      equation}
    \[
    \onslide<10->{\myDfgcomps} \onslide<7->{=}
    \onslide<8->{\myDfcomps}\onslide<9->{\myDgcomps}
    \]
  \end{block}
\end{frame}
\endgroup


\subsection{Cobb-Douglas, Revisited}

\newcommand{\myCobbDouglasDiag}{
  \begin{tikzcd}
    \mathbb{R}^{k}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{m}\arrow{r}{f} \pgfmatrixnextcell
    \mathbb{R}^{n}
  \end{tikzcd}}
\newcommand{\myDQeval}{
  \left[\begin{array}{cc}
      \displaystyle\pderiv{Q}{t} & \displaystyle\pderiv{Q}{r}
    \end{array}\right]}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    We can interpret our Cobb-Douglas model diagrammatically as 
    \begin{align*}
      \onslide<2->{
      \begin{tikzcd}[column sep=small]
        \mathbb{R}^{2}\arrow{r}{g} \pgfmatrixnextcell \mathbb{R}^{2}\arrow{r}{Q}
        \pgfmatrixnextcell \mathbb{R}
      \end{tikzcd}} &&
    \onslide<3->{g(t, r) &= \mycobbg=\sage{g}} &
    \onslide<4->{Q(K, L) &= 4\,K^{\nicefrac{3}{4}}L^{\nicefrac{1}{4}}}
    \end{align*}
    \onslide<5->{The Jacobian derivatives are}
    \begin{align*}
      \onslide<6->{Dg &=} \onslide<7->{\sage{Dg}} &
      \onslide<8->{DQ &=} \onslide<9->{\sage{DQ}}
    \end{align*}
    \onslide<10->{Since $K(10, \nicefrac{1}{10})=\sage{g1Eval}$ and
      $L(10, \nicefrac{1}{10})=\sage{g2Eval}$, the multivariable chain rule
      implies that
    for $t=10$ and $r=\nicefrac{1}{10}$ we have
  }
    \begin{align*}
      \onslide<10->{\myDQeval &=} 
      \onslide<11->{DQ(\sage{g1Eval}, \sage{g2Eval})\cdot Dg(10, \nicefrac{1}{10}) \\
        &=} \onslide<12->{\sage{DQEval}\sage{DgEval} \\
        &=} \onslide<13->{\sage{DQEval * DgEval}}
    \end{align*}
  \end{example}
\end{frame}
\endgroup

\section{Curves in $\mathbb{R}^{3}$}
\subsection{Setup}

\newcommand{\myDf}{
  \begin{bmatrix}
    \pderiv{f}{x} & \pderiv{f}{y} & \pderiv{f}{z}
  \end{bmatrix}}
\newcommand{\myDr}{
  \begin{bmatrix}
    \pderiv{x}{t} \\ \\ \pderiv{y}{t} \\ \\ \pderiv{z}{t}
  \end{bmatrix}}
\begingroup
\small
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{r}:\mathbb{R}\to \mathbb{R}^{3}$ be a curve in $\mathbb{R}^{3}$
    parameterized by
    \[
    \vv{r}(t) = \langle x(t), y(t), z(t)\rangle
    \]\pause%
    Let $f:\mathbb{R}^{3}\to \mathbb{R}$ be a function $f(x,y,z)$. \pause%
    This gives a diagram
    \[
    \begin{tikzcd}[row sep=.01cm]
      \mathbb{R}\arrow{r}{\vv{r}}\pgfmatrixnextcell
      \mathbb{R}^{3}\arrow{r}{f}\pgfmatrixnextcell
      \mathbb{R}
    \end{tikzcd}
    \]\pause%
    The multivariable chain rule implies that
    \[
    \pderiv{f}{t}
    =\pause Df\cdot D\vv{r}
    =\pause \myDf\myDr
    =\pause \pderiv{f}{x}\cdot\pderiv{x}{t}
    + \pderiv{f}{y}\cdot\pderiv{y}{t}
    + \pderiv{f}{z}\cdot\pderiv{z}{t}
    \]
  \end{example}
\end{frame}
\endgroup

\subsection{Examples}

\begin{sagesilent}
  var('t x y z')
  r = vector([cos(t), sin(t), t])
  T = x ** 2 * y * z
  Dr = jacobian(r, t)
  DT = jacobian(T, (x, y, z))
  DTDr = (DT*Dr)[0, 0]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    A fly is flying around a room with position
    $\vv{r}(t)=\langle\cos{t},\sin{t},t\rangle$.  \onslide<2->{Temperature in
      the room is given by $T(x, y, z)=x^2yz$. }%
    \onslide<3->{What is the rate of the change of the temperature experienced
      by the fly at time $t$? }%
    \onslide<4->{What is this rate when $t=\pi$?}
  \end{example}
  \begin{block}{\onslide<5->{Solution}}
    \onslide<5->{The multivariable chain rule gives}
    \begin{align*}
      \onslide<6->{\pderiv{T}{t} 
        &=} \onslide<7->{DT\cdot D\vv{r} \\ 
        &=} \onslide<8->{\sage{DT}\sage{Dr} \\
        &=} \onslide<9->{\sage{DTDr}}
    \end{align*}
    \onslide<10->{At time $t=\pi$, the fly is at position
      $\vv{r}(\pi)=\sage{r(t=pi)}$. }%
    \onslide<11->{Thus at time $t=\pi$ the fly experiences a rate of change of
      temperature of}
    \[
    \onslide<11->{\pderiv{T}{t}
      =} \onslide<12->{(-1)^2(\pi)\cos(\pi)-2\,(-1)(0)(\pi)\sin(\pi)+(-1)^2(0)
      =} \onslide<13->{-\pi}
    \]
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  r = vector([t ** 2, 4* t ** 3, 2 * t -1])
  T = x ** 2 * y - 3 * z
  Dr = jacobian(r, t)
  DT = jacobian(T, (x, y, z))
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that the position of the fly is $\vv{r}(t)=\sage{r}$ and that
    temperature is given by $T(x,y,z)=\sage{T}$.  \onslide<2->{What is the rate
      of change of temperature experienced by the fly at time $t=1$?}
  \end{example}
  \begin{block}{\onslide<3->{Solution}}
    \onslide<3->{The Jacobian derivatives of $T$ and $\vv{r}$ are}
    \begin{align*}
      \onslide<4->{DT &=} \onslide<5->{\sage{DT}} & 
      \onslide<6->{D\vv{r} &=} \onslide<7->{\sage{Dr}}
    \end{align*}
    \onslide<8->{At time $t=1$, the fly is at position
      $\vv{r}(1)=\sage{r(t=1)}$. }%
    \onslide<9->{The rate of change of temperature experienced by the fly at
      time $t=1$ is thus}
    \[
    \onslide<10->{\pderiv{T}{t}
      =} \onslide<11->{DT\sage{tuple(r(t=1))}\cdot D\vv{r}(1)
      =} \onslide<12->{\sage{DT(x=1, y=4, z=1)}\sage{Dr(t=1)}
      =} \onslide<13->{\sage{(DT(x=1, y=4, z=1) * Dr(t=1))[0, 0]}}
    \]
  \end{block}
\end{frame}
\endgroup

\section{Examples with Multiple Inputs}

\newcommand{\mygdiag}{
  \begin{tikzcd}[column sep=small]
    \mathbb{R}^{2}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{3}\arrow{r}{w} \pgfmatrixnextcell
    \mathbb{R}
  \end{tikzcd}}
\renewcommand{\mygcomps}{
  \left[\begin{array}{r}
      p(x, y)\\ q(x, y)\\ c(x, y)
    \end{array}\right]}
\newcommand{\myParDiffW}{
  \left[\begin{array}{cc}
      \pderiv{w}{x} & \pderiv{w}{y}
    \end{array}\right]}

\begin{sagesilent}
  var('x y p q c')
  g = matrix.column([x*y, x-y, x**2+y**2])
  Dg = jacobian(g, (x, y))
  w = p*q-c
  Dw = jacobian(w, (p, q, c))
\end{sagesilent}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}

  \begin{example}
    Let $w:\mathbb{R}^{3}\to \mathbb{R}$ be given by $w(p, q, c)=pq-c$.
    \onslide<2->{Suppose that
    \begin{align*}
      p &= xy & q &= x-y & c &= x^2+y^2
    \end{align*}}
  \onslide<3->{Compute $\pderiv{w}{x}$ and $\pderiv{w}{y}$.}
  \end{example}
  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{We may view this example diagrammatically as}
    \begin{align*}
      \onslide<5->{\mygdiag} && \onslide<6->{g(x, y) &=} \onslide<7->{\mygcomps =} \onslide<8->{\sage{g}}
    \end{align*}
    \onslide<9->{The multivariable chain rule implies}
    \[
    \onslide<10->{\myParDiffW
      =} \onslide<11->{Dw\cdot Dg
      =} \onslide<12->{\mysage{\tiny}{Dw}\sage{Dg}
      =} \onslide<13->{\mysage{\tiny}{Dw * Dg}}
    \]
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  var('x y z u v w')
  h = exp(5 * x - 2 * y - 4 * z)
  Dh = jacobian(h, (x, y, z))
  DhE = Dh(x=4, y=4, z=3)
  g = matrix.column([u ** 2 * v, u + 2 * v * w ** 2, u - w])
  Dg = jacobian(g, (u, v, w))
  DgE = Dg(u=2, v=1, w=-1)
\end{sagesilent}
\renewcommand{\mygdiag}{
  \begin{tikzcd}[column sep=small]
    \mathbb{R}^{3}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{3}\arrow{r}{h} \pgfmatrixnextcell
    \mathbb{R}
  \end{tikzcd}}
\newcommand{\myDhcomps}{
  \left[
    \begin{array}{ccc}
      \pderiv{h}{u} & \pderiv{h}{v} & \pderiv{h}{w}
    \end{array}
  \right]}

\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}

  \begin{example}
    Let $h(x, y, z)=e^{5\,x-2\,y-4\,z}$. \onslide<2->{Suppose that
    \begin{align*}
      x(u, v, w) &= u^2 v & y(u, v, w) &= u+2\,vw^2 & z(u, v, w) &= u-w
    \end{align*}}
  \onslide<3->{Compute $\pderiv{h}{u}$, $\pderiv{h}{v}$, and $\pderiv{h}{w}$ when $u=2$,
    $v=1$, and $w=-1$.}
  \end{example}
  \begin{block}{\onslide<4->{Solution}}
    \onslide<5->{We may view this example diagrammatically as }%
    \onslide<6->{$\mygdiag$ }%
    \onslide<7->{where $g(u, v, w)=\sage{g}$. }%
    \onslide<8->{The Jacobian derivatives of $h$ and $g$ are}
    \begin{align*}
      \onslide<9->{Dh &=} \onslide<10->{\sage{Dh}} & 
      \onslide<11->{Dg &=} \onslide<12->{\sage{Dg}}
    \end{align*}
    \onslide<13->{Note that $(x,y,z)=(4, 4, 3)$ when $(u,v,w)=(2,1,-1)$. }%
    \onslide<14->{The multivariable chain rule implies that}
    \[
    \onslide<14->{\myDhcomps
      =} \onslide<15->{Dh(4, 4, 3)\cdot Dg(2, 1, -1)
      =} \onslide<16->{\sage{DhE}\sage{DgE}
      =} \onslide<17->{\sage{DhE * DgE}}
    \]
    \onslide<18->{when $(u,v,w)=(2,1,-1)$.}
  \end{block}

\end{frame}
\endgroup


\section{Examples with Multiple Outputs}

\begin{sagesilent}
  var('s t')
  f = matrix.column([exp(2*x-y+z), x*y*z])
  Df = jacobian(f, (x, y, z))
  DfE = Df(x=1, y=3, z=1)
  g = matrix.column([s ** 2 * t, s + 2 * t ** 2, s*t])
  Dg = jacobian(g, (s, t))
  DgE = Dg(s=1, t=1)
\end{sagesilent}
\renewcommand{\mygdiag}{
  \begin{tikzcd}[column sep=small]
    \mathbb{R}^{2}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{3}\arrow{r}{f} \pgfmatrixnextcell
    \mathbb{R}^{2}
  \end{tikzcd}}
\newcommand{\myDartmouth}{
  \left[\begin{array}{cc}
      \displaystyle\pderiv{f_1}{s} & \displaystyle\pderiv{f_1}{t} \\ \\
      \displaystyle\pderiv{f_2}{s} & \displaystyle\pderiv{f_2}{t} 
    \end{array}\right]}
\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}

  \begin{example}
    Let $f_1(x,y,z)=e^{2\,x-y+z}$ 
    \onslide<2->{and $f_2(x,y,z)=xyz$ }%
    \onslide<3->{and suppose that
    \begin{align*}
      x(s, t) &= s^2t & y(s, t) &= s+2\,t^2 & z(s, t) &= st
    \end{align*}}%
  \onslide<4->{Compute the partials of $f_1$ and $f_2$ with respect to $s$ and
    $t$ when $s=1$ and $t=1$.}
  \end{example}
  \begin{block}{\onslide<5->{Solution}}
    \onslide<5->{Consider $\mygdiag$ }%
    \onslide<6->{where $f(x,y,z)=\sage{f}$ }%
    \onslide<7->{and $g(s, t)=\sage{g}$. }%
    \onslide<8->{The Jacobian derivatives are}
    \begin{align*}
      \onslide<9->{Df &=} \onslide<10->{\sage{Df}} & 
      \onslide<11->{Dg &=} \onslide<12->{\sage{Dg}}
    \end{align*}%
    \onslide<13->{Note that $(x, y, z)=(1, 3, 1)$ when $(s, t)=(1, 1)$. }%
    \onslide<14->{The multivariable chain rule implies that}
    \[
    \onslide<15->{\myDartmouth
      =} \onslide<16->{Df(1, 3, 1) \cdot Dg(1, 1)
      =} \onslide<17->{\sage{DfE}\sage{DgE}
      =} \onslide<18->{\sage{DfE*DgE}}
    \]
    \onslide<19->{when $(s, t)=(1, 1)$.}
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  var('p1 p2 y')
  Q = matrix.column([6 * p1 ** (-2) * p2 ** (3/2) * y, 4 * p1 * p2 ** (-1) * y ** 2])
  DQ = jacobian(Q, (p1, p2, y))
  DQE = DQ(p1=6, p2=9, y=2)
  g = matrix.column([sqrt(12*t), t ** 2, t-1])
  Dg = jacobian(g, t)
  DgE = Dg(t=3)
\end{sagesilent}
\newcommand{\myEconDiag}{  
  \begin{tikzcd}[column sep=small]
    \mathbb{R}\arrow{r}{g} \pgfmatrixnextcell
    \mathbb{R}^{3}\arrow{r}{Q} \pgfmatrixnextcell
    \mathbb{R}^{2}
  \end{tikzcd}}
\newcommand{\myQComps}{
  \begin{bmatrix}
    6\,p_1^{-2}p_2^{\nicefrac{3}{2}}y \\
    4\,p_1p_2^{-1}y^2
  \end{bmatrix}}
\newcommand{\myEconG}{
  \begin{bmatrix}
    \sqrt{12\,t} \\
    t^2 \\
    t-1
  \end{bmatrix}}
\newcommand{\myEconQ}{
  \begin{bmatrix}
    \displaystyle\pderiv{q_1}{t} \\ \\
    \displaystyle\pderiv{q_2}{t}
  \end{bmatrix}}
\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}

  \begin{example}
    Consider the demand functions $q_1=6\,p_1^{-2}p_2^{\nicefrac{3}{2}}y$ and
    $q_2=4\,p_1p_2^{-1}y^2$ for two products with prices $p_1$ and $p_2$ where
    $y$ represents income. \pause Suppose that $p_1$, $p_2$, and $y$ vary over
    time via the equations
    \begin{align*}
      p_1(t) &= \sqrt{12\,t} & p_2(t) &= t^2 & y(t) &= t-1
    \end{align*}\pause
    How is demand changing with respect to time when $t=3$?
  \end{example}\pause
  \begin{block}{Solution}
    Consider $\myEconDiag$ where $Q(p_1, p_2, y)=\pause\myQComps$ and
    $g(t)=\pause\myEconG$. \pause The Jacobian derivatives are
    \begin{align*}
      DQ &= \sage{DQ} & Dg &= \sage{Dg}
    \end{align*}\pause
    Note that $(p_1, p_2, y)=(6, 9, 2)$ when $t=3$. \pause The multivariable chain rule
    then implies that 
    \[
    \myEconQ
    =\pause DQ(6, 9, 2)\cdot Dg(3)
    =\pause \sage{DQE}\sage{DgE}
    =\pause \sage{DQE*DgE}
    \]\pause
    when $t=3$.
  \end{block}
\end{frame}
\endgroup

\end{document}
