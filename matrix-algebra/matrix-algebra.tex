\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}




\title{Matrix Algebra}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  % \tableofcontents[sections={1-2}]
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1}]
    \column{.5\textwidth}
    \tableofcontents[sections={2-}]
  \end{columns}
\end{frame}


\section{Matrix Algebra}
\subsection{Notation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{$m\times n$ matrix} is an array of numbers with {\color{red}$m$
      rows} and {\color{violet}$n$ columns}.
  \end{definition}

  \pause
  \begin{block}{Note}
    We often represent matrices as
    \[
    A=
    \begin{bmatrix}
      a_{11} & a_{12} & \dotsb & a_{1n} \\
      a_{21} & a_{22} & \dotsb & a_{2n} \\
      \vdots & \vdots & \ddots & \vdots \\
      a_{m1} & a_{m2} & \dotsb & a_{mn}
    \end{bmatrix}
    \]%
    \pause
    We use the notation $a_{ij}$ or $[A]_{ij}$ to refer to the element of $A$ in
    the {\color{red}$i$th row} and {\color{violet}$j$th column}.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(-8, 0, 4, 2), (2, 1, 3, -6), (2, 23, -10, -1)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\sage{A}$. %
    \onslide<2->{Then $A$ is $3\times4$ with entries}%
    \begin{align*}
      \onslide<3->{[A]_{11} &=} \onslide<4->{\sage{A[0,0]}} & \onslide<3->{[A]_{12} &=} \onslide<4->{\sage{A[0,1]}} &
      \onslide<3->{[A]_{13} &=} \onslide<4->{\sage{A[0,2]}} & \onslide<3->{[A]_{14} &=} \onslide<4->{\sage{A[0,3]}} \\
      \onslide<5->{[A]_{21} &=} \onslide<6->{\sage{A[1,0]}} & \onslide<5->{[A]_{22} &=} \onslide<6->{\sage{A[1,1]}} &
      \onslide<5->{[A]_{23} &=} \onslide<6->{\sage{A[1,2]}} & \onslide<5->{[A]_{24} &=} \onslide<6->{\sage{A[1,3]}} \\
      \onslide<7->{[A]_{31} &=} \onslide<8->{\sage{A[2,0]}} & \onslide<7->{[A]_{32} &=} \onslide<8->{\sage{A[2,1]}} &
      \onslide<7->{[A]_{33} &=} \onslide<8->{\sage{A[2,2]}} & \onslide<7->{[A]_{34} &=} \onslide<8->{\sage{A[2,3]}}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Addition}

\begin{sagesilent}
  A = matrix([(-4, -2, -3), (-2, -1, 0)])
  B = matrix([(2, 0, 10), (-2, 22, -3)])
  C = random_matrix(ZZ, 2, 2)
  D = random_matrix(ZZ, 3, 2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{sum} of two $m\times n$ matrices $A$ and $B$ is the $m\times n$
    matrix $A+B$ whose entries are $[A+B]_{ij}=[A]_{ij}+[B]_{ij}$.
  \end{definition}

  \pause

  \begin{example}
    $\sage{A}+\sage{B}=\sage{A+B}$
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Warning}
    The sum $A+B$ is only defined if $A$ and $B$ have the same size.
  \end{block}

  \pause

  \begin{example}
    $\sage{C}+\sage{D}$ is not defined!
  \end{example}
\end{frame}

\subsection{Scalar Multiplication}


\begin{sagesilent}
  A = matrix([(2, 0, 10), (-2, 22, -3)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    For a scalar $c$ and a $m\times n$ matrix $A$, we define $c\cdot A$ as the
    $m\times n$ matrix whose entries are $[c\cdot A]_{ij}=c\cdot[A]_{ij}$.
  \end{definition}

  \pause

  \begin{example}
    $(-6)\cdot\sage{A}=\sage{-6*A}$
  \end{example}
\end{frame}

\subsection{Matrix Multiplication}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{definition}
    The \emph{product} of a $\ell\times m$ matrix $A$ and a $m\times n$ matrix
    $B$ is the $\ell\times n$ matrix $AB$ whose entries are
    \[
    [AB]_{ij}=(\textnormal{$i$th row of $A$})\cdot(\textnormal{$j$th column of $B$})
    \]
  \end{definition}

  \pause

  \begin{block}{Warning}
    The product $AB$ is only defined when
    \[
    \#\textnormal{ columns of } A = \#\textnormal{ rows of } B
    \]
  \end{block}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 2)
  B = random_matrix(ZZ, 3, 2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \[
    \input{./matmult.tex}
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The product $\sage{A}\sage{B}$ is undefined!
  \end{example}
\end{frame}

\subsection{Laws of Matrix Algebra}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    Matrix arithmetic obeys the following laws.
    \begin{description}
    \item<2->[additive associativity] $A+(B+C)=(A+B)+C$
    \item<3->[multiplicative associativity] $(AB)C=A(BC)$
    \item<4->[additive commutativity] $A+B=B+A$
    \item<5->[left distributive] $A(B+C)=AB+AC$
    \item<6->[right distributive] $(A+B)C=AC+BC$
    \end{description}
  \end{block}
\end{frame}

\begin{sagesilent}
  A = matrix(ZZ, 2, [1, 0, 0, 0])
  B = matrix(ZZ, 2, [0, 1, 0, 0])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \onslide<2->{\sage{A}\sage{B} &=} \onslide<3->{\sage{A*B}} &
      \onslide<4->{\sage{B}\sage{A} &=} \onslide<5->{\sage{B*A}}
    \end{align*}
  \end{example}

  \begin{block}{\onslide<6->{Important Observation}}
    \onslide<6->{In general, $AB\neq BA$. Matrix multiplication is not
      commutative!}
  \end{block}
\end{frame}

\subsection{Transpose}

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 2)
  B = random_matrix(ZZ, 3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{transpose} of a $m\times n$ matrix $A$ is the $n\times m$ matrix
    $A^\intercal$ formed by interchanging the rows and columns of $A$.
  \end{definition}

  \onslide<2->{
    \begin{example}}
    \onslide<2->{Note that}
    \begin{align*}
      \onslide<2->{A &= \sage{A} & A^\intercal &=} \onslide<3->{\sage{A.transpose()}} \\
      \onslide<4->{B &= \sage{B} & B^\intercal &=} \onslide<5->{\sage{B.transpose()}} 
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties}
    Matrix transposition satisfies the following properties.
    \begin{description}
    \item<2->[additivity] $(A+B)^\intercal=A^\intercal+B^\intercal$
    \item<3->[involution] $(A^\intercal)^\intercal=A$
    \item<4->[scalar linearity] $(c\cdot A)^\intercal=c\cdot A^\intercal$
    \item<5->[reverse multiplicative] $(AB)^\intercal=B^\intercal A^\intercal$
    \end{description}
  \end{block}
\end{frame}

\subsection{Motivation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How is all of this relevant?  
  \end{block}
  \begin{block}{\onslide<2->{Answer}}
    \onslide<2->{The system}
    \onslide<2->{{\footnotesize
        \[
        \begin{array}{ccccccccc}
          a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
          a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
          \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
          a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
        \end{array}
        \]}}%
    \onslide<2->{can be written as $A\vv{x}=\vv{b}$ where}
    \begin{align*}
      \onslide<3->{A &=
        \begin{bmatrix}
          a_{11}  & a_{12}  & \dotsb & a_{1n}  \\
          a_{21}  & a_{22}  & \dotsb & a_{2n}  \\
          \vdots & \vdots & \ddots & \vdots \\
          a_{m1}  & a_{m2} & \dotsb & a_{mn}
        \end{bmatrix}} &
      \onslide<4->{\vv{x} &=
        \begin{bmatrix}
          x_1\\ x_2\\ \vdots\\ x_n
        \end{bmatrix}} &
      \onslide<5->{\vv{b} &=
        \begin{bmatrix}
          b_1\\ b_2\\ \vdots\\ b_m
        \end{bmatrix}}
    \end{align*}
  \end{block}
\end{frame}


\section{Special Kinds of Matrices}
\subsection{Square Matrices}


\begin{sagesilent}
  A = random_matrix(ZZ, 2)
  B = random_matrix(ZZ, 3)
  C = random_matrix(ZZ, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix is \emph{square} if it is of size $n\times n$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{A} && \sage{B} && \sage{C}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Column and Row Matrices}


\begin{sagesilent}
  c1 = random_matrix(ZZ, 2, 1)
  c2 = random_matrix(ZZ, 3, 1)
  c3 = random_matrix(ZZ, 4, 1)
  c4 = random_matrix(ZZ, 5, 1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{column matrix} is a $n\times 1$ matrix. 
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{c1} && \sage{c2} && \sage{c3} && \sage{c4}
    \end{align*}
  \end{example}
\end{frame}


\begin{sagesilent}
  r1 = random_matrix(ZZ, 1, 2)
  r2 = random_matrix(ZZ, 1, 3)
  r3 = random_matrix(ZZ, 1, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{row matrix} is a $1\times n$ matrix. 
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{r1} && \sage{r2} && \sage{r3}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Diagonal Matrices}


\begin{sagesilent}
  D1 = matrix.diagonal([ZZ.random_element() for x in range(2)])
  D2 = matrix.diagonal([ZZ.random_element() for x in range(3)])
  D3 = matrix.diagonal([ZZ.random_element() for x in range(4)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{diagonal matrix} is a square matrix $A$ whose entries satisfy
    $[A]_{ij}=0$ if $i\neq j$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{D1} && \sage{D2} && \sage{D3}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Upper and Lower Triangular}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{upper triangular matrix} is a square matrix $A$ whose entries
    satisfy $[A]_{ij}=0$ if $i>j$.
  \end{definition}

  \onslide<2->{
    \begin{example}}
    \begin{align*}
      \input{./upper1.tex} && \input{./upper2.tex}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{lower triangular matrix} is a square matrix $A$ whose entries
    satisfy $[A]_{ij}=0$ if $i<j$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \input{./lower1.tex} && \input{./lower2.tex}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Symmetric Matrices}

\begin{sagesilent}
  Q3 = random_quadraticform(ZZ, 3, [-10,10]).matrix()
  Q4 = random_quadraticform(ZZ, 4, [-10,10]).matrix()
  Q5 = random_quadraticform(ZZ, 5, [-10,10]).matrix()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $A$ is \emph{symmetric} if $A^\intercal=A$.
  \end{definition}

  \pause

  \begin{example}
    {\scriptsize
      \begin{align*}
        \sage{Q3} && \sage{Q4} && \sage{Q5}
      \end{align*}}
  \end{example}
\end{frame}


\subsection{Zero Matrices}

\begin{sagesilent}
  A = zero_matrix(3)
  B = zero_matrix(3, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$m\times n$ zero matrix} is the matrix $O_{m\times n}$ whose
    entries are all zeros.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      O_{3\times 3} &= \sage{A} & O_{3\times 4} &= \sage{B}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Identity Matrices}

\begin{sagesilent}
  I2 = identity_matrix(2)
  I3 = identity_matrix(3)
  I4 = identity_matrix(4)
  A = random_matrix(ZZ, 2, 3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$n\times n$ identity matrix} is the matrix $I_n$ whose entries satisfy
    \[
    [I_n]_{ij}
    =
    \begin{cases}
      1 & i=j \\
      0 & i\neq j
    \end{cases}
    \]
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      I_2 &= \sage{I2} &
      I_3 &= \sage{I3} &
      I_4 &= \sage{I4} 
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every $m\times n$ matrix $A$ satisfies $A=AI_n=I_mA$.
  \end{theorem}

  \pause

  \begin{example}
    \begin{align*}
      \sage{A}
      &= \sage{A}\sage{I3} \\
      &= \sage{I2}\sage{A}
    \end{align*}
  \end{example}
\end{frame}



\subsection{Idempotent Matrices}

\begin{sagesilent}
  P3 = random_matrix(ZZ, 3, algorithm='unimodular', upper_bound=10)
  D3 = matrix.diagonal([1,0,1])
  A3= P3*D3*P3.inverse()
  P4 = random_matrix(ZZ, 4, algorithm='unimodular', upper_bound=10)
  D4 = matrix.diagonal([1,0,1,1])
  A4= P4*D4*P4.inverse()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{idempotent matrix} is a matrix that satisfies $A^2=A$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{A3} && \sage{A4}
    \end{align*}
  \end{example}
\end{frame}



\subsection{Permutation Matrices}

\begin{sagesilent}
  rows2 = identity_matrix(2).rows()
  rows3 = identity_matrix(3).rows()
  rows4 = identity_matrix(4).rows()
  shuff2 = choice(Permutations(rows2))
  shuff3 = choice(Permutations(rows3))
  shuff4 = choice(Permutations(rows4))
  P2 = matrix(list(shuff2))
  P3 = matrix(list(shuff3))
  P4 = matrix(list(shuff4))
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{permutation matrix} is a matrix obtained by performing row-swaps on
    $I_n$.
  \end{definition}

  \pause

  \begin{example}
    \begin{align*}
      \sage{P2} && \sage{P3} && \sage{P4}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Full Rank Matrices}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 5, algorithm='echelonizable', rank=2)
  B = random_matrix(ZZ, 5, 3, algorithm='echelonizable', rank=3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix $A$ has \emph{full rank} if $\rank(A)=\#\myrows(A)$ or if
    $\rank(A)=\#\mycolumns(A)$.
  \end{definition}

  \pause

  \begin{example}
    Suppose that
    \begin{align*}
      \rref(A) &= \sage{A.rref()} &
      \rref(B) &= \sage{B.rref()}
    \end{align*}

    \pause

    Then $A$ \emph{does not} have full rank but $B$ \emph{does} have full rank.
  \end{example}
\end{frame}




\subsection{Nonsingular Matrices}

\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='echelonizable', rank=2)
  B = random_matrix(ZZ, 4, algorithm='echelonizable', rank=4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{nonsingular matrix} is a square matrix with full rank. A matrix is
    \emph{singular} if it is a square matrix that is not nonsingular.
  \end{definition}

  \onslide<2->{
    \begin{example}
      Suppose that
      \begin{align*}
        \rref(A) &= \sage{A.rref()} &
        \rref(B) &= \sage{B.rref()} 
      \end{align*}}
    \onslide<3->{Then $A$ is singular and $B$ is nonsingular.}
  \end{example}
\end{frame}


\section{Elementary Matrices}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    There are three elementary row operations:
    \begin{description}
    \item<2->[row switching] $R_i\leftrightarrow R_j$
    \item<3->[row multiplication] $c\cdot R_i\to R_i$ for $c\neq 0$
    \item<4->[row addition] $R_i+c\cdot R_j\to R_i$
    \end{description}
  \end{block}

  \onslide<5->{
    \begin{definition}
      A \emph{$n\times n$ elementary matrix} is a matrix obtained by performing
      exactly one elementary row operation on $I_n$.
    \end{definition}}
\end{frame}


\begin{sagesilent}
  I2 = identity_matrix(2)
  I3 = identity_matrix(3)
  I4 = identity_matrix(4)
  E2 = elementary_matrix(2, row1=1, scale=-7)
  E3 = elementary_matrix(3, row1=1, row2=0, scale=-1/3)
  E4 = elementary_matrix(4, row1=1, row2=3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{align*}
      \onslide<1->{\sage{I2} &\xrightarrow{-7\cdot R_2\to R_2}}                    \onslide<2->{\underbrace{\sage{E2}}_{[-7\cdot R_2\to R_2]}} \\
      \onslide<3->{\sage{I3} &\xrightarrow{R_2-(\nicefrac{1}{3})\cdot R_1\to R_2}} \onslide<4->{\underbrace{\sage{E3}}_{[R_2-(\nicefrac{1}{3})\cdot R_1\to R_2]}} \\
      \onslide<5->{\sage{I4} &\xrightarrow{R_2\leftrightarrow R_4}}                \onslide<6->{\underbrace{\sage{E4}}_{[R_2\leftrightarrow R_4]}}
    \end{align*}
  \end{example}
\end{frame}

\subsection{Row Operations}


\begin{sagesilent}
  E = elementary_matrix(3, row1=2, row2=0, scale=-4)
  A = random_matrix(ZZ, 3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Multiplication by an elementary matrix on the left of a matrix results in
    performing the corresponding elementary row-operation.
  \end{theorem}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrices
    \begin{align*}
      E &= [R_3-4\cdot R_1\to R_3] & A &= \sage{A} \\
      &= \sage{E}
    \end{align*}
    \pause
    Then 
    \[
    EA=\sage{E}\sage{A}=\sage{E*A}
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    To compute $\rref(A)$ we perform elementary row operations. Each operation
    corresponds to multiplication on the left by an elementary matrix. This
    gives 
    \[
    E_r\dotsb E_3E_2E_1A=\rref(A)
    \]
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([[1,2,4],[3,5,-1]])
  E1 = elementary_matrix(2, row1=1, row2=0, scale=-3)
  E2 = elementary_matrix(2, row1=1, scale=-1)
  E3 = elementary_matrix(2, row1=0, row2=1, scale=-2)
  I2 = identity_matrix(2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix
    \[
    A=\sage{A}
    \]
    Find elementary matrices $E_1,E_2,\dotsc,E_r$ satisfying $E_r\dotsb
    E_2E_1A=\rref(A)$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution}
    Row reduce
    \begin{align*}
      \sage{A}
      &\onslide<2->{\xrightarrow{R_2-3\cdot R_1\to R_2}  \sage{E1*A}} \\
      &\onslide<3->{\xrightarrow{-R_2\to R_2}            \sage{E2*E1*A}} \\
      &\onslide<4->{\xrightarrow{R_1-2\cdot R_2\to R_1}  \sage{E3*E2*E1*A}}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    These row reductions correspond to elementary matrices.
    \begin{align*}
      \onslide<2->{\sage{I2}&\xrightarrow{R_2-3\cdot R_1\to R_2}\sage{E1}=E_1} \\ 
      \onslide<3->{\sage{I2}&\xrightarrow{-R_2\to R_2}\sage{E2}=E_2} \\ 
      \onslide<4->{\sage{I2}&\xrightarrow{R_1-2\cdot R_2\to R_1}\sage{E3}=E_3}
    \end{align*}
    \onslide<5->{This gives $E_3E_2E_1A=\rref(A)$.}
  \end{block}
\end{frame}


\end{document}
