\documentclass[]{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Definiteness of Quadratic Forms}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Basic Terminology}
\subsection{Review of Quadratic Forms}


\newcommand{\myQuadFormsText}{
  \begin{array}{c}
    \textnormal{Quadratic Forms} \\
    \textnormal{on }\mathbb{R}^{n}
  \end{array}}
\newcommand{\mySymmText}{
  \begin{array}{c}
    n\times n\textnormal{ Symmetric} \\
    \textnormal{Matrices}
  \end{array}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    There is a correspondance
    \[
    \Set*{\myQuadFormsText}\leftrightarrow\pause
    \Set*{\mySymmText}
    \]\pause
    Every quadratic form $Q(x_1,\dotsc,x_n)$ is of the form
    $Q(\vv{x})=\vv{x}^\intercal A\vv{x}$ for some symmetric matrix $A$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(4794729)
  M = random_matrix(ZZ, 3, x=-3, y=3)
  N = random_matrix(ZZ, 3, x=-3, y=3)
  A = M.T*M-N.T*N
  var('x1 x2 x3')
  x = vector([x1, x2, x3])
  Q = expand(x*A*x)
  vvx = matrix.column([x1, x2, x3])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The quadratic form
    \[
    Q(x_1,x_2,x_3)=\sage{Q}
    \]
    \onslide<2->{is of the form $Q(\vv{x})=}\onslide<3->{\vv{x}^\intercal
      A\vv{x}$ where}
    \begin{align*}
      \onslide<4->{A &=} \onslide<5->{\sage{A}} & 
      \onslide<6->{\vv{x} &=} \onslide<7->{\sage{vvx}}
    \end{align*}
  \end{example}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Every quadratic form satisfies $Q(\vv{O})=\vv{O}^\intercal A\vv{O}=0$.
  \end{block}

  \pause
  \begin{block}{Question}
    Is $0$ a maximum value of $Q$? \pause Is $0$ a minimum value of $Q$? \pause
    Is $0$ neither a maximum nor a minimum value of $Q$?
  \end{block}
\end{frame}



\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A quadratic form $Q$ is
    \begin{description}
    \item<2->[positive definite] if $Q(\vv{x})>0$ for $\vv{x}\neq\vv{O}$
    \item<3->[negative definite] if $Q(\vv{x})<0$ for $\vv{x}\neq\vv{O}$
    \item<4->[positive semidefinite] if $Q(\vv{x})\geq0$ for $\vv{x}\neq\vv{O}$
    \item<5->[negative semidefinite] if $Q(\vv{x})\leq0$ for $\vv{x}\neq\vv{O}$
    \end{description}
    \onslide<6->{Otherwise, $Q$ is called \emph{indefinite}.}
  \end{definition}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  A = identity_matrix(2)
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider
    \[
    \begin{array}{ccc}
      Q(x_1,x_2)=\sage{Q} & \longleftrightarrow & \onslide<2->{A = \sage{A}}
    \end{array}
    \]
    \onslide<3->{Note that for $\vv{x}\neq\vv{O}$}
    \[
    \onslide<3->{Q(x_1,x_2)=\sage{Q}}\onslide<4->{>0}
    \]
    \onslide<5->{This quadratic form is} \onslide<6->{\emph{positive definite}.}
  \end{example}
\end{frame}


\begin{sagesilent}
  A = -identity_matrix(2)
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider
    \[
    \begin{array}{ccc}
      Q(x_1,x_2)=\sage{Q} & \longleftrightarrow & \onslide<2->{A = \sage{A}}
    \end{array}
    \]
    \onslide<3->{Note that for $\vv{x}\neq\vv{O}$}
    \[
    \onslide<3->{Q(x_1,x_2)=\sage{Q}}\onslide<4->{<0}
    \]
    \onslide<5->{This quadratic form is} \onslide<6->{\emph{negative definite}.}
  \end{example}
\end{frame}



\begin{sagesilent}
  A = ones_matrix(2)
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider
    \[
    \begin{array}{ccc}
      Q(x_1,x_2)=\sage{Q} & \longleftrightarrow & \onslide<2->{A = \sage{A}}
    \end{array}
    \]
    \onslide<3->{Note that}
    \[
    \onslide<3->{Q(x_1,x_2)
    = \sage{Q}
    = (x_1+x_2)^2}
  \onslide<4->{\geq 0}
    \]
    \onslide<5->{This quadratic form is } \onslide<6->{\emph{positive
        semidefinite}.}
  \end{example}
\end{frame}



\begin{sagesilent}
  A = -ones_matrix(2)
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider
    \[
    \begin{array}{ccc}
      Q(x_1,x_2)=\sage{Q} & \longleftrightarrow & \onslide<2->{A = \sage{A}}
    \end{array}
    \]
    \onslide<3->{Note that}
    \[
    \onslide<3->{Q(x_1,x_2)
    = \sage{Q}
    = -(x_1+x_2)^2}
  \onslide<4->{\leq 0}
    \]
    \onslide<5->{This quadratic form is }%
    \onslide<6->{\emph{negative semidefinite}.}
  \end{example}
\end{frame}




\begin{sagesilent}
  A = matrix([(0, 1/2), (1/2, 0)])
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
  a0, b0 = 1, -1
  a1, b1 = 1, 1
  p0 = (a0, b0)
  p1 = (a1, b1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider
    \[
    \begin{array}{ccc}
      Q(x_1,x_2)=\sage{Q} & \longleftrightarrow & \onslide<2->{A = \sage{A}}
    \end{array}
    \]
    \onslide<3->{Note that
    \begin{align*}
      Q\sage{p0} &= \sage{Q(x1=a0, x2=b0)}<0 & Q\sage{p1} &= \sage{Q(x1=a1, x2=b1)} >0
    \end{align*}
    }
    \onslide<4->{This quadratic form is }\onslide<5->{\emph{indefinite}.}
  \end{example}
\end{frame}




\section{Principal Minors and Definiteness}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we determine the definiteness of a given quadratic form?
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Let $A$ be a $n\times n$ matrix.

  \pause

  \begin{definition}
    The $k$th \emph{order principal submatrices} of $A$ are the submatrices
    obtained by deleting $n-k$ rows and the same $n-k$ columns.
  \end{definition}

  \pause

  \begin{definition}
    The $k$th \emph{order principal minors} of $A$ are the determinants of the
    $k$th order principal submatrices of $A$.
  \end{definition}
\end{frame}



\newcommand{\myA}{
  \left[\begin{array}{rrr}
      a_{11} & a_{12} & a_{13} \\
      a_{21} & a_{22} & a_{23} \\
      a_{31} & a_{32} & a_{33}
    \end{array}\right]}
\newcommand{\myMbc}{
  \left\lvert\begin{array}{rrr}
      \semitransp[100]{a_{11}} & \semitransp[30]{a_{12}} & \semitransp[30]{a_{13}} \\
      \semitransp[30]{a_{21}}  & \semitransp[30]{a_{22}} & \semitransp[30]{a_{23}} \\
      \semitransp[30]{a_{31}}  & \semitransp[30]{a_{32}} & \semitransp[30]{a_{33}}
    \end{array}\right\rvert}
\newcommand{\myMac}{
  \left\lvert\begin{array}{rrr}
      \semitransp[30]{a_{11}} & \semitransp[30]{a_{12}} & \semitransp[30]{a_{13}} \\
      \semitransp[30]{a_{21}}  & \semitransp[100]{a_{22}} & \semitransp[30]{a_{23}} \\
      \semitransp[30]{a_{31}}  & \semitransp[30]{a_{32}} & \semitransp[30]{a_{33}}
    \end{array}\right\rvert}
\newcommand{\myMab}{
  \left\lvert\begin{array}{rrr}
      \semitransp[30]{a_{11}} & \semitransp[30]{a_{12}} & \semitransp[30]{a_{13}} \\
      \semitransp[30]{a_{21}}  & \semitransp[30]{a_{22}} & \semitransp[30]{a_{23}} \\
      \semitransp[30]{a_{31}}  & \semitransp[30]{a_{32}} & \semitransp[100]{a_{33}}
    \end{array}\right\rvert}

\newcommand{\myMc}{
  \left\lvert\begin{array}{rrr}
      \semitransp[100]{a_{11}} & \semitransp[100]{a_{12}} & \semitransp[30]{a_{13}} \\
      \semitransp[100]{a_{21}}  & \semitransp[100]{a_{22}} & \semitransp[30]{a_{23}} \\
      \semitransp[30]{a_{31}}  & \semitransp[30]{a_{32}} & \semitransp[30]{a_{33}}
    \end{array}\right\rvert}
\newcommand{\myMb}{
  \left\lvert\begin{array}{rrr}
      \semitransp[100]{a_{11}} & \semitransp[30]{a_{12}} & \semitransp[100]{a_{13}} \\
      \semitransp[30]{a_{21}}  & \semitransp[30]{a_{22}} & \semitransp[30]{a_{23}} \\
      \semitransp[100]{a_{31}}  & \semitransp[30]{a_{32}} & \semitransp[100]{a_{33}}
    \end{array}\right\rvert}
\newcommand{\myMa}{
  \left\lvert\begin{array}{rrr}
      \semitransp[30]{a_{11}} & \semitransp[30]{a_{12}} & \semitransp[30]{a_{13}} \\
      \semitransp[30]{a_{21}}  & \semitransp[100]{a_{22}} & \semitransp[100]{a_{23}} \\
      \semitransp[30]{a_{31}}  & \semitransp[100]{a_{32}} & \semitransp[100]{a_{33}}
    \end{array}\right\rvert}
\newcommand{\myM}{
  \left\lvert\begin{array}{rrr}
      \semitransp[100]{a_{11}} & \semitransp[100]{a_{12}} & \semitransp[100]{a_{13}} \\
      \semitransp[100]{a_{21}}  & \semitransp[100]{a_{22}} & \semitransp[100]{a_{23}} \\
      \semitransp[100]{a_{31}}  & \semitransp[100]{a_{32}} & \semitransp[100]{a_{33}}
    \end{array}\right\rvert}

\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{block}{First-Order Principal Minors of $A={\tiny\myA}$}
    \begin{align*}
      \onslide<2->{M_{\Set{2, 3}} &=} \onslide<3->{\myMbc} &
      \onslide<4->{M_{\Set{1, 3}} &=} \onslide<5->{\myMac} &
      \onslide<6->{M_{\Set{1, 2}} &=} \onslide<7->{\myMab}
    \end{align*}
  \end{block}

  \begin{block}{\onslide<8->{Second-Order Principal Minors}}
    \begin{align*}
      \onslide<9->{M_{\Set{3}} &=} \onslide<10->{\myMc} &
      \onslide<11->{M_{\Set{2}} &=} \onslide<12->{\myMb} &
      \onslide<13->{M_{\Set{1}} &=} \onslide<14->{\myMa}
    \end{align*}
  \end{block}
\end{frame}
\endgroup



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The $k$th \emph{order NW principal submatrix of $A$} is the matrix obtained
    by deleting the last $n-k$ rows and columns.
  \end{definition}

  \pause

  \begin{definition}
    The $k$th \emph{order NW principal minor of $A$} is the determinant of the
    $k$th NW principal submatrix of $A$.
  \end{definition}

  \pause

  \begin{block}{Note}
    Simon \& Blume call the NW principal minors the ``leading principal
    minors.''
  \end{block}
\end{frame}



\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The NW principal minors of $A=\myA$ are
    \begin{align*}
      \onslide<2->{M_{\Set{2,3}}  &=}  \onslide<3->{\myMbc} &
      \onslide<4->{M_{\Set{3}}    &=} \onslide<5->{\myMc} &
      \onslide<6->{M_{\varnothing} &=} \onslide<7->{\myM}
    \end{align*}
  \end{example}
\end{frame}
\endgroup


\begin{sagesilent}
  A = matrix([(27, -1, -1), (1, -18, 0), (1, 3, 1)])
  l23 = [0]
  l13 = [1]
  l12 = [2]
  l3 = [0, 1]
  l2 = [0, 2]
  l1 = [1, 2]
  A23 = A[l23, l23]
  A13 = A[l13, l13]
  A12 = A[l12, l12]
  A3 = A[l3, l3]
  A2 = A[l2, l2]
  A1 = A[l1, l1]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{NW Principal Minors of $A=\mysage{\tiny}{A}$}
    \begin{align*}
      \onslide<2->{\det\sage{A23} &=} \onslide<3->{\sage{A23.det()}} &
      \onslide<4->{\det\sage{A3}  &=} \onslide<5->{\sage{A3.det()}}  &
      \onslide<6->{\det\sage{A}   &=} \onslide<7->{\sage{A.det()}}
    \end{align*}
  \end{block}


  \begin{block}{\onslide<8->{Other First-Order Principal Minors}}
    \begin{align*}
      \onslide<9->{\det\sage{A13} &=} \onslide<10->{\sage{A13.det()}} &
      \onslide<11->{\det\sage{A12} &=} \onslide<12->{\sage{A12.det()}}
    \end{align*}
  \end{block}

  \begin{block}{\onslide<13->{Other Second-Order Principal Minors}}
    \begin{align*}
      \onslide<14->{\det\sage{A1} &=} \onslide<15->{\sage{A1.det()}} &
      \onslide<16->{\det\sage{A2} &=} \onslide<17->{\sage{A2.det()}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\subsection{Definiteness Theorem}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 16.1 in Simon \& Blume]
    Let $A$ be a symmetric matrix. Then $A$ is
    \begin{description}
    \item<2->[positive definite] if and only if all NW principal minors are $>0$.
    \item<3->[negative definite] if and only if all $k$th-order NW principal minors
      have sign $(-1)^k$.
    \item<4->[positive semidefinite] if and only if all principal minors are $\geq
      0$.
    \item<5->[negative semidefinite] if and only if all $k$th-order principal minors
      have sign $(-1)^k$ or $0$.
    \end{description}
  \end{theorem}


  \begin{block}{\onslide<6->{Strategy}}
    \onslide<7->{To check the definiteness of a quadratic form corresponding to
      a symmetric matrix $A$, check the NW principal minors first. }%
    \onslide<8->{Then check \emph{all} principal minors, if necessary.}
  \end{block}
\end{frame}
\endgroup


\subsection{Examples}

\begin{sagesilent}
  A = matrix([(1, 2), (2, 5)])
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
  A2 = A[[0], [0]]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine the definiteness of $Q(x_1,x_2)=\sage{Q}$.
  \end{example}

  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{This quadratic form corresponds to $A=\sage{A}$. }%
    \onslide<3->{The NW principal minors are}
    \begin{align*}
      \onslide<4->{\det\sage{A2} &=} \onslide<5->{\sage{A2.det()}} \onslide<6->{>0} &
      \onslide<7->{\det\sage{A} &=} \onslide<8->{\sage{A.det()}}\onslide<9->{>0}
    \end{align*}
    \onslide<10->{It follows that $Q$ is }\onslide<11->{\emph{positive definite}.}
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(-1, 1), (1, -4)])
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
  A2 = A[[0],[0]]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine the definiteness of $Q(x_1,x_2)=\sage{Q}$.
  \end{example}

  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{This quadratic form corresponds to $A=\sage{A}$. }%
    \onslide<3->{The NW principal minors are}
    \begin{align*}
      \onslide<4->{\det\sage{A2} &=} \onslide<5->{\sage{A2.det()}}\onslide<6->{<0} &
      \onslide<7->{\det\sage{A} &=} \onslide<8->{\sage{A.det()}}\onslide<9->{>0}
    \end{align*}
    \onslide<10->{It follows that $Q$ is }\onslide<11->{\emph{negative
        definite}.}
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(2, -2, 0), (-2, 6, -4), (0, -4, 4)]) / 2
  l23 = [0]
  l13 = [1]
  l12 = [2]
  l3 = [0, 1]
  l2 = [0, 2]
  l1 = [1, 2]
  A23 = A[l23, l23]
  A13 = A[l13, l13]
  A12 = A[l12, l12]
  A3 = A[l3, l3]
  A2 = A[l2, l2]
  A1 = A[l1, l1]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Definiteness of $A=\mysage{\tiny}{A}$}
    \onslide<2->{The NW principal minors of $A$ are}
    \begin{align*}
      \onslide<3->{\det\sage{A23} &=} \onslide<4->{\sage{A23.det()}} &
      \onslide<5->{\det\sage{A3}  &=} \onslide<6->{\sage{A3.det()}}  &
      \onslide<7->{\det\sage{A}   &=} \onslide<8->{\sage{A.det()}}
    \end{align*}
    \onslide<9->{The other first-order principal minors of $A$ are}
    \begin{align*}
      \onslide<10->{\det\sage{A13} &=} \onslide<11->{\sage{A13.det()}} &
      \onslide<12->{\det\sage{A12} &=} \onslide<13->{\sage{A12.det()}}
    \end{align*}
    \onslide<14->{The other second-order principal minors of $A$ are}
    \begin{align*}
      \onslide<15->{\det\sage{A2} &=} \onslide<16->{\sage{A2.det()}} &
      \onslide<17->{\det\sage{A1} &=} \onslide<18->{\sage{A1.det()}}
    \end{align*}
    \onslide<19->{Thus $A$ is }\onslide<20->{\emph{positive semidefinite}.}
  \end{block}
\end{frame}
\endgroup



\begin{sagesilent}
  A = matrix([(-5, 1, 2), (1, -1, 0), (2, 0, -1)])
  l23 = [0]
  l13 = [1]
  l12 = [2]
  l3 = [0, 1]
  l2 = [0, 2]
  l1 = [1, 2]
  A23 = A[l23, l23]
  A13 = A[l13, l13]
  A12 = A[l12, l12]
  A3 = A[l3, l3]
  A2 = A[l2, l2]
  A1 = A[l1, l1]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Definiteness of $A=\mysage{\tiny}{A}$}
    \onslide<2->{The NW principal minors of $A$ are}
    \begin{align*}
      \onslide<3->{\det\sage{A23} &=} \onslide<4->{\sage{A23.det()}} &
      \onslide<5->{\det\sage{A3}  &=} \onslide<6->{\sage{A3.det()}}  &
      \onslide<7->{\det\sage{A}   &=} \onslide<8->{\sage{A.det()}}
    \end{align*}
    \onslide<9->{The other first-order principal minors of $A$ are}
    \begin{align*}
      \onslide<10->{\det\sage{A13} &=} \onslide<11->{\sage{A13.det()}} &
      \onslide<12->{\det\sage{A12} &=} \onslide<13->{\sage{A12.det()}}
    \end{align*}
    \onslide<14->{The other second-order principal minors of $A$ are}
    \begin{align*}
      \onslide<15->{\det\sage{A2} &=} \onslide<16->{\sage{A2.det()}} &
      \onslide<17->{\det\sage{A1} &=} \onslide<18->{\sage{A1.det()}}
    \end{align*}
    \onslide<19->{Thus $A$ is }\onslide<20->{\emph{negative semidefinite}.}
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  A = matrix([(2, 2, 3), (2, 2, 7), (3, 7, 0)])
  l23 = [0]
  l13 = [1]
  l12 = [2]
  l3 = [0, 1]
  l2 = [0, 2]
  l1 = [1, 2]
  A23 = A[l23, l23]
  A13 = A[l13, l13]
  A12 = A[l12, l12]
  A3 = A[l3, l3]
  A2 = A[l2, l2]
  A1 = A[l1, l1]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Definiteness of $A=\mysage{\tiny}{A}$}
    \onslide<2->{The NW principal minors of $A$ are}
    \begin{align*}
      \onslide<3->{\det\sage{A23} &=} \onslide<4->{\sage{A23.det()}} &
      \onslide<5->{\det\sage{A3}  &=} \onslide<6->{\sage{A3.det()}}  &
      \onslide<7->{\det\sage{A}   &=} \onslide<8->{\sage{A.det()}}
    \end{align*}
    \onslide<9->{Thus $A$ is }\onslide<10->{\emph{indefinite}.}
  \end{block}
\end{frame}
\endgroup



\section{Linear Constraints and Bordered Matrices}
\subsection{Motivation}

\newcommand{\mySystem}{
  \begin{array}{ccccccccc}
    b_{11}x_1 & + & b_{12}x_2 & + & \dotsb & + & b_{1n}x_n & = & 0      \\
    b_{21}x_1 & + & b_{22}x_2 & + & \dotsb & + & b_{2n}x_n & = & 0      \\
    \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
    b_{m1}x_1 & + & b_{m2}x_2 & + & \dotsb & + & b_{mn}x_n & = & 0
  \end{array}\tag{$\ast$}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Suppose that $Q$ is positive definite. \pause Then $Q$ has a global minimum at
    $\vv{x}=\vv{O}$. \pause Likewise, if $Q$ is negative definite, then $Q$ has a
    global maximum at $\vv{x}=\vv{O}$.
  \end{block}

  \pause
  \begin{block}{Question}
    Let $Q(x_1,\dotsc,x_n)$ be a quadratic form \pause and consider the constraints
    \begin{equation}
      \mySystem\label{eq:constraints}
    \end{equation}\pause
    Does the definiteness of $Q(x_1,\dotsc,x_n)$ change if we impose the
    constraints in \eqref{eq:constraints}?
  \end{block}
\end{frame}

\begin{sagesilent}
  var('x1 x2')
  A = matrix([(1, -3), (-3, 4)])
  x = vector([x1, x2])
  Q = expand(x*A*x)
  f = x1+x2
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the quadratic form $Q(x_1, x_2) = \sage{Q}$ \pause and the
    constraint
    \begin{equation}
      \sage{f}=0\label{eq:anotherconstraint}
      \tag{$\ast$}
    \end{equation}\pause
    The quadratic form $Q$ is indefinite, \pause and thus has no maximum value and no
    minimum value. \pause Imposing the constraint \eqref{eq:anotherconstraint} gives
    \[
    Q(x_1, x_2)
    = \sage{Q(x1=x1, x2=-x1)}
    \]\pause
    So, $Q$ is \emph{positive definite} when the constraint
    \eqref{eq:anotherconstraint} is imposed.
  \end{example}
\end{frame}


\subsection{Forms on $\mathbb{R}^{2}$ Subject to One Linear Constraint}

\begin{sagesilent}
  import quadform
  var('alpha beta a11 a12 a21 a22 x1 x2')
  A = matrix(2, [a11, a12, a12, a22])
  Q = QuadraticForm(A)
  # p = quadform.reindex_quadform(Q)
  p = a11*x1**2+2*a12*x1*x2+a22*x2**2
  f = alpha*x1+beta*x2
  B = matrix([alpha, beta])
  z = zero_matrix(1)
  H = block_matrix([[z, B], [B.transpose(), A]])
  newquad = factor(p(x1=-beta*x2/alpha)(x2=1))
  brandnewquad = factor(p(x2=-alpha*x1/beta)(x1=1))
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation (Theorem 16.3 in Simon \& Blume)}
    \onslide<2->{Consider the quadratic form }%
    \onslide<4->{and constraint}
    \begin{align*}
      \onslide<3->{Q(x_1, x_2) &= \sage{p}} & 
      \onslide<5->{\sage{f} &= 0}
    \end{align*}
    \onslide<6->{Imposing the constraint gives}
    \[
    \onslide<7->{Q(x_1, x_2) 
      =} \onslide<8->{Q\left(-\frac{\beta}{\alpha}\,x_2, x_2\right) 
      =} \onslide<9->{\sage{newquad}x_2^2}
    \]
    \onslide<10->{The definiteness of the constrained quadratic form $Q$ is
      determined by the signature of}
    \[
    \onslide<11->{\sage{newquad*alpha**2}=}\onslide<12->{-\det\sage{H}}
    \]
  \end{block}

\end{frame}
\endgroup



\begin{sagesilent}
  A = matrix(QQ, [(-1, -15), (-15, -2)])
  B = matrix([1,-2])
  z = zero_matrix(B.nrows())
  H = block_matrix([[z, B],[B.T, A]])
  var('x1 x2')
  x = vector([x1, x2])
  Q = expand(x*A*x)
  l = (B*x)[0]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \onslide<2->{Consider the quadratic form }%
    \onslide<4->{and constraint}
    \begin{align*}
      \onslide<3->{Q(x_1, x_2) &= \sage{Q}} &
      \onslide<5->{\sage{l} &= 0}
    \end{align*}
    \onslide<6->{Note that}
    \[
    \onslide<7->{-\det\sage{H} =} \onslide<8->{-\sage{H.det()}} \onslide<9->{< 0}
    \]
    \onslide<10->{Thus the constrained quadratic form is
    }\onslide<11->{\emph{negative definite}.}
  \end{example}
\end{frame}



\subsection{Forms on $\mathbb{R}^{n}$ Subject to $m$ Linear Constraints}


\newcommand{\myx}{
  \begin{bmatrix}
    x_1 \\ x_2 \\ \vdots \\ x_n
  \end{bmatrix}}
\newcommand{\myxT}{
  \begin{bmatrix}
    x_1 & x_2 & \dotsb & x_n
  \end{bmatrix}}
\renewcommand{\myA}{
  \begin{bmatrix}
    a_{11} & a_{12} & \dotsb & a_{1n} \\
    a_{12} & a_{22} & \dotsb & a_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    a_{1n} & a_{2n} & \dotsb & a_{nn}
  \end{bmatrix}}
\newcommand{\myB}{
  \begin{bmatrix}
    b_{11} & b_{12} & \dotsb & b_{1n} \\
    b_{21} & b_{22} & \dotsb & b_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    b_{m1} & b_{m2} & \dotsb & b_{mn}
  \end{bmatrix}}
\newcommand{\myz}{
  \begin{bmatrix}
    0 \\ 0 \\ \vdots \\ 0
  \end{bmatrix}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Set-Up}
    Let $Q$ be the quadratic form on $\mathbb{R}^{n}$ given by
    \[
    Q(x_1,\dotsc,x_n)
    = \myxT\underbrace{\myA}_{A}\myx
    \]\pause
    Consider the linear constraints
    \[
    \underbrace{\myB}_{B}\myx=\myz
    \]
  \end{block}
\end{frame}


\newcommand{\myH}{
  \left[
    \begin{array}{ccc|ccc}
      0      & \dotsb & 0      & b_{11} & \dotsb & b_{1n} \\
      \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
      0      & \dotsb & 0      & b_{m1} & \dotsb & b_{mn} \\ \hline
      b_{11} & \dotsb & b_{m1} & a_{11} & \dotsb & a_{1n} \\
      \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
      b_{1n} & \dotsb & b_{mn} & a_{1n} & \dotsb & a_{nn}
    \end{array}
  \right]}
\newcommand{\myHSym}{
  \left[
    \begin{array}{c|c}
      O_{m\times m} & B \\ \hline
      B^\intercal & A
    \end{array}
  \right]
}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{bordered matrix} of the quadratic form $Q(\vv{x})=\vv{x}^\intercal
    A\vv{x}$ restricted to $\Null(B)$ is \pause
    \[
    H
    = \myH\pause
    = \myHSym
    \]
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 16.4 in Simon \& Blume]
    The restriction of $Q$ to $\Null(B)$ is
    \begin{description}
    \item<2->[positive definite] if and only if every $k$th-order NW principal minor
      of $H$ has sign $(-1)^m$ for $2m+1\leq k\leq m+n$.
    \item<3->[negative definite] if and only if every $k$th-order NW principal minor
      of $H$ has sign $(-1)^{2n+m-k}$ for $2m+1\leq k\leq m+n$.
    \item<4->[indefinite] if and only if one of the above conditions is violated by
      a nonzero NW principal minor of $H$.
    \end{description}
  \end{theorem}
\end{frame}


\subsection{Examples}


\begin{sagesilent}
  A = matrix(QQ, [[1,2,0],[2,0,-3],[0,-3,-1]])
  B = matrix(QQ, [1,1,-1])
  z = zero_matrix(B.nrows())
  H = block_matrix([[z, B],[B.transpose(), A]])
  var('x1 x2 x3')
  x = vector([x1, x2, x3])
  Q = expand(x*A*x)
  H3 = H[[0,1,2],[0,1,2]]
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \onslide<2->{Determine the definiteness of the quadratic form }%
    \onslide<4->{subject to the constraint}
    \begin{align*}
      \onslide<3->{Q(x_1,x_2,x_3) &= \sage{Q}} &
      \onslide<5->{x_1+x_2-x_3 &= 0}
    \end{align*}
    \onslide<6->{Note that $n=3$ and $m=1$. }%
    \onslide<7->{We are interested in the last $n-m=2$ NW
    principal minors of}
    \[
    \onslide<7->{H = \sage{H}}
    \]
    \onslide<8->{These NW principal minors are}
    \begin{align*}
      \onslide<9->{\det H_3 &=} \onslide<10->{\det\sage{H3} =} \onslide<11->{\sage{H3.det()}} \onslide<12->{>0} &
      \onslide<13->{\det H &=} \onslide<14->{\det\sage{H} =} \onslide<15->{\sage{H.det()}}\onslide<16->{>0}
    \end{align*}
    \onslide<17->{It follows that the constrained quadratic form is }\onslide<18->{\emph{indefinite}.}
  \end{example}
\end{frame}
\endgroup




\begin{sagesilent}
  A = -matrix(QQ, [[1,0,0,-1],[0,-1,2,0],[0,2,1,0],[-1,0,0,1]])
  B = matrix(QQ, [[0,1,1,1],[1,-9,0,1]])
  z = zero_matrix(B.nrows())
  H = block_matrix([[z, B],[B.transpose(), A]])
  var('x1 x2 x3 x4')
  x = vector([x1, x2, x3, x4])
  Q = expand(x*A*x)
  H5 = H[[0,1,2,3,4],[0,1,2,3,4]]
\end{sagesilent}
\newcommand{\myConstraint}{
  \begin{array}{rcrcrcrcr}
    &   & x_2    & + & x_3 & + & x_4 & = & 0 \\
    x_1 & - & 9\,x_2 &   &     & + & x_4 & = & 0
  \end{array}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Determine the definiteness of the quadratic form on $\mathbb{R}^{4}$
    \[
    Q(x_1,x_2,x_3,x_4) = \sage{Q} 
    \]\pause
    subject to the constraints
    \[
    \myConstraint
    \]\pause
    Note that $n=4$ and $m=2$. \pause We are interested in the last $n-m=2$ NW
    principal minors of
    \[
    H=\pause\sage{H}
    \]
  \end{example}
\end{frame}
\endgroup


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    These NW principal minors are
    \begin{align*}
      \onslide<2->{\det H_{5} &=} \onslide<3->{\det\sage{H5} =} \onslide<4->{\sage{H5.det()}} \\
      \onslide<5->{\det H_{6}    &=} \onslide<6->{\det\sage{H} =} \onslide<7->{\sage{H.det()}}
    \end{align*}
    \onslide<8->{Note that $\sgn(\det H_5)=(-1)^{2\cdot 4+2-5}=-1$ }%
    \onslide<9->{and $\sgn(\det H_6)=(-1)^{2\cdot 4+2-6}=1$. }%
    \onslide<10->{Thus the constrained quadratic form is }%
    \onslide<11->{\emph{negative definite}.}
  \end{example}
\end{frame}
\endgroup

\end{document}
