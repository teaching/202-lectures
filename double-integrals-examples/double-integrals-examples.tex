\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, decorations.markings,
  matrix, positioning, shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont
% \usepackage[dvipsnames]{xcolor}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\newcommand*\Eval[3]{\left.#1\right\rvert_{#2}^{#3}}



\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Examples of Double Integrals}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\sisetup{
  quotient-mode = fraction,
  fraction-function=\nicefrac
}

\newcommand{\IInt}[3]{
  \Int{\Int{#1}{#2}}{#3}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Example I}
\subsection{Method I}


\begin{sagesilent}
  var('x y')
  f = x/y
  Ifx = integral(f, x)
  Ify = integral(f, y)
  a, b, y1, y2 = 1, 2, 1, x**2
\end{sagesilent}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\displaystyle\IInt{\frac{x}{y}}{y, 1, x^2}{x, 1, 2}$.
  \end{example}

\end{frame}


\newcommand{\myusub}{
  \begin{array}{rclrcr}
    u               &=& x^2   & u(2) &=& 4 \\
    \frac{1}{2}\,du &=& x\,dx & u(1) &=& 1
  \end{array}
}
\newcommand{\myparts}{
  \begin{array}{rclrcr}
    w  &=& \ln(u) & dw &=& \frac{1}{u}\,du \\
    dv &=& du     & v  &=& u
  \end{array}
}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Method I}
    We may compute $\displaystyle\IInt{\frac{x}{y}}{y, 1, x^2}{x, 1, 2}$
    directly
    \begin{align*}
      \onslide<2->{\IInt{\frac{x}{y}}{y, 1, x^2}{x, 1, 2}
      &=} \onslide<3->{\Int{\Eval{x\ln(y)}{y=1}{y=x^2}}{x, 1, 2}                            \\
      &=} \onslide<4->{\Int{x\ln(x^2)}{x, 1, 2}}                  && \onslide<5->{\myusub}  \\
      &\onslide<6->{= \frac{1}{2}\Int{\ln(u)}{u, 1, 4}}             && \onslide<7->{\myparts} \\
      &\onslide<8->{= \frac{1}{2}\Set*{\Eval{u\ln(u)}{u=1}{u=4}-\Int{}{u, 1, 4}}}             \\
      &\onslide<9->{= \frac{1}{2}\Set*{4\ln(4)-3}}                                            \\
      &\onslide<10->{= 2\ln(4)-\frac{3}{2}}
    \end{align*}
    \onslide<11->{Note that this method uses \emph{$x$-slicing}.}
  \end{block}
\end{frame}
\endgroup

\subsection{Method II}

\newcommand{\firstexpic}{
  \begin{tikzpicture}[line join=round, 
    line cap=round, 
    declare function = {myf(\x) = \x*\x;},
    yscale = 1, 
    xscale = 3
    ]

    % \draw[ultra thick, <->] (-1/2, 0) -- (3, 0) node[right] {$x$}; 
    % \draw[ultra thick, <->] (0, -3/2) -- (0, 7) node[above] {$y$};

    \filldraw[ultra thick, draw=blue, fill=teal!40]
    (1, 1)
    -- (2, 1) 
    -- (2, 4)
    -- plot[domain=2:1, samples=200] ({\x},{myf(\x)});

    \draw[very thick, blue]
    plot[domain=2:1.5, samples=200] ({\x},{myf(\x)}) node[left] {$y=x^2$};

    \node[blue] at (3/2, 1) [below] {$y=1$};
    \node[blue] at (2, 2.5) [right] {$x=2$};

    \coordinate (P1) at (1, 1);
    \node at (P1) {\textbullet};
    \node at (P1) [below] {$(1, 1)$};

    \coordinate (P2) at (2, 1);
    \node at (P2) {\textbullet};
    \node at (P2) [below] {$(2, 1)$};

    \coordinate (P3) at (2, 4);
    \node at (P3) {\textbullet};
    \node at (P3) [above] {$(2, 4)$};
    
  \end{tikzpicture}
}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \begin{block}{Method II}
      Compute $\displaystyle\IInt{\frac{x}{y}}{y, 1, x^2}{x, 1, 2}$ using
      $y$-slicing. \onslide<3->{This gives}
      \begin{align*}
        \onslide<4->{\IInt{\frac{x}{y}}{y, 1, x^2}{x, 1, 2}
        &=} \onslide<5->{\IInt{\frac{x}{y}}{x, \sqrt{y}, 2}{y, 1, 4} \\
        &=} \onslide<6->{\frac{1}{2}\Int{\Eval{\frac{x^2}{y}}{x=\sqrt{y}}{x=2}}{y, 1, 4} \\
        &=} \onslide<7->{\frac{1}{2}\Int{\frac{4}{y}-1}{y, 1, 4} \\
        &=} \onslide<8->{\frac{1}{2}\Set*{4\ln(y)-y}_{y=1}^{y=4} \\
        &=} \onslide<9->{\frac{1}{2}\Set*{(4\ln(4)-4)-(4\ln(1)-1)} \\
        &=} \onslide<10->{2\ln(4)-\frac{3}{2}}
      \end{align*}
    \end{block}
    \column{.5\textwidth}
    \[
      \onslide<2->{\firstexpic}
    \]
  \end{columns}

  \onslide<11->{By switching the order of integration, we avoid the
    $u$-substitution and the integration by parts!}
\end{frame}
\endgroup


\section{Example II}
\subsection{Method I}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\displaystyle\IInt{e^{x^2}}{x, y, 1}{y, 0, 1}$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Method I}
    Compute $\displaystyle\IInt{e^{x^2}}{x, y, 1}{y, 0, 1}$ directly.
  \end{block}


  \pause
  \begin{problem}
    The inner integral $\displaystyle \Int{e^{x^2}}{x, y, 1}$ cannot be computed
    using the fundamental theorem of calculus!
  \end{problem}
\end{frame}



\subsection{Method II}

\newcommand{\secondex}{
  \begin{tikzpicture}[line join=round, line cap=round]
    
    \draw[ultra thick, <->] (-1/2, 0) -- (5/2, 0) node[right] {$x$};
    \draw[ultra thick, <->] (0, -1/2) -- (0, 5/2) node[above] {$y$};

    \filldraw[very thick, draw=blue, fill=teal!40] 
    (0, 0)
    -- (2, 2) 
    -- (2, 0)
    -- cycle;

    \coordinate (P1) at (2, 2);
    \node at (P1) {\textbullet};
    \node at (P1) [above] {$(1, 1)$};

    \coordinate (P2) at (2, 0);
    \node at (P2) {\textbullet};
    \node at (P2) [below] {$(1, 0)$};

    \node[blue, left] at (1, 1) {\scriptsize$x=y$};
    \node[blue, right] at (2, 1) {\scriptsize$x=1$};
    \node[blue, below] at (1, 0) {\scriptsize$y=0$};

  \end{tikzpicture}
}
\newcommand{\secondusub}{
  \begin{array}{rclrcr}
    u  &=& x^2   & u(1) &=& 1 \\
    \frac{1}{2}\,du &=& x\,dx & u(0) &=& 0
  \end{array}
}
\begingroup
\small
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth, t]
    \column{.7\textwidth}
    \begin{block}{Method II}
      Use $x$-slicing to compute $\displaystyle\IInt{e^{x^2}}{x, y, 1}{y, 0,
        1}$. \onslide<3->{This gives}
      \begin{align*}
        \onslide<4->{\IInt{e^{x^2}}{x, y, 1}{y, 0, 1}
        &=} \onslide<5->{\IInt{e^{x^2}}{y, 0, x}{x, 0, 1} \\
        &=} \onslide<6->{\Int{\Eval{ye^{x^2}}{y=0}{y=x}}{x, 0, 1} \\
        &=} \onslide<7->{\Int{xe^{x^2}}{x, 0, 1} && \secondusub \\
        &=} \onslide<8->{\frac{1}{2}\Int{e^u}{u, 0, 1} \\
        &=} \onslide<9->{\frac{1}{2}\Eval{e^u}{0}{1} \\
        &=} \onslide<10->{\frac{1}{2}(e-1)}
      \end{align*}
    \end{block}
    \column{.3\textwidth}
    \[
      \onslide<2->{\secondex}
    \]
  \end{columns}

\end{frame}
\endgroup



\section{Example III}
\subsection{Method I}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\displaystyle\IInt{x\sqrt{y^3+1}}{y, \nicefrac{x}{3}, 2}{x, 0, 6}$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Method I}
    Compute $\displaystyle\IInt{x\sqrt{y^3+1}}{y, \nicefrac{x}{3}, 2}{x, 0, 6}$
    directly.
  \end{block}

  \pause
  \begin{problem}
    The inner integral $\displaystyle\Int{x\sqrt{y^3+1}}{y, \nicefrac{x}{3}, 2}$
    cannot be computed using the fundamental theorem of calculus.
  \end{problem}
\end{frame}


\subsection{Method II}
\newcommand{\thirdexample}{
  \begin{tikzpicture}[line join=round, line cap=round, scale=1/2]
    \coordinate (O) at (0, 0);
    \coordinate (P) at (0, 2);
    \coordinate (Q) at (6, 2);

    \filldraw[ultra thick, draw=blue, fill=teal!40]
    (O) -- (P) -- (Q) -- cycle;

    \node at (O) {\textbullet};
    \node[below] at (O) {$(0,0)$};

    \node at (P) {\textbullet};
    \node[above] at (P) {$(0,2)$};

    \node at (Q) {\textbullet};
    \node[above] at (Q) {$(6,2)$};

    \node[left, blue] at ($ (O)!.5!(P)  $) {$x=0$};
    \node[below right, blue] at ($ (O)!.5!(Q)  $) {$y=\nicefrac{x}{3}$};
    \node[above, blue] at ($ (P)!.5!(Q)  $) {$y=2$};

  \end{tikzpicture}
}
\newcommand{\thirdusub}{
  \begin{array}{rclrcr}
    u  &=& y^3+1   & u(2) &=& 9 \\
    \frac{1}{3}\,du &=& y^2\,dy & u(0) &=& 1
  \end{array}
}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Method II}
    \begin{columns}[onlytextwidth]
      \column{.5\textwidth}
      Use $y$-slicing. \onslide<3->{This gives}
      \column{.5\textwidth}
      \onslide<2->{\thirdexample}
    \end{columns}
    \begin{align*}
      &\onslide<4->{\IInt{x\sqrt{y^3+1}}{y, \nicefrac{x}{3}, 2}{x, 0, 6}            \\
      &=} \onslide<5->{\IInt{x\sqrt{y^3+1}}{x, 0, 3\,y}{y, 0, 2}                     \\
      &=} \onslide<6->{\frac{1}{2}\Int{\Eval{x^2\sqrt{y^3+1}}{x=0}{x=3\,y}}{y, 0, 2}  \\
      &=} \onslide<7->{\frac{1}{2}\Int{9\,y^2\sqrt{y^3+1}}{y, 0, 2} && \thirdusub                  \\
      &=} \onslide<8->{\left(\frac{1}{2}\right)\left(9\right)\left(\frac{1}{3}\right)\Int{u^{\nicefrac{1}{2}}}{u, 1, 9} \\
      &=} \onslide<9->{\left(\frac{3}{2}\right)\left(\frac{2}{3}\right)\Eval{u^{\nicefrac{3}{2}}}{1}{9} \\
      &=} \onslide<10->{9^{\nicefrac{3}{2}}-1^{\nicefrac{3}{2}} \\
      &=} \onslide<11->{27-1 \\
      &=} \onslide<12->{26}
    \end{align*}
  \end{block}

\end{frame}
\endgroup

\end{document}
