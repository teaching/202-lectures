\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Unconstrained Optimization}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Local Extrema of Single-Variable Functions}
\subsection{Terminology}


\begin{sagesilent}
  f = x*sin(x**2)+1
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}\to \mathbb{R}$. \onslide<3->{Then $f$ has a }
    \begin{description}
    \item<4->[local maximum] at $x=c$ if $f(x)\leq f(c)$ for $x$ near $c$
    \item<7->[local minimum] at $x=c$ if $f(x)\geq f(c)$ for $x$ near $c$
    \end{description}
  \end{definition}
  \[
  \begin{tikzpicture}[line join=round, line cap=round, xscale=1.75, yscale=.75]
    \pgfmathsetmacro{\xmaxa}{1.3552111252302657}%
    \pgfmathsetmacro{\ymaxa}{2.3076194129914445}%
    \coordinate (Ma) at (\xmaxa, \ymaxa);%

    \pgfmathsetmacro{\xmaxb}{2.8137257654563745}%
    \pgfmathsetmacro{\ymaxb}{3.808131180007005}%
    \coordinate (Mb) at (\xmaxb, \ymaxb);%

    \pgfmathsetmacro{\xmina}{-1.3552111278465047}%
    \pgfmathsetmacro{\ymina}{-0.307619412991444}%
    \coordinate (ma) at (\xmina, \ymina);%

    \pgfmathsetmacro{\xminb}{2.1945027545685698}%
    \pgfmathsetmacro{\yminb}{-1.1827697846777214}%
    \coordinate (mb) at (\xminb, \yminb);%

    \coordinate (N) at (0, 1);%

    \onslide<2->{
      \draw[very thick, blue, <->, domain=-2:3, samples=200, variable=\x]
      plot({\x}, {\x*sin(\x*\x r)+1}) node [right] {$f(x)$};
    }

    \onslide<5->{
      \node at (Ma) {\textbullet};%
      \node at (Ma) [above] {{\tiny loc max}};%
    }

    \onslide<6->{
      \node at (Mb) {\textbullet};%
      \node at (Mb) [above] {{\tiny loc max}};%
    }

    \onslide<8->{
      \node at (ma) {\textbullet};%
      \node at (ma) [below] {{\tiny loc min}};%
    }

    \onslide<9->{
      \node at (mb) {\textbullet};%
      \node at (mb) [below] {{\tiny loc min}};%
    }

    % \node at (N) {\textbullet};%
    % \node at (N) [below] {{\tiny inflection pt}};%
  \end{tikzpicture}
  \]

\end{frame}


\subsection{Fermat's Theorem on Local Extrema}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}\to \mathbb{R}$. A \emph{critical point} of $f$ is a point
    $c$ where $f^\prime(c)=0$ or $f^\prime(c)$ does not exist.
  \end{definition}

  \pause
  \begin{theorem}[Fermat's Theorem on Local Extrema]
    If $f(c)$ is a local maximum of $f$ or a local minimum of $f$, then $c$ is a
    critical point of $f$.
  \end{theorem}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The function $f(x)$ has five critical points. %
    \onslide<2->{Two are local maximums, }%
    \onslide<5->{two are local minimums, }%
    \onslide<8->{and one is an inflection point.}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, xscale=1.75, yscale=.75]
      \pgfmathsetmacro{\xmaxa}{1.3552111252302657}%
      \pgfmathsetmacro{\ymaxa}{2.3076194129914445}%
      \coordinate (Ma) at (\xmaxa, \ymaxa);%

      \pgfmathsetmacro{\xmaxb}{2.8137257654563745}%
      \pgfmathsetmacro{\ymaxb}{3.808131180007005}%
      \coordinate (Mb) at (\xmaxb, \ymaxb);%

      \pgfmathsetmacro{\xmina}{-1.3552111278465047}%
      \pgfmathsetmacro{\ymina}{-0.307619412991444}%
      \coordinate (ma) at (\xmina, \ymina);%

      \pgfmathsetmacro{\xminb}{2.1945027545685698}%
      \pgfmathsetmacro{\yminb}{-1.1827697846777214}%
      \coordinate (mb) at (\xminb, \yminb);%

      \coordinate (N) at (0, 1);%

      \draw[very thick, blue, <->, domain=-2:3, samples=200, variable=\x]
      plot({\x}, {\x*sin(\x*\x r)+1}) node [right] {$f(x)$};

      \onslide<3->{
      \node at (Ma) {\textbullet};%
      \node at (Ma) [above] {{\tiny loc max}};%
      }

      \onslide<4->{
      \node at (Mb) {\textbullet};%
      \node at (Mb) [above] {{\tiny loc max}};%
      }

      \onslide<6->{
      \node at (ma) {\textbullet};%
      \node at (ma) [below] {{\tiny loc min}};%
      }

      \onslide<7->{
      \node at (mb) {\textbullet};%
      \node at (mb) [below] {{\tiny loc min}};%
      }

      \onslide<9->{
      \node at (N) {\textbullet};%
      \node at (N) [below] {{\tiny inflection pt}};%
      }
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\subsection{The Second Derivative Test}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Given a critical point $c$ of a function $f$, how do we determine if $f(c)$
    is a local maximum or a local minimum?
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Second Derivative Test]
    Suppose that $c$ is a critical point of $f$. \onslide<2->{Then}
    \begin{description}
    \item<3->[$f^{\prime\prime}(c)>0$] implies that $f(c)$ is a local minimum of $f$.
    \item<4->[$f^{\prime\prime}(c)<0$] implies that $f(c)$ is a local maximum of $f$.
    \item<5->[$f^{\prime\prime}(c)=0$] implies that the test is inconclusive.
    \end{description}
  \end{theorem}
\end{frame}


\newcommand{\myConcaveUp}{
  \begin{tikzpicture}[line join=round, line cap=round]
    \draw[->, ultra thick, blue] (0, 0) parabola (-1, 1);
    \draw[->, ultra thick, blue] (0, 0) parabola (1, 1);

    \coordinate (O) at (0, 0);

    \node at (O) {\textbullet};
    \node at (O) [below] {local minimum};
    \node at (O) [below] {local minimum};
  \end{tikzpicture}}
\newcommand{\myConcaveDown}{
  \begin{tikzpicture}[line join=round, line cap=round]
    \draw[->, ultra thick, blue] (0, 0) parabola (-1, -1);
    \draw[->, ultra thick, blue] (0, 0) parabola (1, -1);

    \coordinate (O) at (0, 0);

    \node at (O) {\textbullet};
    \node at (O) [above] {local maximum};
  \end{tikzpicture}}
\newcommand{\myInflectionPoint}{
  \begin{tikzpicture}[line join=round, line cap=round]
    \draw[very thick, blue, <->, domain=-1:1, variable=\x]
    plot({\x}, {\x*\x*\x});

    \coordinate (O) at (0, 0);

    \node at (O) {\textbullet};
    \node at (O) [below right=.1cm and -.5cm] {\tiny inflection pt};
  \end{tikzpicture}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    The concavity of $f$ at a critical point $c$ classifies the type of critical
    point of $c$.
    \begin{align*}
      \onslide<2->{\myConcaveUp} && 
      \onslide<3->{\myConcaveDown} && 
      \onslide<4->{\myInflectionPoint}
    \end{align*}
    \onslide<5->{The second derivative only determines concavity when
      $f^{\prime\prime}\neq0$.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The second derivative test is a consequence of the second-degree Taylor
    approximation. \onslide<2->{For a critical point $x=c$, we have}
    \begin{align*}
      \onslide<3->{f(x)
        &\approx f(c)+f^\prime(c)\cdot (x-c)+\frac{1}{2!}f^{\prime\prime}(c)\cdot(x-c)^2} \\
      &\onslide<4->{= f(c)+\frac{1}{2}f^{\prime\prime}(c)\cdot (x-c)^2}
    \end{align*}
    \onslide<5->{If $f^{\prime\prime}(c)=0$, then higher-derivatives may be used
      to classify the critical point as a local maximum, local minimum, or an
      inflection point.}
  \end{block}
\end{frame}


\begin{sagesilent}
  f = -3*x**5+5*x**3
  df = f.diff(x)
  d2f = df.diff(x)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find and classify the critical points of $f(x)=\sage{f}$.
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that 
    \[
    \begin{array}{rcrcr}
      f^\prime(x) &=& \sage{df} &=& \sage{factor(df)} \\ \pause
      f^{\prime\prime}(x) &=& \sage{d2f} &=& \sage{factor(d2f)}
    \end{array}
    \]\pause
    The critical points of $f$ are thus $\Set{-1, 0, 1}$. \pause This gives
    \[
    \begin{array}{|c|c|c|c|}\hline
      c                 & -1                   & 0                   & 1\\ \hline
      f^{\prime\prime}(c)  & \sage{d2f(x=-1)}     & \sage{d2f(x=0)}     & \sage{d2f(x=1)} \\ \hline
      \text{conclusion} & \text{local minimum} & \text{inconclusive} & \text{local maximum}\\ \hline
    \end{array}
    \]\pause
    A higher derivative test is required to classify $c=0$ as a critical point
    of $f$.
  \end{block}
\end{frame}


\subsection{Summary}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  To determine the local extrema of $f:\mathbb{R}\to \mathbb{R}$:
  \begin{enumerate}
  \item<2-> Find the critical points by determining the points $c$ where
    $f^\prime(c)=0$ or $f^\prime(c)$ does not exist.
  \item<3-> The critical points satisfying $f^{\prime\prime}(c)\neq0$ may be
    classified using the second derivative test.
  \item<4-> The critical points satisfying $f^{\prime\prime}(c)=0$ require further
    analysis.
  \end{enumerate}

\end{frame}


\section{Local Extrema of Multivariable Functions}
\subsection{Terminology}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Can we develop a theory of local extrema of functions $f:\mathbb{R}^{n}\to
    \mathbb{R}$?
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. Then $f$ has a 
    \begin{description}
    \item<2->[local maximum] at $\vv{c}$ if $f(\vv{x})\leq f(\vv{c})$ for $\vv{x}$
      near $\vv{c}$
    \item<3->[local minimum] at $\vv{c}$ if $f(\vv{x})\geq f(\vv{c})$ for $\vv{x}$
      near $\vv{c}$
    \end{description}
  \end{definition}

  \onslide<4->{
  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. A \emph{critical point} of $f$ is a
    point $\vv{c}$ where $\nabla f(\vv{c})=\vv{O}$ or $\nabla f(\vv{c})$ does
    not exist.
  \end{definition}
  }
\end{frame}


\subsection{The Multivariable Fermat Theorem}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 17.1 in Simon \& Blume]
    If $f(\vv{c})$ is a local maximum of $f$ or a local minimum of $f$, then
    $\vv{c}$ is a critical point of $f$.
  \end{theorem}

  \pause
  \begin{block}{Idea}
    Suppose that $\nabla f(\vv{c})\neq 0$. \pause Consider the direction vectors\pause
    \begin{align*}
      \vv{u} &= \frac{\nabla f(\vv{c})}{\norm{\nabla f(\vv{c})}} &
      \vv{v} &= -\frac{\nabla f(\vv{c})}{\norm{\nabla f(\vv{c})}}
    \end{align*}\pause
    Then $D_{\vv{u}}f(\vv{c})=\pause\norm{\nabla f(\vv{c})}>0$ and
    $D_{\vv{v}}f(\vv{c})=\pause-\norm{\nabla f(\vv{c})}<0$.\pause
    \begin{description}
    \item[$f$ increasing in $\vv{u}$-direction] implies $f(\vv{c})$ is not a local maximum \pause
    \item[$f$ decreasing in $\vv{v}$-direction] implies $f(\vv{c})$ is not a local minimum
    \end{description}
  \end{block}
\end{frame}


\subsection{Example}
\begin{sagesilent}
  var('x y')
  f = 6*x*y**2-2*x**3-3*y**4
  fx = f.diff(x)
  fy = f.diff(y)
  nablaf = f.gradient((x, y))
  Df = jacobian(f, (x, y))
  z = zero_matrix(Df.nrows(), Df.ncols())
\end{sagesilent}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find the critical points of $f(x, y)=\sage{f}$.
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that $\nabla f=\sage{nablaf}$. \pause%
    Since $\nabla f$ exists everywhere, the critical points are given by the
    solutions to $\nabla f=\vv{O}$. \pause%
    This equation gives the nonlinear system of equations\pause
    \[
    \begin{array}{ccc}
      \sage{fx} &=& 0 \\
      \sage{fy} &=& 0
    \end{array}
    \]\pause
    The first equation gives $x=\pm y$. \pause%
    Substituting $x=y$ and $x=-y$ into the second equation gives\pause
    \begin{align*}
      \sage{factor(fy(x=y))} &= 0 &
      \sage{factor(fy(x=-y))} &= 0 
    \end{align*}
    respectively. \pause%
    These equations give three critical points $\Set{(0, 0),(1, -1),(1, 1)}$.
  \end{block}
\end{frame}
\endgroup


\section{The Multivariable Second Derivative Test}
\subsection{Motivation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Let $\vv{c}$ be a critical point of a function $f:\mathbb{R}^{n}\to
    \mathbb{R}$. \pause%
    How do we determine if $f(\vv{c})$ is a local maximum or a local minimum of
    $f$?
  \end{block}

  \pause
  \begin{block}{Answer}
    We need a second derivative test for multivariable functions!
  \end{block}
\end{frame}


\subsection{The Hessian Matrix}

\newcommand{\myHf}{
  \left[
    \begin{array}{cccc}%{*4{>{\displaystyle}c}p{5cm}}
      \pderiv[2]{f}{x_1}   & \pderiv{f}{x_1, x_2} & \dotsb & \pderiv{f}{x_1, x_n} \\ \\
      \pderiv{f}{x_2, x_1} & \pderiv[2]{f}{x_2}   & \dotsb & \pderiv{f}{x_2, x_n} \\ \\
      \vdots               & \vdots               & \ddots & \vdots \\ \\
      \pderiv{f}{x_n, x_1} & \pderiv{f}{x_n, x_2} & \dotsb & \pderiv[2]{f}{x_n}
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. \pause%
    The \emph{Hessian} of $f$ is\pause
    \[
    D^2f=\myHf
    \]
  \end{definition}

  \pause
  \begin{block}{Notation}
    Some authors write $Hf$ in place of $D^2f$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Clairaut's Theorem implies that \pause%
    $D^2f$ is a \emph{$n\times n$ symmetric matrix}.
  \end{block}

  \pause
  \begin{block}{Observation}
    At a given point $\vv{c}$, the equation
    \[
    Q(\vv{x})=\vv{x}^\intercal D^2f(\vv{c})\vv{x}
    \]
    defines a \emph{quadratic form on $\mathbb{R}^{n}$}.
  \end{block}

  \pause
  \begin{block}{Idea}
    In multivariable calculus, the quadratic form defined by $D^2f(\vv{c})$
    plays the role of the second derivative of $f:\mathbb{R}^{n}\to \mathbb{R}$.
  \end{block}
\end{frame}


\begin{sagesilent}
  var('x y')
  f = 2*x**3*y**2+x*y
  fx = f.diff(x)
  fy = f.diff(y)
  fxx = f.diff(x, x)
  fyy = f.diff(y, y)
  fxy = f.diff(x, y)
  Hf = f.hessian()
  var('f_xx f_yy f_xy')
  Hfsym = matrix([[f_xx, f_xy],[f_xy, f_yy]])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f(x, y)=\sage{f}$. \pause%
    The first-order partials of $f$ are\pause
    \begin{align*}
      f_x &= \sage{fx} & f_y &= \sage{fy}
    \end{align*}\pause
    The second-order partials of $f$ are
    \begin{align*}
      f_{xx} &= \sage{fxx} & f_{yy} &= \sage{fyy} & f_{xy} &= \sage{fxy}
    \end{align*}\pause
    The Hessian of $f$ is thus
    \[
    D^2f
    = \sage{Hfsym}
    = \sage{Hf}
    \]
  \end{example}
\end{frame}


\begin{sagesilent}
  S = PolynomialRing(ZZ, 'x, y, z')
  S.inject_variables()
  f = S.random_element()
  f += sin(x*y)
  fx = f.derivative(x)
  fy = f.derivative(y)
  fz = f.derivative(z)
  fxx = f.derivative(x, x)
  fyy = f.derivative(y, y)
  fzz = f.derivative(z, z)
  fxy = f.derivative(x, y)
  fxz = f.derivative(x, z)
  fyz = f.derivative(y, z)
  Hf = f.hessian()
  var('f_xx f_yy f_zz f_xy f_xz f_yz')
  Hfsym = matrix([[f_xx, f_xy, f_xz],[f_xy,f_yy, f_yz],[f_xz, f_yz, f_zz]])
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f(x, y, z)=\sage{f}$. \pause%
    The partials of $f$ are \pause
    \begin{align*}
      f_{x}  &= \sage{fx}  & f_{y}  &= \sage{fy}  & f_{z}  &= \sage{fz} \\
      f_{xx} &= \sage{fxx} & f_{yy} &= \sage{fyy} & f_{zz} &= \sage{fzz} \\
      f_{xy} &= \sage{fxy} & f_{xz} &= \sage{fxz} & f_{yz} &= \sage{fyz}
    \end{align*}\pause
    The Hessian of $f$ is thus\pause
    \begin{align*}
      D^2f 
      &= \sage{Hfsym} \\
      &= \sage{Hf}
    \end{align*}
  \end{example}
\end{frame}
\endgroup


\subsection{The Second Derivative Test}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorems 17.3 and 17.4 in Simon \& Blume]
    Suppose that $\vv{c}$ is a critical point of $f:\mathbb{R}^{n}\to
    \mathbb{R}$. \onslide<2->{Then}
    \begin{description}
    \item<3->[$D^2f(\vv{c})$ positive definite] implies $f(\vv{c})$ is a local minimum of $f$.
    \item<4->[$D^2f(\vv{c})$ negative definite] implies $f(\vv{c})$ is a local maximum of $f$.
    \item<5->[$D^2f(\vv{c})$ indefinite] implies $f(\vv{c})$ is a ``saddle point'' of $f$.
    \item<6->[$D^2f(\vv{c})$ semidefinite] implies the test is inconclusive.
    \end{description}
  \end{theorem}
\end{frame}


\newcommand{\myBowlUp}[1][1]{
  \tdplotsetmaincoords{70}{135}
  \begin{tikzpicture}[tdplot_main_coords, scale=#1, line cap=round, line join=round]
    \coordinate (O) at (0, 0, 0);
    \coordinate (P1) at (1, -1, 2);
    \coordinate (P2) at (-1, 1, 2);

    \draw[very thick, blue] (O) parabola (P1);
    \draw[very thick, blue] (O) parabola (P2);

    \node at (O) {\textbullet};
    \node at (O) [below] {local minimum};

    \draw[very thick, blue, domain=0:2*pi, smooth, variable=\x]
    plot ({cos(\x r)*sqrt(2)},{sin(\x r)*sqrt(2)},{2});
  \end{tikzpicture}}
\newcommand{\myBowlDown}[1][1]{  \tdplotsetmaincoords{70}{135}
  \begin{tikzpicture}[tdplot_main_coords, scale=#1, line cap=round, line join=round]
    \coordinate (O) at (0, 0, 0);
    \coordinate (P1) at (1, -1, 2);
    \coordinate (P2) at (-1, 1, 2);

    \draw[very thick, blue] (O) parabola ($ -1*(P1) $);
    \draw[very thick, blue] (O) parabola ($ -1*(P2) $);

    \node at (O) {\textbullet};
    \node at (O) [above] {local maximum};

    \draw[very thick, blue, domain=0:2*pi, smooth, variable=\x]
    plot ({cos(\x r)*sqrt(2)},{sin(\x r)*sqrt(2)},{-2});
  \end{tikzpicture}}
\newcommand{\mySaddle}[1][1/2]{
  \begin{tikzpicture}
    \begin{axis}[domain=-1:1, y domain=-1:1, scale=#1, hide axis]
      \addplot3[surf] {x^2-y^2};
      \node[label={87.5:{\tiny saddle pt}},circle,fill,inner sep=2pt] at (axis cs:0,0,0) {};
    \end{axis}
  \end{tikzpicture}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    The concavity of $f$ at a critical point $\vv{c}$ classifies the type of
    critical point of $\vv{c}$.
    \begin{align*}
      \onslide<2->{\myBowlUp[.75]} && 
      \onslide<3->{\myBowlDown[.75]} && 
      \onslide<4->{\mySaddle}
    \end{align*}
    \onslide<5->{The Hessian of $f$ only determines concavity when it is not
      semidefinite.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The multivariable second derivative test is a consequence of the
    second-degree multivariable Taylor approximation. \pause%
    For a critical point $\vv{c}$, we have\pause
    \begin{align*}
      f(\vv{x}) 
      &\approx f(\vv{c})+\nabla
      f(\vv{c})\cdot(\vv{x}-\vv{c})+\frac{1}{2!}(\vv{x}-\vv{c})^\intercal D^2f(\vv{c})\cdot(\vv{x}-\vv{c}) \\
      &= f(\vv{c})+\frac{1}{2}(\vv{x}-\vv{c})^\intercal D^2f(\vv{c})\cdot(\vv{x}-\vv{c}) 
    \end{align*}\pause
    If $D^2f(\vv{c})$ is semidefinite, then higher-derivatives may be used to
    classify the critical point as a local maximum, local minimum, or a saddle
    point.
  \end{block}
\end{frame}


\begin{sagesilent}
  var('x y z')
  f = x^3 + x*y + y*z - z^2
  nablaf = f.gradient()
  fx, fy, fz = [f.diff(v) for v in f.variables()]
  Hf = f.hessian()
  Hf1 = Hf(x=0, y=0, z=0)
  Hf2 = Hf(x=2/3, y=-4/3, z=-2/3)
  Hf1NW1 = Hf1[[0],[0]]
  Hf1NW2 = Hf1[[0,1],[0,1]]
  Hf2NW1 = Hf2[[0],[0]]
  Hf2NW2 = Hf2[[0,1],[0,1]]
\end{sagesilent}
% many exercises on Susan Jan Colley's book p 298/625 (book p276)
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find and classify the critical points of $f(x,y,z)=\sage{f}$.
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that $\nabla f=\sage{nablaf}$. \pause%
    Since $\nabla f$ exists everywhere, the critical points are given by the
    solutions to $\nabla f=\vv{O}$. \pause%
    This equation gives the nonlinear system of equations
    \[
    \begin{array}{rcc}
      \sage{fx} &=& 0 \\
      \sage{fy} &=& 0 \\
      \sage{fz} &=& 0 
    \end{array}
    \]\pause
    The second two equations imply that $y=2\,z=-2\,x$. \pause%
    Substituting $y=-2\,x$ into the first equation gives
    \[
    \sage{factor(fx(y=-2*x))}=0
    \]\pause
    This implies that $x=0$ or $x=\nicefrac{2}{3}$. \pause%
    The critical points of $f$ are thus $\Set{(0,0,0),(\nicefrac{2}{3},
      -\nicefrac{4}{3}, -\nicefrac{2}{3})}$. \pause%
    To classify these critical points, note that
    \[
    D2f=\sage{Hf}
    \]
    % \begin{align*}
    %   D^2f                                                     &= \mysage{\tiny}{Hf}  & 
    %   D^2f(0,0,0)                                              &= \mysage{\tiny}{Hf1} & 
    %   D^2f(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3}) &= \mysage{\tiny}{Hf2}
    % \end{align*}
  \end{block}
\end{frame}
\endgroup

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Classifying $(0,0,0)$}
    To classify $(0,0,0)$, note that\pause
    \[
    D^2f(0,0,0)=\sage{Hf1}
    \]\pause
    The first two NW principal minors of $D^2f(0,0,0)$ are\pause
    \begin{align*}
      \det\sage{Hf1NW1} &= \sage{Hf1NW1.det()} &
      \det\sage{Hf1NW2} &= \sage{Hf1NW2.det()} 
    \end{align*}\pause
    It follows that $D^2f(0,0,0)$ is indefinite. \pause%
    Thus $(0,0,0)$ is a saddle point of $f$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Classifying $(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3})$}
    To classify $(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3})$, note that
    \[
    D^2f(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3})=\sage{Hf2}
    \]\pause
    The first two NW principal minors of $D^2f(\nicefrac{2}{3},-\nicefrac{4}{3},
    -\nicefrac{2}{3})$ are\pause
    \begin{align*}
      \det\sage{Hf2NW1} &= \sage{Hf2NW1.det()} &
      \det\sage{Hf2NW2} &= \sage{Hf2NW2.det()}
    \end{align*}\pause
    It follows that $D^2f(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3})$
    is indefinite. \pause%
    Thus $(\nicefrac{2}{3},-\nicefrac{4}{3}, -\nicefrac{2}{3})$ is a saddle
    point of $f$.
  \end{block}
\end{frame}

\end{document}
