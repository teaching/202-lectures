\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
% \usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}

\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\title{The Dot Product}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}


\section{Defining the Dot Product}
\subsection{Motivation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{What We Know}
    So far, the vector operations we have learned are
    \begin{description}[<+->]
    \item[scalar multiplication] $c\cdot\vv{v}$
    \item[vector addition] $\vv{v}+\vv{w}$
    \end{description}
    \onslide<+->{These operations are both \emph{geometric} and \emph{algebraic}.}
  \end{block}

  \onslide<+->{
    \begin{block}{Question}
      What does it mean to multiply two vectors?
    \end{block}}

  \onslide<+->{
    \begin{block}{Answer}
      There is no ``correct'' answer. There are several useful ways to multiply
      vectors. In this course, we will discuss two theories of vector
      multiplication.
    \end{block}}

\end{frame}


\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{definition}
    The \emph{dot product} of two vectors $\vv{v}=\langle
    v_1,v_2,\dotsc,v_n\rangle$ and $\vv{w}=\langle w_1,w_2,\dotsc,w_n\rangle$
    is
    \[
    \vv{v}\cdot\vv{w}
    = \sum_{i=1}^n v_iw_i
    \onslide<2->{= v_1 w_1+v_2 w_2+\dotsb+v_n w_n}
    \]
  \end{definition}

  \onslide<3->{
    \begin{block}{Note}
      The dot product of two vectors is a scalar, \emph{not} a vector.
    \end{block}}

  \onslide<4->{
    \begin{block}{Note}
      The dot product $\vv{v}\cdot\vv{w}$ is only defined if $\vv{v}$ and
      $\vv{w}$ have the \emph{same number of coordinates}.
    \end{block}}

\end{frame}


\subsection{Examples}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\langle -4, 1, -5\rangle$ and $\vv{w}=\langle 4, 54,
    -1\rangle$. Then
    \begin{align*}
      \vv{v}\cdot\vv{w} 
      &= \langle -4, 1, -5\rangle\cdot\langle 4, 54, -1\rangle \\
      &= (-4)(4)+(1)(54)+(-5)(-1) \\
      &= -16+54+5 \\
      &= 43
    \end{align*}
  \end{example}

  \pause
  \begin{example}
    The dot product of $\vv{v} = \left[\begin{array}{r}1 \\1
        \\0\end{array}\right]$ and $\vv{w}=\left[\begin{array}{r}-1 \\-1 \\-3
        \\12\end{array}\right]$ is not defined!
  \end{example}
\end{frame}


\subsection{Properties}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algebraic Properties of the Dot Product}
    The dot product obeys the following laws.
    \begin{description}[<+->]
    \item[commutative] $\vv{v}\cdot\vv{w}=\vv{w}\cdot\vv{v}$
    \item[distributive] $\vv{v}\cdot(\vv{w}+\vv{x})=\vv{v}\cdot\vv{w}+\vv{v}\cdot\vv{x}$
    \item[associative] $c\cdot(\vv{v}\cdot\vv{w})=(c\cdot\vv{v})\cdot\vv{w}$
    \end{description}
  \end{block}
\end{frame}

\section{The Geometry of the Dot Product}
\subsection{Dot Product and Length}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\vv{v}\cdot\vv{v}=\onslide<2->{\norm{\vv{v}}^2}$
  \end{theorem}
  \onslide<3->{
    \begin{proof}
      For $\vv{v}=\langle v_1,v_2,\dotsc,v_n\rangle$ we have
      \begin{align*}
        \vv{v}\cdot \vv{v}
        &= \langle v_1,v_2,\dotsc,v_n\rangle\cdot \langle v_1,v_2,\dotsc,v_n\rangle \\
        &= v_1^2+v_2^2+\dotsb+v_n^2 \\
        &= \left(\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}\right)^2 \\
        &= \norm{\vv{v}}^2\qedhere
      \end{align*}
    \end{proof}}

\end{frame}


\subsection{Dot Product and Angles}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      How can we measure the angle $\theta$ between two vectors $\vv{v}$ and
      $\vv{w}$?
    \end{block}
    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=3/4]
      \coordinate (O) at (0,0); \coordinate (P) at (5,0); \coordinate (Q) at
      (2,3);

      \draw[ultra thick, blue, ->] (O) -- (P) node[midway, below] {$\vv{v}$};
      \draw[ultra thick, red, ->] (O) -- (Q) node[midway, left] {$\vv{w}$};

      \draw[<->, thick] (1,0) arc (0:56.31:1); \node (theta) at (7/6,3/6)
      {$\theta$};
      
      \onslide<3->{
        \draw[ultra thick, violet, ->] (Q) -- (P) node[midway, right]
        {$\vv{v}-\vv{w}$};}
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<2->{
    \begin{block}{Answer}
      Form a triangle.} \onslide<4->{Measure $\norm{\vv{v}-\vv{w}}^2$ in two ways.}
    \begin{description}
    \item<5->[law of cosines]
      $\norm{\vv{v}-\vv{w}}^2
      = \norm{\vv{v}}^2+\norm{\vv{w}}^2-2\,\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
    \item<6->[dot product] 
      $\norm{\vv{v}-\vv{w}}^2
      = \norm{\vv{v}}^2+\norm{\vv{w}}^2-2\,\vv{v}\cdot\vv{w}$
    \end{description}
  \end{block}

  \onslide<7->{
    \begin{theorem}
      $\vv{v}\cdot\vv{w}=\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
    \end{theorem}}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\vv{v}\cdot\vv{w}=\norm{\vv{v}}\cdot\norm{\vv{w}}\cos\theta$
  \end{theorem}
  
  \pause
  \begin{corollary}[The Cauchy-Schwarz Inequality]
    $\abs{\vv{v}\cdot\vv{w}}\leq\norm{\vv{v}}\cdot\norm{\vv{w}}$
  \end{corollary}

  \pause
  \begin{example}
    Let $\theta$ be the angle between $\vv{v}=\langle1,2,3\rangle$ and
    $\vv{w}=\langle1, 1, 1\rangle$. Compute $\cos\theta$.
    \pause
    \[
    \cos\theta
    = \frac{\vv{v}\cdot\vv{w}}{\norm{\vv{v}}\cdot\norm{\vv{w}}}
    = \frac{(1)(1)+(2)(1)+(3)(1)}{\sqrt{1^2+2^2+3^2}\cdot\sqrt{1^2+1^2+1^2}}
    = \frac{6}{\sqrt{14}\cdot\sqrt{3}}
    \]
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\theta$ be the angle between two vectors $\vv{v}\neq\vv{O}$ and
    $\vv{w}\neq\vv{O}$. Then
    \begin{description}
    \item<2->[$\vv{v}\cdot\vv{w}>0$] means $\theta$ is \onslide<3->{\emph{acute} $\left(0\leq\theta<\nicefrac{\pi}{2}\right)$}
    \item<4->[$\vv{v}\cdot\vv{w}<0$] means $\theta$ is \onslide<5->{\emph{obtuse} $\left(\nicefrac{\pi}{2}<\theta\leq\pi\right)$}
    \item<6->[$\vv{v}\cdot\vv{w}=0$] means $\theta$ is \onslide<7->{\emph{right} $\left(\theta=\nicefrac{\pi}{2}\right)$}
    \end{description}
  \end{theorem}

  \onslide<8->{
    \begin{definition}
      $\vv{v}$ and $\vv{w}$ are \emph{orthogonal} if $\vv{v}\cdot\vv{w}=0$
    \end{definition}}

  \onslide<9->{
    \begin{example}
      Are $\vv{v}=\langle1,\sqrt{2},1,0\rangle$ and
      $\vv{w}=\langle 1, -\sqrt{2},1,1\rangle$ orthogonal? Yes, since
      \[
      \vv{v}\cdot\vv{w}
      = (1)(1)+(\sqrt{2})(-\sqrt{2})+(1)(1)+(0)(1)
      = 1-2+1+0
      = 0
      \]
    \end{example}}
  
\end{frame}


\subsection{The Component of $\vec{v}$ Along $\vec{w}$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{definition}
      The \emph{component} of $\vv{v}$ along $\vv{w}$ is the depicted scalar.
    \end{definition}
    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=1]
      \coordinate (O) at (0,0); \coordinate (P) at (5,0); \coordinate (Q) at
      (2,3); \coordinate (R) at (2,0);

      \draw[thick, dashed] (Q) -- (R); \draw[thick] (1.8, 0) -- (1.8, .2) -- (2,
      .2);

      \draw[<->, thick] (1,0) arc (0:56.31:1); 
      \node (theta) at (7/6,3/6) {$\theta$};

      \draw[ultra thick, blue, ->] (O) -- (P) node[right] {$\vv{w}$};
      \draw[ultra thick, violet, ->] (O) -- (Q) node[midway, left] {$\vv{v}$};


      \draw[very thick, decoration={brace, mirror, raise=5pt}, decorate] (O) --
      node[below=6pt] {$\comp_{\vv{w}}(\vv{v})$} (R);
    \end{tikzpicture}
    \]
  \end{columns}

  \pause
  \begin{block}{Formula}
    To compute $\comp_{\vv{w}}(\vv{v})$, note that $\displaystyle
    \cos\theta=\frac{\comp_{\vv{w}}(\vv{v})}{\norm{\vv{v}}}$. \pause
    Hence
    \[
    \comp_{\vv{w}}(\vv{v})
    = \norm{\vv{v}}\cos\theta\pause
    = \norm{\vv{v}}\cdot\frac{\vv{v}\cdot\vv{w}}{\norm{\vv{v}}\cdot\norm{\vv{w}}}\pause
    = \frac{\vv{v}\cdot\vv{w}}{\norm{\vv{w}}}
    \]
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\langle0, 2, -3\rangle$ and $\vv{w}=\langle3, 5, -6\rangle$. Then
\[
\comp_{\vv{w}}(\vv{v})
= \frac{\vv{v}\cdot\vv{w}}{\norm{\vv{w}}}
= \frac{(0)(3)+(2)(5)+(-3)(-6)}{\sqrt{3^2+5^2+(-6)^2}}
= \frac{28}{\sqrt{70}}
\]
  \end{example}
\end{frame}


\end{document}
