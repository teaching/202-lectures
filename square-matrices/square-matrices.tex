\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}




\title{The Algebra of Square Matrices}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={3-}]
\end{frame}


\section{Definitions and Notation}
\subsection{}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The collection of $m\times n$ matrices is denoted by $\mathbf{M}_{m\times
      n}$. The collection of $n\times n$ square matrices is denoted by
    $\mathbf{M}_n$.
  \end{definition}

  \begin{block}{\onslide<2->{Note}}
    \onslide<2->{The collection of square matrices is ``closed'' under several algebraic
      operations.}%
    \begin{itemize}
    \item<3-> $A,B\in \mathbf{M}_n\implies A+B\in \mathbf{M}_n$
    \item<4-> $A,B\in \mathbf{M}_n\implies AB\in \mathbf{M}_n$
    \item<5-> $A\in   \mathbf{M}_n\implies A^\intercal\in \mathbf{M}_n$
    \end{itemize}
  \end{block}
\end{frame}

\section{Invertible Matrices}
\subsection{Definition and Examples}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $n\times n$ matrix. An \emph{inverse} of $A$ is a $n\times n$
    matrix $B$ satisfying
    \begin{align*}
      AB &= I_n
      &\textnormal{and}&&
      BA &= I_n
    \end{align*}
    The matrix $A$ is called \emph{invertible} if an inverse of $A$ exists.
  \end{definition}
\end{frame}


\begin{sagesilent}
  I3 = identity_matrix(3)
  A = random_matrix(ZZ, 3, algorithm='unimodular')
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    {\scriptsize
      \begin{align*}
        \underbrace{\sage{A}}_{A}\underbrace{\sage{A.inverse()}}_{B} &= \sage{I3} \\
        \onslide<2->{\underbrace{\sage{A.inverse()}}_{B}\underbrace{\sage{A}}_{A} &= \sage{I3}}
      \end{align*}}%
    \onslide<3->{This means that $B$ is an inverse of $A$. }%
    \onslide<4->{Consequently, we say that ``$A$ is invertible.''}%
  \end{example}
\end{frame}


\begin{sagesilent}
  var('a b c d')
  I2 = identity_matrix(2)
  A = matrix(2, [1, 0, 0, 0])
  B = matrix(2, [a, b, c, d])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
    \sage{A}\sage{B}
    \onslide<2->{=\sage{A*B}}
    \onslide<3->{\neq\sage{I2}}
    \]%
    \onslide<4->{The matrix $A=\sage{A}$ is \emph{not invertible}.}%
  \end{example}

  \begin{block}{\onslide<5->{Warning}}
    \onslide<5->{Some matrices are not invertible!}
  \end{block}
\end{frame}


\subsection{Properties of Inverses}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $n\times n$ matrix. If a $n\times n$ matrix $B$ satisfies one
    of the two equations
    \begin{align*}
      AB &= I_n &
      BA &= I_n
    \end{align*}
    then the other equation is also satisfied.
  \end{theorem}

  \begin{block}{\onslide<2->{Note}}
    \onslide<2->{This theorem means that to verify if a matrix $B$ is an inverse of $A$ we
      need only check that $AB=I_n$ or $BA=I_n$, not both.}%
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $B$ and $C$ are inverses of a matrix $A$. Then $B=C$.
  \end{theorem}
  \onslide<2->{
    \begin{proof}}
    $
    \onslide<2->{B}%
    \onslide<3->{=BI}%
    \onslide<4->{=B(AC)}%
    \onslide<5->{=(BA)C}%
    \onslide<6->{=IC}%
    \onslide<7->{=C}
    $\alt<7->{\qedhere}{\phantom{\qedhere}}
  \end{proof}

  \begin{block}{\onslide<8->{Notation}}
    \onslide<9->{If $A$ is invertible, then we denote its inverse by $A^{-1}$.}%
  \end{block}
\end{frame}



\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='unimodular')
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
    \underbrace{\sage{A}}_{A}\underbrace{\sage{A.inverse()}}_{B}=\sage{A*A.inverse()}
    \]
    \onslide<2->{Thus $B=A^{-1}$.}%
  \end{example}
\end{frame}


\subsection{Invertible Means Nonsingular}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A matrix $A$ is invertible if and only if $A$ is nonsingular.
  \end{theorem}
  \onslide<2->{
    \begin{proof}} \onslide<2->{$(\Rightarrow)$ Suppose that $A$ is
      invertible. }%
    \onslide<3->{Consider the system $A\vv{x}=\vv{b}$. }%
    \onslide<4->{Then $A^{-1}A\vv{x}=A^{-1}\vv{b}$, which is equivalent to
      $\vv{x}=A^{-1}\vv{b}$. }%
    \onslide<5->{Thus $A\vv{x}=\vv{b}$ is guaranteed a unique solution. }%
    \onslide<6->{Hence $A$ is nonsingular.}%

    \onslide<7->{$(\Leftarrow)$ Suppose that $A$ is nonsingular. }%
    \onslide<8->{Then $\rref(A)=I$. }%
    \onslide<9->{Thus, there are elementary matrices $E_1,E_2,\dotsc, E_r$
      satisfying}%
    \onslide<10->{
      \[
      E_r\dotsb E_2E_1A=\rref(A)=I
      \]}%
    \onslide<11->{Hence $A$ is invertible and $A^{-1}=E_r\dotsb E_2E_1$.}%
    \alt<11->{\qedhere}{\phantom{\qedhere}}%
  \end{proof}
\end{frame}


\subsection{An Algorithm for Computing $A^{-1}$}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    Let $A$ be a $n\times n$ matrix. The following steps will compute $A^{-1}$,
    provided that $A^{-1}$ exists.
    \begin{description}
    \item<2->[Step 1] Form the augmented matrix $[A\mid I_n]$.
    \item<3->[Step 2] Row-reduce $[A\mid I_n]\rightsquigarrow[\rref(A)\mid B]$.
    \item<4->[Step 3] If $\rref(A)\neq I_n$, then $A$ is not invertible.
    \item<5->[Step 4] If $\rref(A)=I_n$, then $B=A^{-1}$.
    \end{description}
  \end{block}
\end{frame}


\begin{sagesilent}
  I2 = identity_matrix(2)
  A = matrix([(-1, -1), (-1, 2)])
  M = A.augment(I2, subdivide=True)
  E1 = elementary_matrix(2, row1=0, scale=-1)
  E2 = elementary_matrix(2, row1=1, row2=0, scale=1)
  E3 = elementary_matrix(2, row1=1, scale=1/3)
  E4 = elementary_matrix(2, row1=0, row2=1, scale=-1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\sage{A}$. To compute $A^{-1}$, consider
    \begin{align*}
      \sage{M}
      &\onslide<2->{\xrightarrow{-R_1\to R_1}                       \sage{E1*M}} \\
      &\onslide<3->{\xrightarrow{R_2+R_1\to R_2}                    \sage{E2*E1*M}} \\
      &\onslide<4->{\xrightarrow{(\nicefrac{1}{3})\cdot R_2\to R_2} \sage{E3*E2*E1*M}} \\
      &\onslide<5->{\xrightarrow{R_1-R_2\to R_1}                    \sage{E4*E3*E2*E1*M}}
    \end{align*}%
    \onslide<6->{Thus $A$ is invertible and $A^{-1}=\sage{A.inverse()}$.}%
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    Our row-reductions correspond to elementary matrices. 
    \begin{align*}
      \onslide<2->{E_1 &= [-R_1\to R_1]} &
      \onslide<4->{E_2 &= [R_2+R_1\to R_2]} \\
      &\onslide<3->{=\sage{E1}} &
      &\onslide<5->{=\sage{E2}} \\
      \onslide<6->{E_3 &= [(\nicefrac{1}{3})\cdot R_2\to R_2]} &
      \onslide<8->{E_4 &= [R_1-R_2\to R_1]} \\
      &\onslide<7->{=\sage{E3}} &
      &\onslide<9->{=\sage{E4}}
    \end{align*}%
    \onslide<10>{ This allows us to express $A^{-1}$ as a product of elementary
      matrices $A^{-1}=E_4E_3E_2E_1$.}%
  \end{example}
\end{frame}


\subsection{Elementary Matrices}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Elementary matrices are invertible and the inverse of an elementary matrix
    is an elementary matrix.
    \begin{enumerate}
    \item<2-> $[R_i\leftrightarrow R_j]^{-1}=[R_i\leftrightarrow R_j]$
    \item<3-> $[c\cdot R_i\to R_i]^{-1}=[(\nicefrac{1}{c})\cdot R_i\to R_i]$
    \item<4-> $[R_i+c\cdot R_j\to R_i]^{-1}=[R_i-c\cdot R_j\to R_i]$
    \end{enumerate}
  \end{theorem}
\end{frame}



\begin{sagesilent}
  E1 = elementary_matrix(3, row1=0, row2=2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row switching]
    Let $n=3$ and consider $E=[R_1\leftrightarrow R_3]$. Then
    \[
    E=\sage{E1}
    \]
    \pause
    Then $E^{-1}=[R_1\leftrightarrow R_3]$, which gives
    \[
    E^{-1}=\sage{E1.inverse()}
    \]
  \end{example}
\end{frame}



\begin{sagesilent}
  E2 = elementary_matrix(3, row1=2, scale=-7)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row multiplication]
    Let $n=3$ and consider $E=[-7\cdot R_3\to R_3]$. Then
    \[
    E=\sage{E2}
    \]
    \pause
    Then $E^{-1}=[(-\nicefrac{1}{7})\cdot R_3\to R_3]$, which gives
    \[
    E^{-1}=\sage{E2.inverse()}
    \]
  \end{example}
\end{frame}


\begin{sagesilent}
  E3 = elementary_matrix(3, row1=0, row2=2, scale=4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[row addition]
    Let $n=3$ and consider $E=[R_1+4\cdot R_3\to R_1]$. Then
    \[
    E=\sage{E3}
    \]
    \pause
    Then $E^{-1}=[R_1-4\cdot R_3\to R_1]$, which gives
    \[
    E^{-1}=\sage{E3.inverse()}
    \]
  \end{example}
\end{frame}


\subsection{The Fundamental Theorem of Linear Algebra}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $n\times n$ matrix. The the following are equivalent.
    \begin{itemize}[<+->]
    \item $A$ is invertible
    \item $A^{-1}$ exists
    \item $A$ is nonsingular
    \item $\rank(A)=n$
    \item $\rref(A)=I_n$
    \item every system $A\vv{x}=\vv{b}$ has at least one solution for every
      $\vv{b}$
    \item every system $A\vv{x}=\vv{b}$ has at most one solution for every
      $\vv{b}$
    \end{itemize}
  \end{theorem}

\end{frame}



\subsection{Further Properties of Inverses}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ and $B$ be invertible $n\times n$ matrices. Then
    \begin{itemize}[<+->]
    \item $(A^{-1})^{-1}=A$
    \item $(A^\intercal)^{-1}=(A^{-1})^\intercal$
    \item $(AB)^{-1}=B^{-1}A^{-1}$
    \item $(A^m)^{-1}=(A^{-1})^m$
    \item if $c\neq 0$, then $(c\cdot A)^{-1}=(\nicefrac{1}{c})\cdot A^{-1}$
    \end{itemize}
  \end{theorem}
\end{frame}



\section{Determinants}
\subsection{Definitions}


\newcommand{\myA}{
  \left[\begin{array}{rrr}
      1 & 2 & 3 \\
      7 & 0 & 10 \\
      0 & -1 & 6
    \end{array}\right]}
\newcommand{\myAyxtransp}{
  \left[\begin{array}{rrr}
      \semitransp[15]{1} & 2 & 3 \\
      \semitransp[15]{7} & \semitransp[15]{0} & \semitransp[15]{10} \\
      \semitransp[15]{0} & -1 & 6
    \end{array}\right]}
\newcommand{\myAyx}{
  \left[\begin{array}{rrr}
      2 & 3 \\
      -1 & 6
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$(i,j)$-submatrix} of a $n\times n$ matrix $A$ is the
    $(n-1)\times(n-1)$ matrix $A_{ij}$ obtained by deleting the $i$th row and
    $j$th column of $A$.
  \end{definition}

  \pause

  \begin{example}
    Let $A=\myA$. Then
    \[
    A_{21}
    =\myAyxtransp
    =\myAyx
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{determinant} of a $1\times 1$ matrix $A=[a]$ is $\det(A)=a$.
  \end{definition}

  \pause

  \begin{definition}
    The \emph{determinant} of a $n\times n$ matrix is 
    \[
    \det(A)=\sum_{k=1}^n (-1)^{1+k}a_{k1}\det(A_{k1})
    \]
  \end{definition}
\end{frame}


\newcommand{\myB}{
  \left[\begin{array}{rr}
      a & b \\
      c & d
    \end{array}\right]}
\newcommand{\myBxx}{
  \left\lvert\begin{array}{rr}
      \semitransp[15]{a} & \semitransp[15]{b} \\
      \semitransp[15]{c} & d
    \end{array}\right\rvert}
\newcommand{\myByx}{
  \left\lvert\begin{array}{rr}
      \semitransp[15]{a} & b \\
      \semitransp[15]{c} & \semitransp[15]{d}
    \end{array}\right\rvert}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    For $A=\myB$ we have
    \begin{align*}
      \det(A)
      &\onslide<2->{=\sum_{k=1}^2(-1)^{1+k}a_{k1}\det(A_{k1})} \\
      &\onslide<3->{=(-1)^{1+1}a_{11}\myBxx+(-1)^{1+2}a_{21}\myByx} \\
      &\onslide<4->{=ad-bc}
    \end{align*}
  \end{example}
\end{frame}


\newcommand{\myC}{
  \left[\begin{array}{rrr}
      a_1 & b_1 & c_1 \\
      a_2 & b_2 & c_2 \\
      a_3 & b_3 & c_3 
    \end{array}\right]}
\newcommand{\myCxx}{
  \left\lvert\begin{array}{rrr}
      \semitransp[15]{a_1} & \semitransp[15]{b_1} & \semitransp[15]{c_1} \\
      \semitransp[15]{a_2} & b_2 & c_2 \\
      \semitransp[15]{a_3} & b_3 & c_3
    \end{array}\right\rvert}
\newcommand{\myCyx}{
  \left\lvert\begin{array}{rrr}
      \semitransp[15]{a_1} & b_1 & c_1 \\
      \semitransp[15]{a_2} & \semitransp[15]{b_2} & \semitransp[15]{c_2} \\
      \semitransp[15]{a_3} & b_3 & c_3 
    \end{array}\right\rvert}
\newcommand{\myCzx}{
  \left\lvert\begin{array}{rrr}
      \semitransp[15]{a_1} & b_1 & c_1 \\
      \semitransp[15]{a_2} & b_2 & c_2 \\
      \semitransp[15]{a_3} & \semitransp[15]{b_3} & \semitransp[15]{c_3}
    \end{array}\right\rvert}
\newcommand{\myCxxt}{
  \left\lvert\begin{array}{rr}
      b_2 & c_2 \\
      b_3 & c_3 
    \end{array}\right\rvert}
\newcommand{\myCyxt}{
  \left\lvert\begin{array}{rr}
      b_1 & c_1 \\
      b_3 & c_3 
    \end{array}\right\rvert}
\newcommand{\myCzxt}{
  \left\lvert\begin{array}{rr}
      b_1 & c_1 \\
      b_2 & c_2 
    \end{array}\right\rvert}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    For $A=\myC$ we have
    \begin{align*}
      \det(A)
      &= \onslide<2->{\sum_{k=1}^3(-1)^{1+k}a_{k1}\det(A_{k1})} \\
      & \onslide<3->{=(-1)^{1+1}a_{11}\cdot\myCxx} \\
      & \onslide<3->{\qquad+(-1)^{1+2}a_{21}\cdot\myCyx+(-1)^{1+3}a_{31}\cdot\myCzx} \\
      & \onslide<4->{=a_1\cdot\myCxxt-a_2\cdot\myCyxt+a_3\cdot\myCzxt}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Minors and Cofactors}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{$(i,j)$-minor} of $A$ is $M_{ij}=\det(A_{ij})$. The
    \emph{$(i,j)$-cofactor} of $A$ is $C_{ij}=(-1)^{i+j}M_{ij}$.
  \end{definition}
\end{frame}




\newcommand{\myCo}{
  \left[\begin{array}{rrr}
      a_1 & b_1 & c_1 \\
      a_2 & b_2 & c_2 \\
      a_3 & b_3 & c_3 
    \end{array}\right]}

\newcommand{\myCoxx}{
  (-1)^{1+1}\cdot
  \left\lvert\begin{array}{rrr}
      \semitransp[14]{a_1} & \semitransp[14]{b_1} & \semitransp[15]{c_1} \\
      \semitransp[14]{a_2} & b_2 & c_2 \\
      \semitransp[14]{a_3} & b_3 & c_3 
    \end{array}\right\rvert}
\newcommand{\myCoyx}{
  (-1)^{2+1}\cdot
  \left\lvert\begin{array}{rrr}
      \semitransp[14]{a_1} & b_1 & c_1 \\
      \semitransp[14]{a_2} & \semitransp[15]{b_2} & \semitransp[15]{c_2} \\
      \semitransp[14]{a_3} & b_3 & c_3 
    \end{array}\right\rvert}
\newcommand{\myCozx}{
  (-1)^{3+1}\cdot
  \left\lvert\begin{array}{rrr}
      \semitransp[14]{a_1} & b_1 & c_1 \\
      \semitransp[14]{a_2} & b_2 & c_2 \\
      \semitransp[14]{a_3} & \semitransp[15]{b_3} & \semitransp[15]{c_3}
    \end{array}\right\rvert}

\newcommand{\myCoxy}{
  (-1)^{1+2}\cdot
  \left\lvert\begin{array}{rrr}
      \semitransp[15]{a_1} & \semitransp[15]{b_1} & \semitransp[15]{c_1} \\
      a_2                  & \semitransp[15]{b_2} & c_2 \\
      a_3                  & \semitransp[15]{b_3} & c_3
    \end{array}\right\rvert}
\newcommand{\myCoyy}{
  (-1)^{2+2}\cdot
  \left\lvert\begin{array}{rrr}
      a_1                  & \semitransp[15]{b_1} & c_1 \\
      \semitransp[15]{a_2} & \semitransp[15]{b_2} & \semitransp[15]{c_2} \\
      a_3                  & \semitransp[15]{b_3} & c_3
    \end{array}\right\rvert}
\newcommand{\myCozy}{
  (-1)^{3+2}\cdot
  \left\lvert\begin{array}{rrr}
      a_1                  & \semitransp[15]{b_1} & c_1 \\
      a_2                  & \semitransp[15]{b_2} & c_2 \\
      \semitransp[15]{a_3} & \semitransp[15]{b_3} & \semitransp[15]{c_3}
    \end{array}\right\rvert}


\newcommand{\myCoxz}{
  (-1)^{1+3}\cdot
  \left\lvert\begin{array}{rrr}
      \semitransp[15]{a_1} & \semitransp[15]{b_1} & \semitransp[15]{c_1} \\
      a_2                  & b_2                  & \semitransp[15]{c_2} \\
      a_3                  & b_3                  & \semitransp[15]{c_3}
    \end{array}\right\rvert}
\newcommand{\myCoyz}{
  (-1)^{2+3}\cdot
  \left\lvert\begin{array}{rrr}
      a_1                  & b_1                  & \semitransp[15]{c_1} \\
      \semitransp[15]{a_2} & \semitransp[15]{b_2} & \semitransp[15]{c_2} \\
      a_3                  & b_3                  & \semitransp[15]{c_3}
    \end{array}\right\rvert}
\newcommand{\myCozz}{
  (-1)^{3+3}\cdot
  \left\lvert\begin{array}{rrr}
      a_1                  & b_1                  & \semitransp[15]{c_1} \\
      a_2                  & b_2                  & \semitransp[15]{c_2} \\
      \semitransp[15]{a_3} & \semitransp[15]{b_3} & \semitransp[15]{c_3}
    \end{array}\right\rvert}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The cofactors of $A=\myCo$ are
    {\tiny
      \begin{align*}
        C_{11} &= \myCoxx  & C_{12} &= \myCoxy  & C_{13} &= \myCoxz \\
        C_{21} &= \myCoyx  & C_{22} &= \myCoyy  & C_{23} &= \myCoyz \\
        C_{31} &= \myCozx  & C_{32} &= \myCozy  & C_{33} &= \myCozz 
      \end{align*}}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The signs of the cofactors can be remembered by
    \[
    \begin{matrix}
      + & - & + & - & \dotsb \\
      - & + & - & + & \dotsb \\
      + & - & + & - & \dotsb \\
      \vdots & \vdots & \vdots & \vdots & \ddots 
    \end{matrix}
    \]
  \end{block}

  \pause

  \begin{block}{Note}
    The determinant formula can be written in terms of cofactors
    \[
    \det(A)=\sum_{k=1}^n a_{k1}C_{k1}
    \]
  \end{block}
\end{frame}


\subsection{Laplace Cofactor Expansion Theorem}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Laplace Cofactor Expansion Theorem]
    For any $1\leq i\leq n$ we have
    \begin{align*}
      \onslide<2->{\det(A) &= \sum_{k=1}^n a_{ik}C_{ik}}\only<2->{\tag{$i$th row expansion}} \\
      \onslide<3->{\det(A) &= \sum_{k=1}^n a_{ki}C_{ki}}\only<3->{\tag{$i$th col expansion}}
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    The Laplace Cofactor Expansion Theorem allows us to compute determinants by
    ``expanding'' about any row or column. This is useful because some rows or
    columns may be easier to expand about than others.
  \end{block}
\end{frame}



\newcommand{\mydetA}{
  \left[
    \begin{array}{rrr}
      1 & 2 & 3 \\ 
      4 & 5 & 6 \\ 
      7 & 8 & 9
    \end{array}\right]}

\newcommand{\mydetAyx}{
  \left\lvert
    \begin{array}{rrr}
      2 & 3 \\ 
      8 & 9
    \end{array}\right\rvert}
\newcommand{\mydetAyy}{
  \left\lvert
    \begin{array}{rrr}
      1 &  3 \\ 
      7 &  9
    \end{array}\right\rvert}
\newcommand{\mydetAyz}{
  \left\lvert
    \begin{array}{rrr}
      1 & 2  \\ 
      7 & 8 
    \end{array}\right\rvert}


\newcommand{\mydetAxz}{
  \left\lvert
    \begin{array}{rrr}
      4 & 5  \\ 
      7 & 8 
    \end{array}\right\rvert}
\newcommand{\mydetAzz}{
  \left\lvert
    \begin{array}{rrr}
      1 & 2  \\ 
      4 & 5  
    \end{array}\right\rvert}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\det(A)$, where $A=\mydetA$.
  \end{example}

  \pause

  \begin{block}{Solution}
    Expansion about $\Row_2$ gives \pause
    \[
    \det(A)
    = (-1)(4)\mydetAyx
    + (1)(5)\mydetAyy
    + (-1)(6)\mydetAyz
    \]

    \pause
    
    Expansion about $\Col_3$ gives \pause
    \[
    \det(A)
    = (1)(3)\mydetAxz
    + (-1)(6)\mydetAyz
    + (1)(9)\mydetAzz
    \]
  \end{block}

\end{frame}


\newcommand{\myexA}{
  \left[\begin{array}{rrrr}
      7 & -3 &  0 &  4 \\
      0 &  1 &  0 &  3 \\
      2 &  1 & -2 & -5 \\
      0 &  4 &  0 &  6
    \end{array}\right]}
\newcommand{\myexAa}{
  \left\lvert\begin{array}{rrrr}
      7 & -3 &    4 \\
      0 &  1 &    3 \\
      0 &  4 &    6
    \end{array}\right\rvert}
\newcommand{\myexAb}{
  \left\lvert\begin{array}{rrrr}
      1 &    3 \\
      4 &    6
    \end{array}\right\rvert}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\det(A)$ where $A={\tiny\myexA}$.
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \setlength{\abovedisplayskip}{0pt}
    \setlength{\belowdisplayskip}{0pt}
    \begin{align*}
      \onslide<2->{\det(A)
        &= (-2)\myexAa} \only<2->{\tag{$\Col_3$ expansion}}\\
      & \onslide<3->{= (-2)(7)\myexAb} \only<3->{\tag{$\Col_1$ expansion}} \\
      & \onslide<4->{= (-2)(7)(6-12)} \\
      & \onslide<5->{= 84}
    \end{align*}
  \end{block}
\end{frame}


\subsection{Properties of Determinants}

\begin{sagesilent}
  A = random_matrix(ZZ, 6, 5)
  b = zero_vector(6)
  A = A.augment(b)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    If $A$ has a row or column of zeros, then $\det(A)=0$.
  \end{theorem}

  \pause

  \begin{example}
    $\det\sage{A}=0$
  \end{example}
\end{frame}



\newcommand{\myU}{
  \left\lvert\begin{array}{rrrr}
      6 & -3 & -4 & -6 \\
      0 &  3 &  3 & 7 \\
      0 &  0 &  2 & -\nicefrac{17}{3} \\
      0 &  0 &  0 & -\nicefrac{1}{6}
    \end{array}\right\rvert}
\newcommand{\myUa}{
  (6)
  \left\lvert\begin{array}{rrrr}
      3 &  3 & 7 \\
      0 &  2 & -\nicefrac{17}{3} \\
      0 &  0 & -\nicefrac{1}{6}
    \end{array}\right\rvert}
\newcommand{\myUb}{
  (6)(3)
  \left\lvert\begin{array}{rrrr}
      2 & -\nicefrac{17}{3} \\
      0 & -\nicefrac{1}{6}
    \end{array}\right\rvert}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The determinant of a triangular matrix is the product of its diagonal
    entries.
  \end{theorem}

  \onslide<2->{
  \begin{example}}
    {\scriptsize
      \[
      \onslide<2->{\myU}
      \onslide<3->{=\myUa}
      \onslide<4->{=\myUb}
      \onslide<5->{=(6)(3)(2)(-\nicefrac{1}{6})}
      \]}
  \end{example}

  \onslide<6->{
  \begin{example}
    $\det(I_n)=}\onslide<7->{1$
  \end{example}}

\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 4)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\det(A^\intercal)=\det(A)$
  \end{theorem}

  \pause

  \begin{example}
    One computes
    \[
    \det\sage{A}=\sage{A.det()}
    \]

    \pause

    It follows that
    \[
    \det\sage{A.transpose()}=\pause\sage{A.transpose().det()}
    \]
  \end{example}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Determinants play nicely with elementary row operations.
    \begin{itemize}
    \item<2-> $A\xrightarrow{R_i\leftrightarrow R_j}B\implies \det(B)=-\det(A)$
    \item<3-> $A\xrightarrow{c\cdot R_i\to R_i}B\implies\det(B)=c\cdot\det(A)$
    \item<4-> $A\xrightarrow{R_i+c\cdot R_j\to R_i}B\implies\det(B)=\det(A)$
    \end{itemize}
  \end{theorem}

  \onslide<5->{
  \begin{block}{Note}
    Every elementary matrix $E$ is obtained by performing exactly one elementary
    row operation on the identity matrix $I_n\to E$. Thus}
    \begin{itemize}
    \item<6-> $\det[R_i\leftrightarrow R_j]=-1$
    \item<7-> $\det[c\cdot R_i\to R_i]=c$
    \item<8-> $\det[R_i+c\cdot R_j\to R_i]=1$
    \end{itemize}
  \end{block}
\end{frame}


\section{Further Properties of Determinants}
\subsection{Determinants and Invertibility}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 3, algorithm='echelonizable', rank=3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A matrix $A$ is invertible if and only if $\det(A)\neq 0$.
  \end{theorem}

  \pause

  \begin{example}
    Let $A=\sage{A}$. \pause Then $\det(A)=\sage{A.det()}\neq0$. \pause Hence
    $A$ is invertible.
  \end{example}
\end{frame}


\subsection{Determinants are Multiplicative}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ and $B$ be $n\times n$ matrices. Then $\det(AB)=\det(A)\det(B)$.
  \end{theorem}

  \pause

  \begin{theorem}
    If $A$ is invertible, then $\det(A^{-1})=\nicefrac{1}{\det(A)}$.
  \end{theorem}

  \pause

  \begin{proof}
    Note that 
    \[
    \det(A^{-1})\det(A)=\det(A^{-1}A)=\det(I)=1
    \]
    Hence $\det(A^{-1})=\nicefrac{1}{\det(A)}$.
  \end{proof}
\end{frame}


\subsection{Row Reductions and Determinants}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    \onslide<1->{Perform elementary row operations on a matrix $A$ to obtain a
      new matrix $B$. }%
    \onslide<2->{This gives
    \[
    E_r\dotsb E_2E_1A=B
    \]
    where each $E_i$ is the elementary matrix corresponding to an elementary row
    operation. }%
  \onslide<3->{Taking determinants gives
    \[
    \det(E_r)\dotsb\det(E_2)\det(E_1)\det(A)=\det(B)
    \]}%
  \onslide<4->{This simplifies the computation of $\det(A)$, assuming that
    computing $\det(B)$ is ``easy.''}%
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(1, 1, -1, -1), (3, 0, 1, -4), (-2, 1, -2, 0), (1, 2, 0, 0)])
  E1 = elementary_matrix(4, row1=1, row2=0, scale=-3)
  E2 = elementary_matrix(4, row1=2, row2=0, scale=2)
  E3 = elementary_matrix(4, row1=3, row2=0, scale=-1)
  E4 = elementary_matrix(4, row1=2, row2=1, scale=1)
  E5 = elementary_matrix(4, row1=3, row2=1, scale=1/3)
  E6 = elementary_matrix(4, row1=2, row2=3)
\end{sagesilent}

\newcommand{\mystepa}{\tiny
  \begin{array}{lcr}
    R_2-3\cdot R_1 &\to& R_2 \\
    R_3+2\cdot R_1 &\to& R_3 \\
    R_4-R_1 &\to& R_4
  \end{array}}

\newcommand{\mystepb}{\tiny
  \begin{array}{lcr}
    R_3+R_2 &\to& R_3 \\
    R_4+(\nicefrac{1}{3})\cdot R_2 &\to& R_4
  \end{array}}

\newcommand{\mystepc}{\tiny
  \begin{array}{lcr}
    R_3 &\leftrightarrow & R_4
  \end{array}}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute $\det{\tiny\sage{A}}$.
  \end{example}
  \begin{block}{Solution}
    Row reduce.
    \begin{align*}
      {\tiny\sage{A}}
      &\onslide<2->{\xrightarrow{\mystepa}{\tiny\sage{E3*E2*E1*A}}} \\
      &\onslide<3->{\xrightarrow{\mystepb}{\tiny\sage{E5*E4*E3*E2*E1*A}}} \\
      &\onslide<4->{\xrightarrow{\mystepc}{\tiny\sage{E6*E5*E4*E3*E2*E1*A}}}
    \end{align*}
    \onslide<5->{Then $(-1)\det(A)=(-3)(\nicefrac{7}{3})(-3)$. }%
    \onslide<6->{Hence $\det(A)=-21$.}%
  \end{block}
\end{frame}

\subsection{Cofactors and Adjoints}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{cofactor matrix} of a $n\times n$ matrix $A$ is 
    \[
    C=
    \begin{bmatrix}
      C_{11} & C_{12} & \dotsb & C_{1n} \\
      C_{21} & C_{22} & \dotsb & C_{2n} \\
      \vdots & \vdots & \ddots & \vdots \\
      C_{n1} & C_{n2} & \dotsb & C_{nn} \\
    \end{bmatrix}
    \]
  \end{definition}

  \pause

  \begin{definition}
    The \emph{adjoint} of a $n\times n$ matrix $A$ is 
    \[
    \adj(A)
    = C^\intercal
    =
    \begin{bmatrix}
      C_{11} & C_{21} & \dotsb & C_{n1} \\
      C_{12} & C_{22} & \dotsb & C_{n2} \\
      \vdots & \vdots & \ddots & \vdots \\
      C_{1n} & C_{2n} & \dotsb & C_{nn} \\
    \end{bmatrix}
    \]
  \end{definition}
\end{frame}



\newcommand{\mytt}{
  \begin{bmatrix}
    a & b \\
    c & d
  \end{bmatrix}}

\newcommand{\myttxx}{
  (-1)^{1+1}\cdot
  \begin{vmatrix}
    \semitransp[15]{a} & \semitransp[15]{b} \\
    \semitransp[15]{c} & \semitransp[100]{d}
  \end{vmatrix}}
\newcommand{\myttxy}{
  (-1)^{1+2}\cdot
  \begin{vmatrix}
    \semitransp[15]{a} & \semitransp[15]{b} \\
    \semitransp[100]{c} & \semitransp[15]{d}
  \end{vmatrix}}
\newcommand{\myttyx}{
  (-1)^{2+1}\cdot
  \begin{vmatrix}
    \semitransp[15]{a} & \semitransp[100]{b} \\
    \semitransp[15]{c} & \semitransp[15]{d}
  \end{vmatrix}}
\newcommand{\myttyy}{
  (-1)^{2+2}\cdot
  \begin{vmatrix}
    \semitransp[100]{a} & \semitransp[15]{b} \\
    \semitransp[15]{c} & \semitransp[15]{d}
  \end{vmatrix}}
\newcommand{\myttC}{
  \left[
    \begin{array}{rr}
      d & -c \\
      -b &  a
    \end{array}\right]}
\newcommand{\myttadj}{
  \left[
    \begin{array}{rr}
      d & -b \\
      -c &  a
    \end{array}\right]}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\mytt$. Compute $C$ and $\adj(A)$.
  \end{example}

  \pause

  \begin{block}{Solution}
    The cofactors of $A$ are \pause
    \begin{align*}
      C_{11} &= \myttxx=d & C_{12} &= \myttxy=-c \\
      C_{21} &= \myttyx=-b & C_{22} &= \myttyy=a \\
    \end{align*}

    \pause

    Thus
    \begin{align*}
      C &= \myttC & \adj(A) &= \myttadj
    \end{align*}
  \end{block}
\end{frame}



\newcommand{\myttAadj}{
  \left[
    \begin{array}{cc}
      ad-bc & 0 \\
      0     & ad-bc
    \end{array}\right]}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every $n\times n$ matrix $A$ satisfies $A\adj(A)=\adj(A)A=\det(A)\cdot I$.
  \end{theorem}

  \pause

  \begin{example}
    For $A=\mytt$ we found $\adj(A)=\myttadj$. \pause Then
    \[
    A\adj(A)
    = \pause\mytt\myttadj
    = \pause\myttAadj
    = \pause\det(A)\cdot I
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $A$ is invertible. Then
    \[
    A^{-1}=\frac{1}{\det(A)}\cdot\adj(A)
    \]
  \end{theorem}

  \pause

  \begin{example}
    For $A=\mytt$ invertible we have
    \[
    A^{-1}=\frac{1}{ad-bc}\cdot\myttadj
    \]
  \end{example}
\end{frame}


\subsection{Cramer's Rule}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Cramer's Rule]
    Consider the system $A\vv{x}=\vv{b}$ where $A$ is invertible. Let $A_i$ be
    the matrix obtained by replacing the $i$th column of $A$ by $\vv{b}$. Then
    the coordinates of $\vv{x}$ are given by
    \[
    x_i=\frac{\det(A_i)}{\det(A)}
    \]
  \end{theorem}
\end{frame}


\begin{sagesilent}
  A = matrix([(2, -2, -1), (4, -1, 4), (-1, 1, -1)])
  b = vector([-2, -88, -9])
  c1, c2, c3 = A.columns()
  A1 = matrix.column([b, c2, c3])
  A2 = matrix.column([c1, b, c3])
  A3 = matrix.column([c1, c2, b])
  var('x y z')
  vecx = matrix.column([x, y, z])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The system 
    \[
    \begin{array}{rcrcrcr}
      2\,x &-& 2\,y &-&    z &=&  -2 \\
      4\,x &-&    y &+& 4\,z &=& -88 \\
      -\,x &+&    y &-&    z &=&  -9
    \end{array}
    \]
    is of the form $A\vv{x}=\vv{b}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{vecx} & \vv{b} &= \sage{matrix.column(b)}
    \end{align*}
    \pause
    Note that $\det(A)=\sage{A.det()}\neq 0$, so $A$ is invertible.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    Note that
    \begin{align*}
      \onslide<2->{\det(A_1) &= \det{\sage{A1}} = \sage{A1.det()}} \\
      \onslide<3->{\det(A_2) &= \det{\sage{A2}} = \sage{A2.det()}} \\
      \onslide<4->{\det(A_3) &= \det{\sage{A3}} = \sage{A3.det()}} 
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    We are thus studying the system $A\vv{x}=\vv{b}$ where 
    \begin{align*}
      \onslide<2->{\det(A)   &= \sage{A.det()} } &
      \onslide<3->{\det(A_1) &= \sage{A1.det()}} &
      \onslide<4->{\det(A_2) &= \sage{A2.det()}} &
      \onslide<5->{\det(A_3) &= \sage{A3.det()}} 
    \end{align*}%
    \onslide<6->{Cramer's Rule implies that the unique solution to
      $A\vv{x}=\vv{b}$ is given by $\vv{x}=\langle x, y, z\rangle$ where}%
    \begin{align*}
      \onslide<7->{x &= \frac{\det(A_1)}{\det(A)}=\frac{\sage{A1.det()}}{\sage{A.det()}}} &
      \onslide<8->{y &= \frac{\det(A_2)}{\det(A)}=\frac{\sage{A2.det()}}{\sage{A.det()}}} &
      \onslide<9->{z &= \frac{\det(A_3)}{\det(A)}=\frac{\sage{A3.det()}}{\sage{A.det()}}}
    \end{align*}%
    \onslide<10->{This gives $\vv{x}=\left\langle\sage{A1.det()/A.det()},
        \sage{A2.det()/A.det()}, \sage{A3.det()/A.det()}\right\rangle$.}%
  \end{example}
\end{frame}


\end{document}
