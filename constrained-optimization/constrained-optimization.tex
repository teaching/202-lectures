\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Constrained Optimization}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Motivation}
\subsection{Open-Box Problem}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{columns}[onlytextwidth, t]
      \column{.6\textwidth} \onslide<1->{An open rectangular box is to be
        manufactured having a fixed volume of $\SI{4}{m^3}$. }%
      \onslide<2->{What dimensions should the box have so as to minimize the amount of
      material used to make it? }%
    \column{.4\textwidth}
      \[
      \newcommand{\Depth}{3}%
      \newcommand{\Height}{2}%
      \newcommand{\Width}{2}%
      \onslide<3->{
      \begin{tikzpicture}[scale=3/5]
        \coordinate (O) at (0,0,0); \coordinate (A) at (0,\Width,0); \coordinate
        (B) at (0,\Width,\Height); \coordinate (C) at (0,0,\Height); \coordinate
        (D) at (\Depth,0,0); \coordinate (E) at (\Depth,\Width,0); \coordinate
        (F) at (\Depth,\Width,\Height); \coordinate (G) at (\Depth,0,\Height);

        \draw[blue,fill=yellow!80] (O) -- (C) -- (G) -- (D) --
        cycle;% Bottom Face
        \draw[blue,fill=blue!30] (O) -- (A) -- (E) -- (D) -- cycle;% Back Face
        \draw[blue,fill=red!10] (O) -- (A) -- (B) -- (C) -- cycle;% Left Face
        \draw[blue,fill=red!20,opacity=0.8] (D) -- (E) -- (F) -- (G) --
        cycle;% Right Face
        \draw[blue,fill=red!20,opacity=0.6] (C) -- (B) -- (F) -- (G) --
        cycle;% Front Face
        \draw[blue,fill=red!20,opacity=0.8] (A) -- (B) -- (F) -- (E) --
        cycle;% Top Face

        \node at ($ (C)!.5!(G) $) [below] {$x$}; \node at ($ (G)!.5!(D) $)
        [below] {$z$}; \node at ($ (C)!.5!(B) $) [left] {$y$};

        % Following is for debugging purposes so you can see where the points
        % are
        % These are last so that they show up on top
        % \foreach \xy in {O, A, B, C, D, E, F, G}{
        % \node at (\xy) {\xy};
        % }
      \end{tikzpicture}}
      \]
    \end{columns}
  \end{example}

  \begin{block}{\onslide<4->{Idea}}
    \onslide<4->{The surface area of the box is}
    \[
    \onslide<5->{A(x, y, z) =} 
    \onslide<6->{\underbrace{2\,xy}_{\text{front and back}}}
    \onslide<7->{+\underbrace{2\,yz}_{\text{sides}}}
    \onslide<8->{+\underbrace{xz}_{\text{bottom only}}}
    \]
    \onslide<9->{We wish to \emph{minimize} $A(x, y, z)$ }%
    \onslide<10->{subject to the constraint }%
    \onslide<11->{$V(x,y,z)=4$ where $V(x, y, z)=xyz$.}
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Na\"{i}ve Solution}
    The constraint enables us to solve for $z$ in terms of $x$ and $y$\pause
    \[
    z=\pause\frac{4}{xy}
    \]\pause
    We can then create a new area function of only two variables
    \[
    a(x, y)
    = \pause A(x, y, \nicefrac{4}{xy})
    = \pause 2\,xy+\frac{8}{x}+\frac{4}{y}
    \]\pause
    The critical points of $a$ are found via the equation $\nabla
    a=\vv{O}$. \pause The only critical point with $x>0$ is $(x,y)=(2,
    1)$. \pause The desired dimensions of the box are thus $(x, y, z)=(2, 1,
    2)$.
  \end{block}
\end{frame}



\begin{sagesilent}
  var('x y z')
  f = x**2+3*y**2+y**2*z**4
  g = exp(x*y)-x**5*y**2*z+cos(x/y*z)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why is this solution ``na\"{i}ve''?
  \end{block}

  \pause
  \begin{block}{Answer}
    The procedure does not generalize to similar examples.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Maximize the function 
    \[
    f(x,y,z)=\sage{f}
    \]
    subject to the constraint $g(x,y,z)=2$ where
    \[
    g(x,y,z)=\sage{g}
    \]\pause
    The equation $g(x,y,z)=2$ does not allow us to easily ``isolate'' one of the
    variables $x$, $y$, or $z$. \pause We need a better method!
  \end{example}
\end{frame}


\section{The Method of Lagrange Multipliers}
\subsection{Critical Points of Constrained Optimization Problems}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Goal}
    We wish to optimize a function $f(x_1,\dotsc,x_n)$ subject to the constraint
    $g(x_1,\dotsc,x_n)=c$.
  \end{block}

  \pause
  \begin{definition}
    The function $f$ is the \emph{objective function} \pause and the function
    $g$ is the \emph{constraint function}.
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{critical point} of the constrained optimization problem with
    objective function $f$ and constraint $g=c$ is a point $P$ satisfying
    $g(P)=c$ and \pause
    \begin{align*}
      \nabla f(P) &= \lambda\cdot\nabla g(P) & \text{or} &&
      \nabla g(P) &\text{ DNE}
    \end{align*}
    for some scalar $\lambda\in \mathbb{R}$. \pause The scalar $\lambda$ is
    called a \emph{Lagrange multiplier} of the constrained optimization problem.
  \end{definition}

  \pause
  \begin{theorem}
    The extreme values of an objective function $f$ subject to a constraint
    $g=c$ occur only at critical points.
  \end{theorem}
\end{frame}


\subsection{Lagrange Multiplier Algorithm}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    To determine the extreme values of an objective function $f$ subject to a
    constraint $g=c$, proceed in the following manner:\pause
    \begin{enumerate}
    \item Solve the system
      \begin{align*}
        \nabla f(P) &= \lambda\cdot \nabla g(P) &
        g(P) &= c
      \end{align*}
      for $P$ and $\lambda$ and determine where $\nabla g(P)$ DNE. \pause This
      gives a list of critical points $\Set{P_1,\dotsc,P_k}$ for the constrained
      optimization problem.\pause
    \item Determine the maximum and minimum values of the constrained
      optimization problem by plugging each critical point
      $\Set{P_1,\dotsc,P_k}$ into $f$.
    \end{enumerate}
  \end{block}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y z')
  A = 2*x*y+2*y*z+x*z
  V = x*y*z
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider again the constrained optimization problem with objective function
    $A$ and constraint $V=4$ where
    \begin{align*}
      A(x, y, z) &= \sage{A} &
      V(x, y, z) &= \sage{V}
    \end{align*}\pause
    The equation $\nabla A=\lambda\cdot \nabla V$ gives the system
    \begin{align*}
      \sage{A.diff(x)} &= \lambda\sage{V.diff(x)} &
      \sage{A.diff(y)} &= \lambda\sage{V.diff(y)} &
      \sage{A.diff(z)} &= \lambda\sage{V.diff(z)}
    \end{align*}\pause
    Solving each equation for $\lambda$ gives
    \[
    \lambda
    = \sage{expand(A.diff(x)/V.diff(x))}
    = \sage{expand(A.diff(y)/V.diff(y))}
    = \sage{expand(A.diff(z)/V.diff(z))}
    \]
    The first equality simplifies to $x=2\,y$ \pause and the second equality
    simplifies to $z=2\,y$. \pause Plugging into the constraint equation $V=4$
    then gives
    \begin{align*}
      (2\,y)(y)(2\,y) &= 4 & \Leftrightarrow &&
      y &= 1
    \end{align*}\pause
    The only critical point is thus $(x, y, z) = (2, 1, 2)$.
  \end{example}
\end{frame}
\endgroup

\begin{sagesilent}
  var('x y z')
  f = x**2+y**2+z**2
  g = 6*x+3*y+2*z
  c = 49
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find all critical points of $f(x,y,z)=\sage{f}$ subject to
    $\sage{g}=\sage{c}$. %
    \onslide<2->{Geometrically, this constrained problem locates the
    point on the plane $\sage{g}=\sage{c}$ closest to the origin.}
  \end{example}
  \begin{block}{\onslide<3->{Solution}}
    \onslide<3->{Let $g(x,y,z)=\sage{g}$. }%
    \onslide<4->{The equations $\nabla f=\lambda\cdot\nabla g$ and $g=\sage{c}$
      give the system of equations}
    \onslide<5->{
    \begin{align*}
      \sage{f.diff(x)} &= \lambda\cdot\sage{g.diff(x)} &
      \sage{f.diff(y)} &= \lambda\cdot\sage{g.diff(y)} &
      \sage{f.diff(z)} &= \lambda\cdot\sage{g.diff(z)} &
      \sage{g} &= \sage{c}
    \end{align*}}
  \onslide<5->{This is a linear system of equations.}
    \[
    \onslide<7->{\rref}
    \onslide<6->{\left[\begin{array}{rrrr|r}
        2\tikzmark{o1}& 0\tikzmark{o2}& 0\tikzmark{o3}& -6\tikzmark{o4}&  0 \\
        0             & 2             & 0             & -3             &  0 \\
        0             & 0             & 2             & -2             &  0 \\
        6             & 3             & 2             &  0             & 49
      \end{array}\right]}
  \onslide<7->{=}
  \onslide<8->{
    \left[\begin{array}{rrrr|r}
        1\tikzmark{x1} & 0\tikzmark{x2} & 0\tikzmark{x3} & 0\tikzmark{x4} & 6 \\
        0 & 1 & 0 & 0 & 3 \\
        0 & 0 & 1 & 0 & 2 \\
        0 & 0 & 0 & 1 & 2
      \end{array}\right]}
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \onslide<6->{
      \node at (o1.north west) {$x$};
      \node at (o2.north west) {$y$};
      \node at (o3.north west) {$z$};
      \node at (o4.north west) {$\lambda$};
      }

      \onslide<8->{
      \node at (x1.north west) {$x$};
      \node at (x2.north west) {$y$};
      \node at (x3.north west) {$z$};
      \node at (x4.north west) {$\lambda$};
      }
    \end{tikzpicture}
    \]
    \onslide<9->{The only critical point is thus $(x, y, z)=(6, 3, 2)$.}
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  var('x y')
  f = 2*x+y
  g = sqrt(x)+sqrt(y)
  c = 3
\end{sagesilent}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find all critical points of $f(x,y)=\sage{f}$ subject to
    $\sage{g}=\sage{c}$.
  \end{example}

  \pause
  \begin{block}{Solution}
    Let $g(x,y)=\sage{g}$. \pause The equations $\nabla f=\lambda\cdot\nabla g$
    and $g=\sage{c}$ give the system\pause
    \begin{align*}
      \sage{f.diff(x)} &= \lambda\sage{g.diff(x)} &
      \sage{f.diff(y)} &= \lambda\sage{g.diff(y)} &
      \sage{g} &= \sage{c} 
    \end{align*}\pause
    The first two equations give $\lambda=4\,\sqrt{x}=2\sqrt{y}$, \pause so
    $\sqrt{y}=\pause2\,\sqrt{x}$. \pause Substituting $\sqrt{y}=2\,\sqrt{x}$
    into the constraint equation gives $x=\pause1$. \pause This gives the unique
    critical point \pause $(1,4)$.

    \pause Note, however, that $\nabla g=\sage{g.gradient()}$ is undefined
    at \pause $\Set{(9,0), (0,9)}$. \pause Thus our problem has three critical
    points \pause $\Set{(1,4), (9,0), (0,9)}$.

    \pause
    Note that
    \begin{align*}
      f(1,4) &= \sage{f(x=1, y=4)} &
      f(9,0) &= \sage{f(x=9, y=0)} &
      f(0,9) &= \sage{f(x=0, y=9)}
    \end{align*}\pause
    The minimum value of $f$ subject to $g=\sage{c}$ is \pause $6$ and occurs at
    \pause $(1,4)$. \pause The maximum value of $f$ subject to $g=\sage{c}$ is
    \pause $18$ and occurs at \pause $(9,0)$.
  \end{block}
\end{frame}
\endgroup


\section{Global Extrema with One Inequality Constraint}
\subsection{Set-Up and Method}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Goal}
    Optimize $f$ subject to an inequality constraint $g\leq c$.
  \end{block}

  \pause
  \begin{block}{Strategy}
    To optimize $f$ subject to $g\leq c$:\pause
    \begin{description}
    \item[analyze interior] Find the critical points $P$ of $f$ satisfying
      $g(P)<c$.\pause
    \item[analyze boundary] Use Lagrange multipliers to find the critical points
      $P$ for the constrained optimizatation problem with objective function $f$
      and constraint $g=c$.
    \end{description}
  \end{block}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  f = 4*x**2+10*y**2
  g = x**2+y**2
  c = 4
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Optimize $f(x,y)=\sage{f}$ subject to $g(x,y)\leq\sage{c}$, where
    $g(x,y)=\sage{g}$.
  \end{example}

  \pause
  \begin{block}{Interior Critical Points}
    We start by finding the critical points $P$ of $f$ satisfying
    $g(P)<4$. \pause To do so, note that $\nabla f=\sage{f.gradient()}$. \pause
    The only critical point of $f$ is $P=(0,0)$. \pause Since
    $g(0,0)=0<\sage{c}$, we obtain exactly one critical point $\Set{(0,0)}$.
  \end{block}

  \pause
  \begin{block}{Boundary Critical Points}
    Here, we use Lagrange multipliers to find the critical points of $f$ subject
    to $g=\sage{4}$. \pause To do so, note that the equations $\nabla
    f=\lambda\cdot\nabla g$ and $g=\sage{c}$ give the system
    \begin{align*}
      \sage{f.diff(x)} &= \lambda\cdot\sage{g.diff(x)} &
      \sage{f.diff(y)} &= \lambda\cdot\sage{g.diff(y)} &
      \sage{g} &= \sage{c}
    \end{align*}\pause
    If $x\neq0$, then the first equation implies that $\lambda=\pause4$. \pause
    Plugging $\lambda=4$ into the second equation gives $y=\pause0$. \pause The
    last equation then implies that $x=\pause\pm 2$. \pause This gives two
    critical points \pause $\Set{(\pm2, 0)}$.

    \pause If $x=0$, then the last equation is $y=\pause\pm2$. The second
    equation is then solvable with $\lambda=\pause10$. \pause This gives two
    more critical points \pause $\Set{(0,\pm2)}$.
  \end{block}
\end{frame}
\endgroup


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Comparing Critical Points}
    In total, we have found five critical points for the problem of optimizing
    $f(x,y)=\sage{f}$ subject to $\sage{g}\leq\sage{c}$. \pause These critical
    points are
    \[
    \Set{(0,0), (\pm2,0), (0,\pm2)}
    \]\pause
    Applying $f$ to each critical point gives
    \begin{align*}
      f(0,0) &= \sage{f(x=0, y=0)} &
      f(\pm2, 0) &= \sage{f(x=2, y=0)} &
      f(0, \pm2) &= \sage{f(x=0, y=2)}
    \end{align*}\pause
    Thus, when constrained by $\sage{g}\leq\sage{c}$, $f$ has a minimum value of
    \pause $\sage{f(x=0, y=0)}$ at \pause $(0,0)$ and a maximum value of \pause
    $\sage{f(x=0, y=2)}$ at \pause $(0,\pm 2)$.
  \end{block}
\end{frame}


\end{document}
