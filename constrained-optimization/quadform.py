import polyindex
from sage.all import PolynomialRing, Hom, random_quadraticform


def reindex_quadform(Q):
    return polyindex.reindex_poly(Q.polynomial())


def randform(ring, n):
    Q = random_quadraticform(ring, n)
    p = polyindex.reindex_poly(Q.polynomial())
    return Q.matrix(), p
