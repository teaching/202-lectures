\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}


\title{Planes in $\mathbb{R}^n$}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}


\section{Definition and Motivation}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Geometric]
    A \emph{plane} in $\mathbb{R}^n$ is an infinite two-dimensional flat surface.
  \end{definition}
  \pause
  \[
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[line cap=round, line join=round, scale=1/3,tdplot_main_coords]
    % axes extrema
    \def\xmax{16}
    \def\ymax{12}
    \def\zmax{12}

    % axes coordinates
    \coordinate (O) at (0, 0, 0);
    \coordinate (xmax) at (\xmax, 0, 0);
    \coordinate (ymax) at (0, \ymax, 0);
    \coordinate (zmax) at (0, 0, \zmax);

    % axes
    \draw[thick,->] (O) -- (xmax) node[anchor=north east]{$x$};
    \draw[thick,->] (O) -- (ymax) node[anchor=north west]{$y$};
    \draw[thick,->] (O) -- (zmax) node[anchor=south]{$z$};

    % plane coordinates
    \coordinate (P) at (18, -8, 7);
    \coordinate (Q) at (-14, -8, 4);
    \coordinate (R) at (16, 7, 2);
    \coordinate (S) at ($ (Q)+(R)-(P) $);

    % plane
    \filldraw[thick, draw=teal, fill=teal!20]
    (P) -- (Q) -- (S) -- (R) -- cycle;

    % dashed axes
    \draw[thick, dashed,->] (O) -- (xmax) node[anchor=north east]{$x$};
    \draw[thick, dashed,->] (O) -- (ymax) node[anchor=north west]{$y$};
    \draw[thick, dashed,->] (O) -- (zmax) node[anchor=south]{$z$};

    % redraw plane outline
    \draw[very thick, draw=teal]
    (P) -- (Q) -- (S) -- (R) -- cycle;

    % % point M on the plane (labeled as P)
    % \coordinate (M) at ($ (P)!.65!(S)  $);
    % \node[label={left:{$P$}}] at (M) {\textbullet};


    % % cross product PR x PQ
    % \coordinate (n) at (-45, 154, 480);

    % % should connect M in the direction of A = n + M
    % \coordinate (A) at ($ (n)+(M) $);

    % % connect M to a multiple of A
    % \draw[ultra thick, ->] (M) -- ($ (M)!1/75!(A) $);

    % % parallelogram labels for testing
    % \node[label={below:{\tiny$P$}}] at (P) {\textbullet};
    % \node[label={below:{\tiny$Q$}}] at (Q) {\textbullet};
    % \node[label={below:{\tiny$R$}}] at (R) {\textbullet};
    % \node[label={below:{\tiny$S$}}] at (S) {\textbullet};

    % \coordinate (s1) at ($ (M)!1/500!(A)  $);
    % \coordinate (s2) at ($ (s1)!1/10!(S) $);
    % \coordinate (s3) at ($ (M) !1/10!(S) $);

    % \draw[thick] (s1) -- (s2) -- (s3);

    % name of the plane
    \node at ($ (P)!.5!(Q) $) [below right] {$\mathcal{P}$};
  \end{tikzpicture}
  \]

\end{frame}


\subsection{Motivation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Given a plane $\mathcal{P}$, we want to use the language of points and vectors
  to describe the points on $\mathcal{P}$.
\end{frame}


\section{Point-Normal Equations of Planes in $\mathbb{R}^3$}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      \onslide<1->{Let $\mathcal{P}$ be a plane in $\mathbb{R}^3$. }%
      \onslide<3->{Suppose that $P$ is a point on $\mathcal{P}$ }%
      \onslide<5->{and that $\vv{n}$ is a vector orthogonal to $\mathcal{P}$. }%
      \onslide<7->{How can we determine if $Q\in\mathcal{P}$?}
    \end{block}
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3, tdplot_main_coords]
      % the origin
      \coordinate (O) at (0, 0, 0);

      % standard basis
      \coordinate (e1) at (1, 0, 0);
      \coordinate (e2) at (0, 1, 0);
      \coordinate (e3) at (0, 0, 1);

      % plane coordinates
      \def\xmax{18}
      \def\ymax{12}
      \coordinate (X) at ($ \xmax*(e1)  $);
      \coordinate (Y) at ($ \ymax*(e2)  $);

      \onslide<2->{
        % plane
        \filldraw[thick, draw=teal, fill=teal!20]
        (O) -- (X) -- ($ (X)+(Y) $) -- (Y) -- cycle;
      }

      % coordinate of point P on the plane
      \pgfmathsetmacro\px{(2/5)*\xmax}
      \pgfmathsetmacro\py{(2/3)*\ymax}
      \coordinate (P) at (\px, \py, 0);

      % coordinate of point Q on the plane
      \pgfmathsetmacro\px{(4/5)*\xmax}
      \pgfmathsetmacro\py{(1/4)*\ymax}
      \coordinate (Q) at (\px, \py, 0);

      % compute normal vector n
      \coordinate (n) at ($ \xmax/3*(e3)+(P) $);

      % n is orthogonal to plane
      \def\perplen{1}
      \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);
      \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);
      \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);
      \onslide<6->{
        \draw[very thick] (s1) -- (s2) -- (s3);
      }

      \onslide<6->{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above] {$\vv{n}$};
      }

      \onslide<10->{
        % draw PQ
        \draw[ultra thick, blue, ->] (P) -- (Q) node [midway, above] {$\vv{PQ}$};
      }

      \onslide<4->{
        % draw point P
        \node[label={below:{$P$}}] at (P) {\textbullet};
      }

      \onslide<8->{
        % draw point Q
        \node[label={left:{$Q$}}] at (Q) {\textbullet};
      }

      \onslide<2->{
        % label the plane
        \node[label={above:{$\mathcal{P}$}}] at ($ 2*(e2) $) {};
      }
    \end{tikzpicture}
    \]
  \end{columns}
  \onslide<9->{
    \begin{block}{Answer}
      Consider the displacement vector $\vv{PQ}$.} \onslide<11->{Then
      $Q\in\mathcal{P}$ means $\vv{PQ}\sslash\mathcal{P}$.} \onslide<12->{But
      $\vv{PQ}\sslash\mathcal{P}$ means $\vv{n}\perp\vv{PQ}$}\onslide<13->{,
      which can be expressed algebraically as $\vv{n}\cdot\vv{PQ}=0$.}
    \onslide<14->{Thus $Q\in\mathcal{P}$ means
      ${\color{red}\vv{n}\cdot\vv{PQ}=0}$.}
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[The Point-Normal Equation of a Plane]
    Let $P$ be a point on a plane $\mathcal{P}$ and let $\vv{n}$ be a vector
    orthogonal to the plane $\mathcal{P}$. \onslide<2->{The \emph{point-normal
        equation of $\mathcal{P}$} is the equation
      \[
      \vv{n}\cdot\vv{PX}=0
      \]
      where $X=(x,y,z)$.} \onslide<3->{The vector $\vv{n}$ is called a
      \emph{normal vector} of the plane $\mathcal{P}$.}
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    We only need two pieces of information to write a point-normal equation of a
    plane $\mathcal{P}$:
    \begin{enumerate}
    \item<2-> a point $P$ on $\mathcal{P}$
    \item<4-> a vector $\vv{n}$ orthogonal to $\mathcal{P}$
    \end{enumerate}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3, tdplot_main_coords]
      % the origin
      \coordinate (O) at (0, 0, 0);

      % standard basis
      \coordinate (e1) at (1, 0, 0);
      \coordinate (e2) at (0, 1, 0);
      \coordinate (e3) at (0, 0, 1);

      % plane coordinates
      \def\xmax{18}
      \def\ymax{12}
      \coordinate (X) at ($ \xmax*(e1)  $);
      \coordinate (Y) at ($ \ymax*(e2)  $);

      \onslide<6->{
        % plane
        \filldraw[thick, draw=teal, fill=teal!20]
        (O) -- (X) -- ($ (X)+(Y) $) -- (Y) -- cycle;
      }

      % coordinate of point P on the plane
      \pgfmathsetmacro\px{(2/5)*\xmax}
      \pgfmathsetmacro\py{(2/3)*\ymax}
      \coordinate (P) at (\px, \py, 0);

      % coordinate of point Q on the plane
      \pgfmathsetmacro\px{(4/5)*\xmax}
      \pgfmathsetmacro\py{(1/4)*\ymax}
      \coordinate (Q) at (\px, \py, 0);

      % compute normal vector n
      \coordinate (n) at ($ \xmax/3*(e3)+(P) $);

      % n is orthogonal to plane
      \def\perplen{1}
      \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);
      \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);
      \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);
      \onslide<6->{
        \draw[very thick] (s1) -- (s2) -- (s3);
      }

      \onslide<5->{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above] {$\vv{n}$};
      }

      \onslide<3->{
        % draw point P
        \node[label={below:{$P$}}] at (P) {\textbullet};
      }

      \onslide<6->{
        % label the plane
        \node[label={above:{$\mathcal{P}$}}] at ($ 2*(e2) $) {};
      }
    \end{tikzpicture}
    \]
  \end{block}
\end{frame}



\subsection{Point-Normal Equations in Coordinates}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Let $P=(x_0,y_0,z_0)$ be a point on a plane $\mathcal{P}$ and let
    $\vv{n}=\langle a,b,c\rangle$ be a vector orthogonal to
    $\mathcal{P}$. \pause The point-normal equation $\vv{n}\cdot\vv{PX}=0$ of
    $\mathcal{P}$ is
    \[
    \langle a,b,c\rangle\cdot\langle x-x_0, y-y_0, z-z_0\rangle=0
    \]
    \pause which is equivalent to
    \[
    a\cdot(x-x_0)+b\cdot(y-y_0)+c\cdot(z-z_0)=0
    \]
    \pause This equation is often written as
    \[
    ax+by+cz=d
    \]
    where $d=\pause ax_0+by_0+cz_0$.
  \end{block}

\end{frame}


\section{Problem Solving with Point-Normal Equations}
\subsection{The Plane Through Three Points}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{example}
    Let $\mathcal{P}$ be the plane passing through the point $P(-6, 4, 17)$
    orthogonal to $\vv{n}=\langle 5, -11, -32\rangle$. \pause The point-normal
    equation of $\mathcal{P}$ is simply $\vv{n}\cdot\vv{PX}=0$ where
    $X=(x,y,z)$. \pause In coordinates, this equation is
    \[
    5\cdot(x+6)-11\cdot(y-4)-32\cdot(z-17)=0
    \]
    \pause
    which is equivalent to
    \[
    5\,x-11\,y-32\,z=-618
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      Let $\mathcal{P}$ be the plane in $\mathbb{R}^3$ passing through three
      non-colinear points $P$, $Q$, and $R$. Can we compute a point-normal
      equation of $\mathcal{P}$?
    \end{block}
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3,
      tdplot_main_coords]
      % the origin
      \coordinate (O) at (0, 0, 0);

      % standard basis
      \coordinate (e1) at (1, 0, 0); \coordinate (e2) at (0, 1, 0); \coordinate
      (e3) at (0, 0, 1);

      % plane coordinates
      \def\xmax{18} \def\ymax{12} \coordinate (X) at ($ \xmax*(e1) $);
      \coordinate (Y) at ($ \ymax*(e2) $);

      % plane
      \filldraw[thick, draw=teal, fill=teal!20] (O) -- (X) -- ($ (X)+(Y) $) --
      (Y) -- cycle;


      % coordinate of point P on the plane
      \pgfmathsetmacro\px{(4/5)*\xmax} \pgfmathsetmacro\py{(1/3)*\ymax}
      \coordinate (P) at (\px, \py, 0);

      % coordinate of point Q on the plane
      \pgfmathsetmacro\px{(3/5)*\xmax} \pgfmathsetmacro\py{(4/5)*\ymax}
      \coordinate (Q) at (\px, \py, 0);

      % coordinate of point R on the plane
      \pgfmathsetmacro\px{(7/20)*\xmax} \pgfmathsetmacro\py{(3/10)*\ymax}
      \coordinate (R) at (\px, \py, 0);

      \onslide<5->{
        % draw PQ
        \draw[ultra thick, blue, ->] (P) -- (Q);
      }

      \onslide<6->{
        % draw PR
        \draw[ultra thick, red, ->] (P) -- (R);
      }

      % compute normal vector n
      \coordinate (n) at ($ \xmax/3*(e3)+(P) $);

      % % n is orthogonal to plane
      % \def\perplen{1}
      % \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);
      % \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);
      % \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);
      % \draw[very thick] (s1) -- (s2) -- (s3);

      \onslide<3-7>{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above]
        {{\footnotesize$\vv{n}=\makebox[0pt][l]{?}\phantom{\vv{PQ}\times\vv{PR}}$}};
      }

      \onslide<8->{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above]
        {{\footnotesize$\vv{n}=\vv{PQ}\times\vv{PR}$}};
      }

      % draw point P
      \node[label={left:{$P$}}] at (P) {\textbullet};

      % draw point Q
      \node[label={right:{$Q$}}] at (Q) {\textbullet};

      % draw point R
      \node[label={above:{$R$}}] at (R) {\textbullet};

      % label the plane
      \node[label={above left:{$\mathcal{P}$}}] at (Y) {};
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<2->{
    \begin{block}{Answer}
      Since we are given three points on $\mathcal{P}$, we need only find a vector
      $\vv{n}$ orthogonal to $\mathcal{P}$.} \onslide<4->{To do so, note that $\vv{n}$ must be
      orthogonal to both $\vv{PQ}$ and $\vv{PR}$.} \onslide<7->{The choice
      $\vv{n}=\vv{PQ}\times\vv{PR}$ satisfies this requirement.} \onslide<9->{Hence a
      point-normal equation of $\mathcal{P}$ is
      \[
      \vv{n}\cdot\vv{PX}=0
      \]
      where $\vv{n}=\vv{PQ}\times\vv{PR}$ and $X=(x,y,z)$.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Compute a point-normal equation of the plane $\mathcal{P}$ that passes
    through the points $P(1, -3, 1)$, $Q(2, 2, 16)$, and $R(1, -94, -1)$.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \[
    \vv{PQ}\times\vv{PR}
    =
    \langle1, 5, 15\rangle\times
    \langle0, -91, -2\rangle
    =\langle1355, 2, -91\rangle
    \]
    \pause
    The plane $\mathcal{P}$ is thus described by $\vv{n}\cdot\vv{PX}=0$ where
    \begin{align*}
      \vv{n}=\langle1355, 2, -91\rangle && X &=(x,y,z)
    \end{align*}
    \pause
    In coordinates, this equation is
    \[
    1355\cdot(x-1)+2\cdot(y+3)-91\cdot(z-1)=0
    \]
  \end{block}
\end{frame}



\subsection{The Line of Intersection of Two Planes}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.4\textwidth}
    \begin{block}{Question}
      Let $\mathcal{L}$ be the line of intersection of two planes
      $\mathcal{P}_1$ and $\mathcal{P}_2$,
      $\mathcal{L}=\mathcal{P}_1\cap\mathcal{P}_2$. \onslide<3->{Can we
        parameterize $\mathcal{L}$?}
    \end{block}
    \column{.6\textwidth}
    \[
    \input{./intersection.tex}
    \]
  \end{columns}
  \begin{block}{\onslide<4->{Answer}}
    \onslide<4->{Any parameterization of $\mathcal{L}$ is of the form
      $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ }%
    \onslide<5->{where $P$ is }%
    \onslide<6->{a point on $\mathcal{L}$ }%
    \onslide<8->{and $\vv{v}$ is }%
    \onslide<9->{parallel to $\mathcal{L}$. }%
    \onslide<11->{The point $P$ must satisfy the point-normal equations of both
      $\mathcal{P}_1$ and $\mathcal{P}_2$. }%
    \onslide<14->{The vector $\vv{v}$ must be orthogonal to both $\vv*{n}{1}$
      and $\vv*{n}{2}$. }%
    \onslide<15->{The choice $\vv{v}=\vv*{n}{1}\times\vv*{n}{2}$ satisfies this
      requirement.}%
  \end{block}
\end{frame}




\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Find a parameterization of the line of intersection $\mathcal{L}$ of the two
    planes
    \begin{equation}
      \begin{array}{rcrcrcr}
        2\,x &-& y &+& z &=& 5 \\
        x &+& y &-& z &=& 1
      \end{array}\tag{$\ast$}\label{eq:intersection}
    \end{equation}
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Note that $P=(2,-1,0)$ satisfies both equations in
      \eqref{eq:intersection}, so $P$ is on $\mathcal{L}$. }%
    \onslide<3->{The vectors $\vv*{n}{1}=\langle2,-1,1\rangle$ and
      $\vv*{n}{2}=\langle1,1,-1\rangle$ are normal vectors to the planes
      described in \eqref{eq:intersection}. }%
    \onslide<4->{Put
      \[
      \vv{v}=\vv*{n}{1}\times\vv*{n}{2}=\langle0,3,3\rangle
      \]}%
    \onslide<5->{Then $\mathcal{L}$ is parameterized by
      $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ where $P=(2,-1,0)$ and
      $\vv{v}=\langle0,3,3\rangle$.}
  \end{block}
\end{frame}



\section{Parameterizations of Planes in $\mathbb{R}^n$}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      \onslide<1->{Let $P$ be a point on a plane $\mathcal{P}$ and let $\vv{v}$
        and $\vv{w}$ be two non-parallel vectors in $\mathcal{P}$. }%
      \onslide<5->{How can we determine if $Q\in\mathcal{P}$?}%
    \end{block}
    \column{.5\textwidth}
    \[
    \input{./parametric-plane.tex}
    \]
  \end{columns}
  \begin{block}{\onslide<6->{Answer}}
    \onslide<6->{Consider the displacement vector $\vv{PQ}$. }%
    \onslide<8->{Then $Q\in\mathcal{P}$ means $\vv{PQ}$ is a \emph{linear
        combination} of $\vv{v}$ and $\vv{w}$. }%
    \onslide<11->{This means that $\vv{PQ}=s\cdot\vv{v}+t\cdot\vv{w}$ for some
      scalars $s,t\in\mathbb{R}$, }%
    \onslide<12->{which may be rewritten as
      $\vv{Q}-\vv{P}=s\cdot\vv{v}+t\cdot\vv{w}$. }%
    \onslide<13->{Thus $Q\in\mathcal{P}$ means
      {\color{red}$\vv{Q}=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}$}.}%
  \end{block}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{parameterization of $\mathcal{P}$ with initial point $P$ and with
      respect to $\vv{v}$ and $\vv{w}$} is the vector-valued function
    $\vv{L}(s,t)=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}$.
  \end{definition}
  \begin{block}{\onslide<2->{Note}}
    \onslide<2->{We only need three pieces of information to parameterize a plane
      $\mathcal{P}$:}%
    \begin{enumerate}
    \item<3-> a point $P$ on $\mathcal{P}$
    \item<5-> two non-parallel vectors $\vv{v}$ and $\vv{w}$ in $\mathcal{P}$
    \end{enumerate}
    \[
    \input{./parametric-plane-function.tex}
    \]
  \end{block}
\end{frame}


\subsection{Coordinates of Parameterizations}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Suppose that
    \begin{align*}
      P &= (p_1,p_2,\dotsc,p_n) &
      \vv{v} &= \langle v_1,v_2,\dotsc,v_n\rangle &
      \vv{w} &= \langle w_1,w_2,\dotsc,w_n\rangle
    \end{align*}
    \pause
    Then the coordinates of
    $\vv{L}(s,t)=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}$ are $\vv{L}(s,t)=\langle
    x_1(s,t), x_2(s,t),\dotsc,x_n(s,t)\rangle$ where\pause
    \[
    \begin{array}{cccccccccccc}
      x_1(s, t) &=& \pause p_1     &+& v_1\cdot s &+& w_1\cdot t \\ \pause
      x_2(s, t) &=& \pause p_2     &+& v_2\cdot s &+& w_2\cdot t \\ \pause
      \vdots    & &        \vdots & & \vdots && \vdots \\ \pause
      x_n(s, t) &=& \pause p_n     &+& v_n\cdot s &+& w_n\cdot t
    \end{array}
    \]
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the plane $\mathcal{P}$ in $\mathbb{R}^4$ parameterized by
    \[
    \vv{L}(s, t)
    =\left[\begin{array}{r}
        3 \, t + 1 \\
        s - 2 \, t - 3 \\
        4 \, s + t \\
        s - t - 58
      \end{array}\right]
    \]
    Note that
    \[
    \vv{L}(s, t)=
    \underbrace{\left[\begin{array}{r}1 \\-3 \\0 \\-58\end{array}\right]}_{=\vv{P}}
    +s\cdot\underbrace{\left[\begin{array}{r}0 \\1 \\4 \\1\end{array}\right]}_{=\vv{v}}
    +t\cdot\underbrace{\left[\begin{array}{r}3 \\-2 \\1 \\-1\end{array}\right]}_{=\vv{w}}
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[continued]
    \onslide<1->{Is the point $Q(-2, 2, 11, -54)$ on $\mathcal{P}$? }%
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{The equation $\vv{L}(s,t)=\vv{Q}$ is
      \[
      \vv{P}+s\cdot\vv{v}+t\cdot\vv{w}=\vv{Q}
      \]
    }%
    \onslide<3->{which is equivalent to $s\cdot\vv{v}+t\cdot\vv{w}=\vv{PQ}$. }%
    \onslide<4->{In coordinates, this equation is
      \[
      \begin{array}{rcrcrcrcrc}
        && 3\,t &=&  -3\\
        s &-& 2\,t &=&   5\\
        4\,s &+& t    &=&  11\\
        s &-& t    &=&   4
      \end{array}
      \]
    }%
    \onslide<5->{which is solved by $s=3$ and $t=-1$. }%
    \onslide<6->{Hence $Q\in\mathcal{P}$.}%
  \end{block}

\end{frame}

\section{Problem Solving with Parameterizations of Planes}
\subsection{Point-Normal Equation Given a Parameterization}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      \onslide<1->{Let $\vv{L}(s,t)=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}$ be a
        parameterization of a plane $\mathcal{P}$ in $\mathbb{R}^3$. }%
      \onslide<5->{How can we compute a point-normal equation of
        $\mathcal{P}$?}%
    \end{block}
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3,
      tdplot_main_coords]
      % the origin
      \coordinate (O) at (0, 0, 0);

      % standard basis
      \coordinate (e1) at (1, 0, 0);%
      \coordinate (e2) at (0, 1, 0);%
      \coordinate (e3) at (0, 0, 1);%

      % plane coordinates
      \def\xmax{18} \def\ymax{12} \coordinate (X) at ($ \xmax*(e1) $);
      \coordinate (Y) at ($ \ymax*(e2) $);

      % plane
      \filldraw[thick, draw=teal, fill=teal!20]
      (O) -- (X) -- ($ (X)+(Y) $) -- (Y) -- cycle;

      % coordinate of point P on the plane
      \pgfmathsetmacro\px{(2/5)*\xmax} \pgfmathsetmacro\py{(2/3)*\ymax}
      \coordinate (P) at (\px, \py, 0);

      % compute normal vector n
      \coordinate (n) at ($ \xmax/3*(e3)+(P) $);

      % n is orthogonal to plane
      \def\perplen{1}%
      \coordinate (s1) at ($ (P)!\perplen!($ (P)+(e3) $) $);%
      \coordinate (s2) at ($ (P)!\perplen!($ (s1)+(e2) $) $);%
      \coordinate (s3) at ($ (P)!\perplen!($ (P)+(e2) $) $);%

      \onslide<9->{
        \draw[very thick] (s1) -- (s2) -- (s3);
      }

      \onslide<9-15>{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above]
        {$\vv{n}=\myhide{?}{\vv{v}\times\vv{w}}$};
      }

      \onslide<16>{
        % draw n
        \draw[ultra thick, violet, ->] (P) -- (n) node [above]
        {$\vv{n}=\vv{v}\times\vv{w}$};%
      }

      \onslide<3->{
        % draw v
        \draw[ultra thick, blue, ->] (P) -- ($ (P)!.8!(O) $) node [midway, right]
        {$\vv{v}$};%
      }

      \onslide<4->{
        % draw w
        \draw[ultra thick, red, ->] (P) -- ($ (P)!.8!(X) $) node [midway, below]
        {$\vv{w}$};%
      }

      \onslide<2->{
        % draw point P
        \node[label={below:{$P$}}] at (P) {\textbullet};
      }

      % label the plane
      \node[label={above:{$\mathcal{P}$}}] at ($ 2*(e2) $) {};
    \end{tikzpicture}
    \]
  \end{columns}
  \begin{block}{\onslide<6->{Answer}}
    \onslide<6->{A point-normal equation of $\mathcal{P}$ is of the form
      $\vv{n}\cdot\vv{PX}=0$ }%
    \onslide<7->{where $\vv{n}$ is }%
    \onslide<8->{a vector orthogonal to $\mathcal{P}$, }%
    \onslide<10->{$P$ is }%
    \onslide<11->{a point on $\mathcal{P}$, }%
    \onslide<12->{and $X=(x,y,z)$. }%
    \onslide<13->{The parameterization $\vv{L}(s, t)$ provides a point $P$ on
      $\mathcal{P}$. }%
    \onslide<14->{The vector $\vv{n}$ must be orthogonal to both $\vv{v}$ and
      $\vv{w}$. }%
    \onslide<15->{Choosing $\vv{n}=\vv{v}\times\vv{w}$ satisfies this
      requirement.}%
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\mathcal{P}$ be the plane parameterized in coordinates by
    \[
    \begin{array}{rcrcrcrcrcrcrcrcrc}
      x(s, t) &=& -1 &-& 4\,s &+&  7\,t \\
      y(s, t) &=&  7 &-& 9\,s &+& 11\,t \\
      z(s, t) &=& -5 &+& 6\,s &-& t
    \end{array}
    \]
    Find a point-normal equation of $\mathcal{P}$.
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<3->{The given parameterization of $\mathcal{P}$ is
      $\vv{L}(s,t)=\vv{P}+s\cdot\vv{v}+t\cdot\vv{w}$ where}%
    \begin{align*}
      \onslide<4->{P      &= }\onslide<5->{(-1, 7, -5)} &
      \onslide<6->{\vv{v} &= }\onslide<7->{\langle-4, -9, 6\rangle} &
      \onslide<8->{\vv{w} &= }\onslide<9->{\langle7, 11, -1\rangle}
    \end{align*}
    \onslide<10->{A point-normal equation of $\mathcal{P}$ is thus
      $\vv{n}\cdot\vv{PX}=0$ where}%
    \onslide<11->{
      \begin{align*}
        \vv{n}
        &= \vv{v}\times\vv{w} = \langle-57, 38, 19\rangle
        && \text{and} &
        P
        &= (-1, 7, -5)
      \end{align*}}
  \end{block}
\end{frame}

\end{document}
