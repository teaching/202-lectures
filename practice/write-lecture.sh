#!/bin/bash

infile=raw.tex

student=202f18-practice
studentsage=$student.sagetex.sage

professor=202f18-practice--solutions
professorsage=$professor.sagetex.sage

pdflatex -jobname=$student "\def\student{}\input{$infile}"
sage $studentsage
pdflatex -jobname=$student "\def\student{}\input{$infile}"
pdflatex -jobname=$student "\def\student{}\input{$infile}"

pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
sage $professorsage
pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
