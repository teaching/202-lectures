\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning, shapes.geometric}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}
\usepackage{cool}
\usepackage[useregional]{datetime2}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}




\title{Partial Derivatives}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Partial Derivatives}
\subsection{Functions $\mathbb{R}\to \mathbb{R}$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Let $f:\mathbb{R}\to \mathbb{R}$. The \emph{derivative of $f$ at $p$} is
    \[
    f^\prime(p)=\lim_{h\to 0}\frac{f(p+h)-f(p)}{h}
    \]
    provided that this limit exists.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    The number $f^\prime(p)$ is the slope of the line tangent to the graph of
    $f(x)$ at the point $x=p$.
    \[
    \begin{tikzpicture}[line join=round, line cap=round, xscale=2, scale=1]
      \onslide<2->{
      \draw[ultra thick, <->] (-1,0)-- (3,0) node [right] {$x$};
      \draw[ultra thick, <->] (0,-1)-- (0,7/2);

      \draw[ultra thick] (7/4, 3pt) -- (7/4, -3pt) node [below] {$p$};

      \draw[very thick, blue, domain=-1/2:5/2, samples=1000, <->] 
      plot (\x, {\x*\x*\x-3*\x*\x+2*\x+1}) node [right] {$f(x)$};}

    \onslide<4->{
      \draw[very thick, red, domain=-1/4:3, samples=1000, <->]
      plot (\x, {11/16 * \x-17/32}) node [right] {$L(x)$};}
      % node [below right=.1pt] {\tiny$L(x)=f(p)+f^\prime(p)(x-p)$};

    \onslide<3->{
      \coordinate (P) at (7/4, 43/64);
      \node at (P) {\textbullet};}
      % \node at (P) [below right=.025pt] {\scriptsize$(p, f(p))$};
    \end{tikzpicture}
    \]
    \onslide<5->{Note that $L(x)=f(p)+f^\prime(p)(x-p)$.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    At $t$ hours past \DTMdisplaytime{12}{00}{00}, the population of bacteria (in
    millions) in a dish is $P(t)=2^t$. \pause%
    Note that $P^\prime(t)=\pause2^t\ln(2)$. \pause%
    The statement
    \[
    P^\prime(3)=2^3\ln(2)=8\ln(2)
    \]
    means that at \DTMdisplaytime{16}{00}{00} we expect the number of bacteria in the
    dish to \pause%
    increase from $P(3)=2^3=8$ to 
    \[
    P(3+1)\approx 2^3+8\ln(2)\approx 13.545
    \]
  \end{example}
\end{frame}

\subsection{Functions $\mathbb{R}^{2}\to \mathbb{R}$}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{2}\to \mathbb{R}$. %
    \onslide<2->{The \emph{partial derivatives (with respect to $x$ and with
        respect to $y$) of $f(x,y)$} are the two functions defined by}
    \begin{align*}
      \onslide<3->{\pderiv{f}{x} &=}\onslide<4->{\lim_{h\to 0}\frac{f(x+h,y)-f(x,y)}{h}} \\
      \onslide<5->{\pderiv{f}{y} &=}\onslide<6->{\lim_{h\to 0}\frac{f(x,y+h)-f(x,y)}{h}}
    \end{align*}
    \onslide<7->{provided that these limits exist.}
  \end{definition}

  \begin{block}{\onslide<8->{Notation}}
    \onslide<9->{Each of} 
    \[
    \onslide<10->{\pderiv{f}{x}} 
    \onslide<11->{= f_x }
    \onslide<12->{= \pderiv{}{x}f }
    \onslide<13->{= D_xf }
    \onslide<14->{= D_1f }
    \]
    \onslide<15->{is common.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Although $f_x$ and $f_y$ have definitions using limits, $f_x$ and
    $f_y$ are generically easy to compute. \pause
    \begin{description}
    \item[to compute $f_x$] treat $y$ as a constant and differentiate $f(x,y)$
      with respect to $x$\pause
    \item[to compute $f_y$] treat $x$ as a constant and differentiate $f(x,y)$
      with respect to $y$
    \end{description}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $f(x,y)=x^2+y^2$. \onslide<2->{Then}
    \begin{align*}
      \onslide<2->{\pderiv{f}{x} &=} \onslide<3->{2\,x} & 
      \onslide<4->{\pderiv{f}{y} &=} \onslide<5->{2\,y}
    \end{align*}
  \end{example}

  \onslide<6->{
  \begin{example}
    Consider $f(x,y)=x^2y^4$.} \onslide<7->{Then}
    \begin{align*}
      \onslide<7->{f_x &=} \onslide<8->{2\,xy^4} & 
      \onslide<9->{f_y &=} \onslide<10->{4\,x^2y^3}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $g(x,y)=(1+x^2)^{y^2}$. \onslide<2->{Then}
    \begin{align*}
      \onslide<2->{g_x &=} \onslide<3->{y^2(1+x^2)^{y^2-1}(2\,x)} &
      \onslide<4->{g_y &=} \onslide<5->{(1+x^2)^{y^2}\ln(1+x^2)(2\,y)}
    \end{align*}
  \end{example}

  \onslide<6->{
  \begin{example}
    Consider $h(x,y)=x\sin(xy)$.} \onslide<7->{Then}
    \begin{align*}
      \onslide<7->{D_xh &=} \onslide<8->{x\cos(xy)(y)+\sin(xy)} &
      \onslide<9->{D_yh &=} \onslide<10->{x\cos(xy)(x)}
    \end{align*}
  \end{example}
\end{frame}

\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The temperature at a point $(x,y)$ in the $xy$-plane is given by
    \[
    T(x,y)=x^2+2\,xy^2-y^3
    \]
    \onslide<2->{Distance in the $xy$-plane is measured by kilometers and temperature is
    measured in degrees Celsius. }%
  \onslide<3->{Then}
    \begin{align*}
      \onslide<4->{T_x &=} \onslide<5->{2\,x+2\,y^2} & 
      \onslide<6->{T_y &=} \onslide<7->{4\,xy-3\,y^2}
    \end{align*}
    \onslide<8->{Note that}
    \begin{align*}
      \onslide<9->{T_x(1,-1) &=}  \onslide<10->{\SI{4}{\nicefrac{\celsius}{\km}}} & 
      \onslide<11->{T_y(1,-1) &=} \onslide<12->{\SI{-7}{\nicefrac{\celsius}{\km}}}
    \end{align*}
    \onslide<13->{Thus if we start at the point $(1,-1)$ and walk one kilometer
      east, we expect to experience a temperature increase of about
      $\SI{4}{\celsius}$. }%
    \onslide<14->{If instead we started at $(1,-1)$ and walked one kilometer
      north, we would expect to experience a temperature decrease of about
      $\SI{7}{\celsius}$.}
  \end{example}
\end{frame}
\endgroup


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Recall the Cobb-Douglas production function
    $Q(K, L)=4\,K^{\nicefrac{3}{4}}L^{\nicefrac{1}{4}}$. \onslide<2->{Note that}
    \begin{align*}
      \onslide<3->{\pderiv{Q}{K} &=} \onslide<4->{3\,K^{-\nicefrac{1}{4}}L^{\nicefrac{1}{4}}} &
      \onslide<5->{\pderiv{Q}{L} &=} \onslide<6->{K^{\nicefrac{3}{4}}L^{-\nicefrac{3}{4}}}
    \end{align*}
    \onslide<7->{When $K=\num{10000}$ and $L=\num{625}$ we have}
    \begin{align*}
      \onslide<8->{\pderiv{Q}{K}(\num{10000},\num{625}) &=}  \onslide<9->{\frac{3}{2}} &
      \onslide<10->{\pderiv{Q}{L}(\num{10000},\num{625}) &=} \onslide<10->{8}
    \end{align*}
    \onslide<11->{Thus, when $K=\num{10000}$ and $L=\num{625}$, we expect output $Q$ to
    increase by $\nicefrac{3}{2}$ output-units if $K$ is increased by one
    capital unit. }%
  \onslide<12->{If instead we increase labor by one labor-unit, then we would
    expect output to increase by $8$ output-units.}
  \end{example}
\end{frame}


\subsection{Functions $\mathbb{R}^{n}\to \mathbb{R}$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. %
    \onslide<2->{The \emph{partial derivative of
      $f(x_1,x_2,\dotsc,x_n)$ with respect to $x_i$} is}
    \begin{align*}
      \onslide<2->{\pderiv{f}{x_i}}
      \onslide<3->{&= \lim_{h\to0}\frac{f(x_1,\dotsc,x_i+h,\dotsc,x_n)-f(x_1,\dotsc,x_i,\dotsc,x_n)}{h}} \\
      \onslide<4->{&= \lim_{h\to0}\frac{f(\vv{X}+h\cdot\vv*{e}{i})-f(\vv{X})}{h}}
    \end{align*}
  \end{definition}

  \begin{block}{\onslide<5->{Notation}}
    \onslide<6->{Each of}
    \[
    \onslide<7->{\pderiv{f}{x_i}}
    \onslide<8->{= f_{x_i}}
    \onslide<9->{= \pderiv{}{x_i}f}
    \onslide<10->{= D_{x_i}f}
    \onslide<11->{= D_i f}
    \]
    \onslide<12->{is common.}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Although $f_{x_i}$ has a limit definition, $f_{x_i}$ is generically easy to
    compute. \pause%
    To compute $f_{x_i}$, treat the other variables as constants and
    differentiate $f(x_1,x_2,\dotsc,x_n)$ with respect to $x_i$.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $f(x,y,z)=x\sin(yz^2)$. \onslide<2->{Then}
    \begin{align*}
      \onslide<2->{f_x &=} \onslide<3->{\sin(yz^2)} &
      \onslide<4->{f_y &=} \onslide<5->{x\cos(yz^2)(z^2)} &
      \onslide<6->{f_z &=} \onslide<7->{x\cos(yz^2)(2\,yz)}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Higher-Order Partial Derivatives}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Since $f_{x_i}$ is a function, we may compute its partial
    derivatives. \pause Notationally, this is expressed as
    \[
    (f_{x_i})_{x_j}
    =\pause f_{x_ix_j}
    =\pause \pderiv{f_{x_i}}{x_j}
    =\pause \pderiv{}{x_j}\left(\pderiv{f}{x_i}\right)
    =\pause \pderiv{f}{x_j, x_i}
    \]\pause
    These are called \emph{second-order partial derivatives}.
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f:\mathbb{R}^{2}\to \mathbb{R}$. \onslide<2->{Then $f$ has four second-order partials}
    \begin{align*}
      \onslide<3->{f_{xx} &=} \onslide<4->{ \pderiv[2]{f}{x}} &
      \onslide<5->{f_{xy} &=} \onslide<6->{ \pderiv{f}{y,x} } &
      \onslide<7->{f_{yx} &=} \onslide<8->{ \pderiv{f}{x,y} } &
      \onslide<9->{f_{yy} &=} \onslide<10->{ \pderiv[2]{f}{y}}
    \end{align*}
  \end{example}
\end{frame}


\begin{sagesilent}
  var('x y')
  f = x*sin(x*y)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f(x,y)=\sage{f}$. %
    \onslide<2->{The first-order partials of $f$ are}
    \begin{align*}
      \onslide<3->{f_x &= }\onslide<4->{\sage{f.diff(x)}} &
      \onslide<5->{f_y &= }\onslide<6->{\sage{f.diff(y)}}
    \end{align*}
    \onslide<7->{The second-order partials of $f$ are thus}
    \begin{align*}
      \onslide<8->{f_{xx}}    & \onslide<9->{=\pderiv{}{x}\left(\pderiv{f}{x}\right) } & \onslide<12->{f_{yy}} &\onslide<13->{= \pderiv{}{y}\left(\pderiv{f}{y}\right) \\ }   
                             & \onslide<10->{=\pderiv{}{x}(\sage{f.diff(x)})        } &                      &\onslide<14->{= \pderiv{}{y}(\sage{f.diff(y)})         \\   }
                             & \onslide<11->{=\sage{f.diff(x, x)}                   } &                      &\onslide<15->{= \sage{f.diff(y, y)}                    \\   }
        \onslide<16->{f_{xy}} & \onslide<17->{=\pderiv{}{y}\left(\pderiv{f}{x}\right)} & \onslide<20->{f_{yx}} &\onslide<21->{= \pderiv{}{x}\left(\pderiv{f}{y}\right) \\ }
                             & \onslide<18->{=\pderiv{}{y}(\sage{f.diff(x)})        } &                      &\onslide<22->{= \pderiv{}{x}(\sage{f.diff(y)})         \\   }
                             & \onslide<19->{=\sage{f.diff(x,y)}                    } &                      &\onslide<23->{= \sage{f.diff(y,x)}                          }
    \end{align*}
    \onslide<24->{Note that $f_{xy}=f_{yx}$.}
  \end{example}
\end{frame}
\endgroup

\subsection{Clairaut's Theorem}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. We write $f\in C^k$ if all $k$th order
    partial derivatives of $f$ exist and are continuous.
  \end{definition}

  \pause

  \begin{theorem}[Clairaut's Theorem]
    If $f\in C^2$, then $\displaystyle\pderiv{f}{x_i,x_j}=\pderiv{f}{x_j,x_i}$.
  \end{theorem}

  \pause

  \begin{block}{Note}
    Unless otherwise stated, we will always assume $f\in C^2$.
  \end{block}
\end{frame}


\section{Gradients and Directional Derivatives}
\subsection{Gradients of Functions $\mathbb{R}^{n}\to \mathbb{R}$}

\newcommand{\mygrad}{
  \left[
    \begin{array}{c}
      f_{x_1}(x_1,\dotsc,x_n) \\
      f_{x_2}(x_1,\dotsc,x_n) \\
      \vdots \\
      f_{x_n}(x_1,\dotsc,x_n) 
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$. The \emph{gradient} of $f$ is the
    function $\nabla f:\mathbb{R}^{n}\to \mathbb{R}^{n}$ defined by
    \[
    \nabla f(x_1,\dotsc,x_n)=\mygrad
    \]
  \end{definition}

  \pause
  \begin{block}{Convention}
    For $P\in \mathbb{R}^{n}$, we view $\nabla f(P)$ as the vector in
    $\mathbb{R}^{n}$ whose tail is at $P$.
  \end{block}
\end{frame}

\begin{sagesilent}
  var('x y z')
  f = x**2*exp(x*y)
  g = x**3*sin(x*y*z^2)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $f(x,y)=\sage{f}$. \pause%
    Then
    \[
    \nabla f=\pause\sage{matrix.column(f.gradient([x, y]))}
    \]
  \end{example}

  \pause
  \begin{example}
    Consider $g(x,y,z)=\sage{g}$. \pause%
    Then
    \[
    \nabla g = \pause\sage{matrix.column(g.gradient([x, y, z]))}
    \]
  \end{example}
\end{frame}

\subsection{Gradients and Level Sets}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $f(P)=c$. %
    \onslide<2->{Then $\nabla f(P)$ is orthogonal to the level
    set defined by $f(x_1,\dotsc,x_n)=c$ at the point $P$.}
  \end{theorem}

  \onslide<3->{
  \begin{example}
    Consider $f(x,y)=\nicefrac{x^2}{4}+y^2$. }%
  \onslide<4->{The level set $f(x,y)=1$ is the ellipse $\nicefrac{x^2}{4}+y^2=1$
    and the point $P(1, \nicefrac{\sqrt{3}}{2})$ is on this ellipse.}
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \onslide<5->{
      \draw[ultra thick, <->] (-5/2, 0) -- (5/2, 0);
      \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);}

    \onslide<6->{
      \draw[very thick, blue] (0, 0) circle (2 and 1);}

      \pgfmathsetmacro{\mycoord}{sqrt(3)/2}
      \coordinate (P) at (1, \mycoord);
      \onslide<7->{
      \node[below] at (P) {$P$};}

      \pgfmathsetmacro{\mynewcoord}{sqrt(3)*3/2}
      \coordinate (Q) at (3/2, \mynewcoord);
      
      \onslide<8->{
      \draw[very thick, violet, ->] 
      (P) -- (Q) node[below right] 
      {\scriptsize$\nabla f(P)=\langle\nicefrac{1}{2},\sqrt{3}\rangle$};}

      \onslide<7->{
      \node at (P) {\textbullet};}

      % \draw[red, domain=0:2*pi, samples=100]
      % plot ({2*cos(\x r)}, {sin(\x r)});
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\subsection{Directional Derivatives of Functions $\mathbb{R}^{n}\to \mathbb{R}$}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}$ and let $\vv{u}\in \mathbb{R}^{n}$ be a
    unit vector. \pause%
    The \emph{directional derivative of $f$ in the direction
      $\vv{u}$} is
    \[
    D_{\vv{u}}f(\vv{X}) = \pause\lim_{h\to0}\frac{f(\vv{X}+h\cdot\vv{u})-f(\vv{X})}{h}
    \]
    provided that this limit exists.
  \end{definition}

  \pause
  \begin{theorem}
    Suppose that $f\in C^1$. \pause%
    Then the directional derivative $D_{\vv{u}}f$ exists and is given by the dot
    product
    \[
    D_{\vv{u}}f=\nabla f\cdot\vv{u}
    \]
  \end{theorem}
\end{frame}


\begin{sagesilent}
  var('x y')
  f = (1/180)*(7400-4*x-9*y-3/100*x*y)
  v = vector([3, 4])
  u = v/v.norm()
\end{sagesilent}
\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that the temperature at a point $(x,y)$ near an airport is given by
    \[
    f(x,y)=\sage{f}
    \]
    \onslide<2->{Suppose that your aircraft takes off from the airport at the location
    $P(200, 200)$ and heads northeast in the direction specified by the vector
    $\vv{v}=\langle 3, 4\rangle$. }%
  \onslide<3->{What initial rate of change of temperature
    will you observe?}
  \end{example}
  \begin{block}{\onslide<4->{Solution}}
    \onslide<5->{We wish to compute $D_{\vv{u}}f(200, 200)$ where $\vv{u}$ is the
    normalization of $\vv{v}$. }%
  \onslide<6->{To do so, note that}
    \begin{align*}
      \onslide<7->{\vv{u}   &= }\onslide<8->{\frac{1}{\norm{\vv{v}}}\vv{v}= \frac{1}{5}\sage{v}} &
      \onslide<9->{\nabla f &= }\onslide<10->{\sage{matrix.column(f.gradient([x, y]))}}
    \end{align*}
    \onslide<10->{Thus }
    \[
    \onslide<10->{D_{\vv{u}}f(200, 200)}
    \onslide<11->{=\nabla f(200, 200)\cdot\vv{u}}
    \onslide<12->{=\sage{f.gradient([x, y])(x=200, y=200)}\cdot\sage{u}}
    \onslide<13->{=\sage{f.gradient([x,y])(x=200, y=200).dot_product(u)}}
    \]
    \onslide<14->{This means that you will initially observe a decrease of
    $\SI{0.1}{\nicefrac{\celsius}{\km}}$.}
  \end{block}
\end{frame}
\endgroup


\subsection{Maximizing Directional Derivatives}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    In what direction is $f$ increasing most rapidly?
  \end{block}
  \pause
  \begin{block}{Answer}
    We wish to find $\vv{u}$ that maximizes $D_{\vv{u}}f$. \pause%
    To do so, note that
    \[
    D_{\vv{u}}f
    = \pause\nabla f\cdot \vv{u}
    = \pause\norm{\nabla f}\cdot\norm{\vv{u}}\cos(\theta)
    = \pause\norm{\nabla f}\cdot\cos(\theta)
    \]\pause
    This expression is maximized when $\theta=0$, which implies that $\vv{u}$
    must point in the direction as $\nabla f$. \pause%
    Since $\vv{u}$ is a unit vector, it follows that $\vv{u}$ is the
    normalization of $\nabla f$. \pause%
    Thus $f$ is increasing most rapidly in the direction
    \[
    \vv{u}
    =\pause\frac{\nabla f}{\norm{\nabla f}}
    \]
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Significance of the Gradient Vector]
    The maximum value of the directional derivative $D_{\vv{u}}f(P)$ is obtained
    when $\vv{u}$ is the normalization of $\nabla f(P)$ so that 
    \[
    \vv{u}=\frac{\nabla f}{\norm{\nabla f}}
    \]
    The value of the maximum directional derivative is $\norm{\nabla f(P)}$.
  \end{theorem}
\end{frame}


\newcommand{\mynabla}{
  \left[
    \begin{array}{c}
      e^y\\ xe^y
    \end{array}\right]}
\newcommand{\mynablaP}{
  \left[
    \begin{array}{c}
      1\\ 2
    \end{array}\right]}
\newcommand{\myu}{
  \left[
    \begin{array}{c}
      \nicefrac{1}{\sqrt{5}}\\ \nicefrac{2}{\sqrt{5}}
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f(x,y)=xe^y$. In what direction is the maximum rate of change of
    $f(x,y)$ at $P(2, 0)$?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that
    \begin{align*}
      \nabla f &= \mynabla & \nabla f(2, 0) &= \mynablaP
    \end{align*}
    \pause
    The direction of the maximum rate of change of $f(x,y)$ at $P(2,0)$ is thus \pause
    \[
    \vv{u}
    =\frac{\nabla f(2, 0)}{\norm{f(2, 0)}}
    =\myu
    \]
  \end{block}
\end{frame}


\begin{sagesilent}
  var('x y z')
  T = (1/180)*(7400-4*x-9*y-(3/100)*x*y)-2*z
  grad = T.gradient([x, y, z])
  gradP = grad(x=200, y=200, z=5)
\end{sagesilent}
\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that temperature at a point $(x,y,z)$ is given by
    \[
    T(x, y, z)=\sage{T}
    \]
    \onslide<2->{A bird is at the point $P(200, 200, 5)$. }%
    \onslide<3->{Which direction should the bird initially fly to get warmer the
      fastest? }%
    \onslide<4->{What rate of increase in temperature will the bird experience
      if it flies in this direction?}
  \end{example}
  \begin{block}{\onslide<5->{Solution}}
    \onslide<6->{To get warmer the fastest, the bird should fly in the direction $\vv{u}$
    given by the normalization of $\nabla f(P)$. }%
  \onslide<7->{Note that}
    \begin{align*}
      \onslide<7->{\nabla T                   &=} \onslide<8->{\sage{matrix.column(grad)}} & 
      \onslide<9->{\nabla T(200, 200, 5)        &=} \onslide<10->{\sage{matrix.column(gradP)}} &
      \onslide<11->{\norm{\nabla T(200, 200, 5)} &=} \onslide<12->{\sage{gradP.norm()}}
    \end{align*}
    \onslide<13->{This gives $\vv{u}=\sage{gradP/gradP.norm()}$. }%
    \onslide<14->{If the bird flies in this direction, then it will experience
      an initial rate of increase of
      $\nicefrac{\sqrt{5197}}{36}\si{\nicefrac{\celsius}{\km}}$.}
  \end{block}
\end{frame}
\endgroup

\end{document}
