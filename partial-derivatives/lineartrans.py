from sage.all import matrix, PolynomialRing


def linear_trans(A):
    R = PolynomialRing(A.base_ring(), 'x', A.ncols() + 1)
    v = matrix.column(R.gens()[1:])
    return A * v
