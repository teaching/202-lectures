\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
% \usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}



\title{Lines in $\mathbb{R}^n$}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\makeatletter
\let\@@magyar@captionfix\relax
\makeatother
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}

\section{Definition and Motivation}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Geometric]
    A \emph{line} in $\mathbb{R}^n$ is a flat one-dimensional figure extending
    infinitely in two directions.
  \end{definition}

  \begin{columns}[onlytextwidth]
    \pause
    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=3/4]
      \coordinate (xmin) at (-3, 0);
      \coordinate (xmax) at (3, 0);
      \coordinate (ymin) at (0, -2);
      \coordinate (ymax) at (0, 2);

      \onslide<2->{
        \draw[ultra thick, <->] (xmin) -- (xmax) node [right] {$x$};
        \draw[ultra thick, <->] (ymin) -- (ymax) node [above] {$y$};}

      \onslide<3->{\draw[very thick, <->, blue] (-3, -3/2) -- (2, 2);}
      \onslide<4->{\draw[very thick, <->, red] (-2, -2) -- (-2, 2);}
      \onslide<5->{\draw[very thick, <->, violet] (-3, 1) -- (3, -1);}
    \end{tikzpicture}
    \]
    \pause
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, line join=round, line cap=round]
      \onslide<6->{
        \draw[thick,<->] (-1,0,0) -- (3,0,0) node[anchor=north east]{$x$};
        \draw[thick,<->] (0,-1/2,0) -- (0,2,0) node[anchor=north west]{$y$};
        \draw[thick,<->] (0,0,-1/2) -- (0,0,2) node[anchor=south]{$z$};}

      \onslide<7->{\draw[very thick, <->, blue] (1, -3/2, 2) -- (-1, 1, -2);}
      \onslide<8->{\draw[very thick, <->, red] (-1, -3/2, -2) -- (1, 1, 2);}
      \onslide<9->{\draw[very thick, <->, violet] (-1, 1, 2) -- (-1, 1, -1);}
      );
    \end{tikzpicture}
    \]
  \end{columns}

\end{frame}


\subsection{Motivation}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Given a line $\mathcal{L}$, we want to use the language of points and vectors
  to describe motion along $\mathcal{L}$.

  \[
  \begin{tikzpicture}[line join=round, line cap=round]
    \coordinate (O) at (7, -1);
    \coordinate (l0) at (0, 0);
    \coordinate (l1) at (8, 3);
    \coordinate (P) at ( $ (l0)!3/7!(l1) $ );
    \coordinate (v) at ( $ (l0)!4/7!(l1) $ );

    \draw[very thick, <->, blue] (l0) -- (l1) node [above right] {$\mathcal{L}$};

    \foreach \i [count=\j] in {-2,...,3} {
      \pgfmathsetmacro\stretch{\j/7}
      \pgfmathtruncatemacro\mysnum{\j+1}
      \coordinate (Q) at ( $ (l0)!\stretch!(l1) $ );
      \onslide<\mysnum>{\node at (Q) {\textbullet};}
    }
  \end{tikzpicture}
  \]
\end{frame}


\section{Parameterized Lines}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{block}{Question}
      Suppose that $P$ is a point on a line $\mathcal{L}$ and that $\vv{v}$ is
      a vector parallel to $\mathcal{L}$. \onslide<2->{How can we determine if
        $Q\in\mathcal{L}$?}
    \end{block}
    \column{.4\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1/2]
      \coordinate (l0) at (0, 0);
      \coordinate (l1) at (6, 5);
      \coordinate (P) at ( $ .25*(l1)  $ ); 
      \coordinate (v) at ( $ .6*(l1)  $ );
      \coordinate (Q) at (5, 1/2);

      \draw[very thick, <->, blue] (l0) -- (l1) node [above right]
      {$\mathcal{L}$};

      \draw[ultra thick, ->, violet] (P) -- (v) node [midway, above] {$\vv{v}$};
      \onslide<5->{\draw[ultra thick, ->, red] (P) -- (Q);}
      \node[label={below:$P$}] at (P) {\textbullet}; 

      \onslide<3->{\node[label={below:$Q$}] at (Q) {\textbullet};}
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<4->{
    \begin{block}{Answer}
      Consider the displacement vector $\vv{PQ}$.} \onslide<6->{Then
      $Q\in\mathcal{L}$ means $\vv{PQ}\sslash\vv{v}$.}  \onslide<7->{But
      $\vv{PQ}\sslash\vv{v}$ means $\vv{PQ}=t\cdot\vv{v}$}\onslide<8->{,
      which can be rewritten as $\vv{Q}-\vv{P}=t\cdot\vv{v}$.}
    \onslide<9->{Thus $Q\in\mathcal{L}$ means {\color{red}$\vv{Q}=\vv{P}+t\cdot\vv{v}$}
      for some scalar $t$.}
  \end{block}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{parameterization} of $\mathcal{L}$ with
    \emph{initial position} $P$ and \emph{velocity vector} $\vv{v}$ is the
    ``vector-valued'' function $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$.
  \end{definition}

  \[
  \begin{tikzpicture}[line join=round, line cap=round]
    \coordinate (O) at (7, -1);
    \coordinate (l0) at (0, 0);
    \coordinate (l1) at (8, 3);
    \coordinate (P) at ( $ (l0)!3/7!(l1) $ );
    \coordinate (v) at ( $ (l0)!4/7!(l1) $ );

    \node[label={below:$P$}] at (P) {\textbullet};
    \draw[very thick, <->, blue] (l0) -- (l1) node [above right] {$\mathcal{L}$};
    \draw[ultra thick, ->, violet] (P) -- (v) node [midway, above] {$\vv{v}$};

    \foreach \i [count=\j] in {-2,...,3} {
      \pgfmathsetmacro\stretch{\j/7}
      \pgfmathtruncatemacro\mysnum{\j+2}
      \coordinate (Q) at ( $ (l0)!\stretch!(l1) $ );
      \onslide<\mysnum->{\draw[ultra thick, red, ->] (O) -- (Q);}
      \onslide<\mysnum->{\node[label={[red]above:{\footnotesize$\vv{L}(\i)$}}] at (Q) {\textbullet};}
    }

    \onslide<2->{\node[label={right:$O$}] at (O) {\textbullet};}
  \end{tikzpicture}
  \]

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    We only need two pieces of information to parameterize a line $\mathcal{L}$:
    \begin{enumerate}
    \item<2-> a point $P$ on $\mathcal{L}$
    \item<4-> a vector $\vv{v}$ parallel to $\mathcal{L}$
    \end{enumerate}
  \end{block}
  \[
  \begin{tikzpicture}[line join=round, line cap=round]
    \coordinate (O) at (7, -1);
    \coordinate (l0) at (0, 0);
    \coordinate (l1) at (8, 3);
    \coordinate (P) at ( $ (l0)!3/7!(l1) $ );
    \coordinate (v) at ( $ (l0)!4/7!(l1) $ );

    \onslide<6->{\draw[very thick, <->, blue] (l0) -- (l1) node [above right] {$\mathcal{L}$};}

    \onslide<3->{\node[label={below:$P$}] at (P) {\textbullet};}
    \onslide<5->{\draw[ultra thick, violet, ->] (P) -- (v) node [above] {$\vv{v}$};}
  \end{tikzpicture}
  \]


\end{frame}


\subsection{Coordinates of a Parameterization}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    If $P=(p_1,p_2,\dotsc,p_n)$ and $\vv{v}=\langle v_1,v_2,\dotsc,v_n\rangle$,
    then the coordinates of $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ are
    \[    
    \vv{L}(t)=\langle x_1(t), x_2(t),\dotsc,x_n(t)\rangle
    \]
    where
    \[
    \begin{array}{cccccc}\pause
      x_1(t) &=& p_1 &+& v_1\cdot t \\ \pause
      x_2(t) &=& p_2 &+& v_2\cdot t \\ \pause
      \vdots & & \vdots & & \vdots \\ \pause
      x_n(t) &=& p_n &+& v_n\cdot t 
    \end{array}
    \]
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\mathcal{L}$ be the line in $\mathbb{R}^3$ passing through $P(3,-1,4)$
    and parallel to $\vv{v}=\langle6,4,-11\rangle$. \pause Then $\mathcal{L}$
    is parameterized by $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$.  \pause In
    coordinates, $\vv{L}(t)$ is given by
    \[
    \begin{array}{rcrcrc}
      x(t) &=& \pause 3  &+& 6\,t \\ \pause
      y(t) &=& \pause -1 &+& 4\,t \\ \pause
      z(t) &=& \pause 4  &-& 11\,t
    \end{array}
    \]
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    To determine if a point $Q$ is on a line $\mathcal{L}$, first parameterize
    the line $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ and then check if
    $\vv{Q}=\vv{L}(t)$ is solvable.
  \end{block}

  \onslide<2->{
    \begin{example}
      Let $\mathcal{L}$ be the line in $\mathbb{R}^3$ parameterized by
      $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ where $P=(1, -1, 2)$ and
      $\vv{v}=\langle2, 1, -3\rangle$. Is $Q=(3, 11, -1)$ on $\mathcal{L}$?
    \end{example}}

  \onslide<3->{
    \begin{block}{Solution}
      We wish to determine if $\vv{Q}=\vv{L}(t)$ is solvable.} \onslide<4->{In
      coordinates, this equation is}
    \[
    \begin{array}{rcrcrcrcrc}
      \onslide<5->{3   &=& 1  &+& 2\,t &} \onslide<6->{\implies & t&=&1 \\}
      \onslide<5->{11  &=& -1 &+& t    &} \onslide<7->{\implies & t&=&12\\} 
      \onslide<5->{-1  &=& 2 &-& 3\,t     &} \onslide<8->{\implies & t&=&1 \\}
    \end{array}
    \]%
    \onslide<9->{But $1\neq12$, so $Q$ is not on $\mathcal{L}$.}
  \end{block}
\end{frame}


\subsection{Equivalent Parameterizations}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Warning}
    There are \emph{many} ways to parameterize the same line.
  \end{block}
  \[
  \begin{tikzpicture}[line join=round, line cap=round]
    \coordinate (l0) at (0, 0);
    \coordinate (l1) at (8, 3);


    \draw[very thick, <->, blue] (l0) -- (l1) node [above right]
    {$\mathcal{L}$};

    % \coordinate (P) at ( $ .25*(l1) $ ); 
    % \node at (8, 0) {$t=-5$};
    % \node[label={below:$\vv{L_1}(-5)$}] at (P) {\textbullet}; 

    \foreach \i [count=\j] in {-2,...,3} {
      \pgfmathsetmacro\stretch{\j/7}
      \coordinate (P) at ( $ (l0)!\stretch!(l1) $ );
      \onslide<\j->{\node[color=violet, label={[violet]below:{\tiny$\vv{L}_1(\i)$}}] at (P) {\textbullet};}
    }

    \foreach \i [count=\j] in {-2,...,2} {
      \pgfmathsetmacro\stretch{\j/6}
      \pgfmathtruncatemacro\mysnum{\j+6}
      \coordinate (P) at ( $ (l1)!\stretch!(l0) $ );
      \onslide<\mysnum->{\node[color=teal, label={[teal]above:{\tiny$\vv{L}_2(\i)$}}] at (P) {\textbullet};}
    }
  \end{tikzpicture}
  \]
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Two parameterizations $\vv{L}_1(t)=\vv{P}_1+t\cdot\vv{v}_1$ and
    $\vv{L}_2(t)=\vv{P}_2+t\cdot\vv{v}_2$ define the same line if
    $\vv{v}_1\sslash\vv{v}_2$ and $\vv{P}_2=\vv{L}_1(t)$ is solvable.
  \end{block}

\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Do $\vv{L}_1(t)=\underbrace{\left[\begin{array}{r}1
          \\0\end{array}\right]}_{=\vv{P}_1}+t\cdot\underbrace{\left[\begin{array}{r}-8
          \\-16\end{array}\right]}_{=\vv{v}_1}$ and $\vv{L}_2(t)=\underbrace{\left[\begin{array}{r}25
          \\48\end{array}\right]}_{=\vv{P}_2}+t\cdot\underbrace{\left[\begin{array}{r}2
          \\4\end{array}\right]}_{=\vv{v}_2}$ parameterize the same line?
  \end{example}

  \onslide<2->{
    \begin{block}{Solution}
      Note that $\vv{v}_1\sslash\vv{v}_2$ since
      $\vv{v}_1=-4\cdot\vv{v}_2$.} \onslide<3->{In coordinates, the equation
      $\vv{P}_2=\vv{L}_1(t)$ is}
    \[
    \begin{array}{rcrcrcrcrc}
      \onslide<4->{25 &=& 1 &-& 8\,t}  &\onslide<6->{\implies& t &=& -3}\\
      \onslide<5->{48 &=& 0 &-& 16\,t} &\onslide<7->{\implies& t &=& -3}
    \end{array}
    \]
    \onslide<8->{Thus $\vv{P}_2=\vv{L}_1(-3)$.} \onslide<9->{Hence
      $\vv{L}_1(t)$ and $\vv{L}_2(t)$ parameterize the same line.}
  \end{block}

\end{frame}


\section{Examples}
\subsection{The Line Passing Through Two Points}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.55\textwidth}
    \begin{block}{Question}
      Let $\mathcal{L}$ be the line passing through two given points $P_1$ and
      $P_2$. \onslide<5->{How can we find a parameterization of $\mathcal{L}$?}
    \end{block}
    \column{.45\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1/2]
      \coordinate (O) at (7, -1);
      \coordinate (l0) at (0, 0);
      \coordinate (l1) at (8, 3);
      \coordinate (P) at ( $ (l0)!1/7!(l1) $ );
      \coordinate (v) at ( $ (l0)!4/7!(l1) $ );
      \coordinate (Q) at ( $ (l0)!6/7!(l1) $ );

      \onslide<4->{\draw[very thick, <->, blue] (l0) -- (l1) node [above right] {$\mathcal{L}$};}

      \onslide<2->{\node[label={below:$P_1$}] at (P) {\textbullet}; }
      \onslide<3->{\node[label={below:$P_2$}] at (Q) {\textbullet}; }
    \end{tikzpicture}
    \]
  \end{columns}
  \onslide<6->{
    \begin{block}{Answer}
      Our parameterization is of the form $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ where
      $P$ is any point on $\mathcal{L}$ and $\vv{v}$ is a vector parallel to
      $\mathcal{L}$.} \onslide<7->{Since we are given that $P_1$ is on
      $\mathcal{L}$, we may choose $\vv{P}=\vv{P}_1$.} \onslide<8->{Furthermore,
      the vector $\vv{v}=\vv{P_1P_2}$ is parallel to $\mathcal{L}$.}
    \onslide<9->{Hence
      \[
      \vv{L}(t)=\vv{P}_1+t\cdot\vv{P_1P_2}
      \]
      is a valid parameterization of $\mathcal{L}$.}
  \end{block}  
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\mathcal{L}$ be the line passing through $P(-5,14,4)$ and
    $Q(-3,1,-2)$. Find a parameterization $\vv{L}(t)$ of $\mathcal{L}$ and
    write the coordinates of $\vv{L}(t)$.
  \end{example}
  \pause
  \begin{block}{Solution}
    Our formula gives $\vv{L}(t)=\vv{P}+t\cdot\vv{PQ}$ where
    \begin{align*}
      \vv{P} &= \langle-5,14,4\rangle &
      \vv{PQ} &= \langle2,-13,-6\rangle
    \end{align*}
    In coordinates,
    \[
    \vv{L}(t)
    =\left[\begin{array}{r}
        -5 + 2\cdot t\\
        14 -13\cdot t\\
        4-6\cdot t
      \end{array}\right]
    \]
  \end{block}
\end{frame}


\subsection{Distance From a Point to a Line in $\mathbb{R}^3$}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.55\textwidth}
    \begin{block}{Question}
      Let $\mathcal{L}$ be a line in $\mathbb{R}^3$ and let $Q$ be a
      point. \onslide<4->{What is the minimum distance from $Q$ to
        $\mathcal{L}$?}
    \end{block}
    \column{.45\textwidth}
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0, 0);
      \coordinate (P) at (4, 1);
      \coordinate (Q) at (2, 2);
      \coordinate (c) at (40/17, 10/17);

      \def\myradius{1} 
      \def\myradiuslabelshift{.25}
      \pgfmathsetmacro\myradiuslabel{\myradius + \myradiuslabelshift}
      \def\thetaa{0.244978663126864} 
      \def\thetac{0.785398163397448}
      \pgfmathsetmacro\thetab{\thetac - \thetaa}


      \onslide<2->{\draw[ultra thick, blue, <->] ( $ (P)!1.25!(O) $ ) -- (P) node [right] {$\mathcal{L}$};}

      \onslide<10->{\draw[ultra thick, red, ->] (O) -- (Q) node [midway, above left] {$\vv{PQ}$};}
      \onslide<8->{\draw[ultra thick, violet, ->] (O) -- ( $ (O)!.4!(P) $) node [below] {$\vv{v}$};}
      \onslide<5->{\draw[thick, dotted] (c) -- (Q) node [midway, right] {$d=?$};}
      \onslide<7->{\node[label={below:$P$}] at (O) {\textbullet};}
      \onslide<3->{\node[label={above:$Q$}] at (Q) {\textbullet};}

      \onslide<11->{
        \draw [domain = \thetaa r : \thetab r, opacity=0] plot
        ({\myradiuslabel*cos(\x)}, {\myradiuslabel*sin(\x)})
        node [opacity=1] {$\theta$};

        \draw [thick, domain = \thetaa r : \thetac r, <->] plot
        ({\myradius*cos(\x)}, {\myradius*sin(\x)});}
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<6->{
    \begin{block}{Answer}
      Let $P$ be any point on $\mathcal{L}$ and let $\vv{v}$ be any vector
      parallel to $\mathcal{L}$.} \onslide<9->{Consider the angle $\theta$ between
      $\vv{v}$ and $\vv{PQ}$.} \onslide<12->{Then
      $\sin\theta=\nicefrac{d}{\norm{\vv{PQ}}}$.} \onslide<13->{Solving for $d$ gives}
    \[
    \onslide<13->{d=\norm{\vv{PQ}}\cdot\sin\theta}
    \onslide<14->{=\norm{\vv{PQ}}\cdot\frac{\norm{\vv{v}\times\vv{PQ}}}{\norm{\vv{v}}\cdot\norm{\vv{PQ}}}}
    \onslide<15->{=\frac{\norm{\vv{v}\times\vv{PQ}}}{\norm{\vv{v}}}}
    \]
  \end{block}  
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $Q$ be the point $Q=(0, 2, 3)$ and let $\mathcal{L}$ be the line
    parameterized in coordinates by
    \begin{align*}
      x(t) &= 3+2\,t &
      y(t) &= 1+t &
      z(t) &= -1+2\,t
    \end{align*}
    What is the minimum distance from $Q$ to $\mathcal{L}$?
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that $\mathcal{L}$ is parameterized by
    $\vv{L}(t)=\vv{P}+t\cdot\vv{v}$ where $P=\pause(3,1,-1)$ \pause and
    $\vv{v}=\pause\langle2,1,2\rangle$. \pause Then
    $\vv{PQ}=\pause\langle-3,1,4\rangle$ and
    \[
    \vv{v}\times\vv{PQ}
    \pause
    ={\small\left\langle
        \left\lvert\begin{array}{rr}
            1 & 1 \\
            2 & 4
          \end{array}\right\rvert,
        -
        \left\lvert\begin{array}{rr}
            2 & -3 \\
            2 & 4
          \end{array}\right\rvert,
        \left\lvert\begin{array}{rr}
            2 & -3 \\
            1 & 1
          \end{array}\right\rvert
      \right\rangle}
    \pause
    =\langle2,-14,5\rangle
    \]
    \pause
    Hence 
    $\displaystyle 
    d
    \pause
    =\frac{\norm{\vv{v}\times\vv{PQ}}}{\norm{\vv{v}}}
    \pause
    =\frac{\sqrt{4+196+25}}{\sqrt{4+1+4}}
    \pause
    =\frac{\sqrt{225}}{\sqrt{9}}
    =\frac{15}{3}
    =5
    $
  \end{block}
\end{frame}



\end{document}
