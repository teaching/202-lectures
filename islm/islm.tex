\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}




\title{$IS$-$LM$ Analysis}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \tableofcontents[sections={1-2}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{Background}
\subsection{History}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    The $IS$-$LM$ Keynesian model is a linear model for a closed economy.
    \begin{itemize}[<+->]
    \item Developed by economist John Hicks (1904-1989) in 1937.
    \item Throughout the 20th century considered one of the most important tools
      in macroeconomics.
    \item ``Closed economy'' assumes no imports, exports, or other ``leakages.''
    \item The initials ``$IS$-$LM$'' refer to the two linear equations in the
      model.
    \item First equation relates the investment by firms to the consumption by
      consumers, ``investment-saving''.
    \item Second equation represents the ``money market equilibrium,''
      ``liquidity preference-money supply.''
    \end{itemize}
  \end{block}
\end{frame}


\subsection{Variables}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The variables involved in the $IS$-$LM$ model are the following:
  \begin{description}[<+->]
  \item[$Y$] national income (GDP)
  \item[$C$] consumer spending
  \item[$I$] investment spending by firms
  \item[$G$] government spending
  \item[$r$] interest rate
  \item[$M$] money supply (total amount of money in circulation)
  \end{description}
  
\end{frame}


\section{The GDP Equation}
\subsection{Definition of GDP}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{GDP Equation}
    The GDP is the sum of the consumption, investment, and government spending.
    \[
    Y = C + I + G
    \]
  \end{block}
\end{frame}


\subsection{Assumptions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{block}{GDP Assumptions}
    Consumption is assumed to be proportional to GDP 
    \[
    C=b\cdot Y
    \]
    where $0<b<1$. 
    \begin{itemize}
    \item<2-> The scalar $b$ is the \emph{marginal propensity to consume}.
    \item<3-> The scalar $s=1-b$ is the \emph{marginal propensity to save}.
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Investment Assumptions}
    Investment is assumed to negatively correlate with interest rates
    \[
    I = I_0-a\cdot r
    \]
    where $I_0>0$ and $a>0$.
    \begin{itemize}
    \item<2-> The scalar $I_0$ is the investment when $r=0$.
    \item<3-> The scalar $a$ is the \emph{marginal efficiency of capital}.
    \end{itemize}
  \end{block}
\end{frame}


\subsection{The GDP Equation with Assumptions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Substituting $C=b\cdot Y$ and $I=I_0-a\cdot r$ into 
  \[
  Y=C+I+G
  \]
  gives the equation for GDP
  \[
  Y=(b\cdot Y)+(I_0-a\cdot r)+G
  \]\pause
  Rearranging this equation gives  
  \[
  s\cdot Y+a\cdot r=I_0+G
  \]
  where 
  \begin{align*}
    0&<s<1 & a&>0 & I_0&>0
  \end{align*}
\end{frame}


\section{The Liquidity-Money Equation}
\subsection{Equation and Assumptions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{liquidity-money equation} assumes that the money supply is a
  function of GDP and interest rate
  \[
  M=m\cdot Y+(M_0-h\cdot r)
  \setlength\belowdisplayskip{0pt}
  \]
  \begin{itemize}
  \item<2-> The term $m\cdot Y$ represents the demand for funds for transactions
    that is proportional to the size of the economy, $0<m<1$.
  \item<3-> The term $M_0-h\cdot r$ represents the demand for funds from investors
    for the part of their portfolio not invested in bonds, $h>0$ and $M_0>0$.
  \end{itemize}
  \onslide<4->{
  Rearranging the liquidity-money equation gives
  \[
  m\cdot Y-h\cdot r=M-M_0
  \]}%
\end{frame}


\section{The $IS$-$LM$ Model}
\subsection{Definition and Observation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The GDP and the liquidity-money equations are
    \[
    \begin{array}{rcrcr}
      s\cdot Y & + & a\cdot r & = & I_0+G \\
      m\cdot Y & - & h\cdot r & = & M-M_0
    \end{array}
    \]
    This system of equations is called the \emph{$IS$-$LM$ model}. 
  \end{definition}

  \pause
  \begin{block}{Observation}
    The $IS$-$LM$ model is the system $A\vv{x}=\vv{b}$ where
    \newcommand{\islmA}{
      \left[
        \begin{array}{rr}
          s & a \\
          m & -h
        \end{array}
      \right]}
    \newcommand{\islmx}{
      \left[
        \begin{array}{c}
          Y\\ r
        \end{array}
      \right]}
    \newcommand{\islmb}{
      \left[
        \begin{array}{c}
          I_0+G\\ M-M_0
        \end{array}
      \right]}
    \begin{align*}
      A &= \islmA & \vv{x} &= \islmx & \vv{b} &= \islmb
    \end{align*}
  \end{block}
\end{frame}


\subsection{$IS$-$LM$ Analysis via Cramer's Rule}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

    Since the coefficients in the $IS$-$LM$ model are parameters, row-reducing
    this system is difficult and involves many cases.

  \newcommand{\islmCA}{
    \left\lvert\begin{array}{rr}
        s & a \\ m & -h
      \end{array}\right\rvert}
  \newcommand{\islmCAx}{
    \left\lvert\begin{array}{cr}
        I_0+G & a \\ M-M_0 & -h
      \end{array}\right\rvert}
  \newcommand{\islmCAy}{
    \left\lvert\begin{array}{rc}
        s & I_0+G \\ m & M-M_0
      \end{array}\right\rvert}
  \begin{block}{\onslide<2->{Idea}}
    \onslide<2->{Assuming $A$ is invertible, Cramer's Rule allows us to solve
      $A\vv{x}=\vv{b}$ with determinants.}%
    \begin{align*}
      \onslide<3->{Y &= \frac{\islmCAx}{\islmCA} = \frac{(I_0+G)h+(M-M_0)a}{sh+am}} \\
      \onslide<4->{r &= \frac{\islmCAy}{\islmCA} = \frac{(I_0+G)m-(M-M_0)s}{sh+am}}
    \end{align*}
  \end{block}
\end{frame}


\subsection{Conclusion}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    In the $IS$-$LM$ model, considering $G$ and $M$ as the free variables that
    can be controlled allows us to view $Y$ and $r$ as dependent variables
    determined by $G$, $M$, and the other parameters.
  \end{block}

  \pause
  \begin{example}
    Increasing either $G$ or $M$ results in an increase of GDP $Y$.  \pause %
    Increasing $G$ or decreasing $M$ results in an increase of the interest rate
    $r$.
  \end{example}
\end{frame}


\end{document}
