\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
% \usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}

\DeclareMathOperator{\dist}{dist}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\title{Points and Vectors in Euclidean Space}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1-4}]
    \column{.5\textwidth}
    \tableofcontents[sections={5-}]
  \end{columns}
\end{frame}


\section{The Real Number Line $\mathbb{R}$}
\subsection{Definition and Notation}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    $\mathbb{R}=\textnormal{''all real numbers''}$
  \end{definition}

  \pause
  \begin{block}{Visualization}
    $\mathbb{R}=
    \begin{tikzpicture}[line cap=round, line join=round, baseline=-.5ex]
      \draw[ultra thick, <->] (-4.5, 0) -- (4.5, 0);

      \draw[ultra thick] (0, 3pt) -- (0, -3pt) node [below] {$0$};
      \draw[ultra thick] (1, 3pt) -- (1, -3pt) node [below] {$1$};
      \draw[ultra thick] (-4, 3pt) -- (-4, -3pt) node [below] {$-17$};
      \draw[ultra thick] (3.1415, 3pt) -- (3.1415, -3pt) node [below] {$\pi$};
    \end{tikzpicture}$
  \end{block}

  \pause
  \begin{block}{Notation}
    \begin{description}
    \item[$c\in\mathbb{R}$] means \emph{``$c$ is a real number''}
    \item[$c\not\in\mathbb{R}$] means \emph{``$c$ is not a real number''}
    \end{description}
  \end{block}

  \pause
  \begin{example}
    \begin{columns}[onlytextwidth, t]
      \begin{column}[T]{.33\textwidth}
        \begin{itemize}
        \item $22\in\mathbb{R}$
        \item $\int\not\in\mathbb{R}$
        \end{itemize}
      \end{column}
      \begin{column}[T]{.33\textwidth}
        \begin{itemize}
        \item $\sqrt{2}\in\mathbb{R}$
        \item $-\pi\in\mathbb{R}$
        \end{itemize}
      \end{column}
      \begin{column}[T]{.33\textwidth}
        \begin{itemize}
        \item $i\not\in\mathbb{R}$
        \item $\sum\not\in\mathbb{R}$
        \end{itemize}
      \end{column}
    \end{columns}
  \end{example}

\end{frame}


\subsection{Motivation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Many quantities in geometry and physics can be characterized by a single real
  number.

  \pause
  \begin{example}
    \begin{description}[<+->]
    \item[length] ``The Burj Khalifa is {\color{blue}$\SI{829.8}{\metre}$}
      tall.''
    \item[temperature] ``The freezing point of nitrogen is
      {\color{blue}$\SI{-210}{\degreeCelsius}$}.''
    \item[time] ``The running time of Stanley Kubrick's \emph{2001: A Space
        Odyssey} is {\color{blue}{$\SI{8520}{\second}$}}.''
    \item[area] ``The land area of Durham is
      {\color{blue}$\SI{278.1}{\km\squared}$}.''
    \end{description}
  \end{example}

  \onslide<+->{
    \begin{block}{Terminology}
      Elements of $\mathbb{R}$ are often referred to as \emph{scalars}.
    \end{block}}

\end{frame}


\section{Euclidean Space $\mathbb{R}^n$}
\subsection{Definition and Notation}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    $\mathbb{R}^n=\textnormal{"the collection of ordered lists of $n$ real numbers"}$
  \end{definition}

  \pause
  \begin{example}
    There are many ways to denote an ordered list of $n$ real numbers. Each of
    \begin{align*}
      (-3, 1, 4) && \langle -3, 1, 4\rangle && \left(\begin{array}{r}-3 \\1
          \\4\end{array}\right) && \left[\begin{array}{r}-3 \\1
          \\4\end{array}\right]
    \end{align*}
    are elements of $\mathbb{R}^3$.
  \end{example}

  \pause
  \begin{definition}
    A generic element of $\mathbb{R}^n$ is of the form
    $L=(x_1,x_2,\dotsc,x_n)$. The numbers $x_1,x_2,\dotsc,x_n$ are called the
    \emph{coordinates} of $L$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Notation}
    \begin{description}
    \item[$L\in\mathbb{R}^n$] means ``$L$ is an ordered list of $n$ real
      numbers''
    \item[$L\not\in\mathbb{R}^n$] means ``$L$ is not an ordered list of $n$ real
      numbers''
    \end{description}
  \end{block}

  \pause
  \begin{example}
    \begin{columns}[onlytextwidth, t]
      \begin{column}[T]{.5\textwidth}
        \begin{itemize}
        \item<2-> $(1,2)\in\mathbb{R}^2$
        \item<3-> $\langle0,3,-3\rangle\in\mathbb{R}^3$
        \item<4-> $\begin{pmatrix}77\\11\\22\\99\end{pmatrix}\in\mathbb{R}^4$
        \end{itemize}
      \end{column}
      \begin{column}[T]{.5\textwidth}
        \begin{itemize}
        \item<2-> $(1,2)\not\in\mathbb{R}^6$
        \item<3-> $\langle0,3,-3\rangle\not\in\mathbb{R}^{11}$
        \item<4-> $\begin{pmatrix}77\\11\\22\\99\end{pmatrix}\not\in\mathbb{R}^{3}$
        \end{itemize}
      \end{column}
    \end{columns}
  \end{example}

  \onslide<5->{
    \begin{block}{Terminology}
      $\mathbb{R}^n$ is referred to as ``Euclidean $n$-space''
    \end{block}}

\end{frame}


\subsection{Motivation}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Lists of numbers are useful!

  \pause
  \begin{example}[NYSE Opening Prices, 28-August 2017]

    \begin{center}
      \tiny{
        \begin{tabular}{lr}
          Description & Open\\
          \hline
          CHESAPEAKE ENERGY CORP & 3.79\\
          BANK AMER CORP & 23.77\\
          FORD MTR CO DEL & 10.82\\
          VALE S A & 10.68\\
          KROGER CO & 21.7399\\
          ALIBABA GROUP HLDG LTD & 171.74\\
          SNAP INC & 14.78\\
          GENERAL ELECTRIC CO & 24.49\\
          WHITING PETE CORP NEW & 4.41\\
          GAMESTOP CORP NEW & 19.40\\
        \end{tabular}}
    \end{center}
    \pause
    These opening prices can be thought of as an element of $\mathbb{R}^{10}$.
    {\footnotesize\[L=(3.79, 23.77, 10.82, 10.68, 21.7399, 171.74, 14.78, 24.49, 4.41, 19.40)\]}\pause%
    The NYSE carries $2400$ stocks. Thus, on any given day, the list of
    all opening prices on the NYSE is an element of $\mathbb{R}^{2400}$!
  \end{example}

\end{frame}

\section{Interpreting Elements of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}

  The element of $\mathbb{R}^n$ with coordinates $x_1,x_2,\dotsc,x_n$ can be
  interpreted in two ways:
  \pause
  \begin{description}[<+->]
  \item[a point] $P=\begin{pmatrix} x_1\\ \vdots \\ x_n\end{pmatrix}$
    or $P=(x_1,x_2,\dotsc,x_n)$
  \item[a vector] $\vv{P}=\begin{bmatrix} x_1\\ \vdots \\ x_n\end{bmatrix}$
    or $\vv{P}=\langle x_1,x_2,\dotsc,x_n\rangle$
  \end{description}

  \onslide<+->{
    \begin{block}{Idea}
      Points represent \emph{positions} and vectors represent \emph{increments}.
    \end{block}}
\end{frame}

\section{Points}
\subsection{Points in $\mathbb{R}^2$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  A point in $\mathbb{R}^2$ represents a location in Euclidean $2$-space.
  \[
  \begin{tikzpicture}[line cap=round, line join=round]
    \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
    \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

    \pause

    \draw[thick, blue, dashed] (1, 0) -- (1, 2);
    \draw[thick, blue, dashed] (0, 2) -- (1, 2);
    \draw[ultra thick] (1, 3pt) -- (1, -3pt) node [below] {$1$};
    \draw[ultra thick] (3pt, 2) -- (-3pt, 2) node [left] {$2$};
    \node[label={right:$P=(1,2)$}] at (1, 2) {\textbullet};

    \pause

    \draw[thick, blue, dashed] (-3, 0) -- (-3, -2);
    \draw[thick, blue, dashed] (0, -2) -- (-3, -2);
    \draw[ultra thick] (-3, 3pt) -- (-3, -3pt) node [below, fill=white] {$-3$};
    \draw[ultra thick] (3pt, -2) -- (-3pt, -2) node [left, fill=white] {$-2$};
    \node[label={below:$P=(-3,-2)$}] at (-3, -2) {\textbullet};
  \end{tikzpicture}
  \]
\end{frame}

\subsection{Points in $\mathbb{R}^3$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  A point in $\mathbb{R}^3$ represents a location in Euclidean $3$-space.

  \[
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[tdplot_main_coords, scale=3/2]
    \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
    \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
    \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

    \pause

    \draw[] (1.5, 0, .1pt) -- (1.5, 0, -.1pt) node [below] {\tiny{$3$}};
    \draw[] (0, 1, .1pt) -- (0, 1, -.1pt) node [below] {\tiny{$3$}};
    \draw[] (0, .1pt, -3/2) -- (0, -.1pt, -3/2) node [left] {\tiny{$-4$}};
    \draw[thick, blue, dashed] (3/2, 0, 0) -- (3/2, 1, 0);
    \draw[thick, blue, dashed] (0, 1, 0) -- (3/2, 1, 0);
    \draw[thick, blue, dashed] (3/2, 1, 0) -- (3/2, 1, -3/2);
    \node[label={right:\tiny{$P=(3,3,-4)$}}] at (3/2, 1, -3/2) {\textbullet};

    \pause

    \draw[] (-2, 0, .1pt) -- (-2, 0, -.1pt) node [below] {\tiny{$-4$}};
    \draw[] (0, 4/3, .1pt) -- (0, 4/3, -.1pt) node [below] {\tiny{$4$}};
    \draw[] (0, .1pt, 5/3) -- (0, -.1pt, 5/3) node [left] {\tiny{$5$}};
    \draw[thick, blue, dashed] (-2, 0, 0) -- (-2, 4/3, 0);
    \draw[thick, blue, dashed] (0, 4/3, 0) -- (-2, 4/3, 0);
    \draw[thick, blue, dashed] (-2, 4/3, 0) -- (-2, 4/3, 5/3);
    \node[label={right:\tiny{$P=(-4,4,5)$}}] at (-2, 4/3, 5/3) {\textbullet};
  \end{tikzpicture}
  \]
\end{frame}

\subsection{Point Notation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  We typically use capital letters to denote points in $\mathbb{R}^n$. The
  notation $P(x,y,z)$ is often used in place of $P=(x,y,z)$.

  \pause
  \begin{example}
    \begin{description}
    \item[$P(2,-5)$] means ``$P$ is the point in $\mathbb{R}^2$ with coordinates $P=(2,-5)$''
    \item[$Q(3,-4,11)$] means ``$Q$ is the point in $\mathbb{R}^3$ with coordinates $Q=(3,-4,11)$''
    \end{description}
  \end{example}

  \pause
  \begin{definition}
    The \emph{origin} in $\mathbb{R}^n$ is the point $O=(0,0,\dotsc,0)$.
  \end{definition}

  \pause
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[scale=1/3]
      \draw[thick,<->] (-3,0) -- (3,0) node[right]{$x$};
      \draw[thick,<->] (0,-2) -- (0,2) node[above]{$y$};

      \node at (0,0) {\textbullet};
      \node at (3/2, 2/3) {\tiny{$O(0,0)$}};
    \end{tikzpicture}
    \]
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=1/3]
      \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

      \node at (0,0,0) {\textbullet};

      \node at (3/2, 1, 1) [right] {\tiny{$O(0,0,0)$}};
    \end{tikzpicture}
    \]
  \end{columns}

\end{frame}

\subsection{Coordinate Planes in $\mathbb{R}^3$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.3\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords]
      \onslide<3->{ \filldraw[draw=red, fill=red!20] (0,0,0) -- (3,0,0) --
        (3,2,0) -- (0,2,0) -- cycle; \node at (3/2, 1, 0) {$xy$};}

      \onslide<5->{ \filldraw[draw=blue, fill=blue!20] (0,0,0) -- (0,0,2) --
        (0,2,2) -- (0,2,0) -- cycle; \node at (0, 1, 1) {$yz$};}

      \onslide<7->{ \filldraw[draw=green, fill=green!20] (0,0,0) -- (3,0,0) --
        (3,0,2) -- (0,0,2) -- cycle; \node at (3/2, 0, 1) {$xz$};}

      \draw[thick,->] (0,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[thick,->] (0,0,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[thick,->] (0,0,0) -- (0,0,2) node[anchor=south]{$z$};

      \onslide<8->{
        \coordinate (P) at (2.5, 1.5, 1.5);
        \filldraw (P) circle (2pt) node [right] {\tiny{$P$}};}

      \onslide<9->{\draw[thick, dashed] (P) -- (0,1.5,1.5);}
      \onslide<10->{\draw[thick, dashed] (P) -- (2.5,0,1.5);}
      \onslide<11->{\draw[thick, dashed] (P) -- (2.5,1.5,0);}
    \end{tikzpicture}
    \]
    \column{.7\textwidth}
    \begin{description}
    \item<2->[$xy$-plane] all points $P(x,y,z)$ where $z=0$
    \item<4->[$yz$-plane] all points $P(x,y,z)$ where $x=0$
    \item<6->[$xz$-plane] all points $P(x,y,z)$ where $y=0$
    \end{description}
  \end{columns}

  \onslide<8->{Suppose $P=(p_1,p_2,p_3)$.}

  \begin{description}
  \item<9->[$\abs{p_1}=$] distance from $P$ to $yz$-plane
  \item<10->[$\abs{p_2}=$] distance from $P$ to $xz$-plane
  \item<11->[$\abs{p_3}=$] distance from $P$ to $xy$-plane
  \end{description}

\end{frame}

\subsection{Distance}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  What is the distance between two points $P(p_1,p_2)$ and $Q(q_1,q_2)$ in
  $\mathbb{R}^2$?
  \[
  \begin{tikzpicture}[scale=1]
    % \draw (.9, )
    \coordinate (P) at (0,0);
    \coordinate (Q) at (3, 3);
    \coordinate (R) at (3, 0);
    \coordinate (S1) at (2.8, 0);
    \coordinate (S2) at (2.8, .2);
    \coordinate (S3) at (3, .2);

    \onslide<2->{\draw (S1) -- (S2) -- (S3);}
    \onslide<2->{\draw[thick, dashed, blue] (P) -- (R) node[midway, below] {$\abs{q_1-p_1}$};}
    \onslide<2->{\draw[thick, dashed, red] (R) -- (Q) node[midway, right] {$\abs{q_2-p_2}$};}
    \draw[thick, dashed, purple] (P) -- (Q) node[midway, above left] {$\dist(P, Q)=?$};

    \node[label={left:{$P(p_1,p_2)$}}] at (P) {\textbullet};
    \node[label={right:{$Q(q_1,q_2)$}}] at (Q) {\textbullet};
  \end{tikzpicture}
  \]
  \onslide<3->{
    The Pythagorean Theorem implies that
    \[
    \dist(P, Q)^2=\abs{q_1-p_1}^2+\abs{q_2-p_2}^2
    \]}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{distance} between two points $P(p_1,p_2,\dotsc,p_n)$ and
    $Q(q_1,q_2,\dotsc,q_n)$ in $\mathbb{R}^n$ is
    \[
    \dist(P, Q)=\sqrt{(q_1-p_1)^2+(q_2-p_2)^2+\dotsb+(q_n-p_n)^2}
    \]
  \end{definition}

  \pause
  \begin{example}
    What is the distance between $P(1,2,4,-7)$ and $Q(-3,8,5,-7)$?
    \pause
    \begin{align*}
      \dist(P, Q)
      &= \sqrt{(-3-1)^2+(8-2)^2+(5-4)^2+(-7-(-7))^2} \\
      &= \sqrt{(-4)^2+(6)^2+(1)^2+(0)^2} \\
      &= \sqrt{16+36+1+0} \\
      &= \sqrt{53}
    \end{align*}
  \end{example}
\end{frame}

\section{Vectors}
\subsection{Definition and Notation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{vector} in $\mathbb{R}^n$ is a direction and a magnitude. Vectors
    are represented by arrows in $\mathbb{R}^n$.\pause
    \begin{description}[<+->]
    \item[direction] is the direction the arrow points
    \item[magnitude] is the length of the arrow
    \end{description}
  \end{definition}

  \onslide<+->{
    \begin{block}{Notation}
      We will use the $\vv{}$ symbol to denote vectors.
      \begin{description}
      \item[$\vv{v}\in\mathbb{R}^n$] means ``$\vv{v}$ is a vector in $\mathbb{R}^n$''
      \end{description}}
    \onslide<+->{
      Other common notation:
      \begin{align*}
        \mathbf{v} && \overline{v} && \underline{v} && v
      \end{align*}
    \end{block}}
\end{frame}

\subsection{Visualizing Vectors}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=1]
      \draw[thick,<->] (-1,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-1/2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[thick,<->] (0,0,-1/2) -- (0,0,2) node[anchor=south]{$z$};

      \pause
      \draw[thick, blue, ->]
      (0, 0, 0) -- (1, 4/3, 1) node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (0, 2/3, 2) -- (1, 2, 3)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (-3/2, -1/3, -2) -- (-1/2, 1, -1)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (-3/2, -7/3, -2) -- (-1/2, -1, -1)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (-3/2, -7/3, 1) -- (-1/2, -1, 2)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (-3/2, -7/3, -1) -- (-1/2, -1, 0)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (-3/2, -4/3, 0) -- (-1/2, 0, 1)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
      \draw[thick, blue, ->]
      (0, -1/3, -1) -- (1, 1, 0)  node[midway, above] {{\visible<4->{\footnotesize{$\vv{v}$}}}};
    \end{tikzpicture}
    \]
    \column{.5\textwidth} \onslide<3->{These are the same vector!}  \onslide<5->{Each has the
      \emph{same direction} and the \emph{same magnitude}.}
  \end{columns}

  \begin{description}
  \item<6->[vectors] have \emph{direction} and \emph{magnitude} but \emph{no location}
  \item<6->[points] have \emph{no direction} and \emph{no magnitude}, \emph{only location}
  \end{description}

\end{frame}

\subsection{Coordinates}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth} Every arrow in $\mathbb{R}^n$ has an \emph{initial
      point $P$} and a \emph{terminal point $Q$}.
    \column{.4\textwidth}
    \[
    \begin{tikzpicture}[scale=2/3]
      \node[label={left:$P$}] at (0, 0) {\textbullet};
      \node[label={right:$Q$}] at (3, 2) {\textbullet};
      \draw[ultra thick, blue, ->] (0, 0) -- (3, 2);
    \end{tikzpicture}
    \]
  \end{columns}

  \pause
  \begin{definition}
    Let $\vv{v}$ be a vector in $\mathbb{R}^n$ represented by the arrow with
    initial point $P(p_1,\dotsc,p_n)$ and terminal point
    $Q(q_1,\dotsc,q_n)$. The \emph{coordinates of $\vv v$} are
    \begin{align*}
      \vv v &= \langle q_1-p_1, q_2-p_2,\dotsc,q_n-p_n\rangle &
      \textnormal{or} &&
      \vv v &=
      \begin{bmatrix}
        q_1-p_1 \\
        q_2-p_2 \\
        \vdots \\
        q_n-p_n
      \end{bmatrix}
    \end{align*}
    This vector is called the \emph{displacement vector from $P$ to $Q$} and
    often written as $\vv{v}=\vv{PQ}$.
  \end{definition}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{columns}[onlytextwidth]
      \column{.4\textwidth}
      \[
      \tdplotsetmaincoords{70}{110}
      \begin{tikzpicture}[tdplot_main_coords, scale=1/4]
        \coordinate (P) at (11, 2, 0); \coordinate (Q) at (8,-4,6); \coordinate
        (R) at (-2,2,7); \coordinate (S) at (1,8,1);

        \draw[thick,->] (0,0,0) -- (12,0,0) node[anchor=north east]{$x$};
        \draw[thick,->] (0,0,0) -- (0,9,0) node[anchor=north west]{$y$};
        \draw[thick,->] (0,0,0) -- (0,0,9) node[anchor=south]{$z$};

        \filldraw (P) circle (8pt) node [below right] {\tiny{$P(11,2,0)$}};
        \filldraw (Q) circle (8pt) node [below] {\tiny{$Q(8,-4,6)$}}; \filldraw
        (R) circle (8pt) node [above right] {\tiny{$R(-2,2,7)$}}; \filldraw (S)
        circle (8pt) node [above right] {\tiny{$S(1,8,1)$}};

        \onslide<2->{\draw[ultra thick, blue, ->] (Q) -- (R);}
        \onslide<4->{\draw[ultra thick, blue, ->] (P) -- (R);}
        \onslide<6->{\draw[ultra thick, blue, ->] (P) -- (S);}
      \end{tikzpicture}
      \]
      \column{.6\textwidth}
      \begin{align*}
        \onslide<3->{\vv{QR} &=
          \left[\begin{array}{r}
              -2-8\\2-(-4)\\7-6
            \end{array}\right]
          = \left[\begin{array}{r}
              -10 \\
              6 \\
              1
            \end{array}\right] \\}
        \onslide<5->{\vv{PR} &=
          \left[\begin{array}{r}
              -2-11\\2-2\\7-0
            \end{array}\right]
          = \left[\begin{array}{r}
              -13 \\ 0 \\ 7
            \end{array}\right] \\}
        \onslide<7->{\vv{PS} &=
          \left[\begin{array}{r}
              1-11\\8-2\\1-0
            \end{array}\right]
          = \left[\begin{array}{r}
              -10 \\ 6 \\ 1
            \end{array}\right] \\}
      \end{align*}
    \end{columns}
    \onslide<8->{Note that $\vv{QR}=\vv{PS}$!}
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Let $P$ be a point in $\mathbb{R}^n$. We use the notation $\vv{P}=\vv{OP}$.
  \pause
  \begin{example}
    \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=1/4]
      \coordinate (O) at (0,0,0);
      \coordinate (P) at (-2, 5, 7);
      \filldraw (P) circle (8pt) node [right] {\tiny{$P(-2,5,7)$}};

      \draw[thick,->] (0,0,0) -- (12,0,0) node[anchor=north east]{$x$};
      \draw[thick,->] (0,0,0) -- (0,9,0) node[anchor=north west]{$y$};
      \draw[thick,->] (0,0,0) -- (0,0,9) node[anchor=south]{$z$};

      \onslide<3->{
        \filldraw (O) circle (8pt);
        \draw[ultra thick, blue, ->] (O) -- (P) node[midway, right] {\tiny{$\vv{P}=\langle-2,5,7\rangle$}};}
    \end{tikzpicture}
    \]
  \end{example}

  \begin{definition}<4->
    The \emph{zero vector} in $\mathbb{R}^n$ is $\vv{O}=\langle0,0,\dotsc,0\rangle$.
  \end{definition}
\end{frame}

\subsection{Motivation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Many quantities in geometry and physics involve both magnitude and direction
  and cannot be characterized completely by a single real number.

  \pause
  \begin{description}[<+->]
  \item[velocity] involves \emph{direction} and \emph{speed}
  \item[force] involves \emph{direction} and \emph{intensity}
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    A ball is shot into the air. At any given time, the ball's velocity has a
    \emph{direction} and a \emph{speed}.
    \[
    \begin{tikzpicture}[xscale=2]
      \draw[thick, dashed, ->] (0, 4) parabola (2, 0);
      \draw[thick, dashed] (0, 4) parabola (-2, 0);

      \onslide<1>{\node at (-2, 0) {\textbullet};}

      \onslide<2>{\node at (-1,3) {\textbullet};
        \draw[thick, blue, ->] (-1,3) -- (-1/2,4) node[midway, above] {$\vv{v}$};}

      \onslide<3>{\node at (3/2,7/4) {\textbullet};
        \draw[thick, blue, ->] (3/2,7/4) -- (2,1/4) node[midway, above] {$\vv{v}$};}
    \end{tikzpicture}
    \]
  \end{example}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the points $P(1, 2, -1)$, $Q(-3, 2, 5)$, $R(0,4,-1)$, and $S(3, 4,
    -11)$. Does the figure $PQRS$ form a parallelogram?
  \end{example}

\end{frame}

\section{Vector Arithmetic}
\subsection{Length}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{block}{Question}
      Let $\vv{v}$ be a vector in $\mathbb{R}^n$ represented as
      $\vv{v}=\vv{PQ}$. What is the length of $\vv{v}$?
    \end{block}
    \column{.4\textwidth}
    \[
    \begin{tikzpicture}[scale=3/4]
      \node[label={left:$P$}] at (0, 0) {\textbullet};
      \node[label={right:$Q$}] at (3, 2) {\textbullet};
      \draw[ultra thick, blue, ->] (0, 0) -- (3, 2);
    \end{tikzpicture}
    \]
  \end{columns}

  \pause
  \begin{definition}[Geometric]
    The \emph{length} of $\vv{v}=\vv{PQ}$ is $\norm*{\vv{v}}=\dist(P, Q)$.
  \end{definition}

  \pause
  \begin{block}{Notation}
    Some texts use $\abs{\vv{v}}$ instead of $\norm{\vv{v}}$ to denote the
    length of $\vv{v}$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Length From Coordinates}
    The coordinates of $\vv{v}=\vv{PQ}$ are
    \[
    \vv{v}
    = \langle
    \underbrace{q_1-p_1}_{=v_1},
    \underbrace{q_2-p_2}_{=v_2},
    \dotsc,
    \underbrace{q_n-p_n}_{=v_n}
    \rangle
    \]
    \pause
    The length of $\vv{v}$ is thus
    \begin{align*}
      \norm{\vv{v}}
      &= \dist(P, Q) \\
      &= \sqrt{(q_1-p_1)^2+(q_2-p_2)^2+\dotsb+(q_n-p_n)^2} \\
      &= \sqrt{v_1^2+v_2^2+\dotsb+v_n^2}
    \end{align*}
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Algebraic]
    The \emph{length} of $\vv{v}=\langle v_1,v_2,\dotsc,v_n\rangle$ is
    \[
    \norm{\vv{v}}=\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}
    \]
  \end{definition}

  \pause
  \begin{example}
    The length of $\vv{v}=\langle -4, 7, 3, 1\rangle$ is
    \begin{align*}
      \norm{\vv{v}}
      &= \sqrt{(-4)^2+7^2+3^2+1^2} \\
      &= \sqrt{16+49+9+1} \\
      &= \sqrt{75}
    \end{align*}
  \end{example}
\end{frame}


\subsection{Vector Addition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.4\textwidth}
    \begin{block}{Question}
      Let $\vv{v},\vv{w}\in\mathbb{R}^n$ be vectors. What is
      $\vv{v}+\vv{w}$?
    \end{block}
    \column{.6\textwidth}
    \[
    \begin{tikzpicture}[line cap=round, line join=round]
      \coordinate (O) at (0,0);
      \coordinate (P) at (3,1);
      \coordinate (Q) at (1,2);
      \draw[ultra thick, blue, ->] (O) -- (P) node[midway, below] {$\vv{v}$};
      \draw[ultra thick, purple, ->] (O) -- (Q) node[midway, left] {$\vv{w}$};

      \onslide<3->{
        \draw[ultra thick, blue, ->] (Q) -- ($ (P) + (Q) $) node[midway, above] {$\vv{v}$};
        \draw[ultra thick, purple, ->] (P) -- ($ (P) + (Q) $) node[midway, right] {$\vv{w}$};}

      \onslide<5->{
        \draw[ultra thick, violet, ->]
        (O) -- ($ (P) + (Q) $) node[midway, left] {\small{$\vv{v}+\vv{w}$}};}
    \end{tikzpicture}
    \]
  \end{columns}


  \begin{definition}<2->[Geometric] Form a parallelogram $\sslash(\vv{v},
    \vv{w})$ from $\vv{v}$ and $\vv{w}$. \onslide<4->{The \emph{sum} of
      $\vv{v}$ and $\vv{w}$ is the diagonal $\vv{v}+\vv{w}$ of
      $\sslash(\vv{v}, \vv{w})$ obtained by first transversing $\vv{v}$ and
      then transversing $\vv{w}$.}
  \end{definition}

  \begin{block}<6->{Note}
    This definition immediately implies that $\vv{v}+\vv{w}=\vv{w}+\vv{v}$.
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[algebraic]
    Let $\vv{v},\vv{w}\in\mathbb{R}^n$ be vectors. The \emph{sum} of $\vv{v}$
    and $\vv{w}$ is
    \[
    \vv{v}+\vv{w}
    =
    \begin{bmatrix}v_1\\ v_2\\ \vdots\\ v_n \end{bmatrix}+
    \begin{bmatrix}w_1\\ w_2\\ \vdots\\ w_n \end{bmatrix}
    =
    \begin{bmatrix}v_1+w_1\\ v_2+w_2\\ \vdots\\ v_n+w_n \end{bmatrix}
    \]
  \end{definition}

  \pause
  \begin{example}
    $\left[\begin{array}{r}-1 \\-8 \\9 \\1\end{array}\right]
    +\left[\begin{array}{r}5 \\-1 \\-1 \\-3\end{array}\right]
    =\left[\begin{array}{r}-1+5 \\-8+(-1) \\9+(-1) \\1+(-3)\end{array}\right]
    =\left[\begin{array}{r}4 \\-9 \\8 \\-2\end{array}\right]$
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    If $\vv{v}$ and $\vv{w}$ are vectors in $\mathbb{R}^n$, then
    $\vv{v}+\vv{w}$ is a vector in $\mathbb{R}^n$.
  \end{block}

  \pause
  \begin{block}{Note}
    The sum $\vv{v}+\vv{w}$ is only defined if $\vv{v}$ and $\vv{w}$ have
    the same dimension.
  \end{block}

  \pause
  \begin{example}
    The sum
    $\left[\begin{array}{r}3 \\-1 \\2\end{array}\right]
    +\left[\begin{array}{r}1 \\1 \\-1 \\-15\end{array}\right]$ is nonsensical!
  \end{example}
\end{frame}


\subsection{Scalar Multiplication}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Given a vector $\vv{v}\in\mathbb{R}^n$ and a scalar $c\in\mathbb{R}$, what
    ``should'' $c\cdot\vv{v}$ look like?
  \end{block}
  \[
  \begin{tikzpicture}[line cap=round, line join=round]
    \coordinate (O) at (0, 0);
    \coordinate (P) at (2, 1);
    \coordinate (S) at (1, 0);

    \onslide<2->{
      \draw[ultra thick, blue, ->] (O) -- (P) node[midway, above] {$\vv{v}$};}

    \onslide<3->{
      \draw[ultra thick, blue, ->]
      ($ (O) + 2*(S) $) -- ($ 2*(P) + 2*(S) $) node[midway, above] {$2\cdot\vv{v}$};}

    \onslide<4->{
      \draw[ultra thick, blue, ->]
      ($ (O) + 4*(S) $) -- ($ .5*(P) + 4*(S) $) node[midway, above] {$\frac{1}{2}\cdot\vv{v}$};}


    \onslide<5->{
      \draw[ultra thick, blue, <-] ($ (O) + 6*(S) $) -- ($ (P) + 6*(S)$) node[midway, above] {$-\vv{v}$};}

    \onslide<6->{
      \draw[ultra thick, blue, <-]
      ($ (O) + 8*(S) $) -- ($ .5*(P) + 8*(S) $) node[midway, above] {$-\frac{1}{2}\cdot\vv{v}$};}
  \end{tikzpicture}
  \]

  \begin{definition}<7->[Geometric] The \emph{scalar product} of $c$ and
    $\vv{v}$ is the vector $c\cdot\vv{v}\in\mathbb{R}^n$ whose magnitude is
    $\abs{c}\cdot\norm{\vv{v}}$ and whose direction is either the direction of
    $\vv{v}$ if $c>0$ or the opposite direction of $\vv{v}$ if $c<0$.
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Algebraic]
    Let $\vv{v}\in\mathbb{R}^n$ be a vector and let $c\in\mathbb{R}$ be a
    scalar. The \emph{scalar product} of $c$ and $\vv{v}$ is
    \[
    c\cdot\vv{v}
    = c\cdot\begin{bmatrix} v_1\\ v_2\\ \vdots\\ v_n\end{bmatrix}
    = \begin{bmatrix} c\cdot v_1\\ c\cdot v_2\\ \vdots\\ c\cdot v_n\end{bmatrix}
    \]
  \end{definition}

  \pause
  \begin{example}
    $(-4)\cdot\left[\begin{array}{r}3 \\2 \\1 \\-1\end{array}\right]
    =\left[\begin{array}{r}(-4)\cdot3 \\(-4)\cdot2 \\(-4)\cdot1 \\(-4)\cdot(-1)\end{array}\right]
    =\left[\begin{array}{r}-12 \\-8 \\-4 \\4\end{array}\right]$
  \end{example}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\norm{c\cdot\vv{v}}=\abs{c}\cdot\norm{\vv{v}}$
  \end{theorem}
  \pause
  \begin{proof}
    Use coordinates
    \begin{align*}
      \norm{c\cdot\vv{v}}
      \onslide<+->{&= \norm{\langle c\cdot v_1,c\cdot v_2,\dotsc,c\cdot v_n\rangle} \\}
      \onslide<+->{&= \sqrt{(c\cdot v_1)^2+(c\cdot v_2)^2+\dotsb+(c\cdot v_n)^2} \\}
      \onslide<+->{&= \sqrt{c^2\cdot v_1^2+c^2\cdot v_2^2+\dotsb+c^2\cdot v_n^2} \\}
      \onslide<+->{&= \sqrt{c^2\cdot(v_1^2+v_2^2+\dotsb+v_n^2)} \\}
      \onslide<+->{&= \sqrt{c^2}\cdot\sqrt{v_1^2+v_2^2+\dotsb+v_n^2} \\}
      \onslide<+->{&= \abs{c}\cdot\norm{\vv{v}}\qedhere}
    \end{align*}
  \end{proof}

\end{frame}


\subsection{Vector Subtraction}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Vector subtraction is a combination of scalar multiplication and vector
    addition
    \[
    \vv{v}-\vv{w}=\vv{v}+(-1)\cdot\vv{w}
    \]
    \onslide<2->{Subtraction can be visualized with our geometric interpretation of vector addition.}
    \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=1]
      \coordinate (O) at (0,0);
      \coordinate (P) at (5,1);
      \coordinate (Q) at (2,2);
      \onslide<3->{
        \draw[ultra thick, blue, ->] (O) -- (P) node[midway, below] {$\vv{v}$};
        \draw[ultra thick, purple, ->] (O) -- (Q) node[midway, left] {$\vv{w}$};}

      \onslide<4->{
        \draw[ultra thick, violet, ->]
        (Q) -- (P) node[midway, above] {\small{$\vv{v}-\vv{w}$}};}
    \end{tikzpicture}
    \]
  \end{definition}
\end{frame}


\subsection{Algebraic Properties}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of Vector Arithmetic}
    Vector arithmetic obeys the following laws.
    \begin{description}[<+->]
    \item[additive commutativity] $\vv{v}+\vv{w}=\vv{w}+\vv{v}$
    \item[additive associativity] $(\vv{v}+\vv{w})+\vv{x}=\vv{v}+(\vv{w}+\vv{x})$
    \item[multiplicitive associativity] $(c\cdot d)\cdot\vv{v}=c\cdot(d\cdot\vv{v})$
    \item[scalar distribution] $c\cdot(\vv{v}+\vv{w})=c\cdot\vv{v}+c\cdot\vv{w}$
    \item[vector distribution] $(c+d)\cdot\vv{v}=c\cdot\vv{v}+d\cdot\vv{v}$
    \end{description}
  \end{block}
\end{frame}

\section{Terminology and Notation}

\subsection{Parallel Vectors}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{definition}[Geometric]
      Two vectors $\vv{v}$ and $\vv{w}$ are \emph{parallel} if they point in
      the same direction or in the opposite direction.
    \end{definition}
    \column{.4\textwidth}
    \[
    \onslide<2->{
      \begin{tikzpicture}[line cap=round, line join=round, scale=2/3]
        \coordinate (O) at (0,0);
        \coordinate (P) at (1,1);
        \coordinate (Q) at (2,0);
        \coordinate (R) at (4,2);
        \draw[ultra thick, blue, ->] (O) -- (P);
        \draw[ultra thick, purple, ->] (R) -- (Q);
      \end{tikzpicture}}
    \]
  \end{columns}
  
  \onslide<3->{
    \begin{definition}[Algebraic]
      Two vectors $\vv{v}$ and $\vv{w}$ are \emph{parallel} if
      $\vv{v}=c\cdot\vv{w}$ for some $c\in\mathbb{R}$.
    \end{definition}}

  \onslide<4>{
    \begin{example}
      Is $\vv{v}=\left[\begin{array}{r}-1 \\15 \\0 \\2\end{array}\right]$ parallel
      to $\vv{w}=\left[\begin{array}{r}-1 \\-15 \\0 \\2\end{array}\right]$?
    \end{example}}

\end{frame}

\subsection{Unit Vectors}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{unit vector} is a vector $\vv{v}$ with length one $\norm{v}=1$.
  \end{definition}

  \pause
  \begin{example}
    Each of 
    \begin{align*}
      \left[\begin{array}{r}1 \\0\end{array}\right]
      &&
      \left[\begin{array}{r}\nicefrac{3}{13} \\\nicefrac{4}{13}\\ \nicefrac{12}{13}\end{array}\right]
      &&
      \left[\begin{array}{r}\nicefrac{1}{\sqrt{26}} \\\nicefrac{3}{\sqrt{26}}\\ 0\\ -\nicefrac{4}{\sqrt{26}}\end{array}\right]
    \end{align*}
    is a unit vector.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.6\textwidth}
    \begin{theorem}
      Let $\vv{v}\in\mathbb{R}^n$ be a vector with $\vv{v}\neq\vv{O}$. Then
      $\displaystyle\vv{u}=\frac{1}{\norm{\vv{v}}}\cdot\vv{v}$ is a unit
      vector.
    \end{theorem}
    \column{.4\textwidth}
    \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=3/5]
      \coordinate (O) at (0,0);
      \coordinate (P) at (6,3);
      \coordinate (Q) at (2,1);

      \onslide<2->{
        \draw[ultra thick, blue, ->] (O) -- (P) node[midway, below] {$\vv{v}$};}
      
      \onslide<3->{
        \draw[ultra thick, purple, ->] (O) -- (Q) node[midway, below] {$\vv{u}$};}
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<4->{
    \begin{proof}
      Note that
      \[
      \norm{\vv{u}}
      = \norm*{\frac{1}{\norm{\vv{v}}}\cdot\vv{v}}
      = \abs*{\frac{1}{\norm{\vv{v}}}}\cdot\norm{\vv{v}}
      = \frac{1}{\norm{\vv{v}}}\cdot\norm{\vv{v}}
      = 1\qedhere
      \]
    \end{proof}}
  
  \onslide<5->{
    \begin{definition}
      $\displaystyle\vv{u}=\frac{1}{\norm{\vv{v}}}\cdot\vv{v}$ is called the
      \emph{normalization} of $\vv{v}$
    \end{definition}}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\langle 2, 9, -1\rangle$. Then
    \[
    \norm{\vv{v}}
    = \sqrt{2^2+9^2+(-1)^2}
    = \sqrt{86}
    \]
  \end{example}
  The normalization of $\vv{v}$ is thus
  \[
  \vv{u}
  = \frac{1}{\norm{\vv{v}}}\cdot\vv{v}
  = \frac{1}{\sqrt{86}}\left[\begin{array}{r}2 \\9 \\-1\end{array}\right]
  \]
\end{frame}


\subsection{Linear Combinations}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}$ be vectors in $\mathbb{R}^n$. A
    \emph{linear combination} of $\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}$ is a sum
    of the form
    \[
    c_1\cdot \vv*{v}{1} + c_2\cdot \vv*{v}{2} + \dotsb + c_k\cdot \vv*{v}{k}
    \]
    where $c_1,c_2,\dotsc,c_k$ are scalars.
  \end{definition}

  \pause
  \begin{example}
    The sum
    \[
    \left[\begin{array}{r}-114 \\-166 \\-167\end{array}\right]
    =
    -28\cdot \left[\begin{array}{r}4 \\6 \\6\end{array}\right]
    +(-1)\cdot \left[\begin{array}{r}1 \\-1 \\-2\end{array}\right]
    +1\cdot\left[\begin{array}{r}-1 \\1 \\-1\end{array}\right]
    \]
    is a linear combination of 
    \begin{align*}
      \vv*{v}{1} &= \langle 4, 6, 6\rangle &
      \vv*{v}{2} &= \langle 1, -1, -2\rangle &
      \vv*{v}{3} &= \langle -1, 1, -1\rangle
    \end{align*}
  \end{example}
\end{frame}


\subsection{The Standard Basis of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{standard basis} of $\mathbb{R}^n$ consists of the vectors
    $\vv*{e}{1},\vv*{e}{2},\dotsc,\vv*{e}{n}$ whose coordinates are
    \[
    \vv*{e}{i}=\langle
    0,0,\dotsc,0,\underbrace{1}_{\text{$i$th position}}, 0, \dotsc, 0
    \rangle
    \]
  \end{definition}

  \pause
  \begin{example}
    The standard basis of $\mathbb{R}^2$ is
    \begin{align*}
      \vv*{e}{1} &= \left[\begin{array}{r}1 \\0\end{array}\right] &
      \vv*{e}{2} &= \left[\begin{array}{r}0 \\1\end{array}\right] 
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The standard basis of $\mathbb{R}^3$ is
    \begin{align*}
      \vv*{e}{1} &= \left[\begin{array}{r}1 \\0\\0\end{array}\right] &
      \vv*{e}{2} &= \left[\begin{array}{r}0 \\1\\0\end{array}\right] &
      \vv*{e}{3} &= \left[\begin{array}{r}0 \\0\\1\end{array}\right] \\
      &= \vv{\imath} &
      &= \vv{\jmath} &
      &= \vv{k}
    \end{align*}
  \end{example}

  \pause
  \begin{example}
    The standard basis of $\mathbb{R}^4$ is
    \begin{align*}
      \vv*{e}{1} &= \left[\begin{array}{r}1 \\0\\0\\0\end{array}\right] &
      \vv*{e}{2} &= \left[\begin{array}{r}0 \\1\\0\\0\end{array}\right] &
      \vv*{e}{3} &= \left[\begin{array}{r}0 \\0\\1\\0\end{array}\right] &
      \vv*{e}{4} &= \left[\begin{array}{r}0 \\0\\0\\1\end{array}\right] 
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Every vector $\vv{v}=\langle v_1,v_2,v_3\rangle$ is a linear combination of
    $\vv{\imath}$, $\vv{\jmath}$, and $\vv{k}$
    \[
    \vv{v}
    = \langle v_1,v_2,v_3\rangle
    = v_1\cdot\vv{\imath}+v_2\cdot\vv{\jmath}+v_3\cdot\vv{k}
    \]
  \end{block}

  \pause
  \begin{example}
    Let $\vv{v}=\langle-11, 3, -101\rangle$. Then
    \[
    \vv{v}=-11\,\vv{\imath}+3\,\vv{\jmath}-101\vv{k}
    \]
  \end{example}

\end{frame}





\end{document}
