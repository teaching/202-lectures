\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}




\title{Bases and Dimension of Linear Subspaces}
\subtitle{Math 202}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \tableofcontents[sections={1-3}]
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents[sections={4-}]
\end{frame}


\section{Definitions and Important Results}
\subsection{Definition of a Basis}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{definition}
    Let $V$ be a linear subspace of $\mathbb{R}^n$. %
    \onslide<2->{A list $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ of vectors in $V$ is
      a \emph{basis} of $V$ if}
    \begin{enumerate}
    \item<3-> $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ is linearly independent
    \item<4-> $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}=V$
    \end{enumerate}
  \end{definition}

  \begin{block}{\onslide<5->{Intuition}}
    \onslide<5->{A basis of a linear subspace is a \emph{minimal spanning set}.}
  \end{block}

\end{frame}



\begin{sagesilent}
  Id = identity_matrix(3)
  e1, e2, e3 = [matrix.column(col) for col in Id.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \begin{example}
    Consider the vectors
    \begin{align*}
      \vv*{e}{1} &= \mysage{\scriptsize}{e1} &
      \vv*{e}{2} &= \mysage{\scriptsize}{e2} &
      \vv*{e}{3} &= \mysage{\scriptsize}{e3} 
    \end{align*}
    Is $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ a basis of $\mathbb{R}^3$?
  \end{example}

  \pause
  \begin{block}{Answer}
    Note that
    \[
    \rref
    \begin{bmatrix}
      \vv*{e}{1} & \vv*{e}{2} & \vv*{e}{3}
    \end{bmatrix}
    =\mysage{\scriptsize}{Id}
    \]
    \pause %
    Since $\rank=\#\mycolumns$, $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is \pause%
    linearly independent. %
    \pause%
    Since $\rank=\#\myrows$, \pause%
    $\Span\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}=\mathbb{R}^3$. %
    \pause%
    Thus $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is a basis of $\mathbb{R}^3$.
  \end{block}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The list $\Set{\vv*{e}{1},\vv*{e}{2},\dotsc,\vv*{e}{n}}$ is a basis of
    $\mathbb{R}^n$.
  \end{example}
\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 5)
  w1, w2, w3, w4, w5 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\subsection{Important Results on Bases}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 27.2 in Simon \& Blume]
    Suppose that $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of a linear
    subspace $V$ of $\mathbb{R}^n$. If $\Set{\vv*{w}{1},\dotsc,\vv*{w}{k}}$ is a
    list of vectors in $V$ with $k>d$, then $\Set{\vv*{w}{1},\dotsc,\vv*{w}{k}}$
    is linearly dependent.
  \end{theorem}

  \pause
  \begin{example}
    Recall that $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ is a basis of
    $\mathbb{R}^3$. \pause The theorem implies that the list
    \[
    \Set*{
      \sage{w1},
      \sage{w2},
      \sage{w3},
      \sage{w4},
      \sage{w5}}
    \]
    is \pause \emph{linearly dependent} since \pause $5>3$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 27.3 in Simon \& Blume]
    Let $V$ be a linear subspace of $\mathbb{R}^n$. Suppose that both
    \begin{align*}
      \Set{\vv*{v}{1},\dotsc,\vv*{v}{\ell}} &&\text{and}&&
      \Set{\vv*{w}{1},\dotsc,\vv*{w}{m}} 
    \end{align*}
    are bases of $V$. Then $\ell=m$.
  \end{theorem}

  \pause
  \begin{block}{Note}
    This theorem says that any two bases of $V$ contain the same number of
    vectors.
  \end{block}
\end{frame}


\subsection{Definition of Dimension}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $V$ be a linear subspace of $\mathbb{R}^n$. Suppose that
    $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$. The \emph{dimension
      of $V$} is $\dim(V)=d$.
  \end{definition}

  \pause
  \begin{block}{Note}
    The dimension of a linear subspace $V$ is well-defined since any two bases
    of $V$ contain the same number of vectors.
  \end{block}

  \pause
  \begin{block}{Intuition}
    The dimension of $V$ is a measurement of how ``big'' $V$ is.
  \end{block}

  \pause
  \begin{example}
    $\dim(\mathbb{R}^n)=\pause n$
  \end{example}
\end{frame}




\begin{sagesilent}
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=3)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the vectors 
    \begin{align*}
      \vv*{v}{1} &= \mysage{\tiny}{v1} &
      \vv*{v}{2} &= \mysage{\tiny}{v2} &
      \vv*{v}{3} &= \mysage{\tiny}{v3} 
    \end{align*}
    \pause%
    Let $V$ be the linear subspace of $\mathbb{R}^4$ defined by
    \[
    V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}
    \]\pause%
    Then $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is a basis of $V$ since this list
    is linearly independent. \pause% 
    Thus $\dim(V)=\pause 3$.
  \end{example}

  \pause
  \begin{block}{Note}
    In this example, we say ``$V$ is a three-dimensional linear subspace of
    $\mathbb{R}^4$.''
  \end{block}
\end{frame}


\subsection{Using Dimension to Solve Problems}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $\dim(V)=d$.
    \begin{enumerate}
    \item<2-> If $\Span\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}=V$, then
      $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$.
    \item<3-> If $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is linearly independent, then
      $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$.
    \end{enumerate}
  \end{theorem}

  \begin{block}{\onslide<4->{Note}}
    \onslide<4->{This theorem says that checking if
      $\Set{\vv*{v}{1},\dotsc,\vv*{v}{d}}$ is a basis of $V$ is easier if we
      know that $\dim(V)=d$.}
  \end{block}
\end{frame}



\begin{sagesilent}
  U = random_matrix(ZZ, 3, algorithm='unimodular')
  w1, w2, w3 = [matrix.column(col) for col in (A*U).columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    In the previous example, we defined
    $V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ where
    \begin{align*}
      \vv*{v}{1} &= \mysage{\tiny}{v1} &
      \vv*{v}{2} &= \mysage{\tiny}{v2} &
      \vv*{v}{3} &= \mysage{\tiny}{v3} 
    \end{align*}\pause%
    We showed that $\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$ is a basis of $V$,
    so $\dim(V)=3$. \pause%
    It can be shown that each of
    \begin{align*}
      \vv*{w}{1} &= \mysage{\tiny}{w1} &
      \vv*{w}{2} &= \mysage{\tiny}{w2} &
      \vv*{w}{3} &= \mysage{\tiny}{w3} 
    \end{align*}
    is in $V$ and that $\Set{\vv*{w}{1},\vv*{w}{2},\vv*{w}{3}}$ is linearly
    independent. \pause%
    It follows that $\Set{\vv*{w}{1},\vv*{w}{2},\vv*{w}{3}}$ is a basis of $V$.
  \end{example}
\end{frame}



\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  a1, a2, a3 = [matrix.column(col) for col in A.columns()]
  B = random_matrix(ZZ, 3, algorithm='unimodular')
  b1, b2, b3 = [matrix.column(col) for col in B.columns()]
  C = random_matrix(ZZ, 3, algorithm='unimodular')
  c1, c2, c3 = [matrix.column(col) for col in C.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Each of the lists
    \begin{itemize}
    \item $\Set*{\mysage{\scriptsize}{a1}, \mysage{\scriptsize}{a2}, \mysage{\scriptsize}{a3}}$
    \item $\Set*{\mysage{\scriptsize}{b1}, \mysage{\scriptsize}{b2}, \mysage{\scriptsize}{b3}}$
    \item $\Set*{\mysage{\scriptsize}{c1}, \mysage{\scriptsize}{c2}, \mysage{\scriptsize}{c3}}$
    \end{itemize}
    is linearly independent. \pause%
    Since $\dim(\mathbb{R}^3)=3$, each of these lists is a basis of
    $\mathbb{R}^3$.
  \end{example}
\end{frame}


\section{Bases of Null Spaces}
\subsection{Null Spaces and Nullity}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Let $A$ be a $m\times n$ matrix. \pause%
    The \emph{null space} of $A$ is the linear subspace of $\mathbb{R}^n$
    defined by $\Null(A)=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}$.
  \end{block}

  \pause
  \begin{theorem}
    $\dim\Null(A)=\nullity(A)$
  \end{theorem}
\end{frame}


\subsection{Algorithm for Finding Bases of Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    Let $A$ be a $m\times n$ matrix. 
    \begin{itemize}
    \item<2-> The reduced row-echelon form of $A$ allows us to decompose the
      general solution to the system $A\vv{x}=\vv{O}$ as
      \[
      \vv{x}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_d\cdot\vv*{v}{d}
      \]
      where $\Set{c_1,c_2,\dotsc,c_d}$ are the free variables.
    \item<3-> Since $d=\nullity(A)=\dim\Null(A)$, this equation implies that
      $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{d}}$ is a basis of $\Null(A)$.
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  A = matrix([[1,2,3,4],[1,3,5,6],[2,5,8,10]])
  var('x1 x2 x3 x4')
  var('c1 c2')
  v1 = matrix.column([1,-2,1,0])
  v2 = matrix.column([0,-2,0,1])
  X = matrix.column([x1, x2, x3, x4])
\end{sagesilent}
\newcommand{\myCmatx}[1]{{#1
    \begin{bmatrix}
      c_1\\ -2\,c_1-2\,c_2\\ c_1\\ c_2
    \end{bmatrix}}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
    \rref\mysage{\tiny}{A}=\mysage{\tiny}{A.rref()}
    \]\pause%
    It follows that 
    \[
    \dim\Null(A)=\nullity(A)=\pause 2
    \]\pause%
    Furthermore, the solutions to $A\vv{x}=\vv{O}$ are of the form
    \[
    \vv{x}
    =\mysage{\tiny}{X}\pause
    =\myCmatx{\tiny}\pause
    =c_1\cdot\mysage{\tiny}{v1}+c_2\cdot\mysage{\tiny}{v2}
    \]\pause%
    This shows that $\Null(A)=\Span\Set{\vv*{v}{1},\vv*{v}{2}}$ where
    $\vv*{v}{1}=\mysage{\tiny}{v1}$ and $\vv*{v}{2}=\mysage{\tiny}{v2}$. \pause%
    Hence $\Set{\vv*{v}{1},\vv*{v}{2}}$ is a basis of $\Null(A)$.
  \end{example}
\end{frame}


\begin{sagesilent}
  A = matrix([(-3, 9, -11, 14, 16), (2, -6, 7, -9, -10), (-5, 15, -17, 22, 24)])
  var('x1 x2 x3 x4 x5')
  X = matrix.column([x1, x2, x3, x4, x5])
  v1 = matrix.column([3, 1, 0, 0, 0])
  v2 = matrix.column([1, 0, 1, 1, 0])
  v3 = matrix.column([-2, 0, 2, 0, 1])
\end{sagesilent}
\newcommand{\myCmaty}[1]{{#1
    \begin{bmatrix}
      3\,c_1+c_2-2\,c_3\\ c_1\\ c_2+2\,c_3\\ c_2\\ c_3
    \end{bmatrix}}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
    \rref\mysage{\tiny}{A}=\mysage{\tiny}{A.rref()}
    \]\pause%
    The solutions to $A\vv{x}=\vv{O}$ are of the form
    \[
    \vv{x}
    = \mysage{\tiny}{X}\pause
    = \myCmaty{\tiny}\pause
    = c_1\cdot\mysage{\tiny}{v1}+c_2\cdot\mysage{\tiny}{v2}+c_3\cdot\mysage{\tiny}{v3}
    \]\pause%
    This shows that 
    \[
    \Set*{\mysage{\tiny}{v1}, \mysage{\tiny}{v2}, \mysage{\tiny}{v3}}
    \]
    spans $\Null(A)$. \pause%
    Since $\dim\Null(A)=\nullity(A)=\pause 3$, this list is a basis of
    $\Null(A)$.
  \end{example}
\end{frame}


\section{Bases of Column Spaces}
\subsection{Column Spaces and Rank}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Let $A$ be a $m\times n$ matrix. The \emph{column space of $A$} is the
    linear subspace of $\mathbb{R}^m$ defined by
    $\Col(A)=\Span\Set{\textnormal{columns of }A}$.
  \end{block}

  \pause
  \begin{theorem}
    $\dim\Col(A)=\rank(A)$
  \end{theorem}
\end{frame}


\subsection{Algorithm for Finding Bases of Column Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{basic columns} of $A$ are the columns of $A$ corresponding to
    pivot columns in $\rref(A)$.
  \end{definition}

  \pause

  \begin{theorem}[Theorem 27.5 in Simon \& Blume]
    The basic columns of $A$ form a basis of $\Col(A)$.
  \end{theorem}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  A = matrix([(-8, 36, 60, 195, 36), (3, -8, -6, -40, -8), (5, -15, -15, -74, -15), (3, -13, -21, -70, -13)])
  c1, c2, c3, c4, c5 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that 
    \[
    \rref\mysage{\tiny}{A}=\mysage{\tiny}{A.rref()}
    \]\pause%
    This shows that $\dim\Col(A)=\rank(A)=\pause 3$. \pause%
    Moreover, 
    \[
    \Set*{\mysage{\tiny}{c1},\mysage{\tiny}{c2}, \mysage{\tiny}{c4}}
    \]
    is a basis of $\Col(A)$.
  \end{example}
\end{frame}


\begin{sagesilent}
  A = matrix([(11, 22, 54, 86, -119), (-4, -8, -15, -22, 34), (5, 10, 25, 40, -55)])
  c1, c2, c3, c4, c5 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the linear subspace $V$ of $\mathbb{R}^3$ given by
    \[
    V=
    \Span\Set*{
      \mysage{\tiny}{c1},
      \mysage{\tiny}{c2},
      \mysage{\tiny}{c3},
      \mysage{\tiny}{c4},
      \mysage{\tiny}{c5}
    }
    \]
    Compute $\dim(V)$ and find a basis of $V$.
  \end{example}
  \pause
  \begin{block}{Solution}
    Note that $V=\Col(A)$ where $A=\mysage{\tiny}{A}$. \pause%
    Since $\rref(A)=\mysage{\tiny}{A.rref()}$, $\dim(V)=\pause\rank(A)=2$. \pause%
    Moreover, $\Set*{\mysage{\tiny}{c1}, \mysage{\tiny}{c3}}$ is a basis of $V$.
  \end{block}
\end{frame}


\section{The Rank-Nullity Theorem, Revisited}
\subsection{Rank-Nullity with Dimensions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{Rank-Nullity Theorem} states that every matrix $A$ satisfies
    \[
    \rank(A)+\nullity(A)=\#\mycolumns(A)
    \]
    Since $\rank(A)=\dim\Col(A)$ and $\nullity(A)=\dim\Null(A)$, we can restate
    this theorem.
  \end{block}

  \pause

  \begin{theorem}[The Rank-Nullity Theorem]
    Let $A$ be a $m\times n$ matrix. Then
    \[
    \dim\Col(A)+\dim\Null(A)=n
    \]
  \end{theorem}
\end{frame}


\subsection{Rank and Transpose}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every matrix $A$ satisfies $\rank(A^\intercal)=\rank(A)$. Consequently,
    every matrix satisfies 
    \[
    \dim\Col(A^\intercal)=\dim\Col(A)
    \]
  \end{theorem}
\end{frame}



\begin{sagesilent}
  A = matrix([(4, 20, -12, -7, -4), (3, 15, -9, -5, -3), (3, 15, -9, -11, -3)])
  c1, c2, c3, c4, c5 = [matrix.column(col) for col in A.columns()]
  t1, t2, t3 = [matrix.column(col) for col in A.transpose().columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \rref\mysage{\tiny}{A} &= \mysage{\tiny}{A.rref()} \\
      \rref\mysage{\tiny}{A.transpose()} &= \mysage{\tiny}{A.transpose().rref()} 
    \end{align*}\pause%
    This shows that $\rank(A)=\rank(A^\intercal)=\pause 2$. \pause%
    Moreover, bases for $\Col(A)$ and $\Col(A^\intercal)$ are given by \pause
    \begin{align*}
      \Set*{\mysage{\tiny}{c1},\mysage{\tiny}{c4}} &&
      \Set*{\mysage{\tiny}{t1},\mysage{\tiny}{t2}}
    \end{align*}
    respectively. \pause%
    Note that $\Col(A)\subset\mathbb{R}^3$ and
    $\Col(A^\intercal)\subset\mathbb{R}^5$ while
    $\dim\Col(A)=\dim\Col(A^\intercal)=2$.
  \end{example}
\end{frame}


\section{The Fundamental Theorem of Linear Algebra, Revisited}

\begin{frame}
  \frametitle{\secname}
  
  \begin{theorem}
    Let $A$ be a $n\times n$ matrix. The the following are equivalent.
    \begin{itemize}[<+->]
    \item $A$ is invertible
    \item $A^{-1}$ exists
    \item $A$ is nonsingular
    \item $\rank(A)=n$
    \item $\nullity(A)=0$
    \item $\rref(A)=I_n$
    \item every system $A\vv{x}=\vv{b}$ has at least one solution for every
      $\vv{b}$
    \item every system $A\vv{x}=\vv{b}$ has at most one solution for every
      $\vv{b}$
    \item the system $A\vv{x}=\vv{O}$ is only solved by $\vv{x}=\vv{O}$
    \item $\det(A)\neq 0$
    \item $\dim\Col(A)=n$
    \item $\dim\Null(A)=0$
    \end{itemize}
  \end{theorem}
\end{frame}

\end{document}
